# Graphics generator

A generator for the 2022 Pirate Party communal elections' graphics.

## Docker setup

- Create a (preferably PostgreSQL) database and a user who has access to it. Set the `DATABASE_URL` environment variable to its URL in the `postgresql://username:password@host/database_name` format.
- Generate 2 secret keys. Put the first one in the `SECRET_KEY` environment variable, and the second one in `JWT_SECRET_KEY`.
- Set up an OpenID Connect service like Keycloak. Put its base URL in the `config.json` file's `OIDC_BASE_URL`.
- Create a client within the authentication service, and put its ID and secret key in the `OIDC_CLIENT_ID` and `OIDC_CLIENT_SECRET` environment variables.
- `docker-compose up`.

Note that environment variables can either be defined in `docker-compose.yml`, or the `.env` file. An example is provided in `.env.example`, only containing the more sensitive data.

An example for the `config.json` file is provided in `config.example.json`.

---

- Vytvoř (ideálně PostgreSQL) databázi a uživatele, který k ní má přístup. Nastav `DATABASE_URL` proměnnou v prostředí na její URL, ve formátu `postgresql://username:password@host/database_name`.
- Vygeneruj 2 tajné klíče, jeden do `SECRET_KEY` a druhý do `JWT_SECRET_KEY`.
- Vytvoř OpenID službu jako Keycloak. Dej její základní URL do `OIDC_BASE_URL` v `config.json`.
- Vytvoř klienta v OpenID službě, a dej jeho ID do `OIDC_CLIENT_ID`. Klíč do `OIDC_CLIENT_SECRET`.
- `docker-compose up`.

Proměnné v prostředí můžou být definovány buď v `docker-compose.yml`, nebo `.env` souboru. Příklad jen s tajnými informacemi je v `.env.example`.

Příklad pro `config.json` je v `config.example.json`.
