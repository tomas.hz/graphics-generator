class Template {
	// Constructor

	constructor(canvas, context = null) {
		if (typeof(canvas) === "string") {
			this.canvas = document.getElementById(canvas);
		} else {
			this.canvas = canvas;
		}
		
		if (context === null) {
			this.context = this.canvas.getContext("2d");
		} else {
			this.context = context;
		}
	}

	// Attributes

	redrawing = false;
	description = "";
	
 	changeableAttributes = [
 		"primaryImage",
// 		"iconImage",
//		"locationImage",
		"primaryText",
// 		"secondaryText",
 		"nameText",
// 		"iconText",
 		"primaryColorScheme",
// 		"nameColorScheme",
 		"primaryImagePosition",
//		,"logoIsCenter"
	];

	// Colors
	changeableColors = [
		"primaryTextColor",
		// "iconColor",
		"foregroundColor",
		"backgroundColor",
		// "nameBackgroundColor",
		// "nameTextColor",
		// "primaryTextHighlightColor",
		// "secondaryTextHighlightColor"
	];
	
	isPrintable = false;

	primaryImage = null;
	primaryImageX = 0;
	primaryImageY = 0;
	primaryImageZoom = 1;

	logoImage = null;
	logoImageZoom = 1;

	primaryText = "";
	requesterText = "";
	nameText = "";
	
	primaryFont = "'Bebas Neue'";
	primaryFontStyle = "";
	primaryTextTransformationHook = function(text) { return text; }
	
	finalDrawHook = function() {}
	stickerDrawHook = function() {}
	
	defaultResolution = 2000;
	
	lightLogoDefaultSource = "/static/images/logos/default-light.png";
	darkLogoDefaultSource = "/static/images/logos/default-dark.png";

	primaryColorSchemes = [];
	
	// Aspect ratio
	aspectRatio = 1;

	// Canvas

	redrawCanvas() {
		throw new Error("This method is not implemented.");
	}

	// Primary image

	async setPrimaryImageFromInput(imageInput, skipRedraw = false) {
		const classRef = this;
		
		if (imageInput.files.length == 0) {
			classRef.primaryImage = null;
			return;
		}

		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();

				fileReader.onloadend = function(event) {
					classRef.primaryImage = new Image();
					
					classRef.primaryImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.primaryImage.src = event.target.result;
				}

				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}

	movePrimaryImageUp(skipRedraw = false) {
		this.primaryImageY -= this.canvas.height / 100;

		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}

	movePrimaryImageDown(skipRedraw = false) {
		this.primaryImageY += this.canvas.height / 100;

		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}

	movePrimaryImageLeft(skipRedraw = false) {
		this.primaryImageX -= this.canvas.width / 100;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}

	movePrimaryImageRight(skipRedraw = false) {
		this.primaryImageX += this.canvas.width / 100;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}

	resetPrimaryImagePosition(skipRedraw = false) {
		this.primaryImageX = 0;
		this.primaryImageY = 0;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}

	zoomPrimaryImageIn(skipRedraw = false) {
		this.primaryImageZoom += 0.05;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}

	zoomPrimaryImageOut(skipRedraw = false) {
		this.primaryImageZoom -= 0.05;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}

	setPrimaryImageZoom(zoom, skipRedraw = false) {
		this.primaryImageZoom = zoom;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	resetPrimaryImageZoom(skipRedraw = false) {
		this.primaryImageZoom = 1;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	setLogoImageZoom(zoom, skipRedraw = false) {
		this.logoImageZoom = zoom;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	resetLogoImageZoom(skipRedraw = false) {
		this.logoImageZoom = 1;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}

	// Logo image
	async setLogoImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
		
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.logoImage = new Image();
					
					classRef.logoImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.logoImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}

	setLogoImageFromUrl(url, skipRedraw = false) {
		this.logoImage = new Image();
		
		let classRef = this;
		
		this.logoImage.onload = function() {
			if (!skipRedraw) {
				classRef.redrawCanvas();
			}
		}

		classRef.logoImage.src = url;
	}

	resetLogoImage(skipRedraw = false) {
		this.logoImage = null;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	// Text

	setPrimaryText(text, skipRedraw = false) {
		this.primaryText = this.primaryTextTransformationHook(text);

		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	setRequesterText(text, skipRedraw = false) {
		this.requesterText = text;

		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}

	setNameText(text, skipRedraw = false) {
		this.nameText = text;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}

	// Color scheme

	setPrimaryColorScheme(scheme, skipRedraw = false) {
		throw new Error("This method is not implemented.");
	}

	// Colors
	setPrimaryTextColor(color, skipRedraw = false) {
		this.primaryTextColor = color;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	setForegroundColor(color, skipRedraw = false) {
		this.foregroundColor = color;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	setBackgroundColor(color, skipRedraw = false) {
		this.backgroundColor = color;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}

	// Resolution

	async setResolution(resolution, skipRedraw = false) {
		this.canvas.width = resolution * this.aspectRatio;
		this.canvas.height = resolution;

		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Logo
	setLightLogoSource(source, skipRedraw = false) {
		this.lightLogoDefaultSource = source;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	setDarkLogoSource(source, skipRedraw = false) {
		this.darkLogoDefaultSource = source;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}

	// Load data
	async loadData(
		primaryImageInput = null,
		primaryText = "",
		nameText = "",
		primaryColorScheme = "",
		resultion = 2000,
		skipRedraw = false
	) {
		this.setPrimaryText(primaryText, true);
		this.setNameText(nameText, true);
		
		if (
			this.primaryColorSchemes !== undefined &&
			this.primaryColorSchemes.length !== 0 &&
			this.changeableAttributes.includes("primaryColorScheme")
		) {
			this.setPrimaryColorScheme(primaryColorScheme, true);
		}

		if (primaryImageInput !== null) {
			await this.setPrimaryImageFromInput(primaryImageInput, true);
		}

		this.setResolution(resultion, true);
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
}
