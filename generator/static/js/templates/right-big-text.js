class RightBigTextTemplate extends Template {
	description = "Určeno pro sociální sítě.";
	
	changeableAttributes = [
		"logoImage",
		"primaryImage",
		"primaryText",
		"nameText",
		"primaryColorScheme",
		"primaryImagePosition",
		"iconImage",
		"secondaryText"
	];

	iconImage = null;
	iconSource = null;

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];

	primaryTextHighlightColorSchemes = [
		"black-on-gold"
	];
	
	// Colors
	changeableColors = [
		"primaryTextColor",
		"foregroundColor",
		"backgroundColor",
		"nameTextColor",
		"primaryTextHighlightColor",
		"requesterTextColor"
	];

	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const primaryRectangleWidth = Math.ceil(this.canvas.width * 0.464);
		const primaryRectangleAngle = Math.ceil(this.canvas.height * 0.0075);
		const secondaryRectangleWidth = Math.ceil(this.canvas.width * 0.364);
		
		const iconHeight = Math.ceil(this.canvas.height * 0.09);
		
		const primaryRectanglePaddingTop = Math.ceil(this.canvas.height * 0.14);
		const primaryRectanglePaddingSide = Math.ceil(this.canvas.width * 0.054);
		const primaryRectanglePaddingInner = Math.ceil(this.canvas.width * 0.042);
		const primaryRectanglePaddingInnerTop = Math.ceil(this.canvas.height * 0.03);
		const primaryRectanglePaddingBetweenText = Math.ceil(this.canvas.height * 0.035);
		
		const highlightPaddingSides = Math.ceil(this.canvas.width * 0.01);
		const highlightPaddingBottom = Math.ceil(this.canvas.height * 0.0125);
		const highlightPaddingTop = Math.ceil(this.canvas.height * -0.01);
		
		const primaryTextMaxWidth = Math.ceil(this.canvas.width * 0.35);
		
		const nameTextPaddingTop = Math.ceil(this.canvas.height * 0.055);
		const nameTextPaddingSide = Math.ceil(this.canvas.width * 0.01);
		
		const secondaryTextHighlightPaddingSides = Math.ceil(this.canvas.width * 0.005);
		const secondaryTextHighlightPaddingTop = Math.ceil(this.canvas.height * -0.004);
		const secondaryTextHighlightPaddingBottom = Math.ceil(this.canvas.height * 0.0095);
		
		let primaryFontMaxSize = Math.ceil(this.canvas.height * 0.2);
		const primaryFontMaxTotalSizes = {
			1: Math.ceil(this.canvas.height * 0.225),
			2: Math.ceil(this.canvas.height * 0.25),
			3: Math.ceil(this.canvas.height * 0.275),
			4: Math.ceil(this.canvas.height * 0.3)
		};
		const primaryFontLinePadding = Math.ceil(this.canvas.height * 0);;
		let secondaryFontSize = Math.ceil(this.canvas.height * 0.0325);
		const secondaryFontLinePadding =  Math.ceil(this.canvas.height * 0.0016);
		let nameFontSize = Math.ceil(this.canvas.height * 0.035);
		
		const logoHeight = Math.ceil(this.canvas.height * 0.065) * this.logoImageZoom;
		const logoBottomOffset = Math.ceil(this.canvas.height * 0.06) - (
			logoHeight / this.logoImageZoom * (this.logoImageZoom - 1) / 2
		);
		
		const primaryTextMaxLines = 4;
		const secondaryTextMaxLines = {
			1: 9,
			2: 8,
			3: 7,
			4: 5
		};
		
		const primaryTextLines = splitStringIntoLines(
			this.context,
			this.primaryText,
			Infinity,
			primaryTextMaxLines
		);
		
		// Calculate based on initial font size
		const primaryRectangleHeight = (
			iconHeight
			+ primaryFontMaxTotalSizes[primaryTextMaxLines]
			+ secondaryTextMaxLines[primaryTextMaxLines] * (secondaryFontSize + secondaryFontLinePadding)
			+ primaryRectanglePaddingBetweenText * 2
			+ primaryRectanglePaddingInner
			+ primaryRectanglePaddingInnerTop
		);
		
		this.context.font = `${this.primaryFontStyle} ${primaryFontMaxSize}px ${this.primaryFont}`;
		
		while (
			primaryTextLines.length * (
				primaryFontMaxSize
				+ primaryFontLinePadding
			)
			< primaryFontMaxTotalSizes[primaryTextLines.length]
		) {
			primaryFontMaxSize += 2;
		}
		
		while (
			primaryTextLines.length * (
				primaryFontMaxSize
				+ primaryFontLinePadding
			)
			> primaryFontMaxTotalSizes[primaryTextLines.length]
		) {
			primaryFontMaxSize -= 2;
		}
		
		let secondaryTextLines = null;
		const originalSecondaryFontSize = secondaryFontSize;
		
		do {
			this.context.font = `${secondaryFontSize}px 'Roboto Condensed'`;
			
			secondaryTextLines = splitStringIntoLines(
				this.context,
				this.secondaryText,
				primaryRectangleWidth - primaryRectanglePaddingInner * 2,
				secondaryTextMaxLines[primaryTextLines.length],
				true
			)
			
			if (
				secondaryTextLines.length > secondaryTextMaxLines[primaryTextLines.length]
				&& (
					secondaryTextLines.length * (secondaryFontSize + secondaryFontLinePadding)
					> secondaryTextMaxLines[primaryTextLines.length] * (originalSecondaryFontSize + secondaryFontLinePadding)
				)
			) {
				secondaryFontSize -= 2;
			}
		} while (
			secondaryTextLines.length > secondaryTextMaxLines[primaryTextLines.length]
			&& (
				secondaryTextLines.length * (secondaryFontSize + secondaryFontLinePadding)
				> secondaryTextMaxLines[primaryTextLines.length] * (originalSecondaryFontSize + secondaryFontLinePadding)
			)
		);
		
		// Clear the canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Set image
		if (this.primaryImage !== null) {
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D
			
			const imageScaleX = this.canvas.width / this.primaryImage.width;
			const imageScaleY = this.canvas.height / this.primaryImage.height;
			
			const imageScale = Math.max(imageScaleX, imageScaleY) * this.primaryImageZoom;
			
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - this.primaryImage.width * imageScale) / 2 + this.primaryImageX * this.primaryImageZoom,
				(this.canvas.height - this.primaryImage.height * imageScale) / 2 + this.primaryImageY * this.primaryImageZoom
			);
			this.context.drawImage(
				this.primaryImage,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
		}
		
		// Fill background rectangle
		this.context.fillStyle = this.backgroundColor;
		this.context.fillRect(
			this.canvas.width - secondaryRectangleWidth, 0,
			secondaryRectangleWidth, this.canvas.height
		);
		
		// Create primary rectangle
		const primaryRectangleStartingX = this.canvas.width - primaryRectanglePaddingSide;
		const primaryRectangleEndingX = primaryRectangleStartingX - primaryRectangleWidth;
		
		this.context.fillStyle = this.foregroundColor;
		
		this.context.beginPath();
		
		this.context.moveTo(
			primaryRectangleStartingX,
			primaryRectanglePaddingTop
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePaddingTop + primaryRectangleAngle
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePaddingTop + primaryRectangleHeight + primaryRectangleAngle
		);
		this.context.lineTo(
			primaryRectangleStartingX,
			primaryRectanglePaddingTop + primaryRectangleHeight
		);
		
		this.context.closePath();
		
		this.context.fill();
		
		// Create name text
		this.context.textAlign = "right";
		this.context.font = `${nameFontSize}px 'Roboto Condensed'`;
		this.context.fillStyle = this.nameTextColor;
		
		while (this.context.measureText(this.nameText).width > secondaryRectangleWidth - primaryRectanglePaddingSide * 2) {
			nameFontSize -= 2;
			this.context.font = `${nameFontSize}px 'Roboto Condensed'`;
		}
		
		this.context.fillText(
			this.nameText,
			primaryRectangleStartingX - nameTextPaddingSide, nameTextPaddingTop + nameFontSize
		);
		
		// Create icon, if we have it
		
		// See if we want to use a dark or light one
		const foregroundRGB = hexToRgb(this.foregroundColor);
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		)
		
		const useLightHighlight = (foregroundLightness > 207);
		
		let currentPrimarySecondaryLineY = (
			primaryRectanglePaddingTop
			+ primaryRectanglePaddingInnerTop
			+ primaryFontMaxSize
		);
		
		let classRef = this; // FIXME...? Surely there's a better way to do this.
		
		const primaryTextRGB = hexToRgb(this.primaryTextColor);
		
		if (this.iconImage !== null) {
			const iconWidth = Math.ceil(this.iconImage.width * (iconHeight / this.iconImage.height));
			
			this.context.drawImage(
				colorizeImage(
					this.iconImage,
					iconWidth,
					iconHeight,
					primaryTextRGB.r,
					primaryTextRGB.g,
					primaryTextRGB.b
				),
				(
					this.canvas.width
					- primaryRectanglePaddingSide
					- (
						primaryRectangleWidth
						+ iconWidth
					) / 2
				),
				(
					primaryRectanglePaddingTop
					+ primaryRectanglePaddingInnerTop
				),
				iconWidth, iconHeight
			);
			
			currentPrimarySecondaryLineY += iconHeight;
		} else if (this.iconSource !== null) {
			const primaryTextRGB = hexToRgb(classRef.primaryTextColor);
			const primaryTextLightness = (
				0.2126 * primaryTextRGB.r
				+ 0.7152 * primaryTextRGB.g
				+ 0.0722 * primaryTextRGB.b
			);
			
			const icon = new Image();
			
			icon.onload = function() {
				const iconWidth = Math.ceil(icon.width * (iconHeight / icon.height));

				classRef.context.drawImage(
					colorizeImage(
						this,
						iconWidth,
						iconHeight,
						primaryTextRGB.r,
						primaryTextRGB.g,
						primaryTextRGB.b
					),
					(
						classRef.canvas.width
						- primaryRectanglePaddingSide
						- (
							primaryRectangleWidth
							+ iconWidth
						) / 2
					),
					(
						primaryRectanglePaddingTop
						+ primaryRectanglePaddingInnerTop
					),
					iconWidth, iconHeight
				);
			}
			
			icon.src = this.iconSource;
			
			currentPrimarySecondaryLineY += iconHeight;
		}
		
		const primarySecondaryLineX = (
			this.canvas.width
			- primaryRectanglePaddingSide
			- primaryRectanglePaddingInner
			- (
				primaryRectangleWidth
				- primaryRectanglePaddingInner * 2
			) / 2
		);
		
		// Create primary text
		this.context.textAlign = "left";
		this.context.font = `${this.primaryFontStyle} ${primaryFontMaxSize}px ${this.primaryFont}`;
		
		let previousWordHighlighted = false;
		let primaryTextHighlightedColor = null;
		
		const lowercasePrimaryTextHighlightColor = this.primaryTextHighlightColor.toLowerCase();
		const hasColorOverride = (
			lowercasePrimaryTextHighlightColor === "#209a37" ||
			lowercasePrimaryTextHighlightColor === "#e63812"
		);
		
		if (hasColorOverride) {
			if (useLightHighlight) {
				primaryTextHighlightedColor = this.foregroundColor;
			} else {
				primaryTextHighlightedColor = this.primaryTextColor;
			}
		} else if (!useLightHighlight) {
			primaryTextHighlightedColor = this.foregroundColor;
		} else {
			primaryTextHighlightedColor = this.primaryTextColor;
		}
		
		this.context.fillStyle = this.primaryTextColor;
		
		for (let line of primaryTextLines) {
			let currentFontSize = primaryFontMaxSize;
			this.context.font = `${this.primaryFontStyle} ${currentFontSize}px ${this.primaryFont}`;
			previousWordHighlighted = false;
			
			while (
				this.context.measureText(line.join(" ")).width
				> primaryTextMaxWidth
			) {
				currentFontSize -= 2;
				currentPrimarySecondaryLineY -= 2;
				
				this.context.font = `${this.primaryFontStyle} ${currentFontSize}px ${this.primaryFont}`;
			}
			
			let wordPosition = 0;
			
			for (let word of line) {
				const previousWords = line.slice(0, wordPosition).join(" ");
				const previousWordsWidth = this.context.measureText(
					previousWords
					+ (
						(previousWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				const nextWords = line.slice(wordPosition + 1, line.length).join(" ")
				const nextWordsWidth = this.context.measureText(
					nextWords
					+ (
						(nextWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				let currentWordWidth = this.context.measureText(word).width;
				
				for (const word of line.slice(wordPosition + 1, line.length)) {
					if (word.isHighlighted) {
						currentWordWidth += this.context.measureText(word.toString() + " ").width;
					} else {
						break;
					}
				}
				
				if (word.isHighlighted) {
					if (
						wordPosition === 0 ||
						!line[wordPosition - 1].isHighlighted
					) {
						this.context.fillStyle = this.primaryTextHighlightColor;
						this.context.beginPath();
						
						const startingHighlightLineX = (
							primarySecondaryLineX
							+ Math.max(previousWordsWidth / 2)
							- Math.max(nextWordsWidth / 2)
							- Math.max(this.context.measureText(word).width / 2)
							- (
								(previousWordHighlighted) ?
								spaceWidth : secondaryTextHighlightPaddingSides
							)
						);
						
						this.context.moveTo(
							startingHighlightLineX - highlightPaddingSides,
							currentPrimarySecondaryLineY + highlightPaddingBottom
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimarySecondaryLineY
								+ highlightPaddingBottom
								- Math.max(currentWordWidth / primaryRectangleWidth)
							)
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimarySecondaryLineY
								- currentFontSize
								- highlightPaddingTop
								- Math.max(currentWordWidth / primaryRectangleWidth)
							)
						);
						this.context.lineTo(
							startingHighlightLineX - (
								(previousWordHighlighted) ?
								spaceWidth : secondaryTextHighlightPaddingSides
							),
							(
								currentPrimarySecondaryLineY
								- currentFontSize
								- highlightPaddingTop
							)
						);
						
						this.context.closePath();
						
						this.context.fill();
						
						previousWordHighlighted = true;
					} else {
						previousWordHighlighted = false;
					}
					
					this.context.fillStyle = primaryTextHighlightedColor;
				}
				
				this.context.fillText(
					word + " ",
					(
						primarySecondaryLineX
						+ Math.ceil(previousWordsWidth / 2)
						- Math.ceil(nextWordsWidth / 2)
						- Math.ceil(this.context.measureText(word).width / 2)
					),
					currentPrimarySecondaryLineY
				);
				
				wordPosition++;
				
				this.context.fillStyle = this.primaryTextColor;
			}
			
			previousWordHighlighted = false;
			
			currentPrimarySecondaryLineY += primaryFontMaxSize + primaryFontLinePadding;
		}
		
		this.context.textAlign = "center";
		
		currentPrimarySecondaryLineY -= (
			primaryFontMaxSize
			+ primaryFontLinePadding
			- secondaryFontSize
			- primaryRectanglePaddingBetweenText
		);
		
		this.context.font = `${secondaryFontSize}px 'Roboto Condensed'`;
		this.context.fillStyle = this.secondaryTextColor;
		
		for (let line of secondaryTextLines) {
			this.context.fillText(
				line.join(" "),
				primarySecondaryLineX,
				currentPrimarySecondaryLineY
			);
			
			currentPrimarySecondaryLineY += secondaryFontSize + secondaryFontLinePadding;
		}
		
		// Create logo
		// See if we want to use a dark or light one
		const backgroundRGB = hexToRgb(this.backgroundColor);
		const backgroundLightness = (
			0.2126 * backgroundRGB.r
			+ 0.7152 * backgroundRGB.g
			+ 0.0722 * backgroundRGB.b
		)
		
		const useLightLogo = (backgroundLightness < 207);
		
		let logoImage = new Image();
		
		function drawLogoImage(image) {
			const logoWidth = Math.ceil(image.width * (logoHeight / image.height));
			
			classRef.context.drawImage(
				image,
				(
					classRef.canvas.width
					- (
						secondaryRectangleWidth
						+ logoWidth
					) / 2
				), classRef.canvas.height - logoBottomOffset - logoHeight,
				logoWidth, logoHeight
			);
		}
		
		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogoImage(this);
						
						resolve();
					}
					
					if (useLightLogo) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogoImage(this.logoImage);
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Icon
	async setIconSource(url, skipRedraw = false) {
		this.iconSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setIconImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.iconImage = new Image();
					
					classRef.iconImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.iconImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
	
	async resetIconImage(skipRedraw = false) {
		this.iconImage = null;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#000000";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextHighlightColorScheme("gold", true);
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor ="#000000";
				this.backgroundColor ="#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextHighlightColorScheme("gold", true);
				
				break;
			case "forum-black-on-white":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#962a51";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextHighlightColorScheme("gold", true);
				
				break;
			case "forum-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#962a51";
				this.foregroundColor = "#962a51";
				this.backgroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextHighlightColorScheme("gold", true);
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#00ad43";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextHighlightColorScheme("gold", true);
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#00ad43";
				this.backgroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextHighlightColorScheme("gold", true);
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#21274e";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextHighlightColorScheme("gold", true);
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#21274e";
				this.foregroundColor = "#21274e";
				this.backgroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextHighlightColorScheme("gold", true);
				
				break;
			case "louny-spolecne-black-on-white":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#3e2a5b";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextHighlightColorScheme("gold", true);
				
				break;
			case "louny-spolecne-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#3e2a5b";
				this.foregroundColor = "#3e2a5b";
				this.backgroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextHighlightColorScheme("gold", true);
				
				break;
			case "litomerice-blue-on-white":
				this.primaryTextColor = "#123172";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#123172";
				
				this.setPrimaryTextHighlightColorScheme("litomerice", true);
				this.setSecondaryTextHighlightColorScheme("litomerice", true);
				
				break;
			case "litomerice-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#123172";
				this.foregroundColor = "#123172";
				this.backgroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("litomerice", true);
				this.setSecondaryTextHighlightColorScheme("litomerice", true);
				
				break;
			case "stranane-gray-on-yellow":
				this.primaryTextColor = "#ffd500";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#4d4d4d";
				this.foregroundColor = "#4d4d4d";
				this.backgroundColor = "#ffd500";
				
				break;
			case "stranane-yellow-on-white":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#4d4d4d";
				
				break;
			case "stranane-white-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#4d4d4d";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.secondaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-black-on-yellow":
				this.primaryTextColor = "#ffd500";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffd500";
				
				break;
			case "prusanky-yellow-on-white":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.secondaryTextHighlightColor = "#ffd500";
				
				break;
			case "ujezd-green-on-white":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#8ed4a3";
				this.backgroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "ujezd-white-on-green":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#8ed4a3";
				this.secondaryTextHighlightColor = "#ffdd55";
				
				break;
			case "cssd-red-on-black":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#e63812";
				this.backgroundColor = "#000000";
				
				break;
			case "cssd-black-on-red":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#e63812";
				
				break;
			case "jilemnice-purple-on-black":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#6e1646";
				this.backgroundColor = "#000000";
				
				break;
			case "jilemnice-black-on-purple":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#6e1646";
				
				break;
			case "novarole-white-on-green":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#a9ce2d";
				
				break;
			case "novarole-green-on-white":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#ffffff";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-black":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.foregroundColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setPrimaryTextHighlightColorScheme(scheme, skipRedraw = false) {
		switch(scheme) {
			case "gold":
				this.primaryTextHighlightColor = "#ffcc00";
				break;
			case "litomerice":
				this.primaryTextHighlightColor = "#afe87e";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setSecondaryTextHighlightColorScheme(scheme, skipRedraw = false) {
		switch(scheme) {
			case "gold":
				this.secondaryTextHighlightColor = "#ffeda5";
				break;
			case "litomerice":
				this.primaryTextHighlightColor = "#afe87e";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Colors
	async setNameTextColor(color, skipRedraw = false) {
		this.nameTextColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setPrimaryTextHighlightColor(color, skipRedraw = false) {
		this.primaryTextHighlightColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Secondary text
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
