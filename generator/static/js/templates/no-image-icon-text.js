class NoImageIconText extends Template {
	description = "Určeno pro sociální sítě.";
	
	changeableAttributes = [
		"logoImage",
		"primaryText",
		"primaryColorScheme",
		"iconImage",
		"backgroundHasPattern"
	];

	iconImage = null;
	iconSource = null;
	backgroundHasPattern = true;
	
	backgroundPatternSource = "/static/images/background_pattern.svg";

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];

	primaryTextHighlightColorSchemes = [
		"black-on-gold"
	];
	
	// Colors
	changeableColors = [
		"primaryTextColor",
		"foregroundColor",
		"primaryTextHighlightColor",
		"requesterTextColor"
	];

	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const iconHeight = Math.ceil(this.canvas.height * 0.13);
		const iconImageOffsetTop = Math.ceil(this.canvas.height * 0.08);

		let primaryTextOffsetTop = Math.ceil(this.canvas.height * 0.38);
		let primaryTextMaxLines = 4;
		let primaryFontSize = Math.ceil(this.canvas.height * 0.13);
		const primaryTextPaddingSides = Math.ceil(this.canvas.width * 0.05);
		
		const logoHeight = Math.ceil(this.canvas.height * 0.06) * this.logoImageZoom;
		const logoOffsetBottom = Math.ceil(this.canvas.height * 0.09) - (
			logoHeight / this.logoImageZoom * (this.logoImageZoom - 1) / 2
		);
		
		const highlightPaddingSides = Math.ceil(this.canvas.width * 0.01);
		const highlightPaddingBottom = Math.ceil(this.canvas.height * 0.0125);
		const highlightPaddingTop = Math.ceil(this.canvas.height * -0.01); // It is, it's how Roboto works.

		// Clear the canvas
		this.context.fillStyle = this.foregroundColor;
		this.context.fillRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		let classRef = this;
		
		if (this.backgroundHasPattern) {
			// https://stackoverflow.com/a/56341485
			// Thanks to Kimbatt!
			let backgroundImage = null;
			
			const bgImageLoadPromise = new Promise(
				resolve => {
					backgroundImage = new Image();
					
					backgroundImage.onload = function() {
						classRef.context.drawImage(
							this,
							0, 0,
							classRef.canvas.width, classRef.canvas.height
						);
						
						resolve();
					}
					
					backgroundImage.src = this.backgroundPatternSource;
				}
			);
			
			await bgImageLoadPromise;
		}
		
		const primaryTextRGB = hexToRgb(this.primaryTextColor);
		
		function drawIcon(image) {
			const iconWidth = Math.ceil(image.width * (iconHeight / image.height));
			
			classRef.context.drawImage(
				colorizeImage(
					image,
					iconWidth,
					iconHeight,
					primaryTextRGB.r,
					primaryTextRGB.g,
					primaryTextRGB.b
				),
				(classRef.canvas.width - iconWidth)/2, iconImageOffsetTop,
				iconWidth, iconHeight
			);
		}
		
		const foregroundRGB = hexToRgb(this.foregroundColor);
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		)
		
		const useDarkLogoAndLightHighlight = (foregroundLightness > 207);
		
		if (this.iconImage !== null) {
			drawIcon(this.iconImage);
		} else if (this.iconSource !== null) {
			const primaryTextRGB = hexToRgb(classRef.primaryTextColor);
			const primaryTextLightness = (
				0.2126 * primaryTextRGB.r
				+ 0.7152 * primaryTextRGB.g
				+ 0.0722 * primaryTextRGB.b
			);
			
			const icon = new Image();
			
			icon.onload = function() {
				drawIcon(this);
			}
			
			icon.src = classRef.iconSource;
		} else {
			primaryTextOffsetTop -= iconHeight;
		}
		
		let primaryTextLines = [];
		const originalMaxHeight = primaryTextMaxLines * primaryFontSize;
		
		do {
			this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;
			
			primaryTextLines = splitStringIntoLines(
				this.context,
				this.primaryText,
				this.canvas.width - (primaryTextPaddingSides * 2),
				primaryTextMaxLines,
				true
			);
			
			if (primaryTextLines.length > primaryTextMaxLines) {
				primaryFontSize -= 2;
			}
			
			if (((primaryTextMaxLines + 1) * primaryFontSize) < originalMaxHeight) {
				primaryTextMaxLines += 1;
			}
		} while (primaryTextLines.length > primaryTextMaxLines);
		
		let currentPrimaryTextLineY = primaryTextOffsetTop;
		
		this.context.textAlign = "left";
		this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;
		
		let primaryTextHighlightedColor = null;
		
		const lowercasePrimaryTextHighlightColor = this.primaryTextHighlightColor.toLowerCase();
		const hasColorOverride = (
			lowercasePrimaryTextHighlightColor === "#209a37" ||
			lowercasePrimaryTextHighlightColor === "#e63812"
		);
		
		if (hasColorOverride) {
			if (useDarkLogoAndLightHighlight) {
				primaryTextHighlightedColor = this.foregroundColor;
			} else {
				primaryTextHighlightedColor = this.primaryTextColor;
			}
		} else if (!useDarkLogoAndLightHighlight) {
			primaryTextHighlightedColor = this.foregroundColor;
		} else {
			primaryTextHighlightedColor = this.primaryTextColor;
		}
		
		this.context.fillStyle = this.primaryTextColor;
		
		for (let line of primaryTextLines) {
			let wordPosition = 0;
			
			for (let word of line) {
				const previousWords = line.slice(0, wordPosition).join(" ");
				const previousWordsWidth = this.context.measureText(
					previousWords
					+ (
						(previousWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				const nextWords = line.slice(wordPosition + 1, line.length).join(" ")
				const nextWordsWidth = this.context.measureText(
					nextWords
					+ (
						(nextWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				let currentWordWidth = this.context.measureText(word).width;
				
				for (const word of line.slice(wordPosition + 1, line.length)) {
					if (word.isHighlighted) {
						currentWordWidth += this.context.measureText(word.toString() + " ").width;
					} else {
						break;
					}
				}
				
				if (word.isHighlighted) {
					if (
						wordPosition === 0 ||
						!line[wordPosition - 1].isHighlighted
					) {
						const startingHighlightLineX = (
							this.canvas.width / 2
							+ Math.max(previousWordsWidth / 2)
							- Math.max(nextWordsWidth / 2)
							- Math.max(this.context.measureText(word).width / 2)
						);
						
						this.context.fillStyle = this.primaryTextHighlightColor;

						this.context.fillRect(
							startingHighlightLineX - highlightPaddingSides, currentPrimaryTextLineY - highlightPaddingTop,
							highlightPaddingSides * 2 + currentWordWidth, highlightPaddingTop + highlightPaddingBottom - primaryFontSize
						);
						
					}
					
					this.context.fillStyle = primaryTextHighlightedColor;
				}
				
				this.context.fillText(
					word + " ",
					(
						this.canvas.width / 2
						+ Math.ceil(previousWordsWidth / 2)
						- Math.ceil(nextWordsWidth / 2)
						- Math.ceil(this.context.measureText(word).width / 2)
					),
					currentPrimaryTextLineY
				);
				
				wordPosition++;
				
				this.context.fillStyle = this.primaryTextColor;
			}
			
			currentPrimaryTextLineY += primaryFontSize;
		}
		
		function drawLogo(image) {
			let logoWidth = Math.ceil(image.width * (logoHeight / image.height));

			classRef.context.drawImage(
				image,
				(classRef.canvas.width - logoWidth)/2, classRef.canvas.height - logoOffsetBottom - logoHeight,
				logoWidth, logoHeight
			);
		}
		
		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogo(this);
						
						resolve();
					}
					
					if (!useDarkLogoAndLightHighlight) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogo(this.logoImage);
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Icon
	async setIconSource(url, skipRedraw = false) {
		this.iconSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setIconImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.iconImage = new Image();
					
					classRef.iconImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.iconImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
	
	async resetIconImage(skipRedraw = false) {
		this.iconImage = null;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor ="#000000";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "forum-black-on-white":
				this.primaryTextColor = "#962a51";
				this.foregroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "forum-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#962a51";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.primaryTextColor = "#00ad43";
				this.foregroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#00ad43";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.primaryTextColor = "#21274e";
				this.foregroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#21274e";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "louny-spolecne-black-on-white":
				this.primaryTextColor = "#3e2a5b";
				this.foregroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "louny-spolecne-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#3e2a5b";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "litomerice-blue-on-white":
				this.primaryTextColor = "#123172";
				this.foregroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("litomerice", true);
				
				break;
			case "litomerice-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#123172";
				
				this.setPrimaryTextHighlightColorScheme("litomerice", true);
				
				break;
			case "stranane-gray-on-yellow":
				this.primaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				
				break;
			case "stranane-yellow-on-white":
				this.primaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "stranane-white-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-black-on-yellow":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffd500";
				
				break;
			case "prusanky-yellow-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-white-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "ujezd-green-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "ujezd-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "cssd-red-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#e63812";
				
				break;
			case "cssd-black-on-red":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				
				break;
			case "jilemnice-purple-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#6e1646";
				
				break;
			case "jilemnice-black-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				
				break;
			case "novarole-white-on-green":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#a9ce2d";
				
				break;
			case "novarole-green-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-black":
				this.primaryTextColor = "#a9ce2d";
				this.foregroundColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.primaryTextColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setPrimaryTextHighlightColorScheme(scheme, skipRedraw = false) {
		switch(scheme) {
			case "gold":
				this.primaryTextHighlightColor = "#ffcc00";
				break;
			case "litomerice":
				this.primaryTextHighlightColor = "#afe87e";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Colors
	async setPrimaryTextHighlightColor(color, skipRedraw = false) {
		this.primaryTextHighlightColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setBackgroundHasPattern(hasPattern, skipRedraw = false) {
		this.backgroundHasPattern = hasPattern;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
