class NoImageBigTextIconWithTerciary extends Template {
	description = "Určeno pro sociální sítě.";
	
	changeableAttributes = [
		"logoImage",
		"primaryText",
		"secondaryText",
		"terciaryText",
		"primaryColorScheme",
		"iconImage",
		"backgroundHasPattern"
	];

	secondaryText = "";
	terciaryText = "";
	
	iconImage = null;
	iconSource = null;
	backgroundHasPattern = true;
	
	backgroundPatternSource = "/static/images/background_pattern.svg";

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];

	secondaryColorSchemes = [
		"black-on-white",
		"white-on-black",
		"black-on-gray",
		"black-on-blue"
	]

	primaryTextHighlightColorSchemes = [
		"black-on-gold"
	];
	
	// Colors
	changeableColors = [
		"primaryTextColor",
		"foregroundColor",
		"secondaryTextBackgroundColor",
		"secondaryTextColor",
		"primaryTextHighlightColor",
		"requesterTextColor"
	];

	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const primaryTextMaxLines = 5;
		let firstLinePrimaryFontMaxSize = Math.ceil(this.canvas.height * 0.4);
		let otherLinesPrimaryFontMaxSize = Math.ceil(this.canvas.height * 0.3);
		const otherLinesPadding = Math.ceil(this.canvas.height * 0.005);
		
		let primaryTextPaddingSides = Math.ceil(this.canvas.width * 0.15);
		const primaryTextPaddingTop = Math.ceil(this.canvas.width * 0.1);
		const primaryTextPaddingBottom = Math.ceil(this.canvas.width * 0.25);
		
		const logoHeight = Math.ceil(this.canvas.height * 0.06) * this.logoImageZoom;
		const logoOffsetBottom = Math.ceil(this.canvas.height * 0.09) - (
			logoHeight / this.logoImageZoom * (this.logoImageZoom - 1) / 2
		);
		
		const highlightPaddingSides = Math.ceil(this.canvas.width * 0.01);
		const highlightPaddingBottom = Math.ceil(this.canvas.height * 0.0125);
		const highlightPaddingTop = Math.ceil(this.canvas.height * -0.01); // It is, it's how Roboto works.
		
		let secondaryFontSize = Math.ceil(this.canvas.height * 0.035);
		const secondaryTextPaddingTop = Math.ceil(this.canvas.height * -0.005);
		const secondaryRectanglePaddingTopBottom = Math.ceil(this.canvas.height * 0.01);
		const secondaryRectanglePaddingSides = Math.ceil(this.canvas.height * 0.01);
		const secondaryRectangleOffsetTop = Math.ceil(this.canvas.height * 0.025);
		
		const iconPaddingSide = Math.ceil(this.canvas.width * 0.015);
		const iconPaddingTop = Math.ceil(this.canvas.height * -0.02);
		let iconOffsetBottom = Math.ceil(this.canvas.width * 0);
		const iconWidthRatio = 0.38;
		
		let terciaryFontSize = Math.ceil(this.canvas.height * 0.06);
		const terciaryFontPaddingTop =  Math.ceil(this.canvas.height * 0.025);
		
		// Split primary text into 2 bits
		const splitPrimaryText = this.primaryText.split(/\n|\r\n/);
		
		const firstPrimaryText = (
			(splitPrimaryText.length !== 0 && splitPrimaryText[0] !== "") ?
			splitPrimaryText[0] :
			""
		);
		const otherPrimaryText = (
			(splitPrimaryText.length > 1) ?
			splitPrimaryText.slice(1).join("\n") :
			""
		);
		
		// Parse the first line
		let firstPrimaryLine = [];
		
		do {
			this.context.font = `${this.primaryFontStyle} ${firstLinePrimaryFontMaxSize}px ${this.primaryFont}`;
			
			firstPrimaryLine = splitStringIntoLines(
				this.context,
				firstPrimaryText,
				this.canvas.width - primaryTextPaddingSides * 2,
				1,
				true
			);
			
			if (firstPrimaryLine.length > 1) {
				firstLinePrimaryFontMaxSize -= 2;
			}
		} while (firstPrimaryLine.length > 1);
		
		firstPrimaryLine = firstPrimaryLine[0];
		
		// Adjust width accordingly
		if (
			this.context.measureText(firstPrimaryLine).width
			< this.canvas.width - primaryTextPaddingSides * 2
		) {
			primaryTextPaddingSides = (
				this.canvas.width
				- this.context.measureText(firstPrimaryLine).width
			) / 2
		}
		
		// Clear the canvas
		this.context.fillStyle = this.foregroundColor;
		this.context.fillRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		let classRef = this;
		
		if (this.backgroundHasPattern) {
			// https://stackoverflow.com/a/56341485
			// Thanks to Kimbatt!
			let backgroundImage = null;
			
			const bgImageLoadPromise = new Promise(
				resolve => {
					backgroundImage = new Image();
					
					backgroundImage.onload = function() {
						classRef.context.drawImage(
							this,
							0, 0,
							classRef.canvas.width, classRef.canvas.height
						);
						
						resolve();
					}
					
					backgroundImage.src = this.backgroundPatternSource;
				}
			);
			
			await bgImageLoadPromise;
		}
		
		const foregroundRGB = hexToRgb(this.foregroundColor);
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		);
		
		const useDarkLogoAndAllowHighlight = (foregroundLightness > 207);
		
		let currentPrimaryTextLineY = primaryTextPaddingTop + firstLinePrimaryFontMaxSize;
		
		this.context.fillStyle = this.primaryTextColor;
		this.context.textAlign = "center";
		this.context.textBaseline = "bottom";
		this.context.font = `${this.primaryFontStyle} ${firstLinePrimaryFontMaxSize}px ${this.primaryFont}`;
		
		this.context.fillText(
			firstPrimaryLine.join(" "),
			this.canvas.width / 2,
			currentPrimaryTextLineY
		);
		
		if (this.terciaryText !== "") {
			this.context.fillStyle = this.primaryTextColor; // FIXME
			this.context.textAlign = "center";
			this.context.textBaseline = "bottom";
			this.context.font = `${this.primaryFontStyle} ${terciaryFontSize}px ${this.primaryFont}`;
			
			while (
				this.context.measureText(this.terciaryText).width
				> (
					this.canvas.width
					- 2 * primaryTextPaddingSides
				)
			) {
				terciaryFontSize -= 2;
				
				this.context.font = `${this.primaryFontStyle} ${terciaryFontSize}px ${this.primaryFont}`;
			}
		
			iconOffsetBottom += terciaryFontSize + terciaryFontPaddingTop;
			
			this.context.fillText(
				this.terciaryText,
				this.canvas.width / 2,
				(
					this.canvas.height
					- primaryTextPaddingBottom
					+ terciaryFontPaddingTop
				)
			);
		}
		
		const iconHorizontalSpace = (
			this.canvas.width
			- primaryTextPaddingSides * 2
		) * iconWidthRatio;

		let iconHeight = 0;
		let iconWidth = 0;
		
		const secondaryBackgroundRGB = hexToRgb(this.secondaryTextBackgroundColor);
		
		function drawIcon(image) {
			const remainingSpace = (
				classRef.canvas.height
				- firstLinePrimaryFontMaxSize
				- primaryTextPaddingTop
				- primaryTextPaddingBottom
			)
			
			const ratio = Math.min(
				(
					(
						remainingSpace
						- iconOffsetBottom
						- secondaryFontSize
						- secondaryRectanglePaddingTopBottom
					)
					/ image.height
				),
				iconWidthRatio * (
					classRef.canvas.width
					- primaryTextPaddingSides * 2
				) / image.width
			);
			
			iconWidth = Math.ceil(
				image.width
				* ratio
			);
			
			iconHeight = Math.ceil(
				image.height 
				* ratio
			);
			
			classRef.context.drawImage(
				colorizeImage(
					image,
					iconWidth,
					iconHeight,
					secondaryBackgroundRGB.r,
					secondaryBackgroundRGB.g,
					secondaryBackgroundRGB.b
				),
				(
					classRef.canvas.width
					- primaryTextPaddingSides
					- (
						(
							iconWidthRatio * (
								classRef.canvas.width
								- primaryTextPaddingSides * 2
							)
						)
						+ iconWidth
					) / 2
				), (
					primaryTextPaddingTop
					+ firstLinePrimaryFontMaxSize
					+ iconPaddingTop
				),
				iconWidth, iconHeight
			);
		}
		
		if (this.iconImage !== null) {
			let iconDrawPromise = new Promise(
				resolve => {
					drawIcon(classRef.iconImage);
					
					resolve();
				}
			);
			
			await iconDrawPromise;
		} else if (this.iconSource !== null) {
			let iconDrawPromise = new Promise(
				resolve => {
					const icon = new Image();
					
					icon.onload = function() {
						drawIcon(this);
						
						resolve();
					}
					
					icon.src = classRef.iconSource;
				}
			);
			
			await iconDrawPromise;
		}
		
		if (otherPrimaryText !== "") {
			// Other primary text lines
			const otherPrimaryLines = splitStringIntoLines(
				this.context,
				otherPrimaryText,
				Infinity,
				primaryTextMaxLines - 1
			);
			
			while (
				(otherPrimaryLines.length * otherLinesPrimaryFontMaxSize)
				> (
					iconHeight
					+ secondaryFontSize
					+ secondaryRectanglePaddingTopBottom * 2
					+ secondaryRectangleOffsetTop
				)
			) {
				otherLinesPrimaryFontMaxSize -= 2;
			}
			
			this.context.textAlign = "left";
			this.context.textBaseline = "bottom";
			
			currentPrimaryTextLineY += otherLinesPrimaryFontMaxSize + iconPaddingTop + otherLinesPadding;
			
			for (let line of otherPrimaryLines) {
				let currentFontSize = otherLinesPrimaryFontMaxSize;
				this.context.font = `${this.primaryFontStyle} ${currentFontSize}px ${this.primaryFont}`;
				
				if (line.length !== 0) {
					while (
						this.context.measureText(line.join(" ")).width
						> this.canvas.width - primaryTextPaddingSides * 2 - iconHorizontalSpace - iconPaddingSide
					) {
						currentFontSize -= 2;
						currentPrimaryTextLineY -= 2;
						
						this.context.font = `${this.primaryFontStyle} ${currentFontSize}px ${this.primaryFont}`;
					}
				}
				
				this.context.fillText(
					line.join(" "),
					primaryTextPaddingSides,
					currentPrimaryTextLineY
				);
				
				currentPrimaryTextLineY += otherLinesPrimaryFontMaxSize + otherLinesPadding;
			}
		}
		
		
		this.context.font = `${this.primaryFontStyle} ${secondaryFontSize}px ${this.primaryFont}`;
		
		this.context.textBaseline = "middle"
		this.context.textAlign = "center";
		
		this.context.fillStyle = this.secondaryTextBackgroundColor;
		this.context.fillRect(
			(
				this.canvas.width
				- primaryTextPaddingSides
				- iconHorizontalSpace
			),
			(
				primaryTextPaddingTop
				+ firstLinePrimaryFontMaxSize
				+ iconPaddingTop
				+ iconHeight
				+ secondaryRectangleOffsetTop
			),
			iconHorizontalSpace,
			(
				secondaryFontSize
				+ secondaryRectanglePaddingTopBottom * 2
			)
		);
		
		const originalSecondaryFontSize = secondaryFontSize;
		
		if (this.secondaryText !== "" && iconHorizontalSpace !== 0) {
			while (
				this.context.measureText(this.secondaryText).width
				> iconHorizontalSpace - (secondaryRectanglePaddingSides * 2)
			) {
				secondaryFontSize -= 2;
				
				this.context.font = `${this.primaryFontStyle} ${secondaryFontSize}px ${this.primaryFont}`;
			}
			
			this.context.fillStyle = this.secondaryTextColor;
			
			this.context.fillText(
				this.secondaryText,
				(
					this.canvas.width
					- primaryTextPaddingSides
					- iconHorizontalSpace / 2
				),
				(
					primaryTextPaddingTop
					+ firstLinePrimaryFontMaxSize
					+ iconPaddingTop
					+ iconHeight
					+ originalSecondaryFontSize
					+ secondaryRectangleOffsetTop
					+ secondaryTextPaddingTop
				)
			);
		}
		
		function drawLogo(image) {
			let logoWidth = Math.ceil(image.width * (logoHeight / image.height));

			classRef.context.drawImage(
				image,
				(classRef.canvas.width - logoWidth)/2, classRef.canvas.height - logoOffsetBottom - logoHeight,
				logoWidth, logoHeight
			);
		}
		
		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogo(this);
						
						resolve();
					}
					
					if (!useDarkLogoAndAllowHighlight) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogo(this.logoImage);
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.9925 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Icon
	async setIconSource(url, skipRedraw = false) {
		this.iconSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setIconImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.iconImage = new Image();
					
					classRef.iconImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.iconImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
	
	async resetIconImage(skipRedraw = false) {
		this.iconImage = null;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Text
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setTerciaryText(text, skipRedraw = false) {
		this.terciaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextColorScheme("white-on-black", true);
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor ="#000000";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextColorScheme("black-on-white", true);
				
				break;
			case "forum-black-on-white":
				this.primaryTextColor = "#962a51";
				this.foregroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextColorScheme("forum-black-on-white", true);
				
				break;
			case "forum-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#962a51";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextColorScheme("forum-white-on-purple", true);
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.primaryTextColor = "#00ad43";
				this.foregroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextColorScheme("zeleni-volary-bystrc-most-black-on-white", true);
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#00ad43";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setSecondaryTextColorScheme("zeleni-volary-bystrc-most-white-on-green", true);
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffdd55";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#123172";
				this.primaryTextHighlightColor = "#ffdd55";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "louny-spolecne-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffdd55";
				this.secondaryTextBackgroundColor = "#e2d7a9";
				this.secondaryTextColor = "#000000";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "louny-spolecne-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#3e2a5b";
				this.primaryTextHighlightColor = "#ffdd55";
				this.secondaryTextBackgroundColor = "#e2d7a9";
				this.secondaryTextColor = "#000000";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "litomerice-blue-on-white":
				this.primaryTextColor = "#123172";
				this.foregroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("litomerice", true);
				this.setSecondaryTextColorScheme("litomerice-blue-on-white", true);
				
				break;
			case "litomerice-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#21274e";
				
				this.setPrimaryTextHighlightColorScheme("litomerice", true);
				this.setSecondaryTextColorScheme("litomerice-white-on-blue", true);
				
				break;
			case "stranane-gray-on-yellow":
				this.primaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				this.secondaryTextBackgroundColor = "#4d4d4d";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "stranane-yellow-on-white":
				this.primaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#50c450";
				this.secondaryTextColor = "#000000";
				this.primaryTextHighlightColor = "#4d4d4d";
				
				break;
			case "stranane-white-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.secondaryTextBackgroundColor = "#50c450";
				this.secondaryTextColor = "#000000";
				
				break;
			case "prusanky-black-on-yellow":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.secondaryTextBackgroundColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "prusanky-yellow-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#50c450";
				this.secondaryTextColor = "#000000";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.secondaryTextBackgroundColor = "#50c450";
				this.secondaryTextColor = "#000000";
				
				break;
			case "ujezd-green-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#ffdd55";
				this.secondaryTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "ujezd-white-on-green":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#8ed4a3";
				this.secondaryTextBackgroundColor = "#ffdd55";
				this.secondaryTextColor = "#000000";
				
				break;
			case "cssd-red-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.secondaryTextBackgroundColor = "#e63812";
				this.secondaryTextColor = "#000000";
				
				break;
			case "cssd-black-on-red":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#e63812";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "jilemnice-purple-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.secondaryTextBackgroundColor = "#d5ffd5";
				this.secondaryTextColor = "#000000";
				
				break;
			case "jilemnice-black-on-purple":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#6e1646";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "novarole-white-on-green":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#a9ce2d";
				this.secondaryTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#a9ce2d";
				
				break;
			case "novarole-green-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.secondaryTextBackgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-black":
				this.primaryTextColor = "#a9ce2d";
				this.foregroundColor = "#000000";
				this.secondaryTextBackgroundColor = "#a9ce2d";
				this.secondaryTextColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				this.secondaryTextBackgroundColor = "#fde119";
				this.secondaryTextColor = "#000000";
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.primaryTextColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setSecondaryTextColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-gray":
				this.secondaryTextBackgroundColor = "#999999";
				this.secondaryTextColor = "#000000";
				
				break;
			case "black-on-blue":
				this.secondaryTextBackgroundColor = "#18c1df";
				this.secondaryTextColor = "#000000";
				
				break;
			case "black-on-white":
				this.secondaryTextBackgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "white-on-black":
				this.secondaryTextBackgroundColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "forum-black-on-white":
				this.secondaryTextBackgroundColor = "#962a51";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "forum-white-on-purple":
				this.secondaryTextBackgroundColor = "#ffffff";
				this.secondaryTextColor = "#962a51";
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.secondaryTextBackgroundColor = "#00ad43";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.secondaryTextBackgroundColor = "#ffffff";
				this.secondaryTextColor = "#00ad43";
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.secondaryTextBackgroundColor = "#21274e";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.secondaryTextBackgroundColor = "#ffffff";
				this.secondaryTextColor = "#21274e";
				
				break;
			case "litomerice-blue-on-white":
				this.secondaryTextBackgroundColor = "#21274e";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "litomerice-white-on-blue":
				this.secondaryTextBackgroundColor = "#ffffff";
				this.secondaryTextColor = "#21274e";
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setPrimaryTextHighlightColorScheme(scheme, skipRedraw = false) {
		switch(scheme) {
			case "gold":
				this.primaryTextHighlightColor = "#ffcc00";
				break;
			case "litomerice":
				this.secondaryTextHighlightColor = "#afe87e";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Colors
	async setSecondaryTextBackgroundColor(color, skipRedraw = false) {
		this.secondaryTextBackgroundColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setSecondaryTextColor(color, skipRedraw = false) {
		this.secondaryTextColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setPrimaryTextHighlightColor(color, skipRedraw = false) {
		this.primaryTextHighlightColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setBackgroundHasPattern(hasPattern, skipRedraw = false) {
		this.backgroundHasPattern = hasPattern;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
