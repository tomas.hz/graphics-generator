class NoImageTextIconVerticalLogo extends Template {
	description = "Určeno pro sociální sítě. <span style=\"color:red\">Pozor, některé koalice nemají vertikální logo!</span>";
	
	changeableAttributes = [
		"verticalLogoImage",
		"primaryColorScheme",
		"primaryText",
		"nameText",
		"iconText",
		"iconImage"
	];

	iconImage = null;
	iconSource = null;
	iconText = "";
	
	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];

	// Colors
	changeableColors = [
		"backgroundColor",
		"foregroundColor",
		"nameTextColor",
		"primaryTextColor",
		"primaryTextHighlightColor",
		"iconColor"
	];
	
	lightLogoDefaultSource = "static/images/vertical-logos/default-light.png";
	darkLogoDefaultSource = "static/images/vertical-logos/default-dark.png";
	
	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		// Logo
		let logoWidth = Math.ceil(this.canvas.width * 0.13) * this.logoImageZoom;
		const logoHeightMaxArea = 0.9;
		
		// Quote
		const quoteOffsetTop = Math.ceil(this.canvas.height * 0.06);
		const quoteOffsetSide = Math.ceil(this.canvas.width * 0.07);
		const quoteWidth = Math.ceil(this.canvas.width * 0.17);
		
		// Name
		let nameTextFontSize = Math.ceil(this.canvas.height * 0.03);
		const nameTextOffsetSide = Math.ceil(this.canvas.width * 0.11);
		const nameTextOffsetBottom = Math.ceil(this.canvas.height * 0.132);
		const nameTextMaxWidth = Math.ceil(this.canvas.width * 0.3);
		
		// Icon
		const iconWidth = Math.ceil(this.canvas.width * 0.07);
		const iconOffsetSide = Math.ceil(this.canvas.width * 0.07);
		const iconOffsetBottom = Math.ceil(this.canvas.width * 0.105);
		
		// Icon text
		const iconTextOffsetSide = Math.ceil(this.canvas.width * 0.015);
		const iconTextOffsetBottom = Math.ceil(this.canvas.height * 0.0795);
		let iconTextFontSize = Math.ceil(this.canvas.height * 0.03);
		let iconTextMaxLines = 2;
		let iconTextMaxWidth = Math.ceil(this.canvas.width * 0.3);
		
		// Bottom line
		const bottomLineAngle = Math.ceil(this.canvas.height * 0.01);
		const bottomLineOffsetBottom = Math.ceil(this.canvas.height * 0.25);
		const bottomLineOffsetLeft = Math.ceil(this.canvas.width * 0.07);
		const bottomLineThickness = Math.ceil(this.canvas.height * 0.0045);
		
		// Primary text
		let primaryTextFontSize = Math.ceil(this.canvas.height * 0.14);
		let primaryTextMaxLines = 4;
		const primaryTextMaxWidth = Math.ceil(this.canvas.width * 0.8);
		const primaryTextOffsetTop = Math.ceil(this.canvas.height * -0.07);
		
		// Clear the canvas
		this.context.fillStyle = this.backgroundColor;
		this.context.fillRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		const classRef = this;
		
		// Create logo
		// See if we're using the light or dark version
		const backgroundRGB = hexToRgb(this.backgroundColor);
		const backgroundLightness = (
			0.2126 * backgroundRGB.r
			+ 0.7152 * backgroundRGB.g
			+ 0.0722 * backgroundRGB.b
		);
		
		const useLightLogo = (backgroundLightness < 207);
		
		function drawLogoImage(image) {
			let logoHeight = (image.height * (logoWidth / image.width));
			
			if (logoHeight > bottomLineOffsetBottom * logoHeightMaxArea) {
				logoHeight = bottomLineOffsetBottom * logoHeightMaxArea;
				logoWidth = image.width * (logoHeight / image.height);
			}
			
			classRef.context.drawImage(
				image,
				(
					(classRef.canvas.width - logoWidth) / 2
				), (
					classRef.canvas.height - bottomLineOffsetBottom - bottomLineAngle
				),
				logoWidth, logoHeight
			);
		}
		
		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogoImage(this);
						
						resolve();
					}
					
					if (useLightLogo) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogoImage(this.logoImage);
		}
		
		// Create icon, if there is any
		let iconLinesMaxWidth = 0;
		
		// Create icon text
		function drawIconText() {
			if (classRef.iconImage !== null || classRef.iconSource !== null) {
				iconTextMaxWidth -= (iconWidth + iconTextOffsetSide);
			}
			
			// Draw icon text
			let iconTextLines = null;
			const originalIconTextFontSize = iconTextFontSize;
			
			do {
				classRef.context.font = `${iconTextFontSize}px 'Roboto Condensed'`;
				
				iconTextLines = splitStringIntoLines(
					classRef.context,
					classRef.iconText,
					iconTextMaxWidth,
					iconTextMaxLines,
					true
				);
				
				if (
					iconTextLines.length > iconTextMaxLines
					&& (
						iconTextLines.length * iconTextFontSize
						> iconTextMaxLines * originalIconTextFontSize
					)
				) {
					iconTextFontSize -= 2;
				}
			} while (
				iconTextLines.length > iconTextMaxLines
				&& (
					iconTextLines.length * iconTextFontSize
					> iconTextMaxLines * originalIconTextFontSize
				)
			);
			
			let currentIconLineY = (
				classRef.canvas.height
				- iconTextOffsetBottom
				- iconTextMaxLines * originalIconTextFontSize
			);
			classRef.context.fillStyle = classRef.iconColor;
			
			for (const line of iconTextLines) {
				const lineWidth = classRef.context.measureText(line.join(" ")).width;
				
				if (iconLinesMaxWidth < lineWidth) {
					iconLinesMaxWidth = lineWidth;
				}
			}
			
			for (const line of iconTextLines) {
				classRef.context.fillText(
					line.join(" "),
					classRef.canvas.width - iconOffsetSide - iconLinesMaxWidth,
					currentIconLineY
				);
				
				currentIconLineY += iconTextFontSize;
			}
		}
		
		function drawIconImage(image, colorize = true) {
			const iconHeight = (image.height * (iconWidth / image.width));
			
			const iconRGB = hexToRgb(classRef.iconColor);
			
			classRef.context.drawImage(
				(
					(colorize) ?  // Don't colorize when there is a custom icon
					colorizeImage(
						image,
						iconWidth, iconHeight,
						iconRGB.r, iconRGB.g, iconRGB.b
					) : image
				),
				(
					classRef.canvas.width
					- iconOffsetSide
					- iconWidth
					- iconTextOffsetSide
					- iconLinesMaxWidth
				), (
					classRef.canvas.height
					- iconOffsetBottom
					- iconHeight - (
						(iconWidth > iconHeight) ?
						(
							(
								iconWidth
								- iconHeight
							) / 2
						) : 0
					)
				),
				iconWidth, iconHeight
  			);
		}
		
		if (this.iconText !== "") {
			drawIconText();
		}
		
		if (this.iconImage !== null) {
			drawIconImage(this.iconImage, false);
		} else if (this.iconSource !== null) {
			const iconImageLoadPromise = new Promise(
				resolve => {
					const iconImage = new Image();
					
					iconImage.onload = function() {
						drawIconImage(this);
						resolve();
					}
					
					iconImage.src = classRef.iconSource;
				}
			);
			
			await iconImageLoadPromise;
		}
		
		// Draw bottom line
		this.context.fillStyle = this.foregroundColor;
		
		this.context.beginPath();
		
		this.context.moveTo(
			bottomLineOffsetLeft,
			this.canvas.height - bottomLineOffsetBottom
		);
		this.context.lineTo(
			this.canvas.width - iconOffsetSide,
			this.canvas.height - bottomLineOffsetBottom - bottomLineAngle
		);
		this.context.lineTo(
			this.canvas.width - iconOffsetSide,
			this.canvas.height - bottomLineOffsetBottom - bottomLineAngle - bottomLineThickness
		);
		this.context.lineTo(
			bottomLineOffsetLeft,
			this.canvas.height - bottomLineOffsetBottom - bottomLineThickness
		);
		
		this.context.closePath();
		this.context.fill();
		
		// Draw primary text
		if (this.primaryText !== "") {
			let primaryTextLines = null;
			const originalPrimaryTextFontSize = primaryTextFontSize;
			
			do {
				this.context.font = `${this.primaryFontStyle} ${primaryTextFontSize}px ${this.primaryFont}`;
				
				primaryTextLines = splitStringIntoLines(
					this.context,
					this.primaryText,
					primaryTextMaxWidth,
					primaryTextMaxLines,
					true
				)
				
				if (
					primaryTextLines.length > primaryTextMaxLines
					&& (
						primaryTextLines.length * primaryTextFontSize
						> primaryTextMaxLines * originalPrimaryTextFontSize
					)
				) {
					primaryTextFontSize -= 2;
				}
			} while (
				primaryTextLines.length > primaryTextMaxLines
				&& (
					primaryTextLines.length * primaryTextFontSize
					> primaryTextMaxLines * originalPrimaryTextFontSize
				)
			);
			
			this.context.fillStyle = this.primaryTextColor;
			this.context.textAlign = "center";
			
			let currentPrimaryLineY = (
				(
					this.canvas.height
					- (primaryTextLines.length - 1) * primaryTextFontSize
				) / 2
				+ primaryTextOffsetTop
			);
			
			for (const line of primaryTextLines) {
				this.context.fillText(
					line.join(" "),
					this.canvas.width / 2,
					currentPrimaryLineY
				);
				
				currentPrimaryLineY += primaryTextFontSize;
			}
			
			this.context.textAlign = "left";
		}
		
		// Draw name text
		if (this.nameText !== "") {
			let nameTextWidth = 0;
			
			do {
				this.context.font = `${nameTextFontSize}px 'Roboto Condensed'`;
				nameTextWidth = this.context.measureText(this.nameText).width;
				
				if (nameTextWidth > nameTextMaxWidth) {
					nameTextFontSize -= 2;
				}
			} while (nameTextWidth > nameTextMaxWidth);
			
			this.context.fillStyle = this.nameTextColor;
			
			this.context.textBaseline = "middle";
			
			this.context.fillText(
				this.nameText,
				nameTextOffsetSide,
				this.canvas.height - nameTextOffsetBottom
			);
			
			this.context.textBaseline = "alphabetic";
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Icon
	async setIconText(text, skipRedraw = false) {
		this.iconText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setIconSource(url, skipRedraw = false) {
		this.iconSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setIconImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.iconImage = new Image();
					
					classRef.iconImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.iconImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
	
	async resetIconImage(skipRedraw = false) {
		this.iconImage = null;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "white-on-black":
				this.backgroundColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "forum-black-on-white":
				this.backgroundColor = "#962a51";
				this.foregroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "forum-white-on-purple":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#962a51";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#962a51";
				this.iconColor = "#962a51";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.backgroundColor = "#00ad43";
				this.foregroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#00ad43";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#00ad43";
				this.iconColor = "#00ad43";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#21274e";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#21274e";
				this.iconColor = "#21274e";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.backgroundColor = "#21274e";
				this.foregroundColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "louny-spolecne-black-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#3e2a5b";
				this.nameTextColor = "#3e2a5b";
				this.primaryTextColor = "#3e2a5b";
				this.iconColor = "#3e2a5b";
				this.primaryTextHighlightColor = "#e2d7a9";
				
				break;
			case "louny-spolecne-white-on-purple":
				this.backgroundColor = "#3e2a5b";
				this.foregroundColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.primaryTextHighlightColor = "#e2d7a9";
				
				break;
			case "litomerice-blue-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#123172";
				this.nameTextColor = "#123172";
				this.primaryTextColor = "#123172";
				this.iconColor = "#123172";
				this.primaryTextHighlightColor = "#afe87e";
				
				break;
			case "litomerice-white-on-blue":
				this.backgroundColor = "#123172";
				this.foregroundColor = "#ffffff";
				this.nameTextColor = "#123172";
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.primaryTextHighlightColor = "#afe87e";
				
				break;
			case "stranane-gray-on-yellow":
				this.backgroundColor = "#ffd500";
				this.foregroundColor = "#4d4d4d";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#4d4d4d";
				this.iconColor = "#4d4d4d";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "stranane-yellow-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#ffd500";
				this.nameTextColor = "#4d4d4d";
				this.primaryTextColor = "#ffd500";
				this.iconColor = "#ffd500";
				this.primaryTextHighlightColor = "#4d4d4d";
				
				break;
			case "stranane-white-on-yellow":
				this.backgroundColor = "#ffd500";
				this.foregroundColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#4d4d4d";
				this.iconColor = "#4d4d4d";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-black-on-yellow":
				this.backgroundColor = "#ffd500";
				this.foregroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-yellow-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#ffd500";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#ffd500";
				this.iconColor = "#ffd500";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.backgroundColor = "#ffd500";
				this.foregroundColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "ujezd-green-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#8ed4a3";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#8ed4a3";
				this.iconColor = "#8ed4a3";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "ujezd-white-on-green":
				this.backgroundColor = "#8ed4a3";
				this.foregroundColor = "#000000";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "cssd-red-on-black":
				this.backgroundColor = "#000000";
				this.foregroundColor = "#e63812";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#e63812";
				this.iconColor = "#e63812";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "cssd-black-on-red":
				this.backgroundColor = "#e63812";
				this.foregroundColor = "#000000";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "jilemnice-purple-on-black":
				this.backgroundColor = "#000000";
				this.foregroundColor = "#6e1646";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#6e1646";
				this.iconColor = "#6e1646";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "jilemnice-black-on-purple":
				this.backgroundColor = "#6e1646";
				this.foregroundColor = "#000000";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-white-on-green":
				this.backgroundColor = "#a9ce2d";
				this.foregroundColor = "#000000";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#a9ce2d";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#a9ce2d";
				this.iconColor = "#a9ce2d";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "novarole-green-on-black":
				this.backgroundColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#a9ce2d";
				this.iconColor = "#a9ce2d";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.primaryTextColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
