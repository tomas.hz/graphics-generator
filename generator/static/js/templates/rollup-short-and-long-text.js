class RollupShortAndLongText extends Template {
	description = "Určeno pro rollupy.";
	
	changeableAttributes = [
		"logoImage",
		"primaryText",
		"secondaryText",
		"terciaryText",
		"selectableRollupBackground",
		"qrCode"
	];

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];
	
	changeableColors = [
		"primaryTextColor",
		"secondaryTextColor",
		"backgroundColor",
		"foregroundColor",
		"terciaryTextColor",
		"terciaryTextBackgroundColor",
		"terciaryTextColor",
		"qrCodeColor",
		"requesterTextColor"
	];
	
	aspectRatio = 0.4;
	defaultResolution = 12015;

	qrCodeURI = "";
	backgroundSource = "";
	
	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		let primaryTextFontSize = Math.ceil(this.canvas.height * 0.095);
		let qrSourceFontSize = Math.ceil(this.canvas.height * 0.02);
		let secondaryTextFontSize = Math.ceil(this.canvas.height * 0.04);
		let terciaryTextFontSize = Math.ceil(this.canvas.height * 0.019);
		
		const logoWidth = Math.ceil(this.canvas.width * 0.4) * this.logoImageZoom;
		const logoOffsetTop = Math.ceil(this.canvas.height * 0.01) * ((3 - this.logoImageZoom) / 2);
		
		const primaryLineAngle = Math.ceil(this.canvas.height * 0.007);
		const primaryLineThickness = Math.ceil(this.canvas.height * 0.002);
		const primaryLineOffsetTop = Math.ceil(this.canvas.height * -0.007);
		const primaryLineOffsetBottom = Math.ceil(this.canvas.height * 0.02);
		
		const primaryTextMaxWidth = Math.ceil(this.canvas.width * 0.82);
		const primaryTextOffsetTop = Math.ceil(this.canvas.height * 0.1);
		let primaryTextMaxLines = 3;
		
		const terciaryTextMaxWidth = Math.ceil(this.canvas.width * 0.5);
		
		const secondaryTextMaxWidth = Math.ceil(this.canvas.width * 0.75);
		const secondaryTextOffsetBottom = Math.ceil(this.canvas.height * 0.4);
		let secondaryTextMaxLines = 2;
		
		const secondaryRectanglePaddingSides = Math.ceil(this.canvas.width * 0.08);
		const secondaryRectanglePaddingTopBottom = Math.ceil(this.canvas.height * 0.01);
		
		const terciaryRectanglePaddingSides = Math.ceil(this.canvas.width * 0.04);
		const terciaryRectanglePaddingTopBottom = Math.ceil(this.canvas.height * 0.006);
		const terciaryRectangleOffsetTop = Math.ceil(this.canvas.height * -0.035);  // :P
		const terciaryRectangleOffsetSide = Math.ceil(this.canvas.width * 0.04);
		
		const qrCodeWidthHeight = Math.ceil(this.canvas.height * 0.09);
		const qrCodeBottomOffset = Math.ceil(this.canvas.height * 0.1);
		const qrCodeOuterPadding = Math.ceil(this.canvas.height * 0.01);
		
		const classRef = this;
		
		if (this.backgroundSource !== "") {
			const backgroundLoadPromise = new Promise(
				resolve => {
					const backgroundImage = new Image();
					
					backgroundImage.onload = function () {
						classRef.context.drawImage(
							this,
							0, 0,
							classRef.canvas.width, classRef.canvas.height
						);
						
						resolve();
					}
					
					backgroundImage.src = this.backgroundSource;
				}
			);
			
			await backgroundLoadPromise;
		}
		
		if (this.qrCodeURI !== "") {
			const qrCodeImage = new Image();
			
			const code = new QRCode(
				qrCodeImage,
				{
					"text": this.qrCodeURI,
					"width": qrCodeWidthHeight,
					"height": qrCodeWidthHeight,
					"colorDark": this.qrCodeColor,
					"colorLight": this.backgroundColor,
					"correctLevel": QRCode.CorrectLevel.H
				}
			);
			
			this.context.fillStyle = this.backgroundColor;
			
			const qrCodePosition = (this.canvas.width - qrCodeWidthHeight) / 2;
			
			this.context.fillRect(
				qrCodePosition - qrCodeOuterPadding, this.canvas.height - qrCodeBottomOffset - qrCodeWidthHeight - qrCodeOuterPadding,
				qrCodeWidthHeight + qrCodeOuterPadding * 2, qrCodeWidthHeight + qrCodeOuterPadding * 2
			);
			
			this.context.drawImage(
				code._oDrawing._elCanvas, // Ugly, but the fastest way to get the canvas
				(this.canvas.width - qrCodeWidthHeight) / 2, this.canvas.height - qrCodeWidthHeight - qrCodeBottomOffset,
				qrCodeWidthHeight, qrCodeWidthHeight
			);
			
			const maxTextWidth = qrCodeWidthHeight + 2 * qrCodeOuterPadding;
			
			do {
				this.context.font = `${qrSourceFontSize}px ${this.primaryFont}`;
				
				if (this.context.measureText(this.qrCodeURI).width > maxTextWidth) {
					qrSourceFontSize -= 2;
					this.context.font = `${qrSourceFontSize}px ${this.primaryFont}`;
				}
			} while (this.context.measureText(this.qrCodeURI).width > maxTextWidth);
			
			this.context.fillStyle = this.qrCodeColor;
			this.context.textAlign = "center";
			
			this.context.fillText(
				this.qrCodeURI,
				this.canvas.width / 2,
				this.canvas.height - qrCodeWidthHeight - qrCodeBottomOffset - qrCodeOuterPadding * 2
			);
		}
		
		if (this.primaryText !== "") {
			this.context.strokeStyle = this.primaryTextColor;
			
			this.context.beginPath();
			this.context.lineWidth = primaryLineThickness;
			
			this.context.moveTo(
				(this.canvas.width - primaryTextMaxWidth) / 2,
				primaryTextOffsetTop + primaryLineAngle + primaryLineOffsetTop
			);
			this.context.lineTo(
				this.canvas.width - ((this.canvas.width - primaryTextMaxWidth) / 2),
				primaryTextOffsetTop + primaryLineOffsetTop
			);
			
			this.context.closePath();
			this.context.stroke();
			
			let primaryLines = null;
			const originalPrimaryTextFontSize = primaryTextFontSize;
			
			do {
				this.context.font = `${this.primaryFontStyle} ${primaryTextFontSize}px ${this.primaryFont}`;
				
				primaryLines = splitStringIntoLines(
					this.context,
					this.primaryText,
					primaryTextMaxWidth,
					primaryTextMaxLines,
					true
				);
				
				if (
					primaryLines.length > primaryTextMaxLines
					&& (
						primaryLines.length * primaryTextFontSize
						> primaryTextMaxLines * originalPrimaryTextFontSize
					)
				) {
					primaryTextFontSize -= 2;
				}
			} while (
				primaryLines.length > primaryTextMaxLines
				&& (
					primaryLines.length * primaryTextFontSize
					> primaryTextMaxLines * originalPrimaryTextFontSize
				)
			);
			
			let currentPrimaryLineY = primaryTextOffsetTop + primaryTextFontSize;
			this.context.fillStyle = this.primaryTextColor;
			this.context.textAlign = "center";
			
			for (const line of primaryLines) {
				this.context.fillText(
					line.join(" "),
					this.canvas.width / 2,
					currentPrimaryLineY
				);
				
				currentPrimaryLineY += primaryTextFontSize;
			}
			
			currentPrimaryLineY -= primaryTextFontSize;
			
			this.context.beginPath();
			this.context.lineWidth = primaryLineThickness;
			
			this.context.moveTo(
				(this.canvas.width - primaryTextMaxWidth) / 2,
				currentPrimaryLineY + primaryLineAngle + primaryLineOffsetBottom
			);
			this.context.lineTo(
				this.canvas.width - ((this.canvas.width - primaryTextMaxWidth) / 2),
				currentPrimaryLineY + primaryLineOffsetBottom
			);
			
			this.context.closePath();
			this.context.stroke();
			
			currentPrimaryLineY += primaryLineAngle + primaryLineOffsetBottom + primaryLineThickness;
			
			// Create logo
			// See if we're using the light or dark version
			const primaryTextRGB = hexToRgb(this.primaryTextColor);
			const primaryTextLightness = (
				0.2126 * primaryTextRGB.r
				+ 0.7152 * primaryTextRGB.g
				+ 0.0722 * primaryTextRGB.b
			);
			
			const useLightLogo = (primaryTextLightness > 207);
			
			function drawLogoImage(image) {
				const logoHeight = (image.height * (logoWidth / image.width));
				
				classRef.context.drawImage(
					image,
					(classRef.canvas.width - logoWidth) / 2, currentPrimaryLineY + logoOffsetTop,
					logoWidth, logoHeight
				);
			}
			
			if (this.logoImage === null) {
				const logoImageLoadPromise = new Promise(
					resolve => {
						const logoImage = new Image();
						
						logoImage.onload = function() {
							drawLogoImage(this);
							
							resolve();
						}
						
						if (useLightLogo) {
							logoImage.src = classRef.lightLogoDefaultSource;
						} else {
							logoImage.src = classRef.darkLogoDefaultSource;
						}
					}
				);
				
				await logoImageLoadPromise;
			} else {
				drawLogoImage(this.logoImage);
			}
		}
		
		if (this.secondaryText !== "") {
			this.context.save();
			
			let secondaryTextHighestWidth = 0;
			
			let secondaryLines = null;
			const originalSecondaryTextFontSize = secondaryTextFontSize;
			
			do {
				this.context.font = `${this.primaryFontStyle} ${secondaryTextFontSize}px ${this.primaryFont}`;
				
				secondaryLines = splitStringIntoLines(
					this.context,
					this.secondaryText,
					secondaryTextMaxWidth,
					secondaryTextMaxLines,
					true
				);
				
				if (
					secondaryLines.length > secondaryTextMaxLines
					&& (
						secondaryLines.length * secondaryTextFontSize
						> secondaryTextMaxLines * originalSecondaryTextFontSize
					)
				) {
					secondaryTextFontSize -= 2;
				}
			} while (
				secondaryLines.length > secondaryTextMaxLines
				&& (
					secondaryLines.length * secondaryTextFontSize
					> secondaryTextMaxLines * originalSecondaryTextFontSize
				)
			);
			
			for (const line of secondaryLines) {
				const lineWidth = this.context.measureText(line.join(" ")).width;
				
				if (lineWidth > secondaryTextHighestWidth) {
					secondaryTextHighestWidth = lineWidth;
				}
			}
			
			const secondaryRectangleHeight = (
				secondaryLines.length * secondaryTextFontSize +
				2 * secondaryRectanglePaddingTopBottom
			);
			
			this.context.fillStyle = this.foregroundColor;
			
			this.context.translate(
				this.canvas.width / 2,
				(
					this.canvas.height
					- secondaryTextOffsetBottom
					- (
						secondaryTextFontSize
						+ 2 * secondaryRectanglePaddingTopBottom
					) / 2
				)
			);
			this.context.rotate(-Math.PI/64);
			
			this.context.fillRect(
				(
					- secondaryTextHighestWidth / 2
					- secondaryRectanglePaddingSides
				), (
					secondaryTextFontSize / 2
					+ secondaryRectanglePaddingTopBottom
				),
				secondaryTextHighestWidth + 2 * secondaryRectanglePaddingSides, secondaryRectangleHeight
			);
			
			this.context.fillStyle = this.secondaryTextColor;
			this.context.textAlign = "center";
			
			let currentSecondaryLineY = (
				secondaryTextFontSize * 1.7 // :P
				+ secondaryRectanglePaddingTopBottom
			);
			
			for (const line of secondaryLines) {
				this.context.fillText(
					line.join(" "),
					0,
					currentSecondaryLineY
				);
				
				currentSecondaryLineY += secondaryTextFontSize;
			}
			
			this.context.restore();
			
			if (this.terciaryText !== "") {
				let terciaryTextWidth = 0;
				const terciaryTextOriginalFontSize = terciaryTextFontSize;
				
				do {
					this.context.font = `bold ${terciaryTextFontSize}px 'Roboto Condensed'`;
					
					terciaryTextWidth = this.context.measureText(this.terciaryText).width;
					
					if (terciaryTextWidth > terciaryTextMaxWidth) {
						terciaryTextFontSize -= 2;
						this.context.font = `bold ${terciaryTextFontSize}px 'Roboto Condensed'`;
					}
				} while (terciaryTextWidth > terciaryTextMaxWidth);
				
				this.context.fillStyle = this.terciaryTextBackgroundColor;
				
				this.context.fillRect(
					(
						this.canvas.width
						- (
							this.canvas.width
							- (
								secondaryTextHighestWidth
								+ 2 * secondaryRectanglePaddingSides
							)
						) / 2
						- terciaryTextWidth
						- terciaryRectanglePaddingSides * 2
						+ terciaryRectangleOffsetSide
					), (
						this.canvas.height
						- secondaryTextOffsetBottom
						- 2 * secondaryRectanglePaddingTopBottom
						- secondaryTextFontSize
						- terciaryRectangleOffsetTop
					),
					(
						terciaryTextWidth
						+ 2 * terciaryRectanglePaddingSides
					), (
						terciaryTextOriginalFontSize
						+ 2 * terciaryRectanglePaddingTopBottom
					)
				);
				
				this.context.fillStyle = this.terciaryTextColor;
				this.context.textAlign = "left";
				
				this.context.fillText(
					this.terciaryText,
					(
						this.canvas.width
						- (
							this.canvas.width
							- (
								secondaryTextHighestWidth
								+ 2 * secondaryRectanglePaddingSides
							)
						) / 2
						- terciaryTextWidth
						- terciaryRectanglePaddingSides
						+ terciaryRectangleOffsetSide
					),
					(
						this.canvas.height
						- secondaryTextOffsetBottom
						- 2 * secondaryRectanglePaddingTopBottom
						- secondaryTextFontSize
						- terciaryRectangleOffsetTop
						+ terciaryTextFontSize
						+ terciaryRectanglePaddingTopBottom
						+ (
							terciaryTextOriginalFontSize
							- terciaryTextFontSize
						) / 2
					)
				);
			}
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.0175);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.989, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}

	// Secondary text
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Terciary text
	async setTerciaryText(text, skipRedraw = false) {
		this.terciaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// QR code
	async setQrCodeURI(uri, skipRedraw = false) {
		this.qrCodeURI = uri;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Background
	async setBackgroundSource(url, skipRedraw = false) {
		this.backgroundSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffeda5";
				this.qrCodeColor = "#000000";
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffeda5";
				this.qrCodeColor = "#ffffff";
				
				break;
			case "black-on-white-prerov":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				this.foregroundColor = "#32948b";
				this.backgroundColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#000000";
				this.qrCodeColor = "#000000";
				
				break;
			case "white-on-black-prerov":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.foregroundColor = "#32948b";
				this.backgroundColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				this.qrCodeColor = "#ffffff";
				
				break;
			case "black-on-white-pardubice":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				this.foregroundColor = "#c83737";
				this.backgroundColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#000000";
				this.qrCodeColor = "#000000";
				
				break;
			case "white-on-black-pardubice":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.foregroundColor = "#c83737";
				this.backgroundColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				this.qrCodeColor = "#ffffff";
				
				break;
			// We don't have anything for these yet
			case "forum-black-on-white":
				
				
				break;
			case "forum-white-on-purple":
				
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				
				break;
			case "spolecne-s-piraty-black-on-white":
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				
				break;
			case "louny-spolecne-black-on-white":
				
				break;
			case "louny-spolecne-white-on-purple":
				
				break;
			case "litomerice-blue-on-white":
				
				break;
			case "litomerice-white-on-blue":
				
				break;
			case "stranane-gray-on-yellow":
				
				break;
			case "stranane-yellow-on-white":
				
				break;
			case "stranane-white-on-yellow":
				
				break;
			case "ujezd-green-on-white":
				
				break;
			case "ujezd-white-on-green":
				
				break;
			case "cssd-red-on-black":
				
				break;
			case "cssd-black-on-red":
				
				break;
			 case "jilemnice-purple-on-black":
				
				break;
			case "jilemnice-black-on-purple":
				
				break;
			case "novarole-white-on-green":
				
				break;
			case "novarole-green-on-white":
				
				break;
			case "novarole-green-on-black":
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.terciaryTextColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
