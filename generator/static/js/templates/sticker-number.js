class StickerNumber extends Template {
	description = "Určeno pro samolepky.";
	
	changeableAttributes = [
		"primaryText",
		"locationImage",
		"primaryColorScheme",
		"secondaryText",
		"localizationImage",
		"showNumberLabel"
	];

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];
	
	changeableColors = [
		"primaryTextColor",
		"secondaryTextColor",
		"foregroundColor",
		"backgroundColor"
	];
	
	locationSource = null;
	secondaryText = "Pirátská Strana";
	showNumberLabel = true;
	
	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}
		
		this.redrawing = true;
		
		const foregroundCircleWidth = this.canvas.width * 0.7;
		
		const secondaryTopTextFontSize = this.canvas.height * 0.16;
		
		let topTextFontSize = secondaryTopTextFontSize;
		const topTextMaxWidth = this.canvas.width * 1.32; // ?
		
		let secondaryTextFontSize = secondaryTopTextFontSize;
		const secondaryTextMaxWidth = this.canvas.width * 1.32; // ?
		
		const numberLabelFontSize = this.canvas.height * 0.3;
		const numberLabelOffsetSide = this.canvas.width * 0.05;
		
		let primaryTextFontSize = this.canvas.height * 0.8;
		
		const sideIconWidth = this.canvas.width * 0.1;
		
		
		const TOP_TEXT = "Volte celou kandidátku";
		
		// Clear the canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		const classRef = this;
		let locationHeight = 0;
		
		if (this.showNumberLabel) {
			// Draw foreground circle
			this.context.fillStyle = this.foregroundColor;
			
			this.context.beginPath();
			this.context.arc(
				this.canvas.width / 2, // Center position
				this.canvas.height / 2, // Center position
				foregroundCircleWidth / 2, // Radius
				0,
				2 * Math.PI,
				false
			);
			this.context.closePath();
			
			this.context.fill();
			
			const primaryTextRGB = hexToRgb(this.primaryTextColor);
			
			this.context.globalCompositeOperation = "source-atop";
			
			if (this.locationSource !== null) {
				const locationImageLoadPromise = new Promise(
					resolve => {
						const locationImage = new Image();
						
						locationImage.onload = function() {
							const locationWidth = foregroundCircleWidth;
							locationHeight = (this.height * (locationWidth / this.width));
							
							classRef.context.drawImage(
								colorizeImage(
									this,
									locationWidth,
									locationHeight,
									primaryTextRGB.r,
									primaryTextRGB.g,
									primaryTextRGB.b
								),
								(classRef.canvas.width - foregroundCircleWidth) / 2,
								Math.ceil(
									classRef.canvas.height
									- Math.floor((classRef.canvas.height - foregroundCircleWidth) / 2)
									- locationHeight * 0.95, // Slightly imprecise positioning and/or tilted bottom bits
								),
								locationWidth, locationHeight
							);
							
							resolve();
						}
						
						locationImage.src = classRef.locationSource;
					}
				);
				
				await locationImageLoadPromise;
			}
		}
		
		this.context.fillStyle = (
			(this.showNumberLabel) ?
			this.primaryTextColor : this.foregroundColor
		);
		
		// Create number label
		if (this.showNumberLabel) {
			this.context.textBaseline = "middle";
			this.context.font = `bold ${numberLabelFontSize}px 'Roboto Condensed'`;
			
			this.context.fillText(
				"č.",
				(this.canvas.width - foregroundCircleWidth) / 2 + numberLabelOffsetSide,
				(this.canvas.height - locationHeight * 0.7) / 2
			);
		}
		
		// Create primary text with the actual number (hopefully a number, that is.)
		this.context.textBaseline = "top";
		
		let primaryTextStartX = 0;
		let primaryTextMaxWidth = 0;
		
		if (this.showNumberLabel) {
			primaryTextStartX = (
				(this.canvas.width - foregroundCircleWidth) / 2
				+ numberLabelOffsetSide
				+ this.context.measureText("č.").width
			) * (
				(this.primaryText.startsWith("1")) ?
				0.85 : 1
			);
			
			primaryTextMaxWidth = (
				this.canvas.width
				- primaryTextStartX
				- (this.canvas.width - foregroundCircleWidth) / 2
			) * 0.85; // Avoid text overlapping into the circle
		} else {
			primaryTextStartX = this.canvas.width / 2 * (
				(this.primaryText.startsWith("1")) ?
				0.95 : 1
			);
			this.context.textAlign = "center";
			
			primaryTextMaxWidth = (
				this.canvas.width
				- (this.canvas.width - foregroundCircleWidth)
			) * (
				(this.showNumberLabel) ? 
				0.7 : 1
			); // Avoid text overlapping into the circle
		}
		
		if (this.showNumberLabel) {
			primaryTextFontSize -= locationHeight;
		} else {
			primaryTextFontSize += (this.canvas.width - foregroundCircleWidth);
		}
		
		do {
			this.context.font = `${primaryTextFontSize}px 'Bebas Neue'`;
			
			if (this.context.measureText(this.primaryText).width > primaryTextMaxWidth) {
				primaryTextFontSize -= 2;
			}
		} while (this.context.measureText(this.primaryText).width > primaryTextMaxWidth);
		
		this.context.fillText(
			this.primaryText,
			primaryTextStartX,
			(
				this.canvas.height
				- locationHeight * 0.7
				- primaryTextFontSize * 0.8
			) / 2
		);
		
		if (!this.showNumberLabel) {
			this.context.textAlign = "left";
		}
		
		this.context.globalCompositeOperation = "source-over";
		
		// Create background circle
		this.context.globalCompositeOperation = "destination-over";
		
		this.context.fillStyle = this.backgroundColor;
		
		this.context.beginPath();
		this.context.arc(
			this.canvas.width / 2, // Center position
			this.canvas.height / 2, // Center position
			this.canvas.width / 2, // Radius
			0,
			2 * Math.PI,
			false
		);
		this.context.closePath();
		
		this.context.fill();
		
		this.context.globalCompositeOperation = "source-over";
		
		if (this.showNumberLabel) {
			// Create top text
			const topTextWidth = (
				this.canvas.width - (
					this.canvas.width
					- foregroundCircleWidth
					- secondaryTopTextFontSize * 1.5
				)
			);
			
			this.context.font = `${topTextFontSize}px 'Bebas Neue'`;
			
			while (this.context.measureText(TOP_TEXT).width > topTextMaxWidth) {
				topTextFontSize -= 2;
				this.context.font = `${topTextFontSize}px 'Bebas Neue'`;
			}
			
			this.context.drawImage(
				getCircularText(
					TOP_TEXT,
					topTextWidth,
					0,
					"center",
					false,
					true,
					"Bebas Neue",
					`${topTextFontSize}px`,
					this.secondaryTextColor,
					0
				),
				(this.canvas.width - topTextWidth) / 2, (this.canvas.height - topTextWidth) / 2,
				topTextWidth, topTextWidth
			);
			
			// Create first side icon - the cross
			const secondaryTextRGB = hexToRgb(this.secondaryTextColor);
			
			const crossSideIconLoadPromise = new Promise(
				resolve => {
					const cross = new Image();
					
					cross.onload = function() {
						const crossHeight = (this.height * (sideIconWidth / this.width));
						
						classRef.context.drawImage(
							colorizeImage(
								this,
								sideIconWidth,
								crossHeight,
								secondaryTextRGB.r,
								secondaryTextRGB.g,
								secondaryTextRGB.b
							),
							(
								(classRef.canvas.width - foregroundCircleWidth) / 2
								- sideIconWidth
							) / 2,
							(classRef.canvas.height - crossHeight) / 2,
							sideIconWidth, crossHeight
						);
						
						resolve();
					}
					
					cross.src = "static/images/sticker-cross.png";
				}
			);
			
			// Create first side icon - the check
			const checkSideIconLoadPromise = new Promise(
				resolve => {
					const cross = new Image();
					
					cross.onload = function() {
						const checkHeight = (this.height * (sideIconWidth / this.width));
						
						classRef.context.drawImage(
							colorizeImage(
								this,
								sideIconWidth,
								checkHeight,
								secondaryTextRGB.r,
								secondaryTextRGB.g,
								secondaryTextRGB.b
							),
							classRef.canvas.width - (
								(classRef.canvas.width - foregroundCircleWidth) / 2
								+ sideIconWidth
							) / 2,
							(classRef.canvas.height - checkHeight) / 2,
							sideIconWidth, checkHeight
						);
						
						resolve();
					}
					
					cross.src = "static/images/sticker-check.png";
				}
			);
			
			await checkSideIconLoadPromise;
			
			// Create bottom secondary text
			const secondaryTextWidth = (
				this.canvas.width - (
					this.canvas.width
					- foregroundCircleWidth
					- secondaryTopTextFontSize * 1.9
				)
			);
			
			this.context.font = `${secondaryTextFontSize}px 'Bebas Neue'`;
			
			while (this.context.measureText(this.secondaryText).width > secondaryTextMaxWidth) {
				secondaryTextFontSize -= 2;
				this.context.font = `${secondaryTextFontSize}px 'Bebas Neue'`;
			}
			
			this.context.drawImage(
				getCircularText(
					this.secondaryText,
					secondaryTextWidth,
					180,
					"center",
					false,
					false,
					"Bebas Neue",
					`${secondaryTextFontSize}px`,
					this.secondaryTextColor,
					0
				),
				(this.canvas.width - secondaryTextWidth) / 2, (this.canvas.height - secondaryTextWidth) / 2,
				secondaryTextWidth, secondaryTextWidth
			);
		}
		
		this.redrawing = false;
	}
	
	// Enable / disable number label
	async setShowNumberLabel(show, skipRedraw = false) {
		this.showNumberLabel = show;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Location
	async setLocationSource(url, skipRedraw = false) {
		this.locationSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Secondary text
	async setSecondaryText(text, skipRedraw = false) {
		if (text === "" || text === null || text === undefined) {
			text = "Pirátská Strana";
		}
		
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.foregroundColor = "#ffffff";
				this.primaryTextColor = "#000000";
				
				this.backgroundColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "white-on-black":
				this.foregroundColor = "#000000";
				this.primaryTextColor = "#ffffff";
				
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "forum-black-on-white":
				this.foregroundColor = "#ffffff";
				this.primaryTextColor = "#000000";
				
				this.backgroundColor = "#962a51";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "forum-white-on-purple":
				this.foregroundColor = "#962a51";
				this.primaryTextColor = "#ffffff";
				
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.foregroundColor = "#ffffff";
				this.primaryTextColor = "#000000";
				
				this.backgroundColor = "#00ad43";
				this.secondaryTextColor = "#000000";
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.foregroundColor = "#00ad43";
				this.primaryTextColor = "#ffffff";
				
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.foregroundColor = "#ffffff";
				this.primaryTextColor = "#000000";
				
				this.backgroundColor = "#21274e";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.foregroundColor = "#21274e";
				this.primaryTextColor = "#ffffff";
				
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "louny-spolecne-black-on-white":
				this.foregroundColor = "#ffffff";
				this.primaryTextColor = "#000000";
				
				this.backgroundColor = "#3e2a5b";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "louny-spolecne-white-on-purple":
				this.foregroundColor = "#e2d7a9";
				this.primaryTextColor = "#3e2a5b";
				
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#3e2a5b";
				
				break;
			case "litomerice-blue-on-white":
				this.foregroundColor = "#ffffff";
				this.primaryTextColor = "#000000";
				
				this.backgroundColor = "#123172";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "litomerice-white-on-blue":
				this.foregroundColor = "#123172";
				this.primaryTextColor = "#ffffff";
				
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "stranane-gray-on-yellow":
				this.foregroundColor = "#ffffff";
				this.primaryTextColor = "#000000";
				
				this.backgroundColor = "#ffd500";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "stranane-yellow-on-white":
				this.foregroundColor = "#ffd500";
				this.primaryTextColor = "#4d4d4d";
				
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "stranane-white-on-yellow":
				this.foregroundColor = "#ffd500";
				this.primaryTextColor = "#000000";
				
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "prusanky-black-on-yellow":
				this.foregroundColor = "#ffd500";
				this.primaryTextColor = "#000000";
				
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "prusanky-yellow-on-white":
				this.foregroundColor = "#000000";
				this.primaryTextColor = "#ffffff";
				
				this.backgroundColor = "#ffd500";
				this.secondaryTextColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.foregroundColor = "#ffd500";
				this.primaryTextColor = "#ffffff";
				
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "ujezd-green-on-white":
				this.foregroundColor = "#ffffff";
				this.primaryTextColor = "#000000";
				
				this.backgroundColor = "#8ed4a3";
				this.secondaryTextColor = "#000000";
				
				break;
			case "ujezd-white-on-green":
				this.foregroundColor = "#8ed4a3";
				this.primaryTextColor = "#000000";
				
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "cssd-red-on-black":
				this.foregroundColor = "#000000";
				this.primaryTextColor = "#e63812";
				
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "cssd-black-on-red":
				this.foregroundColor = "#e63812";
				this.primaryTextColor = "#000000";
				
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "jilemnice-purple-on-black":
				this.foregroundColor = "#ffffff";
				this.primaryTextColor = "#000000";
				
				this.backgroundColor = "#6e1646";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "jilemnice-black-on-purple":
				this.foregroundColor = "#6e1646";
				this.primaryTextColor = "#ffffff";
				
				this.backgroundColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "novarole-white-on-green":
				this.foregroundColor = "#a9ce2d";
				this.primaryTextColor = "#ffffff";
				
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "novarole-green-on-white":
				this.foregroundColor = "#ffffff";
				this.primaryTextColor = "#a9ce2d";
				
				this.backgroundColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				
				break;
			case "novarole-green-on-black":
				this.foregroundColor = "#000000";
				this.primaryTextColor = "#a9ce2d";
				
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
