class BottomSloganTemplate extends Template {
	description = "Určeno pro sociální sítě.";
	
	changeableAttributes = [
		"logoImage",
		"primaryImage",
		"primaryText",
		"nameText",
		"primaryColorScheme",
		"primaryImagePosition",
		"locationImage",
		"locationImageToTop",
		"iconImage",
		"iconText",
		"nameColorScheme",
		"logoIsCenter"
	];

	locationSource = null;
	iconSource = null;
	iconImage = null;
	
	locationImageToTop = false;

	iconText = "";

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];

	nameColorSchemes = [
		"white-on-black",
		"black-on-white",
		"black-on-gold"
	];

	primaryTextHighlightColorSchemes = [
		"black-on-gold"
	];
	
	changeableColors = [
		"primaryTextColor",
		"iconColor",
		"foregroundColor",
		"backgroundColor",
		"nameBackgroundColor",
		"nameTextColor",
		"primaryTextHighlightColor",
		"requesterTextColor"
	];

	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const primaryRectangleWidth = Math.ceil(this.canvas.width * 0.862);
		const primaryRectangleAngle = Math.ceil(this.canvas.height * 0.01);
		const primaryRectanglePosition = Math.ceil(this.canvas.height * 0.83);
		const primaryRectanglePaddingTopWithName = Math.ceil(this.canvas.height * 0.0275);
		const primaryRectanglePaddingTopWithoutName = Math.ceil(this.canvas.height * 0.02);
		const primaryRectangleAdditionalPaddingWithDiacritics = Math.ceil(this.canvas.height * 0.01);
		const primaryRectanglePaddingBottom = Math.ceil(this.canvas.height * 0.02);
		const primaryRectanglePaddingSides = Math.ceil(this.canvas.width * 0.04);

		const highlightPaddingSides = Math.ceil(this.canvas.width * 0.01);
		const highlightPaddingBottom = Math.ceil(this.canvas.height * 0.0125);
		const highlightPaddingTop = Math.ceil(this.canvas.height * -0.01); // It is, it's how Roboto works.

		const nameRectangleOffsetX = Math.ceil(this.canvas.width * 0.02);
		const nameRectangleOffsetY = Math.ceil(this.canvas.height * 0.01);
		const nameRectanglePaddingTopBottom = Math.ceil(this.canvas.height * 0.01);
		const nameRectanglePaddingSides = Math.ceil(this.canvas.width * 0.032);
		
		let locationWidth = Math.ceil(this.canvas.width * 0.250);
		const locationOffsetTop = locationWidth * 0.04;
		
		const logoHeight = Math.ceil(this.canvas.height * 0.07) * this.logoImageZoom;
		const logoSideOffset = Math.ceil(this.canvas.width * 0.03);
		const logoBottomOffset = Math.ceil(this.canvas.height * 0.05) - (
			logoHeight / this.logoImageZoom * (this.logoImageZoom - 1) / 2
		);
		
		const iconSideOffset = Math.ceil(this.canvas.width * 0.03);
		const iconBottomOffset = Math.ceil(this.canvas.height * 0.05);
		const iconHeight = Math.ceil(this.canvas.width * 0.06);
		
		const iconTextSideOffset = Math.ceil(this.canvas.width * 0.015);
		const iconTextTopOffset = -(Math.ceil(this.canvas.height * 0.002)); // FIXME, if possible. I think this might just be the font's fault.
		
		let primaryFontSize = Math.ceil(this.canvas.height * 0.09);
		const primaryFontLinePadding = 0;
		const primaryTextMaxLines = 3;
		
		const nameFontSize = Math.ceil(this.canvas.height * 0.03);
		
		let iconFontSize = Math.ceil(this.canvas.height * 0.027);
		const iconFontLinePadding = Math.ceil(this.canvas.height * 0.003);
		const iconTextMaxLines = 2;

		// Get primary text split into lines, no more than ``primaryTextMaxLines`` of them
		
		let primaryTextLines = null;
		
		do {
			this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;

			primaryTextLines = splitStringIntoLines(
				this.context,
				this.primaryText,
				primaryRectangleWidth - primaryRectanglePaddingSides,
				primaryTextMaxLines,
				true
			).reverse();

			if (primaryTextLines.length > primaryTextMaxLines) {
				primaryFontSize -= 2;
			}
		} while (primaryTextLines.length > primaryTextMaxLines);

		// Clear the canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Set image
		if (this.primaryImage !== null) {
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D

			const imageScaleX = this.canvas.width / this.primaryImage.width;
			const imageScaleY = this.canvas.height / this.primaryImage.height;

			const imageScale = Math.max(imageScaleX, imageScaleY) * this.primaryImageZoom;
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - this.primaryImage.width * imageScale) / 2 + this.primaryImageX * this.primaryImageZoom,
				(this.canvas.height - this.primaryImage.height * imageScale) / 2 + this.primaryImageY * this.primaryImageZoom,
			);
			this.context.drawImage(
				this.primaryImage,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
		}
		
		// Change bottom rectangle width based on the text height
		const secondaryRectangleHeight = (
			(
				(primaryTextLines.length * (primaryFontSize + primaryFontLinePadding))
				< this.canvas.height / 4.5
			) ?
			Math.ceil(this.canvas.height / 4) : Math.ceil(this.canvas.height / 2.757)
		);
		
		// Fill bottom rectangle
		this.context.fillStyle = this.backgroundColor;
		this.context.fillRect(
			0, this.canvas.height - secondaryRectangleHeight,
			this.canvas.width, secondaryRectangleHeight
		);
		
		const firstPrimaryLine = primaryTextLines[primaryTextLines.length - 1].join(" ");
		
		// Create rectangle behind the primary text
		const primaryRectangleHeight = (
			primaryTextLines.length * (primaryFontSize + primaryFontLinePadding)
			+ (
				(this.nameText !== "") ?
				primaryRectanglePaddingTopWithName : primaryRectanglePaddingTopWithoutName
			)
			+ primaryRectanglePaddingBottom
			+ (
				(
					firstPrimaryLine.replace(/[a-zA-Z0-9À-ž]+/g, "").length
					!== firstPrimaryLine.replace(/[a-zA-Z0-9]+/g, "").length
					&&
					this.nameText !== ""
				) ?
				primaryRectangleAdditionalPaddingWithDiacritics :
				0
			)
		);
		
		const foregroundRGB = hexToRgb(this.foregroundColor);
		
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		);
		
		this.context.beginPath();
		
		const primaryRectangleStartingX = (this.canvas.width - primaryRectangleWidth) / 2;
		const primaryRectangleEndingX = primaryRectangleWidth + primaryRectangleStartingX
		
		this.context.fillStyle = this.foregroundColor;
		
		this.context.moveTo(
			primaryRectangleStartingX,
			primaryRectanglePosition + primaryRectangleAngle
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePosition
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePosition - primaryRectangleHeight
		);
		this.context.lineTo(
			primaryRectangleStartingX,
			primaryRectanglePosition + primaryRectangleAngle - primaryRectangleHeight
		);
		
		this.context.closePath();
		
		this.context.fill();
		
		// Create primary text
		this.context.textAlign = "left";
		
		const useLightHighlight = (foregroundLightness > 207);
		
		const primaryLineX = this.canvas.width / 2;
		let currentPrimaryLineY = (
			primaryRectanglePosition
			- primaryRectanglePaddingBottom
			- primaryFontLinePadding
		);
		
		let primaryTextHighlightedColor = null;
		
		const lowercasePrimaryTextHighlightColor = this.primaryTextHighlightColor.toLowerCase();
		const hasColorOverride = (
			lowercasePrimaryTextHighlightColor === "#209a37" ||
			lowercasePrimaryTextHighlightColor === "#e63812"
		);
		
		if (hasColorOverride) {
			if (useLightHighlight) {
				primaryTextHighlightedColor = this.foregroundColor;
			} else {
				primaryTextHighlightedColor = this.primaryTextColor;
			}
		} else if (!useLightHighlight) {
			primaryTextHighlightedColor = this.foregroundColor;
		} else {
			primaryTextHighlightedColor = this.primaryTextColor;
		}
		
		this.context.fillStyle = this.primaryTextColor;
		
		for (let line of primaryTextLines) {
			let wordPosition = 0;
			
			for (let word of line) {
				const previousWords = line.slice(0, wordPosition).join(" ");
				const previousWordsWidth = this.context.measureText(
					previousWords
					+ (
						(previousWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				const nextWords = line.slice(wordPosition + 1, line.length).join(" ")
				const nextWordsWidth = this.context.measureText(
					nextWords
					+ (
						(nextWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				let currentWordWidth = this.context.measureText(word).width;
				
				for (const word of line.slice(wordPosition + 1, line.length)) {
					if (word.isHighlighted) {
						currentWordWidth += this.context.measureText(word.toString() + " ").width;
					} else {
						break;
					}
				}
				
				if (word.isHighlighted) {
					if (
						wordPosition === 0 ||
						!line[wordPosition - 1].isHighlighted
					) {
						this.context.fillStyle = this.primaryTextHighlightColor;
						this.context.beginPath();

						const startingHighlightLineX = (
							primaryLineX
							+ Math.ceil(previousWordsWidth / 2)
							- Math.ceil(nextWordsWidth / 2)
							- Math.ceil(this.context.measureText(word).width / 2)
						);

						this.context.moveTo(
							startingHighlightLineX - highlightPaddingSides,
							currentPrimaryLineY + highlightPaddingBottom
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								+ highlightPaddingBottom
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ primaryRectangleWidth
								)
							)
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ primaryRectangleWidth
								)
							)
						);
						this.context.lineTo(
							startingHighlightLineX - highlightPaddingSides,
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
							)
						);
						
						this.context.closePath();
						
						this.context.fill();
					}
					
					this.context.fillStyle = primaryTextHighlightedColor;
				}
				
				this.context.fillText(
					word + " ",
					(
						primaryLineX
						+ Math.ceil(previousWordsWidth / 2)
						- Math.ceil(nextWordsWidth / 2)
						- Math.ceil(this.context.measureText(word).width / 2)
					),
					currentPrimaryLineY
				);
				
				wordPosition++;
				
				this.context.fillStyle = this.primaryTextColor;
			}
			
			currentPrimaryLineY -= (primaryFontSize + primaryFontLinePadding);
		}
		
		let classRef = this;
		let nameRectangleTextWidth = null;
		let nameRectangleStartingX = null;
		let nameRectangleStartingY = null;
		
		// Create name, if not empty
		if (this.nameText !== "") {
			// Create rectangle for name text
			this.context.font = `${nameFontSize}px 'Roboto Condensed'`;
			this.context.textAlign = "center";
			
			nameRectangleStartingX = (
				primaryRectangleStartingX
				+ nameRectangleOffsetX
			);
			nameRectangleStartingY = (
				primaryRectanglePosition
				- primaryRectangleHeight
				- nameRectangleOffsetY
			);
			
			nameRectangleTextWidth = this.context.measureText(this.nameText).width;
			
			this.context.fillStyle = this.nameBackgroundColor;
			this.context.fillRect(
				nameRectangleStartingX, nameRectangleStartingY,
				nameRectangleTextWidth + nameRectanglePaddingSides * 2, nameFontSize + nameRectanglePaddingTopBottom * 2
			);
			
			// Create name text itself
			this.context.fillStyle = this.nameTextColor;
			this.context.fillText(
				this.nameText,
				nameRectangleStartingX + nameRectanglePaddingSides + Math.ceil(nameRectangleTextWidth / 2),
				nameRectangleStartingY + nameFontSize + nameRectanglePaddingTopBottom / 2
			);
		}
		
		// Create location image, if there is one
		if (this.locationSource !== null) {
			const locationImageLoadPromise = new Promise(
				resolve => {
					let locationImage = new Image();
					
					locationImage.onload = function() {
						let locationHeight = null;
						
						let imageStartingY = null;
						let imageStartingX = null;
						let locationRGB = null;
						
						if (!classRef.locationImageToTop || nameRectangleTextWidth === null) {
							locationHeight = Math.ceil(this.height * (locationWidth / this.width));
							
							imageStartingY = (
								primaryRectanglePosition
								- primaryRectangleHeight
								- locationHeight
								+ (
									primaryRectangleAngle
									* (locationWidth / primaryRectangleWidth)
									* 1.15 // I don't know if this is imprecise floating point numbers or my broken math.
								)
								+ locationOffsetTop
							);
							imageStartingX = primaryRectangleEndingX - locationWidth;
							locationRGB = foregroundRGB;
						} else {
							locationWidth = nameRectangleTextWidth + nameRectanglePaddingSides * 2;
							locationHeight = Math.ceil(this.height * (locationWidth / this.width));
							
							imageStartingX = nameRectangleStartingX;
							// Account for the slight tilt in most images. Not a great solution, but... oh well.
							imageStartingY = nameRectangleStartingY - (locationHeight * 0.95) + locationOffsetTop;
							
							locationRGB = hexToRgb(classRef.nameBackgroundColor);
						}
						
						classRef.context.drawImage(
							colorizeImage(
								this,
								locationWidth,
								locationHeight,
								locationRGB.r,
								locationRGB.g,
								locationRGB.b
							),
							imageStartingX, imageStartingY
						);
						
						resolve();
					}
					
					locationImage.src = this.locationSource;
				}
			);
			
			await locationImageLoadPromise;
		}
		
		// Create logo
		// See if we're using the light or dark version
		const backgroundRGB = hexToRgb(this.backgroundColor);
		const backgroundLightness = (
			0.2126 * backgroundRGB.r
			+ 0.7152 * backgroundRGB.g
			+ 0.0722 * backgroundRGB.b
		);

		const useLightLogo = (backgroundLightness < 207);

		function drawLogoImage(image) {
			let logoWidth = Math.ceil(image.width * (logoHeight / image.height));
			let logoX = (
				(classRef.logoIsCenter) ?
				(classRef.canvas.width - logoWidth)/2:
				primaryRectangleEndingX
				- logoWidth
				- logoSideOffset
			);

			classRef.context.drawImage(
				image,
				logoX, classRef.canvas.height - logoHeight - logoBottomOffset,
				logoWidth, logoHeight
			);
			
			if (classRef.logoIsCenter) {
				return;
			}
			// FIXME: Not accurate when logo is centered
			// Not really an expected scenario anyway, so low priority
			let iconTextMaxWidth = (
				classRef.canvas.width
				- primaryRectangleStartingX * 2
				- iconTextSideOffset
				- logoWidth
				- logoSideOffset
			);
			let iconTextX = (
				primaryRectangleStartingX
				+ iconTextSideOffset
			);
			let iconWidth = 0;
			
			function drawIconText() {
				// Set icon text
				classRef.context.textAlign = "left";
				classRef.context.fillStyle = classRef.iconColor;
				
				let iconTextLines = null;
				
				do {
					classRef.context.font = `${iconFontSize}px 'Roboto Condensed'`;
					
					iconTextLines = splitStringIntoLines(
						classRef.context,
						classRef.iconText,
						iconTextMaxWidth,
						iconTextMaxLines,
						true
					);
					
					if (iconTextLines.length > iconTextMaxLines) {
						iconFontSize -= 2;
					}
				} while (iconTextLines.length > iconTextMaxLines);
				
				let currentIconLineY = (
					classRef.canvas.height
					- iconHeight
					- iconBottomOffset
					+ iconFontSize
					+ iconTextTopOffset
				);
				
				for (let line of iconTextLines) {
					classRef.context.fillText(
						line.join(" "),
						iconTextX,
						currentIconLineY
					);
					
					currentIconLineY += (iconFontSize + iconFontLinePadding);
				}
			}
			
			const iconRGB = hexToRgb(classRef.iconColor);
			
			// Set icon
			if (classRef.iconImage !== null) {
				iconWidth = Math.ceil(classRef.iconImage.width * (iconHeight / classRef.iconImage.height));
				
				classRef.context.drawImage(
					colorizeImage(
						classRef.iconImage,
						iconWidth,
						iconHeight,
						iconRGB.r,
						iconRGB.g,
						iconRGB.b
					),
					primaryRectangleStartingX + iconSideOffset, classRef.canvas.height - iconHeight - iconBottomOffset,
					iconWidth, iconHeight
				);
				
				iconTextMaxWidth -= (
					iconSideOffset
					+ iconWidth
				);
				iconTextX += (
					+ iconSideOffset
					+ iconWidth
				);
				
				drawIconText();
			} else if (classRef.iconSource !== null) {
				const icon = new Image();
				
				icon.onload = function() {
					iconWidth = Math.ceil(icon.width * (iconHeight / icon.height));
					
					classRef.context.drawImage(
						colorizeImage(
							this,
							iconWidth,
							iconHeight,
							iconRGB.r,
							iconRGB.g,
							iconRGB.b
						),
						primaryRectangleStartingX + iconSideOffset, classRef.canvas.height - iconHeight - iconBottomOffset,
						iconWidth, iconHeight
					);
					
					iconTextMaxWidth -= (
						iconSideOffset
						+ iconWidth
					);
					iconTextX += (
						+ iconSideOffset
						+ iconWidth
					);
					
					drawIconText();
				}
				
				icon.src = classRef.iconSource;
			} else {
				drawIconText();
			}
		}

		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogoImage(this);
						
						resolve();
					}
					
					if (useLightLogo) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogoImage(this.logoImage);
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.0175);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> secondaryRectangleHeight - (this.canvas.height * 0.03)
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> secondaryRectangleHeight - (this.canvas.height * 0.03)
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Icon
	async setIconText(text, skipRedraw = false) {
		this.iconText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setIconSource(url, skipRedraw = false) {
		this.iconSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setIconImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.iconImage = new Image();
					
					classRef.iconImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.iconImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
	
	async resetIconImage(skipRedraw = false) {
		this.iconImage = null;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Location
	async setLocationSource(url, skipRedraw = false) {
		this.locationSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setLocationToTop(top, skipRedraw = false) {
		this.locationImageToTop = top;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#000000";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#000000";
				
				await this.setNameColorScheme("white-on-black", true);
				await this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#000000";
				this.foregroundColor ="#000000";
				this.backgroundColor ="#ffffff";
				
				await this.setNameColorScheme("black-on-white", true);
				await this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "forum-black-on-white":
				this.primaryTextColor = "#000000";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#962a51";
				
				await this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "forum-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#000000";
				this.foregroundColor = "#962a51";
				this.backgroundColor = "#ffffff";
				
				await this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.primaryTextColor = "#000000";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#00ad43";
				
				await this.setNameColorScheme("black-on-most", true);
				await this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#000000";
				this.foregroundColor = "#00ad43";
				this.backgroundColor = "#ffffff";
				
				await this.setNameColorScheme("black-on-most", true);
				await this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.primaryTextColor = "#000000";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#21274e";
				
				await this.setNameColorScheme("pirati-spolecne", true);
				await this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#21274e";
				this.foregroundColor = "#21274e";
				this.backgroundColor = "#ffffff";
				
				await this.setNameColorScheme("pirati-spolecne", true);
				await this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "louny-spolecne-black-on-white":
				this.primaryTextColor = "#000000";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#3e2a5b";
				this.nameBackgroundColor = "#e2d7a9";
				this.nameTextColor = "#000000";
				
				await this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "louny-spolecne-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#3e2a5b";
				this.foregroundColor = "#3e2a5b";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#e2d7a9";
				this.nameTextColor = "#000000";
				
				await this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "litomerice-blue-on-white":
				this.primaryTextColor = "#123172";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#123172";
				
				await this.setNameColorScheme("blue-on-litomerice", true);
				await this.setPrimaryTextHighlightColorScheme("litomerice", true);
				
				break;
			case "litomerice-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#123172";
				this.foregroundColor = "#123172";
				this.backgroundColor = "#ffffff";
				
				await this.setNameColorScheme("blue-on-litomerice", true);
				await this.setPrimaryTextHighlightColorScheme("litomerice", true);
				
				break;
			case "stranane-gray-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#4d4d4d";
				this.foregroundColor = "#4d4d4d";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#4d4d4d";
				
				break;
			case "stranane-yellow-on-white":
				this.primaryTextColor = "#4d4d4d";
				this.iconColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#4d4d4d";
				this.nameTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#4d4d4d";
				
				break;
			case "stranane-white-on-yellow":
				this.primaryTextColor = "#4d4d4d";
				this.iconColor = "#4d4d4d";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#4d4d4d";
				this.nameTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-black-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#000000";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "prusanky-yellow-on-white":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "ujezd-green-on-white":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#8ed4a3";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffdd55";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "ujezd-white-on-green":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#8ed4a3";
				this.nameBackgroundColor = "#ffdd55";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "cssd-red-on-black":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#e63812";
				this.backgroundColor = "#000000";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "cssd-black-on-red":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#e63812";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
			case "jilemnice-purple-on-black":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#6e1646";
				this.backgroundColor = "#000000";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "jilemnice-black-on-purple":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#6e1646";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "novarole-white-on-green":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#a9ce2d";
				
				this.primaryTextHighlightColor = "#a9ce2d";
				
				this.nameBackgroundColor = "#a9ce2d";
				this.nameTextColor = "#000000";
				
				break;
			case "novarole-green-on-white":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#ffffff";
				
				this.primaryTextHighlightColor = "#000000";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "novarole-green-on-black":
				this.primaryTextColor = "#000000";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#000000";
				
				this.primaryTextHighlightColor = "#000000";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				this.nameBackgroundColor = "#fde119";
				this.nameTextColor = "#000000";
				
				break;
		}
		
		this.requesterTextColor = this.iconColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setNameColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-gold":
				this.nameBackgroundColor = "#ffeda5";
				this.nameTextColor = "#000000";
				break;
			case "white-on-black":
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				break;
			case "black-on-white":
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				break;
			case "pirati-spolecne":
				this.nameBackgroundColor = "#9796ca";
				this.nameTextColor = "#000000";
				break;
			case "black-on-most":
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				break;
			case "blue-on-litomerice":
				this.nameBackgroundColor = "#cccccc";
				this.nameTextColor = "#123172";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setPrimaryTextHighlightColorScheme(scheme, skipRedraw = false) {
		switch(scheme) {
			case "gold":
				this.primaryTextHighlightColor = "#ffcc00";
				break;
			case "litomerice":
				this.primaryTextHighlightColor = "#afe87e";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Colors
	async setIconColor(color, skipRedraw = false) {
		this.iconColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setNameBackgroundColor(color, skipRedraw = false) {
		this.nameBackgroundColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setNameTextColor(color, skipRedraw = false) {
		this.nameTextColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setPrimaryTextHighlightColor(color, skipRedraw = false) {
		this.primaryTextHighlightColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Logo
	async setLogoIsCenter(isCenter, skipRedraw = false) {
		this.logoIsCenter = isCenter;

		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
