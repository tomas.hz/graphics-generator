class LeftLongTextTemplate extends Template {
	description = "Určeno pro sociální sítě.";
	
	changeableAttributes = [
		"logoImage",
		"primaryImage",
		"primaryText",
		"nameText",
		"primaryColorScheme",
		"primaryImagePosition",
		"secondaryText",
		"secondaryTextHighlight"
	];
	
	secondaryText = "";
	
	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];

	secondaryTextHighlightColorSchemes = [
		"black-on-gold"
	];
	
	changeableColors = [
		"primaryTextColor",
		"foregroundColor",
		"backgroundColor",
		"nameTextColor",
		"secondaryTextHighlightColor",
		"requesterTextColor"
	];
	
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const primaryRectangleWidth = Math.ceil(this.canvas.width * 0.464);
		const primaryRectangleAngle = Math.ceil(this.canvas.height * 0.0075);
		const secondaryRectangleWidth = Math.ceil(this.canvas.width * 0.364);

		const primaryRectanglePaddingTop = Math.ceil(this.canvas.height * 0.14);
		const primaryRectanglePaddingSide = Math.ceil(this.canvas.width * 0.054);
		const primaryRectanglePaddingInner = Math.ceil(this.canvas.width * 0.042);
		const primaryRectanglePaddingBetweenText = Math.ceil(this.canvas.height * 0.045);
		const nameTextPaddingTop = Math.ceil(this.canvas.height * 0.055);
		const nameTextPaddingSide = Math.ceil(this.canvas.width * 0.01);

		const secondaryTextHighlightPaddingSides = Math.ceil(this.canvas.width * 0.005);
		const secondaryTextHighlightPaddingTop = Math.ceil(this.canvas.height * -0.0065);
		const secondaryTextHighlightPaddingBottom = Math.ceil(this.canvas.height * 0.0075);

		let primaryFontSize = Math.ceil(this.canvas.height * 0.06);
		const primaryFontLinePadding = 0;
		let secondaryFontSize = Math.ceil(this.canvas.height * 0.04);
		const secondaryFontLinePadding = 4;;
		let nameFontSize = Math.ceil(this.canvas.height * 0.035);

		const logoHeight = Math.ceil(this.canvas.height * 0.05) * this.logoImageZoom;
		const logoBottomOffset = Math.ceil(this.canvas.height * 0.055) - (
			logoHeight / this.logoImageZoom * (this.logoImageZoom - 1) / 2
		);

		const primaryTextMaxLines = 3;
		const secondaryTextMaxLines = 9;

		// Calculate based on initial font size
		const primaryRectangleHeight = (
			primaryTextMaxLines * (primaryFontSize + primaryFontLinePadding)
			+ secondaryTextMaxLines * (secondaryFontSize + secondaryFontLinePadding)
			+ primaryRectanglePaddingBetweenText
			+ primaryRectanglePaddingInner * 2
		);

		let primaryTextLines = null;
		
		do {
			this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;
			
			primaryTextLines = splitStringIntoLines(
				this.context,
				this.primaryText,
				primaryRectangleWidth - primaryRectanglePaddingInner * 2,
				primaryTextMaxLines,
				true
			)
			
			if (primaryTextLines.length > primaryTextMaxLines) {
				primaryFontSize -= 2;
			}
		} while (primaryTextLines.length > primaryTextMaxLines);

		let secondaryTextLines = null;
		const originalSecondaryFontSize = secondaryFontSize;
		
		do {
			this.context.font = `${secondaryFontSize}px 'Roboto Condensed'`;
			
			secondaryTextLines = splitStringIntoLines(
				this.context,
				this.secondaryText,
				primaryRectangleWidth - primaryRectanglePaddingInner * 2,
				secondaryTextMaxLines,
				true
			)
			
			if (
				secondaryTextLines.length > secondaryTextMaxLines
				&& (
					secondaryTextLines.length * (secondaryFontSize + secondaryFontLinePadding)
					> secondaryTextMaxLines * (originalSecondaryFontSize + secondaryFontLinePadding)
				)
			) {
				secondaryFontSize -= 2;
			}
		} while (
			secondaryTextLines.length > secondaryTextMaxLines
			&& (
				secondaryTextLines.length * (secondaryFontSize + secondaryFontLinePadding)
				> secondaryTextMaxLines * (originalSecondaryFontSize + secondaryFontLinePadding)
			)
		);

		// Clear the canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Set image
		if (this.primaryImage !== null) {
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D

			const imageScaleX = this.canvas.width / this.primaryImage.width;
			const imageScaleY = this.canvas.height / this.primaryImage.height;

			const imageScale = Math.max(imageScaleX, imageScaleY) * this.primaryImageZoom;

			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - this.primaryImage.width * imageScale) / 2 + this.primaryImageX * this.primaryImageZoom,
				(this.canvas.height - this.primaryImage.height * imageScale) / 2 + this.primaryImageY * this.primaryImageZoom
			);
			this.context.drawImage(
				this.primaryImage,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
		}

		// Fill background rectangle
		this.context.fillStyle = this.backgroundColor;
		this.context.fillRect(
			0, 0,
			secondaryRectangleWidth, this.canvas.height
		);

		// Create primary rectangle
		const primaryRectangleStartingX = primaryRectanglePaddingSide;
		const primaryRectangleEndingX = primaryRectangleStartingX + primaryRectangleWidth;
		
		this.context.fillStyle = this.foregroundColor;
		
		this.context.beginPath();
		
		this.context.moveTo(
			primaryRectangleStartingX,
			primaryRectanglePaddingTop
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePaddingTop - primaryRectangleAngle
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePaddingTop + primaryRectangleHeight - primaryRectangleAngle
		);
		this.context.lineTo(
			primaryRectangleStartingX,
			primaryRectanglePaddingTop + primaryRectangleHeight
		);
		
		this.context.closePath();
		
		this.context.fill();

		// Create name text
		this.context.textAlign = "left";
 		this.context.font = `${nameFontSize}px 'Roboto Condensed'`;
 		this.context.fillStyle = this.nameTextColor;
		
		while (this.context.measureText(this.nameText).width > secondaryRectangleWidth - primaryRectanglePaddingSide * 2) {
			nameFontSize -= 2;
			this.context.font = `${nameFontSize}px 'Roboto Condensed'`;
		}
		
 		this.context.fillText(
 			this.nameText,
 			primaryRectangleStartingX + nameTextPaddingSide, nameTextPaddingTop + nameFontSize
 		);

		// Create primary text
		this.context.textAlign = "left";
		this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;
		this.context.fillStyle = this.primaryTextColor;

		const primarySecondaryLineX = primaryRectanglePaddingSide + primaryRectanglePaddingInner;
		let currentPrimaryLineY = (
			primaryRectanglePaddingTop
			+ primaryRectanglePaddingInner
			+ primaryFontSize
		);

		for (let line of primaryTextLines) {
			this.context.fillText(
				line.join(" "),
				primarySecondaryLineX,
				currentPrimaryLineY
			);
			
			currentPrimaryLineY += primaryFontSize + primaryFontLinePadding;
		}
		
		// Create secondary text
		this.context.font = `${secondaryFontSize}px 'Roboto Condensed'`;
		this.context.fillStyle = this.secondaryTextColor;

		let currentSecondaryLineY = (
			primaryRectanglePaddingTop
			+ primaryRectanglePaddingInner
			+ primaryTextLines.length * (primaryFontSize + primaryFontLinePadding)
			+ primaryRectanglePaddingBetweenText
			+ secondaryFontSize
		);
		let currentSecondaryHighlightLineY = currentSecondaryLineY;

		const foregroundRGB = hexToRgb(this.backgroundColor);
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		);

		const useLightHighlight = (foregroundLightness < 207);
		let previousWordHighlighted = false;
		let secondaryTextHighlightedColor = null;
		
		if (!useLightHighlight) {
			secondaryTextHighlightedColor = this.foregroundColor;
		} else {
			secondaryTextHighlightedColor = this.secondaryTextColor;
		}
		
		this.context.fillStyle = this.secondaryTextColor;
		
		for (let line of secondaryTextLines) {
			previousWordHighlighted = false;
			let wordPosition = 0;

			for (let word of line) {
				const spaceWidth = this.context.measureText(" ").width;
				
				const previousWords = line.slice(0, wordPosition).join(" ");
				const previousWordsWidth = (
					this.context.measureText(previousWords).width
					+ (
						(previousWords.length !== 0) ?
						spaceWidth : 0
					)
				);
				
				const currentWordWidth = this.context.measureText(word).width;
				
				if (word.isHighlighted) {
					this.context.fillStyle = this.secondaryTextHighlightColor;
					this.context.beginPath();
					
					this.context.moveTo(
						primarySecondaryLineX + previousWordsWidth - (
							(previousWordHighlighted) ?
							spaceWidth : secondaryTextHighlightPaddingSides
						),
						currentSecondaryHighlightLineY - secondaryFontSize - secondaryTextHighlightPaddingTop
					);
					this.context.lineTo(
						primarySecondaryLineX + previousWordsWidth + currentWordWidth + secondaryTextHighlightPaddingSides,
						(
							currentSecondaryHighlightLineY
							- secondaryFontSize
							- Math.max(currentWordWidth / primaryRectangleWidth)
							- secondaryTextHighlightPaddingTop
						)
					);
					this.context.lineTo(
						primarySecondaryLineX + previousWordsWidth + currentWordWidth + secondaryTextHighlightPaddingSides,
						currentSecondaryHighlightLineY + secondaryTextHighlightPaddingBottom
					);
					this.context.lineTo(
						primarySecondaryLineX + previousWordsWidth - (
							(previousWordHighlighted) ?
							spaceWidth : secondaryTextHighlightPaddingSides
						),
						currentSecondaryHighlightLineY + secondaryTextHighlightPaddingBottom + Math.max(
							currentWordWidth / primaryRectangleWidth
						)
					);
					
					this.context.fill();
					
					this.context.fillStyle = secondaryTextHighlightedColor;
					
					previousWordHighlighted = true;
				} else {
					previousWordHighlighted = false;
				}
				
				this.context.fillText(
					word + " ",
					(
						primarySecondaryLineX
						+ previousWordsWidth
					),
					currentSecondaryLineY
				);

				wordPosition++;
				
				this.context.fillStyle = this.secondaryTextColor;
			}
			
			currentSecondaryLineY += secondaryFontSize + secondaryFontLinePadding;
			currentSecondaryHighlightLineY += secondaryFontSize + secondaryFontLinePadding;
		}
		
		// Create logo
		// See if we want to use a dark or light one
		const backgroundRGB = hexToRgb(this.backgroundColor);
		const backgroundLightness = (
			0.2126 * backgroundRGB.r
			+ 0.7152 * backgroundRGB.g
			+ 0.0722 * backgroundRGB.b
		)

		const useLightLogo = (backgroundLightness < 207);

		let classRef = this; // FIXME...? Surely there's a better way to do this.
		
		function drawLogoImage(image) {
			const logoWidth = Math.ceil(image.width * (logoHeight / image.height));
			
			classRef.context.drawImage(
				image,
				(secondaryRectangleWidth - logoWidth) / 2, classRef.canvas.height - logoBottomOffset - logoHeight,
				logoWidth, logoHeight
			);
		}
		
		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogoImage(this);
						
						resolve();
					}
					
					if (useLightLogo) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogoImage(this.logoImage);
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, 0 - requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#aa5c65";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#000000";
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor ="#000000";
				this.backgroundColor ="#ffffff";
				break;
			case "forum-black-on-white":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#962a51";
				break;
			case "forum-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#962a51";
				this.foregroundColor = "#962a51";
				this.backgroundColor = "#ffffff";
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#00ad43";
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#00ad43";
				this.backgroundColor = "#ffffff";
				break;
			case "spolecne-s-piraty-black-on-white":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#21274e";
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#21274e";
				this.foregroundColor = "#21274e";
				this.backgroundColor = "#ffffff";
				break;
			case "louny-spolecne-black-on-white":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#3e2a5b";
				break;
			case "louny-spolecne-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#3e2a5b";
				this.foregroundColor = "#3e2a5b";
				this.backgroundColor = "#ffffff";
				break;
			case "litomerice-blue-on-white":
				this.primaryTextColor = "#123172";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#123172";
				
				this.setSecondaryTextHighlightColorScheme("litomerice");
				
				break;
			case "litomerice-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#123172";
				this.foregroundColor = "#123172";
				this.backgroundColor = "#ffffff";
				
				break;
			case "stranane-gray-on-yellow":
				this.primaryTextColor = "#ffd500";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#4d4d4d";
				this.foregroundColor = "#4d4d4d";
				this.backgroundColor = "#ffd500";
				
				break;
			case "stranane-yellow-on-white":
				this.primaryTextColor = "#4d4d4d";
				this.secondaryTextColor = "#4d4d4d";
				this.nameTextColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#4d4d4d";
				
				break;
			case "stranane-white-on-yellow":
				this.primaryTextColor = "#4d4d4d";
				this.secondaryTextColor = "#4d4d4d";
				this.nameTextColor = "#4d4d4d";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.secondaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-black-on-yellow":
				this.primaryTextColor = "#ffd500";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffd500";
				
				break;
			case "prusanky-yellow-on-white":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.secondaryTextHighlightColor = "#ffd500";
				
				break;
			case "ujezd-green-on-white":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#8ed4a3";
				this.backgroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "ujezd-white-on-green":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#8ed4a3";
				this.secondaryTextHighlightColor = "#ffdd55";
				
				break;
			case "cssd-red-on-black":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#e63812";
				this.backgroundColor = "#000000";
				
				break;
			case "cssd-black-on-red":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#e63812";
				
				break;
			case "jilemnice-purple-on-black":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#6e1646";
				this.backgroundColor = "#000000";
				
				break;
			case "jilemnice-black-on-purple":
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#6e1646";
				
				break;
			case "novarole-white-on-green":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#a9ce2d";
				this.secondaryTextHighlightColor = "#a9ce2d";
				
				break;
			case "novarole-green-on-white":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#ffffff";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-black":
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.foregroundColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setSecondaryTextHighlightColorScheme(scheme, skipRedraw = false) {
		switch(scheme) {
			case "gold":
				this.secondaryTextHighlightColor = "#ffcc00";
				break;
			case "litomerice":
				this.secondaryTextHighlightColor = "#afe87e";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Colors
	async setNameTextColor(color, skipRedraw = false) {
		this.nameTextColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setSecondaryTextHighlightColor(color, skipRedraw = false) {
		this.secondaryTextHighlightColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Secondary text
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
