class IvanPoints extends Template {
	description = "Určeno pro sociální sítě.";
	
	changeableAttributes = [
		"primaryImage",
		"primaryText",
		"secondaryText",
		"nameText",
		"underNameText",
		"fourIconSet",
		"primaryImagePosition",
		"primaryColorScheme"
	];

	changeableColors = [
		"primaryTextColor",
		"secondaryTextColor",
		"secondaryTextBackgroundColor",
		"nameBackgroundColor",
		"underNameTextColor",
		"iconColor"
	];
	
	primaryColorSchemes = [
		"white-on-black"
	];
	
	secondaryText = "";
	nameText = "";
	underNameText = "";
	
	iconSet = [
		{
			"source": "",
			"text": ""
		},
		{
			"source": "",
			"text": ""
		},
		{
			"source": "",
			"text": ""
		},
		{
			"source": "",
			"text": ""
		}
	];
	
	imageFlipped = false;
	
	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		//// Constants
		// Primary text
		let primaryTextFontSize = this.canvas.height * 0.06;
		const primaryTextMaxLines = 3;
		const primaryTextOffsetTop = this.canvas.height * 0.14;
		const primaryTextOffsetSide = this.canvas.width * 0.45;
		const primaryTextMaxWidth = this.canvas.width * 0.45;
		
		// Secondary text
		const secondaryTextOffsetTop = this.canvas.height * 0.04;
		let secondaryTextFontSize = this.canvas.height * 0.06;
		
		// Secondary text rectangle
		const secondaryTextRectanglePadding = this.canvas.width * 0.015;
		
		// Name text
		let nameTextFontSize = this.canvas.height * 0.065;
		const nameTextOffsetBottom = this.canvas.height * 0.16;
		const nameTextOffsetSide = this.canvas.width * 0.1;
		const nameTextMaxWidth = this.canvas.width * 0.26;
		
		// Name rectangle
		const nameRectanglePaddingSides = this.canvas.width * 0.03;
		const nameRectanglePaddingTopBottom = this.canvas.height * 0.005;
		
		// Under name text
		let underNameTextFontSize = this.canvas.height * 0.02;
		const underNameTextOffsetBottom = this.canvas.height * 0.13;
		const underNameTextMaxLines = 3;
		
		// Icons points
		const iconMaxWidthHeight = this.canvas.height * 0.1;
		const iconPaddingRight = this.canvas.width * 0.03;
		
		let iconTextFontSize = this.canvas.height * 0.045;
		const iconTextMaxLines = 2;
		
		const iconPointPaddingBetween = this.canvas.height * 0.035;
		
		// Clear canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		const classRef = this;
		
		// Create image
		if (this.primaryImage !== null) {
			const helperCanvas = document.createElement("canvas");
			
			helperCanvas.width = this.primaryImage.width;
			helperCanvas.height = this.primaryImage.height;
			
			const helperContext = helperCanvas.getContext("2d");
			
			if (this.imageFlipped) {
				helperContext.scale(-1, 1);
				
				helperContext.drawImage(
					this.primaryImage,
					-this.primaryImage.width, 0,
					this.primaryImage.width, this.primaryImage.height
				);
			} else {
				helperContext.drawImage(
					this.primaryImage,
					0, 0,
					this.primaryImage.width, this.primaryImage.height
				);
			}
			
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D
			
			const imageScaleX = this.canvas.width / helperCanvas.width;
			const imageScaleY = this.canvas.height / helperCanvas.height;
			
			const imageScale = Math.max(imageScaleX, imageScaleY) * this.primaryImageZoom;
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - helperCanvas.width * imageScale) / 2 + this.primaryImageX * this.primaryImageZoom,
				(this.canvas.height - helperCanvas.height * imageScale) / 2 + this.primaryImageY * this.primaryImageZoom,
			);
			
			this.context.drawImage(
				helperCanvas,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
		}
		
		// Create gradients
		const gradientRightLeft = this.context.createLinearGradient(
			0, this.canvas.height / 2,
			this.canvas.width, this.canvas.height / 2
		);
		
		gradientRightLeft.addColorStop(0, "rgba(0, 0, 0, 0)");
		gradientRightLeft.addColorStop(1, this.backgroundGradientColor);
		
		this.context.fillStyle = gradientRightLeft;
		this.context.fillRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		const gradientBottomTop = this.context.createLinearGradient(
			this.canvas.width / 2, this.canvas.height,
			this.canvas.width / 2, 0
		);
		
		gradientBottomTop.addColorStop(0, this.backgroundGradientColor + "7f"); // Semi-transparent
		gradientBottomTop.addColorStop(1, "rgba(0, 0, 0, 0)");
		
		this.context.fillStyle = gradientBottomTop;
		this.context.fillRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		let primaryTextLines = [];
		
		// Primary text
		if (this.primaryText !== "") {
			const originalPrimaryTextFontSize = primaryTextFontSize;
			
			do {
				this.context.font = `${primaryTextFontSize}px 'Bebas Neue'`;
				
				primaryTextLines = splitStringIntoLines(
					this.context,
					this.primaryText,
					primaryTextMaxWidth,
					primaryTextMaxLines,
					true
				)
				
				if (
					primaryTextLines.length > primaryTextMaxLines
					&& (
						primaryTextLines.length * primaryTextFontSize
						> primaryTextMaxLines * originalPrimaryTextFontSize
					)
				) {
					primaryTextFontSize -= 2;
				}
			} while (
				primaryTextLines.length > primaryTextMaxLines
				&& (
					primaryTextLines.length * primaryTextFontSize
					> primaryTextMaxLines * originalPrimaryTextFontSize
				)
			);
			
			this.context.fillStyle = this.primaryTextColor;
			let currentPrimaryLineY = primaryTextOffsetTop;
			
			for (const line of primaryTextLines) {
				this.context.fillText(
					line.join(" "),
					primaryTextOffsetSide,
					currentPrimaryLineY
				);
				
				currentPrimaryLineY += primaryTextFontSize;
			}
			
			currentPrimaryLineY -= primaryTextFontSize;
			
			// Secondary text
			if (this.secondaryText !== "") {
				const secondaryTextMaxWidth = (
					primaryTextMaxWidth
					- 2 * secondaryTextRectanglePadding
				);
				
				do {
					this.context.font = `${secondaryTextFontSize}px 'Bebas Neue'`;
					
					if (this.context.measureText(this.secondaryText).width > secondaryTextMaxWidth) {
						secondaryTextFontSize -= 2;
					}
				} while (this.context.measureText(this.secondaryText).width > secondaryTextMaxWidth);
				
				this.context.fillStyle = this.secondaryTextBackgroundColor;
				
				this.context.fillRect(
					primaryTextOffsetSide, currentPrimaryLineY + secondaryTextOffsetTop,
					primaryTextMaxWidth, (
						secondaryTextFontSize
						+ secondaryTextRectanglePadding * 2
					)
				);
				
				this.context.fillStyle = this.secondaryTextColor;
				this.context.textAlign = "center";
				
				this.context.fillText(
					this.secondaryText,
					primaryTextOffsetSide + primaryTextMaxWidth / 2,
					currentPrimaryLineY + secondaryTextOffsetTop + secondaryTextFontSize + secondaryTextRectanglePadding / 2
				);
				
				this.context.textAlign = "left";
			}
		}
		
		// Icon points
		let currentIconPointY = (
			primaryTextOffsetTop
			+ primaryTextLines.length * primaryTextFontSize
			+ secondaryTextOffsetTop
			+ secondaryTextRectanglePadding * 2
			// Incorrect calculation, just ignore this for now
			// + secondaryTextFontSize
 			+ iconPointPaddingBetween
		);
		
		const iconLineX = (
			primaryTextOffsetSide
			+ iconMaxWidthHeight
			+ iconPaddingRight
		);
		
		const iconTextMaxWidth = (
			primaryTextMaxWidth
			- iconMaxWidthHeight
			- iconPaddingRight
		);
		
		for (const iconPoint of this.iconSet) {
			if (iconPoint.source === "") {
				continue;
			}
			
			let currentIconHeight = 0;
			
			const iconImageLoadPromise = new Promise(
				resolve => {
					const icon = new Image();
					
					icon.onload = function() {
						currentIconHeight = iconMaxWidthHeight;
						
						let iconWidth = (this.width * (currentIconHeight / this.height));
						
						if (iconWidth > currentIconHeight) {
							currentIconHeight = (this.height * (iconMaxWidthHeight / this.width));
							iconWidth = iconMaxWidthHeight;
						}
						
						const iconRGB = hexToRgb(classRef.iconColor);
						
						classRef.context.drawImage(
							colorizeImage(
								this,
								iconWidth, currentIconHeight,
								iconRGB.r, iconRGB.g, iconRGB.b
							),
							(
								primaryTextOffsetSide
								+ (iconMaxWidthHeight - iconWidth) / 2
							), currentIconPointY,
							iconWidth, currentIconHeight
						);
						
						resolve();
					}
					
					icon.src = iconPoint.source;
				}
			);
			
			await iconImageLoadPromise;
			
			
			if (iconPoint.text === "") {
				currentIconPointY += iconMaxWidthHeight + iconPointPaddingBetween;
				continue;
			}
			
			let iconTextLines = [];
			const originalIconTextFontSize = iconTextFontSize;
			
			do {
				this.context.font = `${iconTextFontSize}px 'Anton'`;
				
				iconTextLines = splitStringIntoLines(
					this.context,
					iconPoint.text,
					iconTextMaxWidth,
					iconTextMaxLines,
					true
				)
				
				if (
					iconTextLines.length > iconTextMaxLines
					&& (
						iconTextLines.length * iconTextFontSize
						> iconTextMaxLines * originalIconTextFontSize
					)
				) {
					iconTextFontSize -= 2;
				}
			} while (
				iconTextLines.length > iconTextMaxLines
				&& (
					iconTextLines.length * iconTextFontSize
					> iconTextMaxLines * originalIconTextFontSize
				)
			);
			
			const iconOffset = (
				currentIconHeight
				- (
					iconTextLines.length
					* iconTextFontSize
					* 1.2
				)
			) / 2
			
			this.context.fillStyle = this.primaryTextColor;
			let currentIconLineY = (
				currentIconPointY
				+ iconTextFontSize
				+ Math.max(
					-iconOffset,
					iconOffset
				)
			);
			
			for (const line of iconTextLines) {
				this.context.fillText(
					line.join(" "),
					iconLineX,
					currentIconLineY
				);
				
				currentIconLineY += iconTextFontSize * 1.2; // Allow space for accented chars
			}
			
			currentIconPointY += iconMaxWidthHeight + iconPointPaddingBetween;
		}
		
		// Name text
		if (this.nameText !== "") {
			do {
				this.context.font = `${nameTextFontSize}px 'Bebas Neue'`;
				
				if (this.context.measureText(this.nameText).width > nameTextMaxWidth) {
					nameTextFontSize -= 2;
				}
			} while (this.context.measureText(this.nameText).width > nameTextMaxWidth);
			
			const helperCanvas = document.createElement("canvas");
			helperCanvas.width = (
				nameRectanglePaddingSides * 2
				+ nameTextMaxWidth
			);
			helperCanvas.height = (
				nameRectanglePaddingTopBottom * 2
				+ nameTextFontSize
			);
			
			const helperContext = helperCanvas.getContext("2d");
			
			helperContext.fillStyle = classRef.nameBackgroundColor;
			
			helperContext.fillRect(
				0, 0,
				helperCanvas.width, helperCanvas.height
			);
			
			helperContext.font = `${nameTextFontSize}px 'Bebas Neue'`;
			helperContext.textAlign = "center";
			helperContext.globalCompositeOperation = "xor";
			
			helperContext.fillText(
				classRef.nameText,
				helperContext.canvas.width / 2,
				helperContext.canvas.height - nameRectanglePaddingTopBottom * 3
			);
			
			classRef.context.drawImage(
				helperCanvas,
				nameTextOffsetSide - nameRectanglePaddingSides,
				classRef.canvas.height - nameTextOffsetBottom - helperCanvas.height
			);
		}
		
		// Under name text
		if (this.underNameText !== "") {
			let underNameTextLines = null;
			const originalUnderNameTextMaxFontSize = underNameTextFontSize;
			
			do {
				this.context.font = `${underNameTextFontSize}px 'Roboto Condensed'`;
				
				underNameTextLines = splitStringIntoLines(
					this.context,
					this.underNameText,
					nameTextMaxWidth,
					underNameTextMaxLines,
					true
				)
				
				if (
					underNameTextLines.length > underNameTextMaxLines
					&& (
						underNameTextLines.length * underNameTextFontSize
						> underNameTextMaxLines * originalUnderNameTextMaxFontSize
					)
				) {
					underNameTextFontSize -= 2;
				}
			} while (
				underNameTextLines.length > underNameTextMaxLines
				&& (
					underNameTextLines.length * underNameTextFontSize
					> underNameTextMaxLines * originalUnderNameTextMaxFontSize
				)
			);
			
			let currentUnderNameTextLineY = this.canvas.height - underNameTextOffsetBottom;
			this.context.fillStyle = this.underNameTextColor;
			
			this.context.textAlign = "center";
			
			for (const line of underNameTextLines) {
				this.context.fillText(
					line.join(" "),
					nameTextOffsetSide + nameTextMaxWidth / 2,
					currentUnderNameTextLineY
				);
				
				currentUnderNameTextLineY += underNameTextFontSize;
			}
			
			this.context.textAlign = "left";
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.0175);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Text
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setNameText(text, skipRedraw = false) {
		this.nameText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setUnderNameText(text, skipRedraw = false) {
		this.underNameText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		this.primaryTextColor = "#ffffff";
		
		this.secondaryTextColor = "#000000";
		this.secondaryTextBackgroundColor = "#3db7ac";
		
		this.nameBackgroundColor = 
		this.underNameTextColor = "#ffffff";
		
		this.iconColor = "#3db7ac";
		
		this.backgroundGradientColor = "#000000";
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
