class BannerNameLeft extends Template {
	description = "Určeno pro tisk na bannery.";
	
	changeableAttributes = [
		"logoImage",
		"primaryImage",
		"primaryText",
		"primaryColorScheme",
		"primaryImagePosition",
		"underNameText",
		"secondaryNameText",
		"secondaryUnderNameText",
		"terciaryText"
	];

	iconText = "";
	secondaryNameText = "";
	underNameText = "";
	secondaryUnderNameText = "";
	terciaryText = "";
	
	defaultResolution = 7874;
	aspectRatio = 1.9;

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];
	
	changeableColors = [
		"primaryTextColor",
		"foregroundColor",
		"backgroundColor",
		"nameBackgroundColor",
		"nameTextColor",
		"primaryTextHighlightColor",
		"underNameTextColor",
		"terciaryTextColor",
		"terciaryTextBackgroundColor",
		"requesterTextColor"
	];

	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const backgroundRectangleWidth = Math.ceil(this.canvas.width * 0.3);
		
		const foregroundRectangleOffsetTop = Math.ceil(this.canvas.height * 0.13);
		const foregroundRectangleOffsetSide = Math.ceil(this.canvas.width * 0.0684);
		const foregroundRectangleWidth = Math.ceil(this.canvas.width * 0.3);
		const foregroundRectanglePaddingInnerSides = Math.ceil(this.canvas.width * 0.01);
		const foregroundRectanglePaddingTopBottom = Math.ceil(this.canvas.height * 0.02);
		const foregroundRectangleAdditionalBottomTerciaryPadding = Math.ceil(this.canvas.height * 0.025);
		const foregroundRectangleAngle = Math.ceil(this.canvas.height * 0.0075);
		
		const highlightPaddingSides = Math.ceil(this.canvas.width * 0.01);
		const highlightPaddingBottom = Math.ceil(this.canvas.height * 0.0125);
		const highlightPaddingTop = Math.ceil(this.canvas.height * -0.01);
		
		let primaryFontSize = Math.ceil(this.canvas.height * 0.126);
		let primaryTextMaxLines = 4;
		
		const logoHeight = Math.ceil(this.canvas.height * 0.09) * this.logoImageZoom;
		const logoOffsetBottom = Math.ceil(this.canvas.height * 0.13) * ((3 - this.logoImageZoom) / 2);
		
		let nameFontSize = Math.ceil(this.canvas.height * 0.05) * ((3 - this.logoImageZoom) / 2);;
		
		const nameRectangleOffsetBottom = Math.ceil(this.canvas.height * 0.18);
		const nameRectangleOffsetSide = Math.ceil(this.canvas.width * 0.0684);
		const nameRectanglePaddingInner = Math.ceil(this.canvas.height * 0.0225);
		const nameRectanglesPaddingBetween = Math.ceil(this.canvas.width * 0.025);
		const nameAreaMaxWidth = Math.ceil((this.canvas.width - nameRectangleOffsetSide - backgroundRectangleWidth) * 0.8);
		
		let secondaryFontSize = Math.ceil(this.canvas.height * 0.035);
		const underNameTextOffsetBottom = Math.ceil(this.canvas.height * 0.13);
		
		const terciaryRectangleOffsetSide = foregroundRectanglePaddingInnerSides;
		const terciaryRectangleOffsetTop = Math.ceil(this.canvas.height * 0.03);
		const terciaryRectanglePaddingInner = Math.ceil(this.canvas.width * 0.01);
		
		const terciaryFontSize = Math.ceil(this.canvas.height * 0.038);
		
		// Clear the canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Set image
		if (this.primaryImage !== null) {
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D
			
			const imageScaleX = this.canvas.width / this.primaryImage.width;
			const imageScaleY = this.canvas.height / this.primaryImage.height;
			
			const imageScale = Math.max(imageScaleX, imageScaleY) * this.primaryImageZoom;
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - this.primaryImage.width * imageScale) / 2 + this.primaryImageX * this.primaryImageZoom,
				(this.canvas.height - this.primaryImage.height * imageScale) / 2 + this.primaryImageY * this.primaryImageZoom,
			);
			this.context.drawImage(
				this.primaryImage,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
		}
		
		let primaryLines = [];
		
		const originalHeight = (primaryFontSize * primaryTextMaxLines) - foregroundRectanglePaddingTopBottom;
		
		do {
			this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;
			
			primaryLines = splitStringIntoLines(
				this.context,
				this.primaryText,
				foregroundRectangleWidth - 2 * foregroundRectanglePaddingInnerSides,
				primaryTextMaxLines,
				true
			);
			
			if (primaryLines.length > primaryTextMaxLines) {
				primaryFontSize -= 2;
			}
			
			if (((primaryTextMaxLines + 1) * primaryFontSize) < originalHeight) {
				primaryTextMaxLines += 1;
			}
		} while (primaryLines.length > primaryTextMaxLines);
		
		const foregroundRectangleHeight = (
			primaryLines.length * primaryFontSize
			+ 2 * foregroundRectanglePaddingTopBottom
			+ (
				(this.terciaryText !== "") ?
				foregroundRectangleAdditionalBottomTerciaryPadding : 0
			)
		);
		
		this.context.fillStyle = this.backgroundColor;
		
		this.context.fillRect(
			this.canvas.width - backgroundRectangleWidth, 0,
			backgroundRectangleWidth, this.canvas.height
		);
		
		let currentForegroundRectangleY = foregroundRectangleOffsetTop;
		const foregroundRectangleStartingX = this.canvas.width - foregroundRectangleOffsetSide - foregroundRectangleWidth;
		const foregroundRectangleEndingX = foregroundRectangleStartingX + foregroundRectangleWidth;
		
		this.context.fillStyle = this.foregroundColor;
		
		this.context.beginPath();
		
		this.context.moveTo(
			foregroundRectangleStartingX,
			currentForegroundRectangleY + foregroundRectangleAngle
		);
		this.context.lineTo(
			foregroundRectangleEndingX,
			currentForegroundRectangleY
		);
		this.context.lineTo(
			foregroundRectangleEndingX,
			currentForegroundRectangleY + foregroundRectangleHeight + (
				(this.terciaryText !== "") ?
				foregroundRectangleAdditionalBottomTerciaryPadding : 0
			)
		);
		this.context.lineTo(
			foregroundRectangleStartingX,
			currentForegroundRectangleY + foregroundRectangleAngle + foregroundRectangleHeight + (
				(this.terciaryText !== "") ?
				foregroundRectangleAdditionalBottomTerciaryPadding : 0
			)
		);
		
		this.context.closePath();
		
		this.context.fill();
		
		currentForegroundRectangleY += foregroundRectanglePaddingTopBottom + primaryFontSize;
		
		// Create primary text
		this.context.textAlign = "left";
		const foregroundRGB = hexToRgb(this.foregroundColor);
		
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		);
		
		const useLightHighlight = (foregroundLightness > 207);
		
		const primaryLineX = foregroundRectangleEndingX - (foregroundRectangleWidth / 2);
		
		let primaryTextHighlightedColor = null;
		
		const lowercasePrimaryTextHighlightColor = this.primaryTextHighlightColor.toLowerCase();
		const hasColorOverride = (
			lowercasePrimaryTextHighlightColor === "#209a37" ||
			lowercasePrimaryTextHighlightColor === "#e63812"
		);
		
		if (hasColorOverride) {
			if (useLightHighlight) {
				primaryTextHighlightedColor = this.foregroundColor;
			} else {
				primaryTextHighlightedColor = this.primaryTextColor;
			}
		} else if (!useLightHighlight) {
			primaryTextHighlightedColor = this.foregroundColor;
		} else {
			primaryTextHighlightedColor = this.primaryTextColor;
		}
		
		this.context.fillStyle = this.primaryTextColor;
		
		for (let line of primaryLines) {
			let wordPosition = 0;
			
			for (let word of line) {
				const previousWords = line.slice(0, wordPosition).join(" ");
				const previousWordsWidth = this.context.measureText(
					previousWords
					+ (
						(previousWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				const nextWords = line.slice(wordPosition + 1, line.length).join(" ")
				const nextWordsWidth = this.context.measureText(
					nextWords
					+ (
						(nextWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				let currentWordWidth = this.context.measureText(word).width;
				
				for (const word of line.slice(wordPosition + 1, line.length)) {
					if (word.isHighlighted) {
						currentWordWidth += this.context.measureText(word.toString() + " ").width;
					} else {
						break;
					}
				}
				
				if (word.isHighlighted) {
					if (
						wordPosition === 0 ||
						!line[wordPosition - 1].isHighlighted
					) {
						this.context.fillStyle = this.primaryTextHighlightColor;
						this.context.beginPath();
						
						const startingHighlightLineX = (
							primaryLineX
							+ Math.max(previousWordsWidth / 2)
							- Math.max(nextWordsWidth / 2)
							- Math.max(this.context.measureText(word).width / 2)
						);
						
						this.context.moveTo(
							startingHighlightLineX - highlightPaddingSides,
						currentForegroundRectangleY + highlightPaddingBottom
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentForegroundRectangleY
								+ highlightPaddingBottom
								- Math.max(
									(currentWordWidth * foregroundRectangleAngle)
									/ foregroundRectangleWidth
								)
							)
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentForegroundRectangleY
								- primaryFontSize
								- highlightPaddingTop
								- Math.max(
									(currentWordWidth * foregroundRectangleAngle)
									/ foregroundRectangleWidth
								)
							)
						);
						this.context.lineTo(
							startingHighlightLineX - highlightPaddingSides,
							(
								currentForegroundRectangleY
								- primaryFontSize
								- highlightPaddingTop
							)
						);
						
						this.context.closePath();
						
						this.context.fill();
					}
					
					this.context.fillStyle = primaryTextHighlightedColor;
				}
				
				this.context.fillText(
					word + " ",
					(
						primaryLineX
						+ Math.ceil(previousWordsWidth / 2)
						- Math.ceil(nextWordsWidth / 2)
						- Math.ceil(this.context.measureText(word).width / 2)
					),
					currentForegroundRectangleY
				);
				
				wordPosition++;
				
				this.context.fillStyle = this.primaryTextColor;
			}
			
			currentForegroundRectangleY += primaryFontSize;
		}
		
		this.context.textAlign = "center";
		
		const backgroundRGB = hexToRgb(this.backgroundColor);
		
		const backgroundLightness = (
			0.2126 * backgroundRGB.r
			+ 0.7152 * backgroundRGB.g
			+ 0.0722 * backgroundRGB.b
		)
		
		const useLightLogo = (backgroundLightness < 207);
		
		let classRef = this;

		function drawLogoImage(image) {
			let logoWidth = Math.ceil(image.width * (logoHeight / image.height));
			
			classRef.context.drawImage(
				image,
				(
					classRef.canvas.width - backgroundRectangleWidth
					+ (backgroundRectangleWidth - logoWidth) / 2
				), classRef.canvas.height - logoHeight - logoOffsetBottom,
				logoWidth, logoHeight
			);
		}
		
		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogoImage(this);
						
						resolve();
					}
					
					if (useLightLogo) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogoImage(this.logoImage);
		}
		
		if (this.terciaryText !== "") {
			this.context.fillStyle = this.terciaryTextBackgroundColor;
			this.context.font = `${terciaryFontSize}px 'Roboto Condensed'`;
			this.context.textAlign = "right";
			this.context.textBaseline = "bottom";
			
			this.context.fillRect(
				(
					this.canvas.width
					- foregroundRectangleOffsetSide
					- terciaryRectangleOffsetSide
					- 2 * terciaryRectanglePaddingInner
					- this.context.measureText(this.terciaryText).width
				),
				(
					foregroundRectangleOffsetTop
					+ foregroundRectangleHeight
					+ foregroundRectangleAdditionalBottomTerciaryPadding
					+ terciaryRectangleOffsetTop
					- terciaryFontSize
					- 2 * terciaryRectanglePaddingInner
				),
				(
					this.context.measureText(this.terciaryText).width + 2 * terciaryRectanglePaddingInner
				),
				(
					2 * terciaryRectanglePaddingInner
					+ terciaryFontSize
				)
			);
			
			this.context.fillStyle = this.terciaryTextColor;
			
			this.context.fillText(
				this.terciaryText,
				(
					this.canvas.width
					- foregroundRectangleOffsetSide
					- terciaryRectangleOffsetSide
					+ terciaryRectanglePaddingInner
					- 2 * terciaryRectanglePaddingInner
				),
				(
					foregroundRectangleOffsetTop
					+ foregroundRectangleHeight
					+ foregroundRectangleAdditionalBottomTerciaryPadding
					+ terciaryRectangleOffsetTop
					- terciaryRectanglePaddingInner * 0.85 // :P
				)
			);
			
			this.context.textBaseline = "alphabetic";
		}
		
		if (this.nameText !== "") {
			this.context.fillStyle = this.nameBackgroundColor;
			this.context.font = `${nameFontSize}px 'Roboto Condensed'`;
			this.context.textBaseline = "bottom";
			this.context.textAlign = "left";
			
			let nameTextWidth = 0;
			let secondaryNameTextWidth = 0;
			
			do {
				nameTextWidth = this.context.measureText(this.nameText).width;
				secondaryNameTextWidth = this.context.measureText(this.secondaryNameText).width;
				
				if (
					(nameTextWidth + secondaryNameTextWidth + nameRectanglesPaddingBetween)
					> nameAreaMaxWidth
				) {
					nameFontSize -= 2;
					this.context.font = `${nameFontSize}px 'Roboto Condensed'`;
				}
			} while (
				(nameTextWidth + secondaryNameTextWidth + nameRectanglesPaddingBetween)
				> nameAreaMaxWidth
			);
			
			this.context.fillRect(
				nameRectangleOffsetSide, (
					this.canvas.height
					- nameRectangleOffsetBottom
					- nameFontSize
					- nameRectanglePaddingInner * 2
				),
				(
					nameTextWidth
					+ nameRectanglePaddingInner * 2
				),
				(
					nameFontSize
					+ nameRectanglePaddingInner * 2
				)
			);
			
			this.context.fillStyle = this.nameTextColor;
			
			this.context.fillText(
				this.nameText,
				(
					nameRectangleOffsetSide
					+ nameRectanglePaddingInner
				),
				(
					this.canvas.height
					- nameRectangleOffsetBottom
					- nameRectanglePaddingInner * 0.85 // :P
				)
			);
			
			this.context.fillStyle = this.underNameTextColor;
			this.context.font = `${secondaryFontSize}px 'Roboto Condensed'`;
			
			while (this.context.measureText(this.underNameText).width > nameTextWidth) {
				secondaryFontSize -= 2;
				
				this.context.font = `${secondaryFontSize}px 'Roboto Condensed'`;
			}
			
			this.context.fillText(
				this.underNameText,
				(
					nameRectangleOffsetSide
					+ nameRectanglePaddingInner
				),
				(
					this.canvas.height
					- underNameTextOffsetBottom
				)
			);
			
			if (this.secondaryNameText !== "") {
				this.context.font = `${nameFontSize}px 'Roboto Condensed'`;
				
				this.context.fillRect(
					nameRectangleOffsetSide
					+ nameTextWidth
					+ nameRectanglePaddingInner * 2
					+ nameRectanglesPaddingBetween
					+ (
						nameAreaMaxWidth
						- (nameTextWidth + secondaryNameTextWidth + nameRectanglesPaddingBetween)
					) / 2, (
						this.canvas.height
						- nameRectangleOffsetBottom
						- nameFontSize
						- nameRectanglePaddingInner * 2
					),
					(
						secondaryNameTextWidth
						+ nameRectanglePaddingInner * 2
					),
					(
						nameFontSize
						+ nameRectanglePaddingInner * 2
					)
				);
				
				this.context.fillStyle = this.nameTextColor;
				
				this.context.fillText(
					this.secondaryNameText,
					nameRectangleOffsetSide
					+ nameTextWidth
					+ nameRectanglePaddingInner * 3
					+ nameRectanglesPaddingBetween
					+ (
						nameAreaMaxWidth
						- (nameTextWidth + secondaryNameTextWidth + nameRectanglesPaddingBetween)
					) / 2,
					(
						this.canvas.height
						- nameRectangleOffsetBottom
						- nameRectanglePaddingInner * 0.85 // :P
					)
				);
				
				this.context.fillStyle = this.underNameTextColor;
				this.context.font = `${secondaryFontSize}px 'Roboto Condensed'`;
				
				while (this.context.measureText(this.underNameText).width > nameTextWidth) {
					secondaryFontSize -= 2;
					
					this.context.font = `${secondaryFontSize}px 'Roboto Condensed'`;
				}
				
				this.context.fillText(
					this.secondaryUnderNameText,
					nameRectangleOffsetSide
					+ nameTextWidth
					+ nameRectanglePaddingInner * 3
					+ nameRectanglesPaddingBetween
					+ (
						nameAreaMaxWidth
						- (nameTextWidth + secondaryNameTextWidth + nameRectanglesPaddingBetween)
					) / 2,
					(
						this.canvas.height
						- underNameTextOffsetBottom
					)
				);
			}
			
			this.context.textBaseline = "alphabetic";
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.88
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.88
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.94, -this.canvas.width * 0.9658 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}

	// Text
	async setUnderNameText(text, skipRedraw = false) {
		this.underNameText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setSecondaryNameText(text, skipRedraw = false) {
		this.secondaryNameText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setSecondaryUnderNameText(text, skipRedraw = false) {
		this.secondaryUnderNameText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setTerciaryText(text, skipRedraw = false) {
		this.terciaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#000000";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#999999";
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				// this.primaryTextHighlightColor = "#ffcc00";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#32948b";
				
				break;
			case "forum-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#962a51";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#000000";
				
				break;
			case "forum-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#962a51";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				// this.primaryTextHighlightColor = "#ffcc00";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#000000";
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#00ad43";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#000000";
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#00ad43";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				// this.primaryTextHighlightColor = "#ffcc00";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#000000";
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#21274e";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#9796ca";
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#21274e";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				// this.primaryTextHighlightColor = "#ffcc00";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#9796ca";
				
				break;
			case "louny-spolecne-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#3e2a5b";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#e2d7a9";
				
				break;
			case "louny-spolecne-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#3e2a5b";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				// this.primaryTextHighlightColor = "#ffcc00";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#e2d7a9";
				
				break;
			case "litomerice-blue-on-white":
				this.primaryTextColor = "#123172";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#123172";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#afe87e";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#cccccc";
				
				break;
			case "litomerice-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#123172";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				// this.primaryTextHighlightColor = "#ffcc00";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#cccccc";
				
				break;
			case "stranane-gray-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#4d4d4d";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#4d4d4d";
				this.nameTextColor = "#ffffff";
				// 				this.primaryTextHighlightColor = "#afe87e";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#4d4d4d";
				this.terciaryTextBackgroundColor = "#ffffff";
				
				break;
			case "stranane-yellow-on-white":
				this.primaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#4d4d4d";
				this.primaryTextHighlightColor = "#4d4d4d";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#4d4d4d";
				this.terciaryTextBackgroundColor = "#ffffff";
				
				break;
			case "stranane-white-on-yellow":
				this.primaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#4d4d4d";
				this.nameTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#4d4d4d";
				
				break;
			case "prusanky-black-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
// 				this.primaryTextHighlightColor = "#afe87e";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				
				break;
			case "prusanky-yellow-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#000000";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				
				break;
			case "prusanky-white-on-yellow":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#000000";
				
				break;
			case "ujezd-green-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#8ed4a3";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#8ed4a3";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffdd55";
				
				break;
			case "ujezd-white-on-green":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#8ed4a3";
				this.nameBackgroundColor = "#8ed4a3";
				this.nameTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffdd55";
				this.underNameTextColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffdd55";
				
				break;
			case "cssd-red-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#e63812";
				this.backgroundColor = "#000000";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.underNameTextColor = "#ffffff";
				
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				
				break;
			case "cssd-black-on-red":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#e63812";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.underNameTextColor = "#ffffff";
				
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				
				break;
			case "jilemnice-purple-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#6e1646";
				this.backgroundColor = "#000000";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.underNameTextColor = "#ffffff";
				
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				
				break;
			case "jilemnice-black-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#6e1646";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.underNameTextColor = "#ffffff";
				
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				
				break;
			case "novarole-white-on-green":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#a9ce2d";
				
				this.nameBackgroundColor = "#a9ce2d";
				this.nameTextColor = "#000000";
				
				this.primaryTextHighlightColor = "#a9ce2d";
				
				this.underNameTextColor = "#ffffff";
				
				this.terciaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#000000";
				
				break;
			case "novarole-green-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#ffffff";
				
				this.nameBackgroundColor = "#a9ce2d";
				this.nameTextColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				this.underNameTextColor = "#ffffff";
				
				this.terciaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#000000";
				
				break;
			case "novarole-green-on-black":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#000000";
				
				this.nameBackgroundColor = "#a9ce2d";
				this.nameTextColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				this.underNameTextColor = "#ffffff";
				
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				this.nameBackgroundColor = "#fde119";
				this.nameTextColor = "#000000";
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.foregroundColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
