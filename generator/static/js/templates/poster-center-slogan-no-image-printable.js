class PosterCenterSloganNoImagePrintable extends PosterCenterSloganNoImage {
	isPrintable = true;
	aspectRatio = 0.707070707071;
	description = "Určeno pro tisk na plakáty.";
}
