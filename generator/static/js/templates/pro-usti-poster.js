class ProUstiPoster extends Template {
	description = "";
	
	changeableAttributes = [
		"primaryText",
		"secondaryText",
		"terciaryText",
		"proUstiIcon",
		"proUstiColorScheme"
	];

	defaultResolution = 4430;
	aspectRatio = 0.70955882353;
	
	changeableColors = [
		"primaryTextColor",
		"foregroundColor",
		"backgroundColor",
		"iconColor",
		"secondaryTextBackgroundColor",
		"secondaryTextColor",
		"terciaryTextColor"
	];
	
	iconSource = "";
	iconImage = null;
	
	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const foregroundRectangleHeight = Math.ceil(this.canvas.height * 0.85);
		const foregroundRectangleAngle = Math.ceil(this.canvas.height * 0.15);
		
		const foregroundRectangleNippleWidth = Math.ceil(this.canvas.width * 0.18);
		const foregroundRectangleNippleOffsetBottom = Math.ceil(foregroundRectangleAngle * 0.1);
		
		// ---
		
		const secondaryRectangleHeight = Math.ceil(this.canvas.height * 0.073);
		
		// ---
		
		const logoWidth = Math.ceil(this.canvas.width * 0.333);
		const logoOffsetSide = Math.ceil(this.canvas.width * 0.1);
		const logoOffsetBottom = Math.ceil(this.canvas.height * 0.018);
		
		// ---
		
		const logoBigWidth = Math.ceil(this.canvas.width * 0.2);
		const logoBigOffsetBottom = Math.ceil(this.canvas.height * 0.018);
		
		// ---
		
		const iconOffsetTop = Math.ceil(this.canvas.height * 0.07);
		const iconHeight = Math.ceil(this.canvas.height * 0.3);
		
		// ---
		
		let primaryTextFontSize = Math.ceil(this.canvas.height * 0.075);
		let primaryTextMaxLines = 2;
		const primaryTextMaxWidth = Math.ceil(this.canvas.width * 0.8);
		const primaryTextOffsetTop = Math.ceil(this.canvas.height * 0.03);
		
		// ---
		
		let secondaryTextFontSize = Math.ceil(this.canvas.height * 0.028);
		let secondaryTextMaxLines = 4;
		const secondaryTextOffsetTop = Math.ceil(this.canvas.height * 0.03);
		
		// ---
		
		let terciaryTextFontSize = Math.ceil(this.canvas.height * 0.028);
		const terciaryTextMaxWidth = Math.ceil(this.canvas.width * 0.45);
		
		// -----------------------------------------------------
		
		
		// Background
		this.context.fillStyle = this.backgroundColor;
		this.context.fillRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Secondary rectangle
		this.context.fillStyle = this.secondaryTextBackgroundColor;
		
		this.context.fillRect(
			0, this.canvas.height - secondaryRectangleHeight,
			this.canvas.width, secondaryRectangleHeight
		);
		
		// Foreground rectangle
		this.context.fillStyle = this.foregroundColor;
		this.context.beginPath();
		
		this.context.moveTo(0, 0);
		this.context.lineTo(this.canvas.width, 0);
		this.context.lineTo(
			this.canvas.width,
			foregroundRectangleHeight - foregroundRectangleAngle
		);
		this.context.lineTo(
			0,
			foregroundRectangleHeight
		);
		this.context.closePath();
		
		this.context.fill();
		
		// Foreground rectangle nipple
		this.context.beginPath();
		this.context.moveTo(
			(this.canvas.width - foregroundRectangleNippleWidth) / 2,
			foregroundRectangleHeight - foregroundRectangleAngle
		);
		this.context.lineTo(
			this.canvas.width / 2,
			foregroundRectangleHeight - foregroundRectangleNippleOffsetBottom
		);
		this.context.lineTo(
			(this.canvas.width + foregroundRectangleNippleWidth) / 2,
			foregroundRectangleHeight - foregroundRectangleAngle
		);
		this.context.closePath();
		
		this.context.fill();
		
		// Logo image
		const classRef = this;
		
		const secondaryTextRGB = hexToRgb(this.secondaryTextColor);
		
		const logoImageLoadPromise = new Promise(
			resolve => {
				const logoImage = new Image();
				
				logoImage.onload = function() {
					const logoHeight = Math.ceil(this.height * (logoWidth / this.width));
					
					classRef.context.drawImage(
						colorizeImage(
							this,
							logoWidth,
							logoHeight,
							secondaryTextRGB.r,
							secondaryTextRGB.g,
							secondaryTextRGB.b
						),
						classRef.canvas.width - logoOffsetSide - logoWidth, classRef.canvas.height - logoHeight - logoOffsetBottom,
						logoWidth, logoHeight
					);
					
					resolve();
				}
				
				logoImage.src = "static/images/pro-usti/logo.png";
			}
		);
		
		await logoImageLoadPromise;
		
		// Big logo image
		const terciaryTextRGB = hexToRgb(this.terciaryTextColor);
		
		const logoBigImageLoadPromise = new Promise(
			resolve => {
				const logoImage = new Image();
				
				logoImage.onload = function() {
					const logoBigHeight = Math.ceil(this.height * (logoBigWidth / this.width));
					
					classRef.context.drawImage(
						colorizeImage(
							this,
							logoBigWidth,
							logoBigHeight,
							terciaryTextRGB.r,
							terciaryTextRGB.g,
							terciaryTextRGB.b
						),
						classRef.canvas.width - logoOffsetSide - logoBigWidth, (
							classRef.canvas.height
							- logoBigHeight
							- logoBigOffsetBottom
							- secondaryRectangleHeight
						),
						logoBigWidth, logoBigHeight
					);
					
					resolve();
				}
				
				logoImage.src = "static/images/pro-usti/logo-big.png";
			}
		);
		
		await logoBigImageLoadPromise;
		
		// Icon
		function drawIcon(image, colorize = false) {
			const iconWidth = Math.ceil(image.width * (iconHeight / image.height));
			
			if (colorize) {
				const primaryRGB = hexToRgb(classRef.primaryTextColor);
				
				image = colorizeImage(
					image,
					iconWidth, iconHeight,
					primaryRGB.r, primaryRGB.g, primaryRGB.b
				);
			}
			
			classRef.context.drawImage(
				image,
				(classRef.canvas.width - iconWidth) / 2, iconOffsetTop,
				iconWidth, iconHeight
			);
		}
		
		if (this.iconImage !== null) {
			drawIcon(this.iconImage);
		} else if (this.iconSource !== "") {
			const iconLoadPromise = new Promise(
				resolve => {
					const icon = new Image();
					
					icon.onload = function() {
						drawIcon(this, true);
						
						resolve();
					}
					
					icon.src = classRef.iconSource;
				}
			);
			
			await iconLoadPromise;
		}
		
		let primaryTextLines = [];
		
		// Primary text
		if (this.primaryText !== "") {
			const originalPrimaryTextFontSize = primaryTextFontSize;
			
			do {
				this.context.font = `${primaryTextFontSize}px 'Raleway Black'`;
				
				primaryTextLines = splitStringIntoLines(
					this.context,
					this.primaryText,
					primaryTextMaxWidth,
					primaryTextMaxLines,
					true
				)
				
				if (
					primaryTextLines.length > primaryTextMaxLines
					&& (
						primaryTextLines.length * primaryTextFontSize
						> primaryTextMaxLines * originalPrimaryTextFontSize
					)
				) {
					primaryTextFontSize -= 2;
				}
			} while (
				primaryTextLines.length > primaryTextMaxLines
				&& (
					primaryTextLines.length * primaryTextFontSize
					> primaryTextMaxLines * originalPrimaryTextFontSize
				)
			);
			
			this.context.fillStyle = this.primaryTextColor;
			
			let currentPrimaryLineY = (
				iconOffsetTop
				+ iconHeight
				+ primaryTextOffsetTop
				+ primaryTextFontSize
			);
			
			for (const line of primaryTextLines) {
				this.context.fillText(
					line.join(" "),
					(this.canvas.width - primaryTextMaxWidth) / 2,
					currentPrimaryLineY
				);
				
				currentPrimaryLineY += primaryTextFontSize;
			}
		}
		
		// Secondary text
		if (this.secondaryText !== "") {
			let secondaryTextLines = null;
			const originalSecondaryTextFontSize = secondaryTextFontSize;
			
			do {
				this.context.font = `${secondaryTextFontSize}px 'Raleway'`;
				
				secondaryTextLines = splitStringIntoLines(
					this.context,
					this.secondaryText,
					primaryTextMaxWidth,
					secondaryTextMaxLines,
					true
				)
				
				if (
					secondaryTextLines.length > secondaryTextMaxLines
					&& (
						secondaryTextLines.length * secondaryTextFontSize
						> secondaryTextMaxLines * originalSecondaryTextFontSize
					)
				) {
					secondaryTextFontSize -= 2;
				}
			} while (
				secondaryTextLines.length > secondaryTextMaxLines
				&& (
					secondaryTextLines.length * secondaryTextFontSize
					> secondaryTextMaxLines * originalSecondaryTextFontSize
				)
			);
			
			this.context.fillStyle = this.primaryTextColor;
			
			let currentSecondaryLineY = (
				iconOffsetTop
				+ iconHeight
				+ primaryTextOffsetTop
				+ primaryTextLines.length * primaryTextFontSize
				+ secondaryTextOffsetTop
				+ secondaryTextFontSize
			);
			
			for (const line of secondaryTextLines) {
				this.context.fillText(
					line.join(" "),
					(this.canvas.width - primaryTextMaxWidth) / 2,
					currentSecondaryLineY
				);
				
				currentSecondaryLineY += secondaryTextFontSize;
			}
		}
		
		// Terciary text
		if (this.terciaryText !== "") {
			this.context.fillStyle = this.terciaryTextColor;
			
			do {
				this.context.font = `${terciaryTextFontSize}px 'Raleway Black'`;
				
				if (this.context.measureText(this.terciaryText).width > terciaryTextMaxWidth) {
					terciaryTextFontSize -= 2;
				}
			} while (this.context.measureText(this.terciaryText).width > terciaryTextMaxWidth);
			
			this.context.textBaseline = "middle";
			
			this.context.fillText(
				this.terciaryText,
				logoOffsetSide,
				(
					this.canvas.height
					- secondaryRectangleHeight / 2
					- (
						this.canvas.height
						- foregroundRectangleHeight
					) / 2
				)
			);
			
			this.context.textBaseline = "alphabetic";
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.986
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.986
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.993, -this.canvas.width * 0.993 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Secondary & terciary text
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setTerciaryText(text, skipRedraw = false) {
		this.terciaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Icons
	async setIconSource(url, skipRedraw = false) {
		this.iconSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setIconImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.iconImage = new Image();
					
					classRef.iconImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.iconImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
	
	async resetIconImage(skipRedraw = false) {
		this.iconImage = null;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setProUstiColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "blue-on-orange":
				this.primaryTextColor = "#133134";
				this.foregroundColor = "#51c7bc";
				
				this.backgroundColor = "#ff6f34";
				this.secondaryTextBackgroundColor = "#dd5027";
				
				this.iconColor = "#000000";
				
				this.secondaryTextColor = "#2c2e35";
				this.terciaryTextColor = "#231f20";
				
				this.requesterTextColor = "#000000";
				
				break;
			case "purple-on-pink":
				this.primaryTextColor = "#fdbedb";
				this.foregroundColor = "#812b4c";
				
				this.backgroundColor = "#fdbedb";
				this.secondaryTextBackgroundColor = "#eda7c5";
				
				this.iconColor = "#af5778";
				
				this.secondaryTextColor = "#812b4c";
				this.terciaryTextColor = "#812b4c";
				
				this.requesterTextColor = "#000000";
				
				break;
			case "orange-on-brown":
				this.primaryTextColor = "#341202";
				this.foregroundColor = "#ff6f34";
				
				this.backgroundColor = "#341202";
				this.secondaryTextBackgroundColor = "#261005";
				
				this.iconColor = "#8e4120";
				
				this.secondaryTextColor = "#ff6f34";
				this.terciaryTextColor = "#ff6f34";
				
				this.requesterTextColor = "#ffffff";
				
				break;
			case "pink-on-orange":
				this.primaryTextColor = "#ffa82d";
				this.foregroundColor = "#a71e42";
				
				this.backgroundColor = "#ffa82d";
				this.secondaryTextBackgroundColor = "#fd9529";
				
				this.iconColor = "#c9501c";
				
				this.secondaryTextColor = "#a71e42";
				this.terciaryTextColor = "#a71e42";
				
				this.requesterTextColor = "#000000";
				
				break;
			case "blue-on-white":
				this.primaryTextColor = "#cfebf4";
				this.foregroundColor = "#1a79b1";
				
				this.backgroundColor = "#d2e9f2";
				this.secondaryTextBackgroundColor = "#bbd5e6";
				
				this.iconColor = "#5a9ac1";
				
				this.secondaryTextColor = "#1a79b1";
				this.terciaryTextColor = "#1a79b1";
				
				this.requesterTextColor = "#000000";
				
				break;
			case "green-on-pink":
				this.primaryTextColor = "#ff9d71";
				this.foregroundColor = "#1c705e";
				
				this.backgroundColor = "#ff9d71";
				this.secondaryTextBackgroundColor = "#ff8f62";
				
				this.iconColor = "#d1512e";
				
				this.secondaryTextColor = "#1c705e";
				this.terciaryTextColor = "#1c705e";
				
				this.requesterTextColor = "#000000";
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
