class EventTextBottom extends Template {
	description = "Určeno pro události na Facebooku.";
	
	changeableAttributes = [
		"logoImage",
		"primaryImage",
		"secondaryImage",
		"primaryText",
		"dateText",
		"timeText",
		"locationText",
		"primaryColorScheme",
		"primaryImagePosition",
		"nameText",
		"secondaryText",
		"terciaryText"
	];
	
	secondaryImage = null;
	
	secondaryText = "";
	terciaryText = "";
	dateText = "";
	timeText = "";
	locationText = "";
	
	defaultResolution = 4430;
	aspectRatio = 2;
	
	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];
	
	changeableColors = [
		"primaryTextColor",
		"terciaryTextColor",
		"iconColor",
		"foregroundColor",
		"backgroundColor",
		"nameTextColor",
		"nameBackgroundColor",
		"requesterTextColor"
	];

	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const primaryRectangleWidth = Math.ceil(this.canvas.width * 0.7);
		const primaryRectangleAngle = Math.ceil(this.canvas.height * 0.01);
		const primaryRectanglePosition = Math.ceil(this.canvas.height * 0.83);
		const primaryRectanglePaddingTopWithName = Math.ceil(this.canvas.height * 0.038);
		const primaryRectanglePaddingTopWithoutName = Math.ceil(this.canvas.height * 0.02);
		const primaryRectangleAdditionalPaddingWithDiacritics = Math.ceil(this.canvas.height * 0.01);
		const primaryRectanglePaddingBottom = Math.ceil(this.canvas.height * 0.02);
		const primaryRectanglePaddingSides = Math.ceil(this.canvas.width * 0.04);

		const highlightPaddingSides = Math.ceil(this.canvas.width * 0.01);
		const highlightPaddingBottom = Math.ceil(this.canvas.height * 0.0125);
		const highlightPaddingTop = Math.ceil(this.canvas.height * -0.01); // It is, it's how Roboto works.

		const nameRectangleOffsetX = Math.ceil(this.canvas.width * 0.02);
		const nameRectangleOffsetY = Math.ceil(this.canvas.height * 0.01);
		const nameRectanglePaddingTopBottom = Math.ceil(this.canvas.height * 0.01);
		const nameRectanglePaddingSides = Math.ceil(this.canvas.width * 0.015);
		
		const logoHeight = Math.ceil(this.canvas.height * 0.07) * this.logoImageZoom;
		const logoSideOffset = Math.ceil(this.canvas.width * 0.04);
		const logoTopOffset = Math.ceil(this.canvas.width * 0.04) - (
			logoHeight / this.logoImageZoom * (this.logoImageZoom - 1) / 2
		);

		let primaryFontSize = Math.ceil(this.canvas.height * 0.15);
		const primaryFontLinePadding = 0;
		const primaryTextMaxLines = 3;
		
		const nameFontSize = Math.ceil(this.canvas.height * 0.05);
		
		let secondaryFontSize = Math.ceil(this.canvas.height * 0.045);
		const secondaryTextPaddingTop = Math.ceil(this.canvas.height * 0.02);
		const secondaryTextPaddingBottom = Math.ceil(this.canvas.height * 0.045);
		
		let informationTextFontSize = Math.ceil(this.canvas.height * 0.055);
		let informationIconHeight = Math.ceil(this.canvas.height * 0.04);
		let informationInnerPadding = Math.ceil(this.canvas.width * 0.005);
		const informationInnerOffset = Math.ceil(this.canvas.width * 0.015);
		const informationMaxWidth = Math.ceil(this.canvas.width * 0.7);
		let informationBottomOffset = Math.ceil(this.canvas.height * 0.027);
		
		const secondaryImageWidth = Math.ceil(this.canvas.width * 0.03);
		const secondaryImagePaddingSide = Math.ceil(this.canvas.width * 0.0035);
		
		let terciaryFontSize = Math.ceil(this.canvas.height * 0.025);
		const terciaryTextOffsetBottom = Math.ceil(this.canvas.height * 0.005); // :P
		let terciaryTextMaxLines = 2;
		const terciaryTextMaxWidth = Math.ceil(primaryRectangleWidth * 0.7);
		
		// Get primary text split into lines, no more than ``primaryTextMaxLines`` of them
		
		let primaryTextLines = null;
		
		do {
			this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;

			primaryTextLines = splitStringIntoLines(
				this.context,
				this.primaryText,
				primaryRectangleWidth - primaryRectanglePaddingSides,
				primaryTextMaxLines,
				true
			).reverse();

			if (primaryTextLines.length > primaryTextMaxLines) {
				primaryFontSize -= 2;
			}
		} while (primaryTextLines.length > primaryTextMaxLines);

		// Clear the canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Set image
		if (this.primaryImage !== null) {
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D

			const imageScaleX = this.canvas.width / this.primaryImage.width;
			const imageScaleY = this.canvas.height / this.primaryImage.height;

			const imageScale = Math.max(imageScaleX, imageScaleY) * this.primaryImageZoom;
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - this.primaryImage.width * imageScale) / 2 + this.primaryImageX * this.primaryImageZoom,
				(this.canvas.height - this.primaryImage.height * imageScale) / 2 + this.primaryImageY * this.primaryImageZoom,
			);
			this.context.drawImage(
				this.primaryImage,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
		}
		
		// Change bottom rectangle width based on the text height
		const secondaryRectangleHeight = (
			(
				(primaryTextLines.length * (primaryFontSize + primaryFontLinePadding))
				< this.canvas.height / 4.5
			) ?
			Math.ceil(this.canvas.height / 3.5) : Math.ceil(this.canvas.height / 2.757)
		);
		
		// Fill bottom rectangle
		this.context.fillStyle = this.backgroundColor;
		this.context.fillRect(
			0, this.canvas.height - secondaryRectangleHeight,
			this.canvas.width, secondaryRectangleHeight
		);
		
		const firstPrimaryLine = primaryTextLines[primaryTextLines.length - 1].join(" ");
		
		// Create rectangle behind the primary text
		const primaryRectangleHeight = (
			primaryTextLines.length * (primaryFontSize + primaryFontLinePadding)
			+ (
				(this.nameText !== "") ?
				primaryRectanglePaddingTopWithName : primaryRectanglePaddingTopWithoutName
			)
			+ primaryRectanglePaddingBottom
			+ (
				(
					firstPrimaryLine.replace(/[a-zA-Z0-9À-ž]+/g, "").length
					!== firstPrimaryLine.replace(/[a-zA-Z0-9]+/g, "").length
					&&
					this.nameText !== ""
				) ?
				primaryRectangleAdditionalPaddingWithDiacritics :
				0
			)
			+ (
				(this.secondaryText !== "") ?
				(secondaryFontSize + secondaryTextPaddingTop + secondaryTextPaddingBottom) :
				0
			)
		);
		
		const foregroundRGB = hexToRgb(this.foregroundColor);
		
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		)
		
		this.context.beginPath();
		
		const primaryRectangleStartingX = (this.canvas.width - primaryRectangleWidth) / 2;
		const primaryRectangleEndingX = primaryRectangleWidth + primaryRectangleStartingX
		
		this.context.fillStyle = this.foregroundColor;
		
		this.context.moveTo(
			primaryRectangleStartingX,
			primaryRectanglePosition + primaryRectangleAngle
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePosition
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePosition - primaryRectangleHeight
		);
		this.context.lineTo(
			primaryRectangleStartingX,
			primaryRectanglePosition + primaryRectangleAngle - primaryRectangleHeight
		);
		
		this.context.closePath();
		
		this.context.fill();
		
		// Create primary text
		this.context.textAlign = "left";
		
		const useLightHighlight = (foregroundLightness > 207);
		
		const primaryLineX = this.canvas.width / 2;
		let currentPrimaryLineY = (
			primaryRectanglePosition
			- primaryRectanglePaddingBottom
			- primaryFontLinePadding
			- (
				(this.secondaryText !== "") ?
				(secondaryFontSize + secondaryTextPaddingBottom + secondaryTextPaddingTop) :
				0
			)
		);
		
		let primaryTextHighlightedColor = null;
		
		const lowercasePrimaryTextHighlightColor = this.primaryTextHighlightColor.toLowerCase();
		const hasColorOverride = (
			lowercasePrimaryTextHighlightColor === "#209a37" ||
			lowercasePrimaryTextHighlightColor === "#e63812"
		);
		
		if (hasColorOverride) {
			if (useLightHighlight) {
				primaryTextHighlightedColor = this.foregroundColor;
			} else {
				primaryTextHighlightedColor = this.primaryTextColor;
			}
		} else if (!useLightHighlight) {
			primaryTextHighlightedColor = this.foregroundColor;
		} else {
			primaryTextHighlightedColor = this.primaryTextColor;
		}
		
		this.context.fillStyle = this.primaryTextColor;

		for (let line of primaryTextLines) {
			let wordPosition = 0;
			
			for (let word of line) {
				const previousWords = line.slice(0, wordPosition).join(" ");
				const previousWordsWidth = this.context.measureText(
					previousWords
					+ (
						(previousWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				const nextWords = line.slice(wordPosition + 1, line.length).join(" ")
				const nextWordsWidth = this.context.measureText(
					nextWords
					+ (
						(nextWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				let currentWordWidth = this.context.measureText(word).width;
				
				for (const word of line.slice(wordPosition + 1, line.length)) {
					if (word.isHighlighted) {
						currentWordWidth += this.context.measureText(word.toString() + " ").width;
					} else {
						break;
					}
				}
				
				if (word.isHighlighted) {
					if (
						wordPosition === 0 ||
						!line[wordPosition - 1].isHighlighted
					) {
						this.context.fillStyle = this.primaryTextHighlightColor;
						this.context.beginPath();

						const startingHighlightLineX = (
							primaryLineX
							+ Math.max(previousWordsWidth / 2)
							- Math.max(nextWordsWidth / 2)
							- Math.max(this.context.measureText(word).width / 2)
						);

						this.context.moveTo(
							startingHighlightLineX - highlightPaddingSides,
							currentPrimaryLineY + highlightPaddingBottom
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								+ highlightPaddingBottom
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ primaryRectangleWidth
								)
							)
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ primaryRectangleWidth
								)
							)
						);
						this.context.lineTo(
							startingHighlightLineX - highlightPaddingSides,
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
							)
						);
						
						this.context.closePath();
						
						this.context.fill();
						
						this.context.fillStyle = primaryTextHighlightedColor;
					}
				}
				
				this.context.fillText(
					word + " ",
					(
						primaryLineX
						+ Math.ceil(previousWordsWidth / 2)
						- Math.ceil(nextWordsWidth / 2)
						- Math.ceil(this.context.measureText(word).width / 2)
					),
					currentPrimaryLineY
				);
				
				wordPosition++;
				
				this.context.fillStyle = this.primaryTextColor;
			}
			
			currentPrimaryLineY -= (primaryFontSize + primaryFontLinePadding);
		}
		
		this.context.textAlign = "center";
		
		// Create secondary text, if there is any
		
		if (this.secondaryText !== "") {
			this.context.font = `${secondaryFontSize}px 'Roboto Condensed'`;
			
			while (this.context.measureText(this.secondaryText).width > primaryRectangleWidth - 2 * primaryRectanglePaddingSides) {
				secondaryFontSize -= 2;
				
				this.context.font = `${secondaryFontSize}px 'Roboto Condensed'`;
			}
			
			this.context.fillStyle = this.primaryTextColor;
			
			this.context.fillText(
				this.secondaryText,
				this.canvas.width / 2,
				primaryRectanglePosition - secondaryTextPaddingBottom
			);
		}
		
		// Create name, if not empty

		if (this.nameText !== "") {
			// Create rectangle for name text
			this.context.font = `${nameFontSize}px 'Roboto Condensed'`;
			
			const nameRectangleStartingX = (
				primaryRectangleStartingX
				+ nameRectangleOffsetX
			);
			const nameRectangleStartingY = (
				primaryRectanglePosition
				- primaryRectangleHeight
				- nameRectangleOffsetY
			);
			
			const nameRectangleTextWidth = this.context.measureText(this.nameText).width;
			
			this.context.fillStyle = this.nameBackgroundColor;
			this.context.fillRect(
				nameRectangleStartingX, nameRectangleStartingY,
				nameRectangleTextWidth + nameRectanglePaddingSides * 2, nameFontSize + nameRectanglePaddingTopBottom * 2
			);
			
			// Create name text itself
			this.context.fillStyle = this.nameTextColor;
			this.context.fillText(
				this.nameText,
				nameRectangleStartingX + nameRectanglePaddingSides + Math.ceil(nameRectangleTextWidth / 2),
				nameRectangleStartingY + nameFontSize + nameRectanglePaddingTopBottom / 2
			);
		}

		let classRef = this; // FIXME...? Surely there's a better way to do this.

		const iconRGB = hexToRgb(this.iconColor);
		
		const iconTextLightness = (
			0.2126 * iconRGB.r
			+ 0.7152 * iconRGB.g
			+ 0.0722 * iconRGB.b
		)
		
		const useLightIcons = (iconTextLightness > 207);
		
		if (this.terciaryText !== "") {
			let helperCanvas = document.createElement("canvas");
			
			let terciaryLines = [];
			
			const originalTerciaryHeight = terciaryFontSize * terciaryTextMaxLines;
			
			do {
				this.context.font = `${terciaryFontSize}px 'Roboto Condensed'`;
				
				terciaryLines = splitStringIntoLines(
					this.context,
					this.terciaryText,
					terciaryTextMaxWidth - (
						(this.secondaryImage !== null) ?
						secondaryImageWidth + secondaryImagePaddingSide :
						0
					),
					terciaryTextMaxLines,
					true
				);
				
				if (terciaryLines.length > terciaryTextMaxLines) {
					terciaryFontSize -= 2;
				}
				
				if (((terciaryTextMaxLines + 1) * terciaryFontSize) < originalTerciaryHeight) {
					terciaryTextMaxLines += 1;
				}
			} while (terciaryLines.length > terciaryTextMaxLines);
			
			let longestLineWidth = 0;
			
			for (let line of terciaryLines) {
				const length = this.context.measureText(line.join(" ")).width;
				
				if (length > longestLineWidth) {
					longestLineWidth = length;
				}
			}
			
			let totalWidth = Math.ceil(longestLineWidth);
			
			let helperContext = null;
			
			if (this.secondaryImage !== null) {
				totalWidth += secondaryImageWidth + secondaryImagePaddingSide;
			
				const secondaryImageHeight = (this.secondaryImage.height * (secondaryImageWidth / this.secondaryImage.width));
				
				helperCanvas.height = Math.ceil(Math.max(terciaryFontSize * (terciaryLines.length + 1), secondaryImageHeight));
				helperCanvas.width = totalWidth;
				
				helperContext = helperCanvas.getContext("2d");
				
				helperContext.drawImage(
					this.secondaryImage,
					0, 0,
					secondaryImageWidth, secondaryImageHeight
				);
			} else {
				helperCanvas.height = terciaryFontSize * (terciaryLines.length + 1);
				helperCanvas.width = totalWidth;
				helperContext = helperCanvas.getContext("2d");
			}
			
			helperContext.font = `${terciaryFontSize}px 'Roboto Condensed'`;
			helperContext.fillStyle = this.terciaryTextColor;
			
			let currentHelperLineY = terciaryFontSize;
			
			for (let line of terciaryLines) {
				helperContext.fillText(
					line.join(" "),
					(
						(this.secondaryImage !== null) ?
						secondaryImageWidth + secondaryImagePaddingSide :
						0
					),
					currentHelperLineY
				);
				
				currentHelperLineY += terciaryFontSize;
			}
			
			this.context.drawImage(
				helperCanvas,
				primaryRectangleEndingX - helperCanvas.width,
				this.canvas.height - primaryRectangleHeight - (this.canvas.height - primaryRectanglePosition) - helperCanvas.height - terciaryTextOffsetBottom
			);
		}
		
		if (
			this.dateText !== "" ||
			this.timeText !== "" ||
			this.locationText !== ""
		) {
			let helperCanvas = document.createElement("canvas");
			
			let contentWidth = 0;
			
			let dateIcon = null;
			let dateIconWidth = 0;
			let dateTextWidth = 0;
			
			let timeIcon = null;
			let timeIconWidth = 0;
			let timeTextWidth = 0;
			
			let locationIcon = null;
			let locationIconWidth = 0;
			let locationTextWidth = 0;
			
			do {
				contentWidth = 0;

				this.context.font = `${this.primaryFontStyle} ${informationTextFontSize}px ${this.primaryFont}`;
				
				if (this.dateText !== "") {
					dateTextWidth = this.context.measureText(this.dateText).width;
					
					contentWidth += (
						dateTextWidth
						+ informationInnerPadding
						+ informationInnerOffset
					);
					
					let dateIconLoadPromise = new Promise(
						resolve => {
							dateIcon = new Image();
							
							dateIcon.onload = function() {
								dateIconWidth = this.width * (informationIconHeight / this.height);
								
								contentWidth += dateIconWidth;
								
								resolve();
							}
							
							dateIcon.src = "/static/images/mini-icons/calendar.png";
						}
					);
					
					await dateIconLoadPromise;
				}
				
				if (this.timeText !== "") {
					timeTextWidth = this.context.measureText(this.timeText).width;
					
					contentWidth += (
						timeTextWidth
						+ informationInnerPadding
						+ informationInnerOffset
					);
					
					let timeIconLoadPromise = new Promise(
						resolve => {
							timeIcon = new Image();
							
							timeIcon.onload = function() {
								timeIconWidth = this.width * (informationIconHeight / this.height);
								
								contentWidth += timeIconWidth;
								
								resolve();
							}
							
							timeIcon.src = "/static/images/mini-icons/time.png";
						}
					);
					
					await timeIconLoadPromise;
				}
				
				if (this.locationText !== "") {
					locationTextWidth = this.context.measureText(this.locationText).width;
					
					contentWidth += (
						locationTextWidth
						+ informationInnerPadding
					);
					
					let locationIconLoadPromise = new Promise(
						resolve => {
							locationIcon = new Image();
							
							locationIcon.onload = function() {
								locationIconWidth = this.width * (informationIconHeight / this.height);
								
								contentWidth += locationIconWidth;
								
								resolve();
							}
							
							locationIcon.src = "/static/images/mini-icons/location.png";
						}
					);
					
					await locationIconLoadPromise;
				}
				
				if (informationMaxWidth < contentWidth) {
					informationIconHeight -= 1.2; // :P
					informationBottomOffset += 2;
					informationInnerPadding -= 0.6;
					informationTextFontSize -= 2;
					
					this.context.font = `${this.primaryFontStyle} ${informationTextFontSize}px ${this.primaryFont}`;
				}
			} while (informationMaxWidth < contentWidth);
			
			helperCanvas.height = informationIconHeight + informationTextFontSize;
			helperCanvas.width = contentWidth;
			let helperContext = helperCanvas.getContext("2d");
			
			let currentHelperLineX = 0;
			
			helperContext.fillStyle = this.iconColor;
			helperContext.font = `${this.primaryFontStyle} ${informationTextFontSize}px ${this.primaryFont}`;
			
			const helperCanvasY = helperCanvas.height * 0.1;
			
			const informationTextRGB = hexToRgb(this.iconColor);
			
			if (this.dateText !== "") {
				helperContext.drawImage(
					colorizeImage(
						dateIcon,
						dateIconWidth,
						informationIconHeight,
						informationTextRGB.r,
						informationTextRGB.g,
						informationTextRGB.b
					),
					currentHelperLineX, helperCanvasY,
					dateIconWidth, informationIconHeight
				);
				
				currentHelperLineX += dateIconWidth + informationInnerPadding;
				
				helperContext.fillText(
					this.dateText,
					currentHelperLineX, informationIconHeight + helperCanvasY
				);
				
				currentHelperLineX += dateTextWidth + informationInnerOffset;
			}
			
			if (this.timeText !== "") {
				helperContext.drawImage(
					colorizeImage(
						timeIcon,
						timeIconWidth,
						informationIconHeight,
						informationTextRGB.r,
						informationTextRGB.g,
						informationTextRGB.b
					),
					currentHelperLineX, helperCanvasY,
					timeIconWidth, informationIconHeight
				);
				
				currentHelperLineX += timeIconWidth + informationInnerPadding;
				
				helperContext.fillText(
					this.timeText,
					currentHelperLineX, informationIconHeight + helperCanvasY
				);
				
				currentHelperLineX += timeTextWidth + informationInnerOffset;
			}
			
			if (this.locationText !== "") {
				helperContext.drawImage(
					colorizeImage(
						locationIcon,
						locationIconWidth,
						informationIconHeight,
						informationTextRGB.r,
						informationTextRGB.g,
						informationTextRGB.b
					),
					currentHelperLineX, helperCanvasY,
					locationIconWidth, informationIconHeight
				);
				
				currentHelperLineX += locationIconWidth + informationInnerPadding;
				
				helperContext.fillText(
					this.locationText,
					currentHelperLineX, informationIconHeight + helperCanvasY
				);
				
				currentHelperLineX += locationTextWidth;
			}
			
			this.context.drawImage(
				helperCanvas,
				(this.canvas.width - helperCanvas.width) / 2,
				this.canvas.height - informationBottomOffset - helperCanvas.height
			);
		}
		
		// Create logo
		// See if we're using the light or dark version
		const backgroundRGB = hexToRgb(this.backgroundColor);
		const backgroundLightness = (
			0.2126 * backgroundRGB.r
			+ 0.7152 * backgroundRGB.g
			+ 0.0722 * backgroundRGB.b
		)

		const useLightLogo = (backgroundLightness < 207);

		function drawLogoImage(image) {
 			let logoWidth = Math.ceil(image.width * (logoHeight / image.height));

			classRef.context.drawImage(
 				image,
				logoSideOffset, logoTopOffset,
 				logoWidth, logoHeight
 			);
		}

		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogoImage(this);
						
						resolve();
					}
					
					if (useLightLogo) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogoImage(this.logoImage);
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> secondaryRectangleHeight - (this.canvas.height * 0.03)
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> secondaryRectangleHeight - (this.canvas.height * 0.03)
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.995 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Text
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setTerciaryText(text, skipRedraw = false) {
		this.terciaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setDateText(text, skipRedraw = false) {
		this.dateText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setTimeText(text, skipRedraw = false) {
		this.timeText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setLocationText(text, skipRedraw = false) {
		this.locationText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				this.setNameColorScheme("white-on-black", true);
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#000000";
				this.foregroundColor ="#000000";
				this.backgroundColor ="#ffffff";
				
				this.setNameColorScheme("black-on-white", true);
				
				break;
			case "forum-black-on-white":
				this.primaryTextColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#962a51";
				this.primaryTextHighlightColor = "#ffcc00";
				break;
			case "forum-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#000000";
				this.foregroundColor = "#962a51";
				this.backgroundColor = "#ffffff";
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.primaryTextColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#00ad43";
				
				this.setNameColorScheme("black-on-most", true);
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#000000";
				this.foregroundColor = "#00ad43";
				this.backgroundColor = "#ffffff";
				
				this.setNameColorScheme("black-on-most", true);
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.primaryTextColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#21274e";
				this.primaryTextHighlightColor = "#ffcc00";
				
				this.setNameColorScheme("pirati-spolecne", true);
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#000000";
				this.foregroundColor = "#21274e";
				this.backgroundColor = "#ffffff";
				
				this.setNameColorScheme("pirati-spolecne", true);
				
				break;
			case "louny-spolecne-black-on-white":
				this.primaryTextColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#3e2a5b";
				this.primaryTextHighlightColor = "#ffcc00";
				this.nameBackgroundColor = "#e2d7a9";
				this.nameTextColor = "#000000";
				
				break;
			case "louny-spolecne-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#000000";
				this.foregroundColor = "#3e2a5b";
				this.backgroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffcc00";
				this.nameBackgroundColor = "#e2d7a9";
				this.nameTextColor = "#000000";
				
				break;
			case "litomerice-blue-on-white":
				this.primaryTextColor = "#123172";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#123172";
				this.primaryTextHighlightColor = "#afe87e";
				
				this.setNameColorScheme("blue-on-litomerice", true);
				
				break;
			case "litomerice-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#000000";
				this.foregroundColor = "#123172";
				this.backgroundColor = "#ffffff";
				
				this.setNameColorScheme("blue-on-litomerice", true);
				
				break;
			case "stranane-gray-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#4d4d4d";
				this.foregroundColor = "#4d4d4d";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#4d4d4d";
				
				break;
			case "stranane-yellow-on-white":
				this.primaryTextColor = "#4d4d4d";
				this.terciaryTextColor = "#4d4d4d";
				this.iconColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#4d4d4d";
				this.nameTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#4d4d4d";
				
				break;
			case "stranane-white-on-yellow":
				this.primaryTextColor = "#4d4d4d";
				this.terciaryTextColor = "#4d4d4d";
				this.iconColor = "#4d4d4d";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#4d4d4d";
				this.nameTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-black-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#000000";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "prusanky-yellow-on-white":
				this.primaryTextColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.primaryTextColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "ujezd-green-on-white":
				this.primaryTextColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#8ed4a3";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffdd55";
				this.primaryTextHighlightColor = "#ffdd55";
				this.nameTextColor = "#000000";
				
				break;
			case "ujezd-white-on-green":
				this.primaryTextColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#8ed4a3";
				this.nameBackgroundColor = "#ffdd55";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "cssd-red-on-black":
				this.primaryTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#e63812";
				this.backgroundColor = "#000000";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "cssd-black-on-red":
				this.primaryTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#e63812";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "jilemnice-purple-on-black":
				this.primaryTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#6e1646";
				this.backgroundColor = "#000000";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "jilemnice-black-on-purple":
				this.primaryTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#6e1646";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "novarole-white-on-green":
				this.primaryTextColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.iconColor = "#000000";
				
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#a9ce2d";
				
				this.primaryTextHighlightColor = "#ffdd55";
				
				this.nameBackgroundColor = "#a9ce2d";
				this.nameTextColor = "#000000";
				
				break;
			case "novarole-green-on-white":
				this.primaryTextColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.iconColor = "#000000";
				
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#ffffff";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-black":
				this.primaryTextColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.iconColor = "#ffffff";
				
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#000000";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				this.nameBackgroundColor = "#fde119";
				this.nameTextColor = "#000000";
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.foregroundColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setNameColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-gold":
				this.nameBackgroundColor = "#ffeda5";
				this.nameTextColor = "#000000";
				break;
			case "white-on-black":
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				break;
			case "black-on-white":
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				break;
			case "pirati-spolecne":
				this.nameBackgroundColor = "#9796ca";
				this.nameTextColor = "#000000";
				break;
			case "black-on-most":
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				break;
			case "blue-on-litomerice":
				this.nameBackgroundColor = "#cccccc";
				this.nameTextColor = "#123172";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Secondary image
	async setSecondaryImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.secondaryImage = new Image();
					
					classRef.secondaryImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.secondaryImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
}
