class RollupBigLogoShortText extends Template {
	description = "Určeno pro rollupy.";
	
	changeableAttributes = [
		"logoImage",
		"primaryText",
		"secondaryText",
		"selectableRollupBackground",
		"qrCode"
	];

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];
	
	changeableColors = [
		"primaryTextColor",
		"backgroundColor",
		"foregroundColor",
		"secondaryTextColor",
		"terciaryTextBackgroundColor",
		"terciaryTextColor",
		"qrCodeColor",
		"requesterTextColor"
	];
	
	aspectRatio = 0.4;
	defaultResolution = 12015;

	qrCodeURI = "";
	backgroundSource = "";
	
	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		let qrSourceFontSize = Math.ceil(this.canvas.height * 0.02);
		let primaryTextFontSize = Math.ceil(this.canvas.height * 0.04);
		let secondaryTextFontSize = Math.ceil(this.canvas.height * 0.019);
		
		const secondaryTextMaxWidth = Math.ceil(this.canvas.width * 0.5);
		
		const primaryTextMaxWidth = Math.ceil(this.canvas.width * 0.75);
		const primaryTextOffsetBottom = Math.ceil(this.canvas.height * 0.4);
		let primaryTextMaxLines = 2;
		
		const primaryRectanglePaddingSides = Math.ceil(this.canvas.width * 0.08);
		const primaryRectanglePaddingTopBottom = Math.ceil(this.canvas.height * 0.01);
		
		const secondaryRectanglePaddingSides = Math.ceil(this.canvas.width * 0.04);
		const secondaryRectanglePaddingTopBottom = Math.ceil(this.canvas.height * 0.006);
		const secondaryRectangleOffsetTop = Math.ceil(this.canvas.height * -0.035);  // :P
		const secondaryRectangleOffsetSide = Math.ceil(this.canvas.width * 0.04);
		
		const qrCodeWidthHeight = Math.ceil(this.canvas.height * 0.09);
		const qrCodeBottomOffset = Math.ceil(this.canvas.height * 0.1);
		const qrCodeOuterPadding = Math.ceil(this.canvas.height * 0.01);
		
		const logoWidth = Math.ceil(this.canvas.width * 0.85) * this.logoImageZoom;
		const logoOffsetTop = Math.ceil(this.canvas.height * 0.13) * ((3 - this.logoImageZoom) / 2);
		
		const classRef = this;
		
		if (this.backgroundSource !== "") {
			const backgroundLoadPromise = new Promise(
				resolve => {
					const backgroundImage = new Image();
					
					backgroundImage.onload = function () {
						classRef.context.drawImage(
							this,
							0, 0,
							classRef.canvas.width, classRef.canvas.height
						);
						
						resolve();
					}
					
					backgroundImage.src = this.backgroundSource;
				}
			);
			
			await backgroundLoadPromise;
		}
		
		const logoRGB = hexToRgb(this.qrCodeColor);
		
		const logoLoadPromise = new Promise(
			resolve => {
				const logo = new Image();
				
				logo.onload = function() {
					const logoHeight = (this.height * (logoWidth / this.width));
					
					classRef.context.drawImage(
						colorizeImage(
							this,
							logoWidth, logoHeight,
							logoRGB.r, logoRGB.g, logoRGB.b
						),
						(classRef.canvas.width - logoWidth) / 2, logoOffsetTop,
						logoWidth, logoHeight
					);
					
					resolve();
				}
				
				logo.src = "static/images/base_icon.png";
			}
		);
		
		await logoLoadPromise;
		
		if (this.qrCodeURI !== "") {
			const qrCodeImage = new Image();
			
			const code = new QRCode(
				qrCodeImage,
				{
					"text": this.qrCodeURI,
					"width": qrCodeWidthHeight,
					"height": qrCodeWidthHeight,
					"colorDark": this.qrCodeColor,
					"colorLight": this.backgroundColor,
					"correctLevel": QRCode.CorrectLevel.H
				}
			);
			
			this.context.fillStyle = this.backgroundColor;
			
			const qrCodePosition = (this.canvas.width - qrCodeWidthHeight) / 2;
			
			this.context.fillRect(
				qrCodePosition - qrCodeOuterPadding, this.canvas.height - qrCodeBottomOffset - qrCodeWidthHeight - qrCodeOuterPadding,
				qrCodeWidthHeight + qrCodeOuterPadding * 2, qrCodeWidthHeight + qrCodeOuterPadding * 2
			);
			
			this.context.drawImage(
				code._oDrawing._elCanvas, // Ugly, but the fastest way to get the canvas
				(this.canvas.width - qrCodeWidthHeight) / 2, this.canvas.height - qrCodeWidthHeight - qrCodeBottomOffset,
				qrCodeWidthHeight, qrCodeWidthHeight
			);
			
			const maxTextWidth = qrCodeWidthHeight + 2 * qrCodeOuterPadding;
			
			do {
				this.context.font = `${qrSourceFontSize}px ${this.primaryFont}`;
				
				if (this.context.measureText(this.qrCodeURI).width > maxTextWidth) {
					qrSourceFontSize -= 2;
					this.context.font = `${qrSourceFontSize}px ${this.primaryFont}`;
				}
			} while (this.context.measureText(this.qrCodeURI).width > maxTextWidth);
			
			this.context.fillStyle = this.qrCodeColor;
			this.context.textAlign = "center";
			
			this.context.fillText(
				this.qrCodeURI,
				this.canvas.width / 2,
				this.canvas.height - qrCodeWidthHeight - qrCodeBottomOffset - qrCodeOuterPadding * 2
			);
		}
		
		if (this.primaryText !== "") {
			this.context.save();
			
			let primaryTextHighestWidth = 0;
			
			let primaryLines = null;
			const originalPrimaryTextFontSize = primaryTextFontSize;
			
			do {
				this.context.font = `${this.primaryFontStyle} ${primaryTextFontSize}px ${this.primaryFont}`;
				
				primaryLines = splitStringIntoLines(
					this.context,
					this.primaryText,
					primaryTextMaxWidth,
					primaryTextMaxLines,
					true
				);
				
				if (
					primaryLines.length > primaryTextMaxLines
					&& (
						primaryLines.length * primaryTextFontSize
						> primaryTextMaxLines * originalPrimaryTextFontSize
					)
				) {
					primaryTextFontSize -= 2;
				}
			} while (
				primaryLines.length > primaryTextMaxLines
				&& (
					primaryLines.length * primaryTextFontSize
					> primaryTextMaxLines * originalPrimaryTextFontSize
				)
			);
			
			for (const line of primaryLines) {
				const lineWidth = this.context.measureText(line.join(" ")).width;
				
				if (lineWidth > primaryTextHighestWidth) {
					primaryTextHighestWidth = lineWidth;
				}
			}
			
			const primaryRectangleHeight = (
				primaryLines.length * primaryTextFontSize +
				2 * primaryRectanglePaddingTopBottom
			);
			
			this.context.fillStyle = this.foregroundColor;
			
			this.context.translate(
				this.canvas.width / 2,
				(
					this.canvas.height
					- primaryTextOffsetBottom
					- (
						primaryTextFontSize
						+ 2 * primaryRectanglePaddingTopBottom
					) / 2
				)
			);
			this.context.rotate(-Math.PI/64);
			
			this.context.fillRect(
				(
					- primaryTextHighestWidth / 2
					- primaryRectanglePaddingSides
				), (
					primaryTextFontSize / 2
					+ primaryRectanglePaddingTopBottom
				),
				primaryTextHighestWidth + 2 * primaryRectanglePaddingSides, primaryRectangleHeight
			);
			
			this.context.fillStyle = this.primaryTextColor;
			this.context.textAlign = "center";
			
			let currentPrimaryLineY = (
				primaryTextFontSize * 1.7 // :P
				+ primaryRectanglePaddingTopBottom
			);
			
			for (const line of primaryLines) {
				this.context.fillText(
					line.join(" "),
					0,
					currentPrimaryLineY
				);
				
				currentPrimaryLineY += primaryTextFontSize;
			}
			
			this.context.restore();
			
			if (this.secondaryText !== "") {
				let secondaryTextWidth = 0;
				const secondaryTextOriginalFontSize = secondaryTextFontSize;
				
				do {
					this.context.font = `bold ${secondaryTextFontSize}px 'Roboto Condensed'`;
					
					secondaryTextWidth = this.context.measureText(this.secondaryText).width;
					
					if (secondaryTextWidth > secondaryTextMaxWidth) {
						secondaryTextFontSize -= 2;
						this.context.font = `bold ${secondaryTextFontSize}px 'Roboto Condensed'`;
					}
				} while (secondaryTextWidth > secondaryTextMaxWidth);
				
				this.context.fillStyle = this.terciaryTextBackgroundColor;
				
				this.context.fillRect(
					(
						this.canvas.width
						- (
							this.canvas.width
							- (
								primaryTextHighestWidth
								+ 2 * primaryRectanglePaddingSides
							)
						) / 2
						- secondaryTextWidth
						- secondaryRectanglePaddingSides * 2
						+ secondaryRectangleOffsetSide
					), (
						this.canvas.height
						- primaryTextOffsetBottom
						- 2 * primaryRectanglePaddingTopBottom
						- primaryTextFontSize
						- secondaryRectangleOffsetTop
					),
					(
						secondaryTextWidth
						+ 2 * secondaryRectanglePaddingSides
					), (
						secondaryTextOriginalFontSize
						+ 2 * secondaryRectanglePaddingTopBottom
					)
				);
				
				this.context.fillStyle = this.secondaryTextColor;
				this.context.textAlign = "left";
				
				this.context.fillText(
					this.secondaryText,
					(
						this.canvas.width
						- (
							this.canvas.width
							- (
								primaryTextHighestWidth
								+ 2 * primaryRectanglePaddingSides
							)
						) / 2
						- secondaryTextWidth
						- secondaryRectanglePaddingSides
						+ secondaryRectangleOffsetSide
					),
					(
						this.canvas.height
						- primaryTextOffsetBottom
						- 2 * primaryRectanglePaddingTopBottom
						- primaryTextFontSize
						- secondaryRectangleOffsetTop
						+ secondaryTextFontSize
						+ secondaryRectanglePaddingTopBottom
						+ (
							secondaryTextOriginalFontSize
							- secondaryTextFontSize
						) / 2
					)
				);
			}
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.0175);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.989, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}

	// Secondary text
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// QR code
	async setQrCodeURI(uri, skipRedraw = false) {
		this.qrCodeURI = uri;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Background
	async setBackgroundSource(url, skipRedraw = false) {
		this.backgroundSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#ffeda5";
				this.terciaryTextColor = "#000000";
				this.qrCodeColor = "#000000";
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffeda5";
				this.terciaryTextColor = "#ffffff";
				this.qrCodeColor = "#ffffff";
				
				break;
			case "black-on-white-prerov":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#32948b";
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				this.qrCodeColor = "#000000";
				
				break;
			case "white-on-black-prerov":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#32948b";
				this.backgroundColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				this.qrCodeColor = "#ffffff";
				
				break;
			case "black-on-white-pardubice":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#c83737";
				this.backgroundColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				this.qrCodeColor = "#000000";
				
				break;
			case "white-on-black-pardubice":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#c83737";
				this.backgroundColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				this.qrCodeColor = "#ffffff";
				
				break;
			// We don't have anything for these yet
			case "forum-black-on-white":
				
				
				break;
			case "forum-white-on-purple":
				
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				
				break;
			case "spolecne-s-piraty-black-on-white":
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				
				break;
			case "louny-spolecne-black-on-white":
				
				break;
			case "louny-spolecne-white-on-purple":
				
				break;
			case "litomerice-blue-on-white":
				
				break;
			case "litomerice-white-on-blue":
				
				break;
			case "stranane-gray-on-yellow":
				
				break;
			case "stranane-yellow-on-white":
				
				break;
			case "stranane-white-on-yellow":
				
				break;
			case "ujezd-green-on-white":
				
				break;
			case "ujezd-white-on-green":
				
				break;
			case "cssd-red-on-black":
				
				break;
			case "cssd-black-on-red":
				
				break;
			 case "jilemnice-purple-on-black":
				
				break;
			case "jilemnice-black-on-purple":
				
				break;
			case "novarole-white-on-green":
				
				break;
			case "novarole-green-on-white":
				
				break;
			case "novarole-green-on-black":
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.terciaryTextColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
