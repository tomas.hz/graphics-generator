class PosterCenterSloganIconsNoImage extends Template {
	description = "Není určeno pro tisk na plakáty.";
	
	changeableAttributes = [
		"logoImage",
		"primaryText",
		"primaryColorScheme",
		"secondaryText",
		"qrCode",
		"fourIconSet"
	];

	defaultResolution = 4430;
	aspectRatio = 0.67720090293;
	
	iconSet = [
		{
			"source": "",
			"text": ""
		},
		{
			"source": "",
			"text": ""
		},
		{
			"source": "",
			"text": ""
		},
		{
			"source": "",
			"text": ""
		}
	];
	
	qrCodeUri = "";
	
	secondaryText = "";

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];

	primaryTextHighlightColorSchemes = [
		"black-on-gold"
	];
	
	changeableColors = [
		"primaryTextColor",
		"primaryTextHighlightColor",
		"secondaryTextColor",
		"secondaryTextBackgroundColor",
		"foregroundColor",
		"iconColor",
		"requesterTextColor"
	];

	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const primaryTextOffsetTop = Math.ceil(this.canvas.height * 0.175);
		const primaryTextOffsetSides = Math.ceil(this.canvas.width * 0.1);
		
		const highlightPaddingSides = Math.ceil(this.canvas.width * 0.01);
		const highlightPaddingBottom = Math.ceil(this.canvas.height * 0.0125);
		let highlightPaddingTop = 0;
		
		const logoHeight = Math.ceil(this.canvas.height * 0.05) * this.logoImageZoom;
		const logoBottomOffset = Math.ceil(this.canvas.height * 0.062) - (
			logoHeight / this.logoImageZoom * (this.logoImageZoom - 1) / 2
		);
		
		const qrCodeWidth = Math.ceil(this.canvas.width * 0.125);
		const qrCodeTopOffset = Math.ceil(this.canvas.height * 0.065);
		const qrCodeSideOffset = Math.ceil(this.canvas.height * 0.1);
		
		let secondaryTextFontSize = Math.ceil(this.canvas.height * 0.03);
		const secondaryTextOffsetTop = Math.ceil(this.canvas.height * 0.0725);
		const secondaryTextPaddingInner = Math.ceil(this.canvas.height * 0.015);
		const secondaryTextBackgroundPaddingRight = Math.ceil(this.canvas.width * 0.1);
		const secondaryTextBackgroundPaddingLeft = Math.ceil(this.canvas.width * 0.15);
		const secondaryTextMaxWidth =  Math.ceil(this.canvas.width * 0.35);
		
		const separatorHeight = Math.ceil(this.canvas.height * 0.002);
		const separatorOffsetTopBottom = Math.ceil(this.canvas.height * 0.0225);
		
		const iconMaxWidth = Math.ceil(this.canvas.width * 0.06);
		const iconHeight = Math.ceil(this.canvas.width * 0.05);
		let iconTextFontSize = Math.ceil(this.canvas.height * 0.0165);
		const iconTextMaxWidth = Math.ceil(this.canvas.width * 0.125);
		const iconPaddingBetween = Math.ceil(this.canvas.width * 0.008);
		const iconPaddingBeforeText = Math.ceil(this.canvas.width * 0.007);
		const iconTextMaxLines = 2;
		
		const primaryFontMaxSize = Math.ceil(this.canvas.height * 0.2);
		let primaryFontLinePadding = primaryFontMaxSize * -0.075;
		const primaryTextMaxLines = 3;
		
		this.context.textBaseline = "alphabetic";
		
		const primaryFontMaxTotalSizes = {
			1: Math.ceil(this.canvas.height * 0.35),
			2: Math.ceil(this.canvas.height * 0.625),
			3: Math.ceil(this.canvas.height * 0.625)
		};
		
		// Get primary text split into lines, no more than ``primaryTextMaxLines`` of them
		
		let primaryTextLines = splitStringIntoLines(
			this.context,
			this.primaryText,
			Infinity,
			primaryTextMaxLines
		);
		
		let primaryFontSizeOffset = 0;
		
		while (
			primaryTextLines.length * (
				primaryFontMaxSize
				+ primaryFontLinePadding
				+ primaryFontSizeOffset
			)
			< primaryFontMaxTotalSizes[primaryTextLines.length]
		) {
			primaryFontSizeOffset += 2;

			primaryFontLinePadding = (primaryFontMaxSize + primaryFontSizeOffset) * -0.075;
		}
		
		while (
			primaryTextLines.length * (
				primaryFontMaxSize
				+ primaryFontLinePadding
				+ primaryFontSizeOffset
			)
			> primaryFontMaxTotalSizes[primaryTextLines.length]
		) {
			primaryFontSizeOffset -= 2;

			primaryFontLinePadding = (primaryFontMaxSize + primaryFontSizeOffset) * -0.075;
		}
		
		// Clear the canvas
		this.context.fillStyle = this.foregroundColor;
		this.context.fillRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		if (this.secondaryText !== "") {
			this.context.textAlign = "left";
			
			this.context.fillStyle = this.secondaryTextBackgroundColor;
			this.context.font = `${secondaryTextFontSize}px 'Roboto Condensed'`;
			
			while (this.context.measureText(this.secondaryText).width > secondaryTextMaxWidth) {
				secondaryTextFontSize -= 2;
				
				this.context.font = `${secondaryTextFontSize}px 'Roboto Condensed'`;
			}
			
			this.context.beginPath();
			
			this.context.moveTo(
				0,
				secondaryTextOffsetTop
			);
			
			this.context.lineTo(
				(
					secondaryTextBackgroundPaddingLeft
					+ this.context.measureText(this.secondaryText).width
					+ secondaryTextBackgroundPaddingRight
				),
				secondaryTextOffsetTop
			);
			
			this.context.lineTo(
				(
					secondaryTextBackgroundPaddingLeft
					+ this.context.measureText(this.secondaryText).width
					+ secondaryTextBackgroundPaddingRight
					- (
						secondaryTextFontSize
						+ 2 * secondaryTextPaddingInner
					) / 2
				),
				(
					secondaryTextOffsetTop
					+ (
						secondaryTextFontSize
						+ 2 * secondaryTextPaddingInner
					) / 2
				)
			);
			
			this.context.lineTo(
				(
					secondaryTextBackgroundPaddingLeft
					+ this.context.measureText(this.secondaryText).width
					+ secondaryTextBackgroundPaddingRight
				),
				(
					secondaryTextOffsetTop
					+ (
						secondaryTextFontSize
						+ 2 * secondaryTextPaddingInner
					)
				)
			);
			
			this.context.lineTo(
				0,
				(
					secondaryTextOffsetTop
					+ (
						secondaryTextFontSize
						+ 2 * secondaryTextPaddingInner
					)
				)
			);
			
			this.context.closePath();
			
			this.context.fill();
			
			this.context.fillStyle = this.secondaryTextColor;
			
			this.context.fillText(
				this.secondaryText,
				secondaryTextBackgroundPaddingLeft,
				(
					secondaryTextOffsetTop
					+ secondaryTextPaddingInner / 1.25  // :P
					+ secondaryTextFontSize
				)
			);
		}
		
		if (this.qrCodeURI !== "") {
			const qrCodeImage = new Image();
			
			const code = new QRCode(
				qrCodeImage,
				{
					"text": this.qrCodeURI,
					"width": qrCodeWidth,
					"height": qrCodeWidth,
					"colorDark": this.primaryTextColor,
					"colorLight": this.foregroundColor,
					"correctLevel": QRCode.CorrectLevel.H
				}
			);
			
			this.context.drawImage(
				code._oDrawing._elCanvas, // Ugly, but the fastest way to get the canvas
				this.canvas.width - qrCodeWidth - qrCodeSideOffset, qrCodeTopOffset,
				qrCodeWidth, qrCodeWidth
			);
		}
		
		const firstPrimaryLine = primaryTextLines[primaryTextLines.length - 1].join(" ");
		
		// Create primary text
		this.context.textAlign = "left";
		
		const foregroundRGB = hexToRgb(this.foregroundColor);
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		)
		
		const useLightHighlightAndUseDarkLogoAndIcon = (foregroundLightness > 207);
		
		const primaryLineX = this.canvas.width / 2;
		let currentPrimaryLineY = (
			primaryTextOffsetTop
			+ primaryFontMaxSize
			+ primaryFontSizeOffset
		);
		
		let primaryTextHighlightedColor = null;
		
		const lowercasePrimaryTextHighlightColor = this.primaryTextHighlightColor.toLowerCase();
		const hasColorOverride = (
			lowercasePrimaryTextHighlightColor === "#209a37" ||
			lowercasePrimaryTextHighlightColor === "#e63812"
		);
		
		if (hasColorOverride) {
			if (useLightHighlightAndUseDarkLogoAndIcon) {
				primaryTextHighlightedColor = this.foregroundColor;
			} else {
				primaryTextHighlightedColor = this.primaryTextColor;
			}
		} else if (!useLightHighlightAndUseDarkLogoAndIcon) {
			primaryTextHighlightedColor = this.foregroundColor;
		} else {
			primaryTextHighlightedColor = this.primaryTextColor;
		}
		
		this.context.fillStyle = this.primaryTextColor;
		
		for (let line of primaryTextLines) {
			let currentFontSize = primaryFontMaxSize;
			
			this.context.font = `${this.primaryFontStyle} ${currentFontSize + primaryFontSizeOffset}px ${this.primaryFont}`;
			
			while (
				this.context.measureText(line.join(" ")).width
				> this.canvas.width - 2 * primaryTextOffsetSides
			) {
				currentFontSize -= 2;
				currentPrimaryLineY -= 2;
				
				primaryFontLinePadding = (currentFontSize * -0.075);
				
				this.context.font = `${this.primaryFontStyle} ${currentFontSize + primaryFontSizeOffset}px ${this.primaryFont}`;
			}
			
			let wordPosition = 0;
			
			for (let word of line) {
				const previousWords = line.slice(0, wordPosition).join(" ");
				const previousWordsWidth = this.context.measureText(
					previousWords
					+ (
						(previousWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				const nextWords = line.slice(wordPosition + 1, line.length).join(" ")
				const nextWordsWidth = this.context.measureText(
					nextWords
					+ (
						(nextWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				let currentWordWidth = this.context.measureText(word).width;
				
				for (const word of line.slice(wordPosition + 1, line.length)) {
					if (word.isHighlighted) {
						currentWordWidth += this.context.measureText(word.toString() + " ").width;
					} else {
						break;
					}
				}
				
				if (word.isHighlighted) {
					if (
						wordPosition === 0 ||
						!line[wordPosition - 1].isHighlighted
					) {
						const startingHighlightLineX = (
							this.canvas.width / 2
							+ Math.max(previousWordsWidth / 2)
							- Math.max(nextWordsWidth / 2)
							- Math.max(this.context.measureText(word).width / 2)
						);
						
						this.context.fillStyle = this.primaryTextHighlightColor;
						
						this.context.fillRect(
							(
								startingHighlightLineX
								- highlightPaddingSides
							), (
								currentPrimaryLineY
							),
							(
								highlightPaddingSides * 2
								+ currentWordWidth
							), (
								highlightPaddingBottom
								- currentFontSize
								- primaryFontSizeOffset
								+ highlightPaddingTop
							)
						);
					}
					
					this.context.fillStyle = primaryTextHighlightedColor;
				}
				
				this.context.fillText(
					word + " ",
					(
						primaryLineX
						+ Math.ceil(previousWordsWidth / 2)
						- Math.ceil(nextWordsWidth / 2)
						- Math.ceil(this.context.measureText(word).width / 2)
					),
					currentPrimaryLineY + primaryFontLinePadding
				);
				
				wordPosition++;
				
				this.context.fillStyle = this.primaryTextColor;
			}
			
			currentPrimaryLineY += primaryFontMaxSize + primaryFontSizeOffset + primaryFontLinePadding;
		}
		
		this.context.textAlign = "center";
		
		currentPrimaryLineY -= primaryFontMaxSize + primaryFontSizeOffset - separatorOffsetTopBottom + primaryFontLinePadding;
		
		this.context.fillRect(
			primaryTextOffsetSides, currentPrimaryLineY,
			this.canvas.width - 2 * primaryTextOffsetSides, separatorHeight
		);
		
		currentPrimaryLineY += separatorOffsetTopBottom
		
		let classRef = this; // FIXME...? Surely there's a better way to do this.
		
		// Create icons
		let iconSetRealLength = 0;
		
		for (let icon of this.iconSet) {
			if (icon.source !== "" && icon.text !== "") {
				iconSetRealLength += 1;
			}
		}
		
		if (iconSetRealLength !== 0) {
			let currentIconX = (
				primaryTextOffsetSides
				+ (
					(4 - iconSetRealLength)
					* (
						iconMaxWidth
						+ iconTextMaxWidth
						+ iconPaddingBetween
					) / 1.8 // :P
				)
			);
			
			this.context.fillStyle = this.iconColor;
			this.context.textAlign = "left";
			this.context.textBaseline = "bottom";
			
			for (let icon of this.iconSet) {
				if (icon.source === "" || icon.text === "") {
					continue;
				}
				
				const primaryTextRGB = hexToRgb(classRef.primaryTextColor);
				
				const iconImageLoadPromise = new Promise(
					resolve => {
						const iconImage = new Image();
						let centeredLineX = currentIconX;
						
						iconImage.onload = function() {
							let currentIconHeight = iconHeight;
							let iconWidth = this.width * (currentIconHeight / this.height);
							
							while (iconWidth > iconMaxWidth) {
								currentIconHeight -= 2;
								
								iconWidth = this.width * (currentIconHeight / this.height);
							}
							
							if (iconWidth < iconMaxWidth) {
								centeredLineX = (currentIconX + (iconMaxWidth - iconWidth) / 2);
							}
							
							classRef.context.drawImage(
								colorizeImage(
									this,
									iconWidth,
									currentIconHeight,
									primaryTextRGB.r,
									primaryTextRGB.g,
									primaryTextRGB.b
								),
								centeredLineX, currentPrimaryLineY,
								iconWidth, currentIconHeight
							);
							
							currentIconX += iconMaxWidth + iconPaddingBeforeText;
							
							resolve();
						}
						
						iconImage.src = icon.source;
					}
				);
				
				await iconImageLoadPromise;
				
				let currentLineY = currentPrimaryLineY + iconTextFontSize;
				
				this.context.font = `${iconTextFontSize}px 'Roboto Condensed'`;
				
				let iconLines = splitStringIntoLines(
					this.context,
					icon.text,
					iconTextMaxWidth,
					iconTextMaxLines
				);
				
				while (iconLines.length > iconTextMaxLines) {
					iconTextFontSize -= 2;
					this.context.font = `${iconTextFontSize}px 'Roboto Condensed'`;
					
					iconLines = splitStringIntoLines(
						this.context,
						icon.text,
						iconTextMaxWidth,
						iconTextMaxLines
					);
				}
				
				for (let line of iconLines) {
					this.context.fillText(
						line.join(" "),
						currentIconX,
						currentLineY
					);
					
					currentLineY += iconTextFontSize;
				}

				currentIconX += iconTextMaxWidth + iconPaddingBetween;
			}
		}
		
		// Create logo
		function drawLogoImage(image) {
			const logoWidth = Math.ceil(image.width * (logoHeight / image.height));
			
 			classRef.context.drawImage(
 				image,
 				(classRef.canvas.width - logoWidth)/2, (
					classRef.canvas.height
					- logoBottomOffset
					- logoHeight
 				),
 				logoWidth, logoHeight
 			);
		}
		
		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogoImage(this);
						
						resolve();
					}
					
					if (!useLightHighlightAndUseDarkLogoAndIcon) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogoImage(this.logoImage);
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.986
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.986
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.993, -this.canvas.width * 0.993 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#000000";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor ="#000000";
				this.secondaryTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "forum-black-on-white":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#962a51";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "forum-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#962a51";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#962a51";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#00ad43";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#00ad43";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#00ad43";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.primaryTextColor = "#21274e";
				this.iconColor = "#21274e";
				this.foregroundColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#21274e";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#21274e";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#9796ca";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "louny-spolecne-black-on-white":
				this.primaryTextColor = "#3e2a5b";
				this.iconColor = "#3e2a5b";
				this.foregroundColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#3e2a5b";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "louny-spolecne-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#3e2a5b";
				this.secondaryTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#e2d7a9";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "litomerice-blue-on-white":
				this.primaryTextColor = "#123172";
				this.iconColor = "#123172";
				this.foregroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#9796ca";
				
				this.setPrimaryTextHighlightColorScheme("litomerice", true);
				
				break;
			case "litomerice-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#123172";
				this.secondaryTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("litomerice", true);
				
				break;
			case "stranane-gray-on-yellow":
				this.primaryTextColor = "#4d4d4d";
				this.iconColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#4d4d4d";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "stranane-yellow-on-white":
				this.primaryTextColor = "#4d4d4d";
				this.iconColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#4d4d4d";
				this.primaryTextHighlightColor = "#4d4d4d";
				
				break;
			case "stranane-white-on-yellow":
				this.primaryTextColor = "#4d4d4d";
				this.iconColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#4d4d4d";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-black-on-yellow":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#000000";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-yellow-on-white":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#000000";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#000000";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "ujezd-green-on-white":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#8ed4a3";
				this.secondaryTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#ffdd55";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "ujezd-white-on-green":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#8ed4a3";
				this.secondaryTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#ffdd55";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "cssd-red-on-black":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#e63812";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#000000";
				
				break;
			case "cssd-black-on-red":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#e63812";
				
				break;
			case "jilemnice-purple-on-black":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#6e1646";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#000000";
				
				break;
			case "jilemnice-black-on-purple":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#6e1646";
				
				break;
			case "novarole-white-on-green":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#a9ce2d";
				this.primaryTextHighlightColor = "#a9ce2d";
				
				break;
			case "novarole-green-on-white":
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.secondaryTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#ffffff";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-black":
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#a9ce2d";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				this.secondaryTextBackgroundColor = "#fde119";
				this.secondaryTextColor = "#000000";
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.primaryTextColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setPrimaryTextHighlightColorScheme(scheme, skipRedraw = false) {
		switch(scheme) {
			case "gold":
				this.primaryTextHighlightColor = "#ffcc00";
				break;
			case "litomerice":
				this.primaryTextHighlightColor = "#afe87e";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Colors
	async setPrimaryTextHighlightColor(color, skipRedraw = false) {
		this.primaryTextHighlightColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async seticonColor(color, skipRedraw = false) {
		this.iconColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// QR Code
	async setQrCodeURI(uri, skipRedraw = false) {
		this.qrCodeURI = uri;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Text
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Icons
	async setIconSet(iconSet, skipRedraw = false) {
		if (iconSet.length > 4) {
			throw new Error("Too many icons.");
		}
		
		this.iconSet = iconSet;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
