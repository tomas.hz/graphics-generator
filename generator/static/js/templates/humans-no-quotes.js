class HumansNoQuote extends Template {
	description = "Určeno pro sociální sítě.";
	
	changeableAttributes = [
		"logoImage",
		"primaryImage",
		"nameText",
		"secondaryText",
		"primaryImagePosition",
		"primaryColorScheme"
	];

	changeableColors = [
		"primaryTextColor",
		"primaryTextHighlightColor",
		"foregroundColor",
		"nameTextColor",
		"requesterTextColor"
	];

	secondaryText = "";

	primaryTextColor = "#ffffff";
	primaryTextHighlightColor = "#ffcc00";
	foregroundColor = "#000000";
	nameTextColor = "#ffffff";
	requesterTextColor = "#000000";
	
	primaryColorSchemes = [
		"white-on-black"
	];
	
	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		// Constants
		// Logo
		const logoWidth = Math.ceil(this.canvas.width * 0.21) * this.logoImageZoom;
		const logoOffsetSide = Math.ceil(this.canvas.height * 0.055) * ((3 - this.logoImageZoom) / 2);
		const logoInnerWidthMultiplier = 0.65;
		
		// Primary text
		let primaryTextMaxLines = 4;
		let primaryTextFontSize = Math.ceil(this.canvas.height * 0.086);
		const primaryTextMaxWidth = Math.ceil(this.canvas.width * 0.65);
		const primaryTextOffsetBottom = Math.ceil(this.canvas.height * 0.13);
		const primaryTextOffsetLeft = Math.ceil(this.canvas.width * 0.28);
		const primaryTextHighlightPaddingSides = Math.ceil(this.canvas.width * 0.01);
		const primaryTextHighlightPaddingTop = Math.ceil(this.canvas.height * -0.005);
		const primaryTextHighlightPaddingBottom = Math.ceil(this.canvas.height * 0.01);
		
		// Name text
		let nameTextFontSize = Math.ceil(this.canvas.height * 0.027);
		const nameTextOffsetBottom = primaryTextOffsetBottom * 1.15;
		
		// Primary rectangle
		const primaryRectangleAngle = Math.ceil(this.canvas.height * 0.0125);
		const primaryRectanglePaddingLeft = Math.ceil(this.canvas.width * 0.06);
		const primaryRectanglePaddingRight = Math.ceil(this.canvas.width * 0.08);
		const primaryRectanglePaddingTop = Math.ceil(this.canvas.height * 0.025);
		const primaryRectanglePaddingBottom = Math.ceil(this.canvas.height * 0.02);
		
		// Clear the canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		const classRef = this;
		
		// Set image
		if (this.primaryImage !== null) {
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D

			const imageScaleX = this.canvas.width / this.primaryImage.width;
			const imageScaleY = this.canvas.height / this.primaryImage.height;

			const imageScale = Math.max(imageScaleX, imageScaleY) * this.primaryImageZoom;
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - this.primaryImage.width * imageScale) / 2 + this.primaryImageX * this.primaryImageZoom,
				(this.canvas.height - this.primaryImage.height * imageScale) / 2 + this.primaryImageY * this.primaryImageZoom,
			);
			this.context.drawImage(
				this.primaryImage,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
		}
		
		// Create frame
		const frameImageLoadPromise = new Promise(
			resolve => {
				const frameImage = new Image();
				
				frameImage.onload = function() {
					classRef.context.drawImage(
						this,
						0, 0,
						classRef.canvas.width, classRef.canvas.height
					);
					
					resolve();
				}
				
				frameImage.src = "static/images/humans-frame.png";
			}
		);
		
		await frameImageLoadPromise;
		
		// Create logo
		const logoMainLoadPromise = new Promise(
			resolve => {
				const logoBackgroundImage = new Image();
				
				logoBackgroundImage.onload = function() {
					const logoBackgroundImageHeight = (this.height * (logoWidth / this.width));
					
					classRef.context.drawImage(
						this,
						logoOffsetSide, 0,
						logoWidth, logoBackgroundImageHeight
					);
					
					function drawLogoImage(image) {
						const logoInnerHeight = (image.height * (logoWidth / image.width)) * logoInnerWidthMultiplier;
						
						classRef.context.drawImage(
							image,
							logoOffsetSide + (
								logoWidth
								- (logoWidth * logoInnerWidthMultiplier)
							) / 2, (
								logoBackgroundImageHeight * 0.86 // Ignore the bottom bit. Should be correct within 2 precision points
								- logoInnerHeight
							) / 2,
							logoWidth * logoInnerWidthMultiplier, logoInnerHeight
						);
					}
					
					if (classRef.logoImage === null) {
						let logoImage = new Image();
						
						logoImage.onload = function() {
							drawLogoImage(this);
							
							resolve();
						}
						
						// The back will always be light
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						drawLogoImage(classRef.logoImage);
					}
					
					resolve();
				}
				
				logoBackgroundImage.src = "static/images/humans_flag_bg.png";
			}
		);
		
		await logoMainLoadPromise;
		
		// Create primary text
		if (this.nameText !== "") {
			let primaryTextLines = null;
			const originalPrimaryFontSize = primaryTextFontSize;
			let maximumActualPrimaryTextWidth = 0;
			
			do {
				this.context.font = `${primaryTextFontSize}px 'Bebas Neue'`;
				
				primaryTextLines = splitStringIntoLines(
					this.context,
					this.nameText,
					primaryTextMaxWidth,
					primaryTextMaxLines,
					true
				)
				
				if (
					primaryTextLines.length > primaryTextMaxLines
					&& (
						primaryTextLines.length * primaryTextFontSize
						> primaryTextMaxLines * originalPrimaryFontSize
					)
				) {
					primaryTextFontSize -= 2;
				}
				
				for (const line of primaryTextLines) {
					const lineWidth = this.context.measureText(line.join(" ")).width;
					
					if (lineWidth > maximumActualPrimaryTextWidth) {
						maximumActualPrimaryTextWidth = lineWidth;
					}
				}
			} while (
				primaryTextLines.length > primaryTextMaxLines
				&& (
					primaryTextLines.length * primaryTextFontSize
					> primaryTextMaxLines * originalPrimaryFontSize
				)
			);
			
			// Create primary rectangle
			this.context.fillStyle = this.foregroundColor;
			this.context.beginPath();
			
			this.context.moveTo(
				this.canvas.width, (
					this.canvas.height
					- nameTextOffsetBottom
					+ primaryRectanglePaddingBottom
				)
			);
			this.context.lineTo(
				(
					primaryTextOffsetLeft
					+ (
						primaryTextMaxWidth
						- maximumActualPrimaryTextWidth
					)
					- primaryRectanglePaddingLeft
					- primaryRectanglePaddingRight
				), (
					this.canvas.height
					- nameTextOffsetBottom
					+ primaryRectanglePaddingBottom
					+ primaryRectangleAngle
				)
			);
			this.context.lineTo(
				(
					primaryTextOffsetLeft
					+ (
						primaryTextMaxWidth
						- maximumActualPrimaryTextWidth
					)
					- primaryRectanglePaddingLeft
					- primaryRectanglePaddingRight
				), (
					this.canvas.height
					- primaryTextOffsetBottom
					- (primaryTextLines.length + 1) * primaryTextFontSize
					- primaryRectanglePaddingTop
				)
			);
			this.context.lineTo(
				this.canvas.width, (
					this.canvas.height
					- primaryTextOffsetBottom
					- (primaryTextLines.length + 1) * primaryTextFontSize
					- primaryRectanglePaddingTop
					- primaryRectangleAngle
				)
			);
			
			this.context.closePath();
			this.context.fill();
			
			// Continue with text
			this.context.fillStyle = this.primaryTextColor;
			let currentPrimaryLineY = this.canvas.height - primaryTextOffsetBottom - primaryTextFontSize;
			
			const primaryLineX = primaryTextOffsetLeft + (primaryTextMaxWidth - maximumActualPrimaryTextWidth) - primaryRectanglePaddingRight;
			
			const foregroundRGB = hexToRgb(this.foregroundColor);
			const foregroundLightness = (
				0.2126 * foregroundRGB.r
				+ 0.7152 * foregroundRGB.g
				+ 0.0722 * foregroundRGB.b
			);
			
			const useLightHighlight = (foregroundLightness > 207);
			let previousWordHighlighted = false;
			let primaryTextHighlightedColor = null;
			const primaryRectangleWidth = (
				maximumActualPrimaryTextWidth
				- primaryRectanglePaddingLeft
				- primaryRectanglePaddingRight
			);
			
			const lowercasePrimaryTextHighlightColor = this.primaryTextHighlightColor.toLowerCase();
			const hasColorOverride = (
				lowercasePrimaryTextHighlightColor === "#209a37" ||
				lowercasePrimaryTextHighlightColor === "#e63812"
			);
		
			if (hasColorOverride) {
				if (useLightHighlight) {
					primaryTextHighlightedColor = this.foregroundColor;
				} else {
					primaryTextHighlightedColor = this.primaryTextColor;
				}
			} else if (!useLightHighlight) {
				primaryTextHighlightedColor = this.foregroundColor;
			} else {
				primaryTextHighlightedColor = this.primaryTextColor;
			}
			
			this.context.fillStyle = this.primaryTextColor;
			
			let linePosition = 0;
			
			for (const line of primaryTextLines.reverse()) {
				linePosition++;
				previousWordHighlighted = false;
				let wordPosition = 0;
				
				for (let word of line) {
					const spaceWidth = this.context.measureText(" ").width;
					
					const previousWords = line.slice(0, wordPosition).join(" ");
					const previousWordsWidth = (
						this.context.measureText(previousWords).width
						+ (
							(previousWords.length !== 0) ?
							spaceWidth : 0
						)
					);
					
					const currentWordWidth = this.context.measureText(word).width;
					
					if (word.isHighlighted) {
						this.context.fillStyle = this.primaryTextHighlightColor;
						this.context.beginPath();
						
						this.context.moveTo(
							primaryLineX + previousWordsWidth - (
								(previousWordHighlighted) ?
								spaceWidth : primaryTextHighlightPaddingSides
							),
							currentPrimaryLineY - primaryTextFontSize - primaryTextHighlightPaddingTop
						);
						this.context.lineTo(
							primaryLineX + previousWordsWidth + currentWordWidth + primaryTextHighlightPaddingSides,
							(
								currentPrimaryLineY
								- primaryTextFontSize
								- Math.max(currentWordWidth / primaryRectangleWidth)
								- primaryTextHighlightPaddingTop
							)
						);
						this.context.lineTo(
							primaryLineX + previousWordsWidth + currentWordWidth + primaryTextHighlightPaddingSides,
							currentPrimaryLineY + primaryTextHighlightPaddingBottom
						);
						this.context.lineTo(
							primaryLineX + previousWordsWidth - (
								(previousWordHighlighted) ?
								spaceWidth : primaryTextHighlightPaddingSides
							), 
							currentPrimaryLineY + primaryTextHighlightPaddingBottom + Math.max(
								currentWordWidth / primaryRectangleWidth
							)
						);
						
						this.context.fill();
						
						this.context.fillStyle = primaryTextHighlightedColor;
						
						previousWordHighlighted = true;
					} else {
						previousWordHighlighted = false;
					}
					
					this.context.fillText(
						word + " ",
						(
							primaryLineX
							+ previousWordsWidth
						),
						currentPrimaryLineY
					);

					wordPosition++;
					
					this.context.fillStyle = this.primaryTextColor;
				}
				
				currentPrimaryLineY -= primaryTextFontSize;
			}
			
			// Create secondary text
			if (this.secondaryText !== "") {
				this.context.fillStyle = this.primaryTextHighlightColor;
				this.context.textAlign = "left";
				
				this.context.font = `${nameTextFontSize}px 'Roboto Condensed'`;
				
				while (
					(
						this.context.measureText(this.secondaryText).width
						> maximumActualPrimaryTextWidth
					) && nameTextFontSize >= 1
				) {
					nameTextFontSize -= 2;
					this.context.font = `${nameTextFontSize}px 'Roboto Condensed'`;
				}
				
				this.context.textBaseline = "middle";
				this.context.fillStyle = this.nameTextColor;
				
				this.context.fillText(
					this.secondaryText,
					(
						primaryTextOffsetLeft
						+ (
							primaryTextMaxWidth
							- maximumActualPrimaryTextWidth
						)
						- primaryRectanglePaddingRight
					),
					this.canvas.height - nameTextFontSize - nameTextOffsetBottom
				);
				
				this.context.textBaseline = "alphabetic";
			}
		}
		
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.0175);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	setPrimaryColorScheme(scheme, skipRedraw = false) {
		// TODO
	}
}
