class StoryWithRectangleTemplate extends Template {
	description = "Určeno pro story na sociálních sítích.";
	
	changeableAttributes = [
		"logoImage",
		"primaryImage",
		"primaryText",
		"secondaryText",
		"nameText",
		"primaryColorScheme",
		"nameColorScheme",
		"secondaryColorScheme",
		"primaryImagePosition"
	];

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];

	secondaryColorSchemes = [
		"black-on-gray",
		"white-on-black",
		"black-on-white",
		"black-on-blue"
	];
	
	nameColorSchemes = [
		"white-on-black",
		"black-on-white",
		"black-on-blue",
		"black-on-gray"
	];

	primaryTextHighlightColorSchemes = [
		"black-on-gold"
	];
	
	changeableColors = [
		"primaryTextColor",
		"foregroundColor",
		"backgroundColor",
		"nameBackgroundColor",
		"nameTextColor",
		"secondaryTextBackgroundColor",
		"secondaryTextColor",
		"primaryTextHighlightColor",
		"requesterTextColor"
	];
	
	lightLogoDefaultSource = "/static/images/icon_light.png";
	darkLogoDefaultSource = "/static/images/icon_dark.png";
	
	aspectRatio = 0.5625;
	defaultResolution = 1920;

	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const primaryRectangleWidth = Math.ceil(this.canvas.width * 0.8);
		const primaryRectangleAngle = Math.ceil(this.canvas.height * 0.01);
		const primaryRectanglePosition = Math.ceil(this.canvas.height * 0.78);
		const primaryRectanglePaddingTopWithName = Math.ceil(this.canvas.height * 0.0275);
		const primaryRectanglePaddingTopWithoutName = Math.ceil(this.canvas.height * 0.02);
		const primaryRectangleAdditionalPaddingWithDiacritics = Math.ceil(this.canvas.height * 0.01);
		const primaryRectanglePaddingBottomWithSecondaryText = Math.ceil(this.canvas.height * 0.0275);
		const primaryRectanglePaddingBottomWithoutSecondaryText = Math.ceil(this.canvas.height * 0.02);
		const primaryRectanglePaddingSides = Math.ceil(this.canvas.width * 0.04);

		const highlightPaddingSides = Math.ceil(this.canvas.width * 0.01);
		const highlightPaddingBottom = Math.ceil(this.canvas.height * 0.0125);
		const highlightPaddingTop = Math.ceil(this.canvas.height * -0.01); // It is, it's how Roboto works.

		const nameRectangleOffsetX = Math.ceil(this.canvas.width * 0.02);
		const nameRectangleOffsetY = Math.ceil(this.canvas.height * 0.01);
		const nameRectanglePaddingTopBottom = Math.ceil(this.canvas.height * 0.01);
		const nameRectanglePaddingSides = Math.ceil(this.canvas.width * 0.032);
		
		const secondaryTextRectangleOffsetX = Math.ceil(this.canvas.width * 0.02);
		const secondaryTextRectangleOffsetY = Math.ceil(this.canvas.height * 0.01);
		const secondaryTextRectanglePaddingTopBottom = Math.ceil(this.canvas.height * 0.01);
		const secondaryTextRectanglePaddingSides = Math.ceil(this.canvas.width * 0.032);
		
		const logoHeight = Math.ceil(this.canvas.height * 0.07) * this.logoImageZoom;
		const logoBottomOffset = Math.ceil(this.canvas.height * 0.1) - (
			logoHeight / this.logoImageZoom * (this.logoImageZoom - 1) / 2
		);
		
		let primaryFontSize = Math.ceil(this.canvas.height * 0.09);
		const primaryFontLinePadding = 0;
		const primaryTextMaxLines = 3;
		
		const nameFontSize = Math.ceil(this.canvas.height * 0.025);
		const secondaryFontSize = Math.ceil(this.canvas.height * 0.025);

		// Get primary text split into lines, no more than ``primaryTextMaxLines`` of them
		
		let primaryTextLines = null;
		
		do {
			this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;

			primaryTextLines = splitStringIntoLines(
				this.context,
				this.primaryText,
				primaryRectangleWidth - primaryRectanglePaddingSides * 2,
				primaryTextMaxLines,
				true
			).reverse();

			if (primaryTextLines.length > primaryTextMaxLines) {
				primaryFontSize -= 2;
			}
		} while (primaryTextLines.length > primaryTextMaxLines);

		// Clear the canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Set image
		if (this.primaryImage !== null) {
			
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D

			const imageScaleX = this.canvas.width / this.primaryImage.width;
			const imageScaleY = this.canvas.height / this.primaryImage.height;

			const imageScale = Math.max(imageScaleX, imageScaleY) * this.primaryImageZoom;
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - this.primaryImage.width * imageScale) / 2 + this.primaryImageX * this.primaryImageZoom,
				(this.canvas.height - this.primaryImage.height * imageScale) / 2 + this.primaryImageY * this.primaryImageZoom
			);
			this.context.drawImage(
				this.primaryImage,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
		}
		
		// Change bottom rectangle width based on the text height
		const secondaryRectangleHeight = (
			(
				(primaryTextLines.length * (primaryFontSize + primaryFontLinePadding))
				< this.canvas.height / 5.25
			) ?
			Math.ceil(this.canvas.height / 3.75) : Math.ceil(this.canvas.height / 2.457)
		);
		
		// Fill bottom rectangle
		this.context.fillStyle = this.backgroundColor;
		this.context.fillRect(
			0, this.canvas.height - secondaryRectangleHeight,
			this.canvas.width, secondaryRectangleHeight
		);
		
		const firstPrimaryLine = primaryTextLines[primaryTextLines.length - 1].join(" ");
		
		// Create rectangle behind the primary text
		const primaryRectangleHeight = (
			primaryTextLines.length * (primaryFontSize + primaryFontLinePadding)
			+ (
				(this.nameText !== "") ?
				primaryRectanglePaddingTopWithName : primaryRectanglePaddingTopWithoutName
			)
			+ (
				(this.secondaryText !== "") ?
				primaryRectanglePaddingBottomWithSecondaryText: primaryRectanglePaddingBottomWithoutSecondaryText
			)
			+ (
				(
					firstPrimaryLine.replace(/[a-zA-Z0-9À-ž]+/g, "").length
					!== firstPrimaryLine.replace(/[a-zA-Z0-9]+/g, "").length
				) ?
				primaryRectangleAdditionalPaddingWithDiacritics :
				0
			)
		);
		
		const foregroundRGB = hexToRgb(this.foregroundColor);
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		)
		
		this.context.beginPath();
		
		const primaryRectangleStartingX = (this.canvas.width - primaryRectangleWidth) / 2;
		const primaryRectangleEndingX = primaryRectangleWidth + primaryRectangleStartingX
		
		this.context.fillStyle = this.foregroundColor;
		
		this.context.moveTo(
			primaryRectangleStartingX,
			primaryRectanglePosition + primaryRectangleAngle
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePosition
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePosition - primaryRectangleHeight
		);
		this.context.lineTo(
			primaryRectangleStartingX,
			primaryRectanglePosition + primaryRectangleAngle - primaryRectangleHeight
		);
		
		this.context.closePath();
		
		this.context.fill();
		
		// Create primary text
		this.context.textAlign = "left";
		
		const useLightHighlight = (foregroundLightness > 207);
		
		const primaryLineX = this.canvas.width / 2;
		let currentPrimaryLineY = (
			primaryRectanglePosition
			- (
				(this.secondaryText !== "") ?
				primaryRectanglePaddingBottomWithSecondaryText: primaryRectanglePaddingBottomWithoutSecondaryText
			)
			- primaryFontLinePadding
		);
		
		let primaryTextHighlightedColor = null;
		
		const lowercasePrimaryTextHighlightColor = this.primaryTextHighlightColor.toLowerCase();
		const hasColorOverride = (
			lowercasePrimaryTextHighlightColor === "#209a37" ||
			lowercasePrimaryTextHighlightColor === "#e63812"
		);
		
		if (hasColorOverride) {
			if (useLightHighlight) {
				primaryTextHighlightedColor = this.foregroundColor;
			} else {
				primaryTextHighlightedColor = this.primaryTextColor;
			}
		} else if (!useLightHighlight) {
			primaryTextHighlightedColor = this.foregroundColor;
		} else {
			primaryTextHighlightedColor = this.primaryTextColor;
		}
		
		this.context.fillStyle = this.primaryTextColor;

		for (let line of primaryTextLines) {
			let wordPosition = 0;
			
			for (let word of line) {
				const previousWords = line.slice(0, wordPosition).join(" ");
				const previousWordsWidth = this.context.measureText(
					previousWords
					+ (
						(previousWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				const nextWords = line.slice(wordPosition + 1, line.length).join(" ")
				const nextWordsWidth = this.context.measureText(
					nextWords
					+ (
						(nextWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				let currentWordWidth = this.context.measureText(word).width;
				
				for (const word of line.slice(wordPosition + 1, line.length)) {
					if (word.isHighlighted) {
						currentWordWidth += this.context.measureText(word.toString() + " ").width;
					} else {
						break;
					}
				}
				
				if (word.isHighlighted) {
					if (
						wordPosition === 0 ||
						!line[wordPosition - 1].isHighlighted
					) {
						this.context.fillStyle = this.primaryTextHighlightColor;
						this.context.beginPath();

						const startingHighlightLineX = (
							primaryLineX
							+ Math.max(previousWordsWidth / 2)
							- Math.max(nextWordsWidth / 2)
							- Math.max(this.context.measureText(word).width / 2)
						);

						this.context.moveTo(
							startingHighlightLineX - highlightPaddingSides,
							currentPrimaryLineY + highlightPaddingBottom
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								+ highlightPaddingBottom
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ primaryRectangleWidth
								)
							)
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ primaryRectangleWidth
								)
							)
						);
						this.context.lineTo(
							startingHighlightLineX - highlightPaddingSides,
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
							)
						);
						
						this.context.closePath();
						
						this.context.fill();
					}
					
					this.context.fillStyle = primaryTextHighlightedColor;
				}

				this.context.fillText(
					word + " ",
					(
						primaryLineX
						+ Math.ceil(previousWordsWidth / 2)
						- Math.ceil(nextWordsWidth / 2)
						- Math.ceil(this.context.measureText(word).width / 2)
					),
					currentPrimaryLineY
				);
				
				wordPosition++;
				
				this.context.fillStyle = this.primaryTextColor;
			}

			currentPrimaryLineY -= (primaryFontSize + primaryFontLinePadding);
		}
		
		// Create name, if not empty

		if (this.nameText !== "") {
			// Create rectangle for name text
			this.context.font = `${nameFontSize}px 'Roboto Condensed'`;
			this.context.textAlign = "center";
			
			const nameRectangleStartingX = (
				primaryRectangleStartingX
				+ nameRectangleOffsetX
			);
			const nameRectangleStartingY = (
				primaryRectanglePosition
				- primaryRectangleHeight
				- nameRectangleOffsetY
			);
			
			const nameRectangleTextWidth = this.context.measureText(this.nameText).width;
			
			this.context.fillStyle = this.nameBackgroundColor;
			this.context.fillRect(
				nameRectangleStartingX, nameRectangleStartingY,
				nameRectangleTextWidth + nameRectanglePaddingSides * 2, nameFontSize + nameRectanglePaddingTopBottom * 2
			);
			
			// Create name text itself
			this.context.fillStyle = this.nameTextColor;
			this.context.fillText(
				this.nameText,
				nameRectangleStartingX + nameRectanglePaddingSides + Math.ceil(nameRectangleTextWidth / 2),
				nameRectangleStartingY + nameFontSize + nameRectanglePaddingTopBottom / 2
			);
		}
		
		if (this.secondaryText !== "") {
			// Create rectangle for name text
			this.context.font = `${secondaryFontSize}px 'Roboto Condensed'`;
			this.context.textAlign = "center";
			
			const secondaryTextWidth = this.context.measureText(this.secondaryText).width;
			
			const secondaryTextRectangleStartingX = (
				primaryRectangleEndingX
				- secondaryTextWidth
				- secondaryTextRectangleOffsetX
				- secondaryTextRectanglePaddingSides * 2
			);
			const secondaryTextRectangleStartingY = (
				primaryRectanglePosition
				- secondaryTextRectangleOffsetY
			);
			
			this.context.fillStyle = this.secondaryTextBackgroundColor;
			this.context.fillRect(
				secondaryTextRectangleStartingX, secondaryTextRectangleStartingY,
				secondaryTextWidth + secondaryTextRectanglePaddingSides * 2, secondaryFontSize + secondaryTextRectanglePaddingTopBottom * 2
			);
			
			// Create the secondary text itself
			this.context.fillStyle = this.secondaryTextColor;
			this.context.fillText(
				this.secondaryText,
				secondaryTextRectangleStartingX + secondaryTextRectanglePaddingSides + Math.ceil(secondaryTextWidth / 2),
				secondaryTextRectangleStartingY + secondaryFontSize + secondaryTextRectanglePaddingTopBottom / 2
			);
		}

		let classRef = this; // FIXME...? Surely there's a better way to do this.

		// Create logo
		// See if we're using the light or dark version
		const backgroundRGB = hexToRgb(this.backgroundColor);
		const backgroundLightness = (
			0.2126 * backgroundRGB.r
			+ 0.7152 * backgroundRGB.g
			+ 0.0722 * backgroundRGB.b
		)

		const useLightLogo = (backgroundLightness < 207);

		function drawLogoImage(image) {
			const logoWidth = Math.ceil(image.width * (logoHeight / image.height));

			classRef.context.drawImage(
				image,
				(classRef.canvas.width - logoWidth)/2, classRef.canvas.height - logoHeight - logoBottomOffset,
				logoWidth, logoHeight
			);
		}

		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogoImage(this);
						
						resolve();
					}
					
					if (useLightLogo) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogoImage(this.logoImage);
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.0175);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> secondaryRectangleHeight - (this.canvas.height * 0.03)
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> secondaryRectangleHeight - (this.canvas.height * 0.03)
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.989, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}

	// Secondary text
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#000000";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("white-on-black", true);
				this.setSecondaryTextColorScheme("black-on-gray", true);
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor ="#000000";
				this.backgroundColor ="#ffffff";
				
				this.setNameColorScheme("black-on-white", true);
				this.setSecondaryTextColorScheme("black-on-gray", true);
				
				break;
			case "forum-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#962a51";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("black-on-white", true);
				this.setSecondaryTextColorScheme("white-on-black", true);
				
				break;
			case "forum-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#962a51";
				this.backgroundColor = "#ffffff";
				
				this.setNameColorScheme("white-on-black", true);
				this.setSecondaryTextColorScheme("white-on-black", true);
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#00ad43";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("black-on-most", true);
				this.setSecondaryTextColorScheme("black-on-most", true);
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#00ad43";
				this.backgroundColor = "#ffffff";
				
				this.setNameColorScheme("black-on-most", true);
				this.setSecondaryTextColorScheme("black-on-most", true);
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#21274e";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("pirati-spolecne", true);
				this.setSecondaryTextColorScheme("pirati-spolecne", true);
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#21274e";
				this.backgroundColor = "#ffffff";
				
				this.setNameColorScheme("pirati-spolecne", true);
				this.setSecondaryTextColorScheme("pirati-spolecne", true);
				
				break;
			case "louny-spolecne-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#3e2a5b";
				
				this.nameBackgroundColor = "#e2d7a9";
				this.nameTextColor = "#000000";
				
				this.secondaryTextBackgroundColor = "#e2d7a9";
				this.secondaryTextColor = "#000000";
				
				this.primaryTextHighlightColor = "#e2d7a9";
				
				break;
			case "louny-spolecne-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#3e2a5b";
				this.backgroundColor = "#ffffff";
				
				this.nameBackgroundColor = "#e2d7a9";
				this.nameTextColor = "#000000";
				
				this.secondaryTextBackgroundColor = "#e2d7a9";
				this.secondaryTextColor = "#000000";
				
				this.primaryTextHighlightColor = "#e2d7a9";
				
				break;
			case "litomerice-blue-on-white":
				this.primaryTextColor = "#123172";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#123172";
				
				this.setPrimaryTextHighlightColorScheme("litomerice", true);
				this.setNameColorScheme("blue-on-litomerice", true);
				this.setSecondaryTextColorScheme("blue-on-litomerice", true);
				
				break;
			case "litomerice-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#123172";
				this.backgroundColor = "#ffffff";
				
				this.setNameColorScheme("blue-on-litomerice", true);
				this.setSecondaryTextColorScheme("blue-on-litomerice", true);
				
				break;
			case "stranane-gray-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#4d4d4d";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#50c450";
				this.nameTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#ffffff";
				this.secondaryTextColor = "#4d4d4d";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "stranane-yellow-on-white":
				this.primaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#50c450";
				this.nameTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#4d4d4d";
				this.secondaryTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#4d4d4d";
				
				break;
			case "stranane-white-on-yellow":
				this.primaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#50c450";
				this.nameTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#4d4d4d";
				this.secondaryTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-black-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#50c450";
				this.nameTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
// 				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "prusanky-yellow-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#50c450";
				this.nameTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#50c450";
				this.nameTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "ujezd-green-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#8ed4a3";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffdd55";
				this.nameTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "ujezd-white-on-green":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#8ed4a3";
				this.nameBackgroundColor = "#ffdd55";
				this.nameTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "cssd-red-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#e63812";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "cssd-black-on-red":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#e63812";
				this.backgroundColor = "#000000";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			 case "jilemnice-purple-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#6e1646";
				this.backgroundColor = "#000000";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "jilemnice-black-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#6e1646";
				this.backgroundColor = "#000000";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				
				break;
			case "novarole-white-on-green":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#a9ce2d";
				this.nameBackgroundColor = "#a9ce2d";
				this.nameTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#a9ce2d";
				
				break;
			case "novarole-green-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.secondaryTextBackgroundColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-black":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#000000";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.secondaryTextBackgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				this.primaryTextHighlightColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				this.setPrimaryColorScheme("white-on-black");
				
				this.nameBackgroundColor = "#fde119";
				this.nameTextColor = "#000000";
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.foregroundColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setNameColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-blue":
				this.nameBackgroundColor = "#18c1df";
				this.nameTextColor = "#000000";
				break;
			case "black-on-gray":
				this.nameBackgroundColor = "#999999";
				this.nameTextColor = "#000000";
				break;
			case "black-on-white":
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				break;
			case "white-on-black":
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				break;
			case "pirati-spolecne":
				this.nameBackgroundColor = "#9796ca";
				this.nameTextColor = "#000000";
				break;
			case "black-on-most":
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				break;
			case "blue-on-litomerice":
				this.nameBackgroundColor = "#cccccc";
				this.nameTextColor = "#123172";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setSecondaryTextColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-gray":
				this.secondaryTextBackgroundColor = "#999999";
				this.secondaryTextColor = "#000000";
				break;
			case "black-on-blue":
				this.secondaryTextBackgroundColor = "#18c1df";
				this.secondaryTextColor = "#000000";
				break;
			case "black-on-white":
				this.secondaryTextBackgroundColor = "#ffffff";
				this.secondaryTextColor = "#000000";
				break;
			case "white-on-black":
				this.secondaryTextBackgroundColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				break;
			case "pirati-spolecne":
				this.secondaryTextBackgroundColor = "#9796ca";
				this.secondaryTextColor = "#000000";
				break;
			case "black-on-most":
				this.secondaryTextBackgroundColor = "#000000";
				this.secondaryTextColor = "#ffffff";
				break;
			case "blue-on-litomerice":
				this.secondaryTextBackgroundColor = "#cccccc";
				this.secondaryTextColor = "#123172";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setPrimaryTextHighlightColorScheme(scheme, skipRedraw = false) {
		switch(scheme) {
			case "gold":
				this.primaryTextHighlightColor = "#ffcc00";
				break;
			case "litomerice":
				this.primaryTextHighlightColor = "#afe87e";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Colors
	async setSecondaryTextBackgroundColor(color, skipRedraw = false) {
		this.secondaryTextBackgroundColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setSecondaryTextColor(color, skipRedraw = false) {
		this.secondaryTextColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setNameBackgroundColor(color, skipRedraw = false) {
		this.nameBackgroundColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setNameTextColor(color, skipRedraw = false) {
		this.nameTextColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setPrimaryTextHighlightColor(color, skipRedraw = false) {
		this.primaryTextHighlightColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
