class Poll extends Template {
	description = "Určeno pro ankety na Fejsbůčku.";
	
	changeableAttributes = [
		"logoImage",
		"primaryImage",
		"locationImage",
		"primaryText",
		"primaryColorScheme",
		"primaryImagePosition",
		"twoReactionSet"
	];

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];
	
	changeableColors = [
		"primaryTextColor",
		"foregroundColor",
		"backgroundColor",
		"primaryTextHighlightColor",
		"reactionTextColor",
		"requesterTextColor"
	];
	
	reactions = [
		{
			source: "",
			text: ""
		},
		{
			source: "",
			text: ""
		}
	];
	locationSource = null;

	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const primaryRectangleWidth = Math.ceil(this.canvas.width * 0.9);
		const primaryRectangleAngle = Math.ceil(this.canvas.height * 0.01);
		const primaryRectanglePosition = Math.ceil(this.canvas.height * 0.71);
		const primaryRectanglePaddingTop = Math.ceil(this.canvas.height * 0.02);
		const primaryRectangleAdditionalPaddingWithDiacritics = Math.ceil(this.canvas.height * 0.01);
		const primaryRectanglePaddingBottom = Math.ceil(this.canvas.height * 0.06);
		const primaryRectanglePaddingSides = Math.ceil(this.canvas.width * 0.04);

		const highlightPaddingSides = Math.ceil(this.canvas.width * 0.01);
		const highlightPaddingBottom = Math.ceil(this.canvas.height * 0.0125);
		const highlightPaddingTop = Math.ceil(this.canvas.height * -0.01); // It is, it's how Roboto works.

		const logoHeight = Math.ceil(this.canvas.height * 0.07) * this.logoImageZoom;
		const logoBottomOffset = Math.ceil(this.canvas.height * 0.05) - (
			logoHeight / this.logoImageZoom * (this.logoImageZoom - 1) / 2
		);
		
		let primaryFontSize = Math.ceil(this.canvas.height * 0.09);
		const primaryFontLinePadding = 0;
		const primaryTextMaxLines = 3;
		
		const nameFontSize = Math.ceil(this.canvas.height * 0.03);
		
		const reactionImageOffsetSide = Math.ceil(this.canvas.width * 0.15);
		const reactionImageOffsetTop = Math.ceil(this.canvas.height * -0.02);
		const reactionImagePaddingInner = Math.ceil(this.canvas.width * 0.015);
		const reactionImageWidth = Math.ceil(this.canvas.width * 0.17);
		const reactionTextOffsetSide = Math.ceil(this.canvas.width * 0.015);
		let reactionTextFontSize = Math.ceil(this.canvas.height * 0.06);
		const reactionTextMaxWidth = Math.ceil(this.canvas.width * 0.15);
		
		const locationWidth = Math.ceil(this.canvas.width * 0.250);
		const locationOffsetTop = locationWidth * 0.04;

		// Get primary text split into lines, no more than ``primaryTextMaxLines`` of them
		
		let primaryTextLines = null;
		
		do {
			this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;

			primaryTextLines = splitStringIntoLines(
				this.context,
				this.primaryText,
				primaryRectangleWidth - primaryRectanglePaddingSides,
				primaryTextMaxLines,
				true
			).reverse();

			if (primaryTextLines.length > primaryTextMaxLines) {
				primaryFontSize -= 2;
			}
		} while (primaryTextLines.length > primaryTextMaxLines);

		// Clear the canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Set image
		if (this.primaryImage !== null) {
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D

			const imageScaleX = this.canvas.width / this.primaryImage.width;
			const imageScaleY = this.canvas.height / this.primaryImage.height;

			const imageScale = Math.max(imageScaleX, imageScaleY) * this.primaryImageZoom;
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - this.primaryImage.width * imageScale) / 2 + this.primaryImageX * this.primaryImageZoom,
				(this.canvas.height - this.primaryImage.height * imageScale) / 2 + this.primaryImageY * this.primaryImageZoom,
			);
			this.context.drawImage(
				this.primaryImage,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
		}
		
		// Change bottom rectangle width based on the text height
		const secondaryRectangleHeight = (
			(primaryTextLines.length > 1) ?
			this.canvas.height / 2 :
			this.canvas.height / 2.5
		);
		
		// Fill bottom rectangle
		this.context.fillStyle = this.backgroundColor;
		this.context.fillRect(
			0, this.canvas.height - secondaryRectangleHeight,
			this.canvas.width, secondaryRectangleHeight
		);
		
		const firstPrimaryLine = primaryTextLines[primaryTextLines.length - 1].join(" ");
		
		// Create rectangle behind the primary text
		const primaryRectangleHeight = (
			primaryTextLines.length * (primaryFontSize + primaryFontLinePadding)
			+ primaryRectanglePaddingTop
			+ primaryRectanglePaddingBottom
			+ (
				(
					firstPrimaryLine.replace(/[a-zA-Z0-9À-ž]+/g, "").length
					!== firstPrimaryLine.replace(/[a-zA-Z0-9]+/g, "").length
				) ?
				primaryRectangleAdditionalPaddingWithDiacritics :
				0
			)
		);
		
		const foregroundRGB = hexToRgb(this.foregroundColor);
		
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		)
		
		// Create location image, if there is one
		if (this.locationSource !== null) {
			let locationImage = new Image();
			
			locationImage.onload = function() {
				const locationHeight = Math.ceil(this.height * (locationWidth / this.width));

				const imageStartingY = (
					primaryRectanglePosition
					- primaryRectangleHeight
					- locationHeight
					+ (
						primaryRectangleAngle
						* (locationWidth / primaryRectangleWidth)
						* 1.15 // I don't know if this is imprecise floating point numbers or my broken math.
					)
					+ locationOffsetTop
				)

				let temporaryCanvas = document.createElement("canvas");
				
				temporaryCanvas.width = locationWidth;
				temporaryCanvas.height = locationHeight;
				
				let temporaryContext = temporaryCanvas.getContext("2d");
				
				temporaryContext.drawImage(
					this,
					0, 0,
					locationWidth, locationHeight
				);
				
				let pixels = temporaryContext.getImageData(
					0, 0,
					locationWidth, locationHeight
				);

				for (let pixelPosition = 0; pixelPosition < pixels.data.length; pixelPosition += 4) {
					pixels.data[pixelPosition] = pixels.data[pixelPosition] / 255 * foregroundRGB.r;
					pixels.data[pixelPosition + 1] = pixels.data[pixelPosition + 1] / 255 * foregroundRGB.g;
					pixels.data[pixelPosition + 2] = pixels.data[pixelPosition + 2] / 255 * foregroundRGB.b;
				}
				
				temporaryContext.putImageData(
					pixels,
					0, 0
				);
				
				classRef.context.drawImage(
					temporaryCanvas,
					primaryRectangleEndingX - locationWidth, imageStartingY
				);
			}
			
			locationImage.src = this.locationSource;
		}
		
		this.context.beginPath();
		
		const primaryRectangleStartingX = (this.canvas.width - primaryRectangleWidth) / 2;
		const primaryRectangleEndingX = primaryRectangleWidth + primaryRectangleStartingX
		
		this.context.fillStyle = this.foregroundColor;
		
		this.context.moveTo(
			primaryRectangleStartingX,
			primaryRectanglePosition + primaryRectangleAngle
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePosition
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePosition - primaryRectangleHeight
		);
		this.context.lineTo(
			primaryRectangleStartingX,
			primaryRectanglePosition + primaryRectangleAngle - primaryRectangleHeight
		);
		
		this.context.closePath();
		
		this.context.fill();
		
		// Create primary text
		this.context.textAlign = "left";
		
		const useLightHighlight = (foregroundLightness > 207);
		
		const primaryLineX = this.canvas.width / 2;
		let currentPrimaryLineY = (
			primaryRectanglePosition
			- primaryRectanglePaddingBottom
			- primaryFontLinePadding
		);
		
		let primaryTextHighlightedColor = null;
		
		const lowercasePrimaryTextHighlightColor = this.primaryTextHighlightColor.toLowerCase();
		const hasColorOverride = (
			lowercasePrimaryTextHighlightColor === "#209a37" ||
			lowercasePrimaryTextHighlightColor === "#e63812"
		);
		
		if (hasColorOverride) {
			if (useLightHighlight) {
				primaryTextHighlightedColor = this.foregroundColor;
			} else {
				primaryTextHighlightedColor = this.primaryTextColor;
			}
		} else if (!useLightHighlight) {
			primaryTextHighlightedColor = this.foregroundColor;
		} else {
			primaryTextHighlightedColor = this.primaryTextColor;
		}
		
		this.context.fillStyle = this.primaryTextColor;

		for (let line of primaryTextLines) {
			let wordPosition = 0;
			
			for (let word of line) {
				const previousWords = line.slice(0, wordPosition).join(" ");
				const previousWordsWidth = this.context.measureText(
					previousWords
					+ (
						(previousWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				const nextWords = line.slice(wordPosition + 1, line.length).join(" ")
				const nextWordsWidth = this.context.measureText(
					nextWords
					+ (
						(nextWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				let currentWordWidth = this.context.measureText(word).width;
				
				for (const word of line.slice(wordPosition + 1, line.length)) {
					if (word.isHighlighted) {
						currentWordWidth += this.context.measureText(word.toString() + " ").width;
					} else {
						break;
					}
				}
				
				if (word.isHighlighted) {
					if (
						wordPosition === 0 ||
						!line[wordPosition - 1].isHighlighted
					) {
						this.context.fillStyle = this.primaryTextHighlightColor;
						this.context.beginPath();

						const startingHighlightLineX = (
							primaryLineX
							+ Math.ceil(previousWordsWidth / 2)
							- Math.ceil(nextWordsWidth / 2)
							- Math.ceil(this.context.measureText(word).width / 2)
						);

						this.context.moveTo(
							startingHighlightLineX - highlightPaddingSides,
							currentPrimaryLineY + highlightPaddingBottom
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								+ highlightPaddingBottom
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ primaryRectangleWidth
								)
							)
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ primaryRectangleWidth
								)
							)
						);
						this.context.lineTo(
							startingHighlightLineX - highlightPaddingSides,
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
							)
						);
						
						this.context.closePath();
						
						this.context.fill();
					}
					
					this.context.fillStyle = primaryTextHighlightedColor;
				}
				
				this.context.fillText(
					word + " ",
					(
						primaryLineX
						+ Math.ceil(previousWordsWidth / 2)
						- Math.ceil(nextWordsWidth / 2)
						- Math.ceil(this.context.measureText(word).width / 2)
					),
					currentPrimaryLineY
				);
				
				wordPosition++;
				
				this.context.fillStyle = this.primaryTextColor;
			}
			
			currentPrimaryLineY -= (primaryFontSize + primaryFontLinePadding);
		}
		
		let classRef = this; // FIXME...? Surely there's a better way to do this.

		// Create logo
		// See if we're using the light or dark version
		const backgroundRGB = hexToRgb(this.backgroundColor);
		const backgroundLightness = (
			0.2126 * backgroundRGB.r
			+ 0.7152 * backgroundRGB.g
			+ 0.0722 * backgroundRGB.b
		)

		const useLightLogoAndIcon = (backgroundLightness < 207);

		function drawLogoImage(image) {
			let logoWidth = Math.ceil(image.width * (logoHeight / image.height));
			let logoX = (classRef.canvas.width - logoWidth)/2;

			classRef.context.drawImage(
				image,
				logoX, classRef.canvas.height - logoHeight - logoBottomOffset,
				logoWidth, logoHeight
			);
		}

		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogoImage(this);
						
						resolve();
					}
					
					if (useLightLogoAndIcon) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogoImage(this.logoImage);
		}

		if (
			this.reactions[0].source !== "" &&
			this.reactions[0].text !== "" &&
			this.reactions[1].source !== "" &&
			this.reactions[1].text !== ""
		) {
			this.context.beginPath();
			this.context.fillStyle = this.backgroundColor;

			this.context.arc(
				reactionImageOffsetSide + (reactionImageWidth / 2),
				primaryRectanglePosition + (reactionImageWidth / 2) + reactionImageOffsetTop,
				reactionImageWidth / 2,
				0,
				2 * Math.PI,
				false
			);
			
			this.context.arc(
				this.canvas.width - reactionImageOffsetSide - (reactionImageWidth / 2),
				primaryRectanglePosition + (reactionImageWidth / 2) + reactionImageOffsetTop,
				reactionImageWidth / 2,
				0,
				2 * Math.PI,
				false
			);
			
			this.context.closePath();
			
			this.context.fill();
			
			const firstReactionImage = new Image();
			
			firstReactionImage.onload = function() {
				classRef.context.drawImage(
					this,
					(
						reactionImageOffsetSide
						+ (reactionImagePaddingInner / 2)
					), (
						primaryRectanglePosition
						+ reactionImageOffsetTop
						+ (reactionImagePaddingInner / 2)
					),
					(
						reactionImageWidth
						- reactionImagePaddingInner
					), (
						reactionImageWidth
						- reactionImagePaddingInner
					)
				);
			}
			
			firstReactionImage.src = this.reactions[0].source;
			
			const secondReactionImage = new Image();
			
			secondReactionImage.onload = function() {
				classRef.context.drawImage(
					this,
					(
						classRef.canvas.width
						- reactionImageWidth
						- reactionImageOffsetSide
						+ (reactionImagePaddingInner / 2)
					), (
						primaryRectanglePosition
						+ reactionImageOffsetTop
						+ (reactionImagePaddingInner / 2)
					),
					(
						reactionImageWidth
						- reactionImagePaddingInner
					), (
						reactionImageWidth
						- reactionImagePaddingInner
					)
				);
			}
			
			secondReactionImage.src = this.reactions[1].source;
			
			const originalReactionFontSize = reactionTextFontSize;
			
			this.context.fillStyle = this.reactionTextColor;
			this.context.textAlign = "left";
			
			let reactionTextMaxLines = 2;
			let reactionLines = null;
			
			do {
				this.context.font = `${this.primaryFontStyle} ${reactionTextFontSize}px ${this.primaryFont}`;
				
				reactionLines = splitStringIntoLines(
					this.context,
					this.reactions[0].text,
					reactionTextMaxWidth,
					reactionTextMaxLines,
					true
				);
				
				if (
					reactionLines.length > reactionTextMaxLines
					&& (
						reactionLines.length * reactionTextFontSize
						> reactionTextMaxLines * originalReactionFontSize
					)
				) {
					reactionTextFontSize -= 2;
				}
			} while (
				reactionLines.length > reactionTextMaxLines
				&& (
					reactionLines.length * reactionTextFontSize
					> reactionTextMaxLines * originalReactionFontSize
				)
			);
			
			let currentReactionLineY = (
				primaryRectanglePosition
				+ reactionImageOffsetTop
				+ (
					reactionImageWidth
					+ reactionTextFontSize
				) / 2
				- reactionTextFontSize * (reactionLines.length - 1) / 2
			);
			
			for (const line of reactionLines) {
				this.context.fillText(
					line.join(" "),
					(
						reactionImageOffsetSide
						+ reactionImageWidth
						+ reactionTextOffsetSide
					),
					currentReactionLineY
				);
				
				currentReactionLineY += reactionTextFontSize;
			}
			
			reactionTextFontSize = originalReactionFontSize;
			
			this.context.textAlign = "right";
			
			reactionTextMaxLines = 2;
			reactionLines = null;
			
			do {
				this.context.font = `${this.primaryFontStyle} ${reactionTextFontSize}px ${this.primaryFont}`;
				
				reactionLines = splitStringIntoLines(
					this.context,
					this.reactions[1].text,
					reactionTextMaxWidth,
					reactionTextMaxLines,
					true
				);
				
				if (
					reactionLines.length > reactionTextMaxLines
					&& (
						reactionLines.length * reactionTextFontSize
						> reactionTextMaxLines * originalReactionFontSize
					)
				) {
					reactionTextFontSize -= 2;
				}
			} while (
				reactionLines.length > reactionTextMaxLines
				&& (
					reactionLines.length * reactionTextFontSize
					> reactionTextMaxLines * originalReactionFontSize
				)
			);
			
			currentReactionLineY = (
				primaryRectanglePosition
				+ reactionImageOffsetTop
				+ (
					reactionImageWidth
					+ reactionTextFontSize
				) / 2
				- reactionTextFontSize * (reactionLines.length - 1) / 2
			);
			
			for (const line of reactionLines) {
				this.context.fillText(
					line.join(" "),
					(
						this.canvas.width
						- reactionImageOffsetSide
						- reactionImageWidth
						- reactionTextOffsetSide
					),
					currentReactionLineY
				);
				
				currentReactionLineY += reactionTextFontSize;
			}
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> secondaryRectangleHeight - (this.canvas.height * 0.03)
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> secondaryRectangleHeight - (this.canvas.height * 0.03)
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.9925 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Icon
	async setIconText(text, skipRedraw = false) {
		this.iconText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setIconSource(url, skipRedraw = false) {
		this.iconSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setIconImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.iconImage = new Image();
					
					classRef.iconImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.iconImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
	
	async resetIconImage(skipRedraw = false) {
		this.iconImage = null;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Location
	async setLocationSource(url, skipRedraw = false) {
		this.locationSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#000000";
				this.reactionTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor ="#000000";
				this.backgroundColor ="#ffffff";
				this.reactionTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "forum-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#962a51";
				this.reactionTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "forum-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#962a51";
				this.backgroundColor = "#ffffff";
				this.reactionTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#00ad43";
				this.reactionTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#00ad43";
				this.backgroundColor = "#ffffff";
				this.reactionTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#21274e";
				this.reactionTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#21274e";
				this.backgroundColor = "#ffffff";
				this.reactionTextColor = "#21274e";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "louny-spolecne-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#3e2a5b";
				this.reactionTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "louny-spolecne-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#3e2a5b";
				this.backgroundColor = "#ffffff";
				this.reactionTextColor = "#3e2a5b";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "litomerice-blue-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#123172";
				this.reactionTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#afe87e";
				
				break;
			case "litomerice-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#123172";
				this.backgroundColor = "#ffffff";
				this.reactionTextColor = "#123172";
				this.primaryTextHighlightColor = "#afe87e";
				
				break;
			case "stranane-gray-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#4d4d4d";
				this.backgroundColor = "#ffd500";
				this.reactionTextColor = "#4d4d4d";
				
				break;
			case "stranane-yellow-on-white":
				this.primaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.reactionTextColor = "#4d4d4d";
				this.primaryTextHighlightColor = "#4d4d4d";
				
				break;
			case "stranane-white-on-yellow":
				this.primaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.reactionTextColor = "#4d4d4d";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-black-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffd500";
				this.reactionTextColor = "#000000";
				
				break;
			case "prusanky-yellow-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.reactionTextColor = "#000000";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.reactionTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "ujezd-green-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#8ed4a3";
				this.backgroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffdd55";
				this.reactionTextColor = "#000000";
				
				break;
			case "ujezd-white-on-green":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#8ed4a3";
				this.reactionTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "cssd-red-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#e63812";
				this.backgroundColor = "#000000";
				this.reactionTextColor = "#ffffff";
				
				break;
			case "cssd-black-on-red":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#e63812";
				this.reactionTextColor = "#ffffff";
				
				break;
			case "jilemnice-purple-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#6e1646";
				this.backgroundColor = "#000000";
				this.reactionTextColor = "#ffffff";
				
				break;
			case "jilemnice-black-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#6e1646";
				this.reactionTextColor = "#ffffff";
				
				break;
			case "novarole-white-on-green":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#a9ce2d";
				this.reactionTextColor = "#000000";
				this.primaryTextHighlightColor = "#a9ce2d";
				
				break;
			case "novarole-green-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#ffffff";
				this.reactionTextColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-black":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#000000";
				this.reactionTextColor = "#ffffff";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.foregroundColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setNameBackgroundColor(color, skipRedraw = false) {
		this.nameBackgroundColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setNameTextColor(color, skipRedraw = false) {
		this.nameTextColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setPrimaryTextHighlightColor(color, skipRedraw = false) {
		this.primaryTextHighlightColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Logo
	async setLogoIsCenter(isCenter, skipRedraw = false) {
		this.logoIsCenter = isCenter;

		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
