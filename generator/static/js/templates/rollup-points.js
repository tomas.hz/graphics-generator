class RollupPoints extends Template {
	description = "Určeno pro rollupy. Vlaječka v terciárním textu se přidává zvýrazněním.";
	
	changeableAttributes = [
		"logoImage",
		"fivePointSet",
		"secondaryText",
		"terciaryText",
		"selectableRollupBackground",
		"qrCode"
	];

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];
	
	changeableColors = [
		"primaryTextBackgroundColor",
		"foregroundColor",
		"primaryTextColor",
		"secondaryTextColor",
		"terciaryTextColor",
		"terciaryTextBackgroundColor",
		"qrCodeColor"
	];
	
	aspectRatio = 0.4;
	defaultResolution = 12015;

	qrCodeURI = "";
	backgroundSource = "";
	
	pointSet = [
		"",
		"",
		"",
		"",
		""
	];
	
	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		let qrSourceFontSize = Math.ceil(this.canvas.height * 0.02);
		const qrCodeWidthHeight = Math.ceil(this.canvas.height * 0.09);
		const qrCodeBottomOffset = Math.ceil(this.canvas.height * 0.15);
		const qrCodeOuterPadding = Math.ceil(this.canvas.height * 0.01);
		
		const pointNumberRectangleWidth = Math.ceil(this.canvas.width * 0.1);
		const pointNumberRectangleHeight = Math.ceil(this.canvas.height * 0.07);
		const pointNumberInnerPadding = Math.ceil(pointNumberRectangleWidth * 0.15);
		
		const pointOffsetTop = Math.ceil(this.canvas.height * 0.035);
		const pointOffsetSide = pointOffsetTop;
		const pointPaddingBetweenX = Math.ceil(this.canvas.width * 0.03);
		const pointOffsetBetweenY = Math.ceil(this.canvas.height * 0.01);
		const pointTextMaxWidth = Math.ceil(this.canvas.width * 0.7);
		const pointTextFontSize = pointNumberRectangleHeight / 2;
		let pointTextMaxLines = 2;
		
		const secondaryTextOffsetBottom = Math.ceil(this.canvas.height * 0.51);
		const secondaryTextMaxWidth = Math.ceil(this.canvas.width * 0.7);
		let secondaryTextFontSize = Math.ceil(this.canvas.height * 0.03);
		
		const terciaryTextOffsetBottom = Math.ceil(this.canvas.height * 0.47);
		const terciaryTextMaxWidth = secondaryTextMaxWidth;
		let terciaryTextFontSize = Math.ceil(this.canvas.height * 0.035);
		let terciaryTextMaxLines = 2;
		
		const flagPaddingTopBottom = Math.ceil(this.canvas.height * 0.005);
		const flagPaddingLeft = Math.ceil(this.canvas.width * 0.04);
		const flagPaddingRight = Math.ceil(this.canvas.width * 0.07);
		
		const classRef = this;
		
		if (this.backgroundSource !== "") {
			const backgroundLoadPromise = new Promise(
				resolve => {
					const backgroundImage = new Image();
					
					backgroundImage.onload = function () {
						classRef.context.drawImage(
							this,
							0, 0,
							classRef.canvas.width, classRef.canvas.height
						);
						
						resolve();
					}
					
					backgroundImage.src = this.backgroundSource;
				}
			);
			
			await backgroundLoadPromise;
		}
		
		if (this.qrCodeURI !== "") {
			const qrCodeImage = new Image();
			
			const code = new QRCode(
				qrCodeImage,
				{
					"text": this.qrCodeURI,
					"width": qrCodeWidthHeight,
					"height": qrCodeWidthHeight,
					"colorDark": this.qrCodeColor,
					"colorLight": this.backgroundColor,
					"correctLevel": QRCode.CorrectLevel.H
				}
			);
			
			this.context.fillStyle = this.backgroundColor;
			
			const qrCodePosition = (this.canvas.width - qrCodeWidthHeight) / 2;
			
			this.context.fillRect(
				qrCodePosition - qrCodeOuterPadding, this.canvas.height - qrCodeBottomOffset - qrCodeWidthHeight - qrCodeOuterPadding,
				qrCodeWidthHeight + qrCodeOuterPadding * 2, qrCodeWidthHeight + qrCodeOuterPadding * 2
			);
			
			this.context.drawImage(
				code._oDrawing._elCanvas, // Ugly, but the fastest way to get the canvas
				(this.canvas.width - qrCodeWidthHeight) / 2, this.canvas.height - qrCodeWidthHeight - qrCodeBottomOffset,
				qrCodeWidthHeight, qrCodeWidthHeight
			);
			
			const maxTextWidth = qrCodeWidthHeight + 2 * qrCodeOuterPadding;
			
			do {
				this.context.font = `${qrSourceFontSize}px ${this.primaryFont}`;
				
				if (this.context.measureText(this.qrCodeURI).width > maxTextWidth) {
					qrSourceFontSize -= 2;
					this.context.font = `${qrSourceFontSize}px ${this.primaryFont}`;
				}
			} while (this.context.measureText(this.qrCodeURI).width > maxTextWidth);
			
			this.context.fillStyle = this.terciaryTextColor;
			this.context.textAlign = "center";
			
			this.context.fillText(
				this.qrCodeURI,
				this.canvas.width / 2,
				this.canvas.height - qrCodeWidthHeight - qrCodeBottomOffset - qrCodeOuterPadding * 2
			);
		}
		
		// Create points
		let pointSetRealLength = 0;
		
		for (const point of this.pointSet) {
			if (point !== "") {
				pointSetRealLength += 1;
			}
		}
		
		if (pointSetRealLength !== 0) {
			let currentPointY = pointOffsetTop;
			
			let pointNumber = 1;
			
			this.context.textAlign = "left";
			this.context.textBaseline = "alphabetic";
			
			for (const point of this.pointSet) {
				if (point === "") {
					continue;
				}
				
				this.context.fillStyle = this.primaryTextBackgroundColor;
				
				this.context.fillRect(
					pointOffsetSide, currentPointY,
					pointNumberRectangleWidth, pointNumberRectangleHeight
				);
				
				this.context.fillStyle = this.foregroundColor;
				this.context.font = `${pointNumberRectangleHeight - pointNumberInnerPadding}px ${this.primaryFont}`;
				
				this.context.fillText(
					pointNumber.toString(),
					(
						pointOffsetSide
						+ pointNumberInnerPadding
					), (
						currentPointY
						+ pointNumberRectangleHeight - pointNumberInnerPadding * 2 // Center numbers, they are never accented
					)
				);
				
				const currentPointY_ = currentPointY;
				let currentPointTextY = currentPointY_;
				
				let pointLines = [];
				
				const originalPointTextHeight = (pointTextFontSize * pointTextMaxLines);
				let currentPointTextFontSize = pointTextFontSize;
				
				do {
					this.context.font = `${this.primaryFontStyle} ${currentPointTextFontSize}px ${this.primaryFont}`;
					
					pointLines = splitStringIntoLines(
						this.context,
						point,
						pointTextMaxWidth,
						pointTextMaxLines,
						true
					);
					
					if (pointLines.length > pointTextMaxLines) {
						currentPointTextFontSize -= 2;
					}
					
					if (((pointTextMaxLines + 1) * currentPointTextFontSize) < originalPointTextHeight) {
						pointTextMaxLines += 1;
					}
				} while (pointLines.length > pointTextMaxLines);
				
				this.context.textBaseline = "ideographic";
				this.context.fillStyle = this.primaryTextColor;
				currentPointTextY += currentPointTextFontSize;
				
				for (const line of pointLines) {
					this.context.fillText(
						line.join(" "),
						pointOffsetSide + pointNumberRectangleWidth + pointPaddingBetweenX,
						currentPointTextY
					);
					
					currentPointTextY += currentPointTextFontSize;
				}
				
				this.context.textBaseline = "alphabetic";
				
				currentPointY += pointNumberRectangleHeight + pointOffsetBetweenY;
				pointNumber++;
			}
		}
		
		if (this.secondaryText !== "") {
			this.context.fillStyle = this.secondaryTextColor;
			
			do {
				this.context.font = `${secondaryTextFontSize}px ${this.primaryFont}`;
				
				if (this.context.measureText(this.secondaryText).width > secondaryTextMaxWidth) {
					secondaryTextFontSize -= 2;
					this.context.font = `${secondaryTextFontSize}px ${this.primaryFont}`;
				}
			} while (this.context.measureText(this.secondaryText).width > secondaryTextMaxWidth);
			
			this.context.textAlign = "center";
			
			this.context.fillText(
				this.secondaryText,
				this.canvas.width / 2, this.canvas.height - secondaryTextOffsetBottom
			);
		}
		
		if (this.terciaryText !== "") {
			this.context.textAlign = "left";
			
			let terciaryLines = [];
			
			const originalTerciaryMaxHeight = (terciaryTextFontSize * terciaryTextMaxLines);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${terciaryTextFontSize}px ${this.primaryFont}`;
				
				terciaryLines = splitStringIntoLines(
					this.context,
					this.terciaryText,
					terciaryTextMaxWidth,
					terciaryTextMaxLines,
					true
				);
				
				if (terciaryLines.length > terciaryTextMaxLines) {
					terciaryTextFontSize -= 2;
				}
				
				if (((terciaryTextMaxLines + 1) * terciaryTextFontSize) < originalTerciaryMaxHeight) {
					terciaryTextMaxLines += 1;
				}
			} while (terciaryLines.length > terciaryTextMaxLines);
			
			let currentTerciaryLineY = (
				this.canvas.height
				- terciaryTextOffsetBottom
				+ terciaryTextFontSize
			);
			
			this.context.fillStyle = this.terciaryTextColor;
			
			for (const line of terciaryLines) {
				let lineWidth = 0;
				
				let wordPosition = 0;
				let usedWordPositions = [];
				
				const flagTextFontSize = (terciaryTextFontSize - flagPaddingTopBottom * 2) * 0.6; // Font stuff
				
				for (const word of line) {
					if (usedWordPositions.includes(wordPosition)) {
						continue;
					}
					
					let wordWidth = this.context.measureText(word).width;
					
					if (word.isHighlighted) {
						this.context.font = `bold ${flagTextFontSize}px 'Roboto Condensed'`;
						
						wordWidth += flagPaddingLeft + flagPaddingRight;
						
						const originalWordPosition = wordPosition;
						let nextWordsHighlighted = false;
						
						while (line.length !== wordPosition + 1 && line[wordPosition + 1].isHighlighted) {
							nextWordsHighlighted = true;
							wordPosition++;
							
							wordWidth += this.context.measureText(" " + line[wordPosition].toString()).width;
							usedWordPositions.push(wordPosition);
						}
						
						if (!nextWordsHighlighted) {
							wordWidth += this.context.measureText(" ").width;
						} else {
							wordPosition = originalWordPosition;
						}
						
						this.context.font = `${this.primaryFontStyle} ${terciaryTextFontSize}px ${this.primaryFont}`;
					} else {
						wordWidth += this.context.measureText(" ").width;
					}
					
					lineWidth += wordWidth;
					wordPosition++;
				}
				
				let currentWordX = (this.canvas.width - lineWidth) / 2;
				
				wordPosition = 0;
				usedWordPositions = [];
				
				for (const word of line) {
					if (usedWordPositions.includes(wordPosition)) {
						continue;
					}
					
					if (!word.isHighlighted) {
						this.context.fillText(
							word + " ",
							currentWordX,
							currentTerciaryLineY
						);
						
						currentWordX += this.context.measureText(word + " ").width;
					} else {
						let currentWord = word;
						const originalWordPosition = wordPosition;
						
						while (line.length !== wordPosition + 1 && line[wordPosition + 1].isHighlighted) {
							wordPosition++;
							
							currentWord += " " + line[wordPosition].toString();
							usedWordPositions.push(wordPosition);
						}
						
						wordPosition = originalWordPosition;
						
						this.context.fillStyle = this.terciaryTextColor;
						
						this.context.font = `bold ${flagTextFontSize}px 'Roboto Condensed'`;
						
						const wordWidth = this.context.measureText(currentWord).width;
						
						this.context.beginPath();
						
						this.context.moveTo(
							currentWordX,
							currentTerciaryLineY
						);
						
						this.context.lineTo(
							(
								currentWordX
								+ flagPaddingLeft
								+ wordWidth
								+ flagPaddingRight
							),
							currentTerciaryLineY
						);
						
						this.context.lineTo(
							(
								currentWordX
								+ flagPaddingLeft
								+ wordWidth
								+ flagPaddingRight
								- (
									flagTextFontSize
									+ 2 * flagPaddingTopBottom
								) / 2
							),
							(
								currentTerciaryLineY
								- (
									flagTextFontSize
									+ 2 * flagPaddingTopBottom
								) / 2
							)
						);
						
						this.context.lineTo(
							(
								currentWordX
								+ flagPaddingLeft
								+ wordWidth
								+ flagPaddingRight
							),
							(
								currentTerciaryLineY
								- (
									flagTextFontSize
									+ 2 * flagPaddingTopBottom
								)
							)
						);
						
						this.context.lineTo(
							currentWordX,
							(
								currentTerciaryLineY
								- (
									flagTextFontSize
									+ 2 * flagPaddingTopBottom
								)
							)
						);
						
						this.context.closePath();
						this.context.fill();
						
						this.context.textBaseline = "alphabetic";
						
						this.context.fillStyle = this.terciaryTextBackgroundColor;
						this.context.fillText(
							currentWord,
							currentWordX + flagPaddingLeft,
							currentTerciaryLineY - flagPaddingTopBottom
						);
						
						this.context.textBaseline = "alphabetic";
						this.context.font = `${this.primaryFontStyle} ${terciaryTextFontSize}px ${this.primaryFont}`;
						
						currentWordX += flagPaddingLeft + wordWidth + flagPaddingRight;
						
						this.context.fillStyle = this.terciaryTextColor;
					}
					
					usedWordPositions.push(wordPosition);
					wordPosition++;
				}
				
				currentTerciaryLineY += terciaryTextFontSize;
			}
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.0175);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.989, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}

	// Secondary text
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Terciary text
	async setTerciaryText(text, skipRedraw = false) {
		this.terciaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// QR code
	async setQrCodeURI(uri, skipRedraw = false) {
		this.qrCodeURI = uri;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Background
	async setBackgroundSource(url, skipRedraw = false) {
		this.backgroundSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextBackgroundColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				this.qrCodeColor = "#ffffff";
				this.backgroundColor = "#000000";
				
				break;
			case "white-on-black":
				this.primaryTextBackgroundColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#000000";
				this.qrCodeColor = "#000000";
				this.backgroundColor = "#ffffff";
				
				break;
			case "black-on-white-prerov":
				this.primaryTextBackgroundColor = "#32948b";
				this.foregroundColor = "#ffffff";
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#32948b";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				this.qrCodeColor = "#ffffff";
				this.backgroundColor = "#000000";
				
				break;
			case "white-on-black-prerov":
				this.primaryTextBackgroundColor = "#32948b";
				this.foregroundColor = "#ffffff";
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#32948b";
				this.terciaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#000000";
				this.qrCodeColor = "#000000";
				this.backgroundColor = "#ffffff";
				
				break;
			case "black-on-white-pardubice":
				this.primaryTextBackgroundColor = "#c83737";
				this.foregroundColor = "#ffffff";
				this.primaryTextColor = "#000000";
				this.secondaryTextColor = "#c83737";
				this.terciaryTextColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				this.qrCodeColor = "#ffffff";
				this.backgroundColor = "#000000";
				
				break;
			case "white-on-black-pardubice":
				this.primaryTextBackgroundColor = "#c83737";
				this.foregroundColor = "#ffffff";
				this.primaryTextColor = "#ffffff";
				this.secondaryTextColor = "#c83737";
				this.terciaryTextColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#000000";
				this.qrCodeColor = "#000000";
				this.backgroundColor = "#ffffff";
				
				break;
			// We don't have anything for these yet
			case "forum-black-on-white":
				
				
				break;
			case "forum-white-on-purple":
				
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				
				break;
			case "spolecne-s-piraty-black-on-white":
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				
				break;
			case "louny-spolecne-black-on-white":
				
				break;
			case "louny-spolecne-white-on-purple":
				
				break;
			case "litomerice-blue-on-white":
				
				break;
			case "litomerice-white-on-blue":
				
				break;
			case "stranane-gray-on-yellow":
				
				break;
			case "stranane-yellow-on-white":
				
				break;
			case "stranane-white-on-yellow":
				
				break;
			case "ujezd-green-on-white":
				
				break;
			case "ujezd-white-on-green":
				
				break;
			case "cssd-red-on-black":
				
				break;
			case "cssd-black-on-red":
				
				break;
			 case "jilemnice-purple-on-black":
				
				break;
			case "jilemnice-black-on-purple":
				
				break;
			case "novarole-white-on-green":
				
				break;
			case "novarole-green-on-white":
				
				break;
			case "novarole-green-on-black":
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.terciaryTextColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
