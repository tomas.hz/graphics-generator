class PosterEvent extends Template {
	description = "Určeno pro tisk na plakáty.";
	
	changeableAttributes = [
		"logoImage",
		"primaryImage",
		"primaryText",
		"dateText",
		"timeText",
		"locationText",
		"primaryColorScheme",
		"primaryImagePosition",
		"secondaryText",
		"terciaryText"
	];
	
	secondaryText = "";
	terciaryText = "";
	dateText = "";
	timeText = "";
	locationText = "";
	
	defaultResolution = 4430;
	aspectRatio = 0.707070707071;
	
	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];
	
	changeableColors = [
		"primaryTextColor",
		"informationTextColor",
		"informationTextBackgroundColor",
		"terciaryTextColor",
		"foregroundColor",
		"backgroundColor",
		"primaryTextHighlightColor",
		"requesterTextColor"
	];

	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;

		const highlightPaddingSides = Math.ceil(this.canvas.width * 0.01);
		const highlightPaddingBottom = Math.ceil(this.canvas.height * 0.0125);
		const highlightPaddingTop = Math.ceil(this.canvas.height * -0.01); // It is, it's how Roboto works.
		
		const logoHeight = Math.ceil(this.canvas.height * 0.05) * this.logoImageZoom;
		const logoBottomOffset = Math.ceil(this.canvas.height * 0.06) - (
			logoHeight / this.logoImageZoom * (this.logoImageZoom - 1) / 2
		);

		let primaryFontSize = Math.ceil(this.canvas.height * 0.1);
		const primaryFontLinePadding = 0;
		const primaryTextMaxLines = 3;
		
		let secondaryFontSize = Math.ceil(this.canvas.height * 0.02);
		const secondaryTextPaddingTop = Math.ceil(this.canvas.height * 0);
		const secondaryTextPaddingBottom = Math.ceil(this.canvas.height * 0.02);
		
		let informationTextFontSize = Math.ceil(this.canvas.height * 0.03);
		let informationIconHeight = Math.ceil(this.canvas.height * 0.023);
		let informationInnerPadding = Math.ceil(this.canvas.width * 0.01);
		const informationInnerOffset = Math.ceil(this.canvas.width * 0.035);
		const informationMaxWidth = Math.ceil(this.canvas.width * 0.7);
		let informationBottomOffset = Math.ceil(this.canvas.height * 0.027);
		const informationRectanglePaddingSides = Math.ceil(this.canvas.height * 0.02);
		const informationRectanglePaddingTopBottom = Math.ceil(this.canvas.height * 0.005);
		const informationRectangleOffsetBottom = Math.ceil(this.canvas.height * -0.02);
		
		let terciaryFontSize = Math.ceil(this.canvas.height * 0.02);
		let terciaryFontLinePadding = Math.ceil(this.canvas.height * 0.006);
		const terciaryTextOffsetBottom = Math.ceil(this.canvas.height * 0.19); // :P
		let terciaryTextMaxLines = 2;
		
		const primaryRectangleWidth = Math.ceil(this.canvas.width * 0.8);
		const primaryRectangleAngle = Math.ceil(this.canvas.height * 0.01);
		const primaryRectanglePosition = Math.ceil(this.canvas.height * 0.825) - (
			(this.terciaryText !== "") ?
			Math.ceil(this.canvas.height * 0.06) : 0
		);
		const primaryRectanglePaddingTop = Math.ceil(this.canvas.height * 0.02);
		const primaryRectanglePaddingBottom = Math.ceil(this.canvas.height * 0.02);
		const primaryRectanglePaddingSides = Math.ceil(this.canvas.width * 0.04);
		
		const terciaryTextMaxWidth = primaryRectangleWidth;
		
		// Get primary text split into lines, no more than ``primaryTextMaxLines`` of them
		
		let primaryTextLines = null;
		
		do {
			this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;

			primaryTextLines = splitStringIntoLines(
				this.context,
				this.primaryText,
				primaryRectangleWidth - primaryRectanglePaddingSides,
				primaryTextMaxLines,
				true
			).reverse();

			if (primaryTextLines.length > primaryTextMaxLines) {
				primaryFontSize -= 2;
			}
		} while (primaryTextLines.length > primaryTextMaxLines);

		// Clear the canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Set image
		if (this.primaryImage !== null) {
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D

			const imageScaleX = this.canvas.width / this.primaryImage.width;
			const imageScaleY = this.canvas.height / this.primaryImage.height;

			const imageScale = Math.max(imageScaleX, imageScaleY) * this.primaryImageZoom;
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - this.primaryImage.width * imageScale) / 2 + this.primaryImageX * this.primaryImageZoom,
				(this.canvas.height - this.primaryImage.height * imageScale) / 2 + this.primaryImageY * this.primaryImageZoom,
			);
			this.context.drawImage(
				this.primaryImage,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
		}
		
		// Change bottom rectangle width based on the text height
		const secondaryRectangleHeight = (
			(
				(primaryTextLines.length * (primaryFontSize + primaryFontLinePadding))
				< this.canvas.height / 4
			) ?
			Math.ceil(this.canvas.height / 3.6) : Math.ceil(this.canvas.height / 2.5)
		);
		
		// Fill bottom rectangle
		this.context.fillStyle = this.backgroundColor;
		this.context.fillRect(
			0, this.canvas.height - secondaryRectangleHeight,
			this.canvas.width, secondaryRectangleHeight
		);
		
		const firstPrimaryLine = primaryTextLines[primaryTextLines.length - 1].join(" ");
		
		// Create rectangle behind the primary text
		const primaryRectangleHeight = (
			primaryTextLines.length * (primaryFontSize + primaryFontLinePadding)
			+ primaryRectanglePaddingTop
			+ primaryRectanglePaddingBottom
			+ (
				(this.secondaryText !== "") ?
				(secondaryFontSize + secondaryTextPaddingTop + secondaryTextPaddingBottom) :
				0
			)
		);
		
		const foregroundRGB = hexToRgb(this.foregroundColor);
		
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		)
		
		this.context.beginPath();
		
		const primaryRectangleStartingX = (this.canvas.width - primaryRectangleWidth) / 2;
		const primaryRectangleEndingX = primaryRectangleWidth + primaryRectangleStartingX
		
		this.context.fillStyle = this.foregroundColor;
		
		this.context.moveTo(
			primaryRectangleStartingX,
			primaryRectanglePosition + primaryRectangleAngle
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePosition
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePosition - primaryRectangleHeight
		);
		this.context.lineTo(
			primaryRectangleStartingX,
			primaryRectanglePosition + primaryRectangleAngle - primaryRectangleHeight
		);
		
		this.context.closePath();
		
		this.context.fill();
		
		// Create primary text
		this.context.textAlign = "left";
		
		const useLightHighlight = (foregroundLightness > 207);
		
		const primaryLineX = this.canvas.width / 2;
		let currentPrimaryLineY = (
			primaryRectanglePosition
			- primaryRectanglePaddingBottom
			- primaryFontLinePadding
			- (
				(this.secondaryText !== "") ?
				(secondaryFontSize + secondaryTextPaddingBottom + secondaryTextPaddingTop) :
				0
			)
		);
		
		let primaryTextHighlightedColor = null;
		
		const lowercasePrimaryTextHighlightColor = this.primaryTextHighlightColor.toLowerCase();
		const hasColorOverride = (
			lowercasePrimaryTextHighlightColor === "#209a37" ||
			lowercasePrimaryTextHighlightColor === "#e63812"
		);
		
		if (hasColorOverride) {
			if (useLightHighlight) {
				primaryTextHighlightedColor = this.foregroundColor;
			} else {
				primaryTextHighlightedColor = this.primaryTextColor;
			}
		} else if (!useLightHighlight) {
			primaryTextHighlightedColor = this.foregroundColor;
		} else {
			primaryTextHighlightedColor = this.primaryTextColor;
		}
		
		this.context.fillStyle = this.primaryTextColor;

		for (let line of primaryTextLines) {
			let wordPosition = 0;
			
			for (let word of line) {
				const previousWords = line.slice(0, wordPosition).join(" ");
				const previousWordsWidth = this.context.measureText(
					previousWords
					+ (
						(previousWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				const nextWords = line.slice(wordPosition + 1, line.length).join(" ")
				const nextWordsWidth = this.context.measureText(
					nextWords
					+ (
						(nextWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				let currentWordWidth = this.context.measureText(word).width;
				
				for (const word of line.slice(wordPosition + 1, line.length)) {
					if (word.isHighlighted) {
						currentWordWidth += this.context.measureText(word.toString() + " ").width;
					} else {
						break;
					}
				}
				
				if (word.isHighlighted) {
					if (
						wordPosition === 0 ||
						!line[wordPosition - 1].isHighlighted
					) {
						this.context.fillStyle = this.primaryTextHighlightColor;
						this.context.beginPath();

						const startingHighlightLineX = (
							primaryLineX
							+ Math.max(previousWordsWidth / 2)
							- Math.max(nextWordsWidth / 2)
							- Math.max(this.context.measureText(word).width / 2)
						);

						this.context.moveTo(
							startingHighlightLineX - highlightPaddingSides,
							currentPrimaryLineY + highlightPaddingBottom
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								+ highlightPaddingBottom
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ primaryRectangleWidth
								)
							)
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ primaryRectangleWidth
								)
							)
						);
						this.context.lineTo(
							startingHighlightLineX - highlightPaddingSides,
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
							)
						);
						
						this.context.closePath();
						
						this.context.fill();
					}
					
					this.context.fillStyle = primaryTextHighlightedColor;
				}

				this.context.fillText(
					word + " ",
					(
						primaryLineX
						+ Math.ceil(previousWordsWidth / 2)
						- Math.ceil(nextWordsWidth / 2)
						- Math.ceil(this.context.measureText(word).width / 2)
					),
					currentPrimaryLineY
				);
				
				wordPosition++;
				
				this.context.fillStyle = this.primaryTextColor;
			}
			
			currentPrimaryLineY -= (primaryFontSize + primaryFontLinePadding);
		}
		
		this.context.textAlign = "center";
		
		// Create secondary text, if there is any
		
		if (this.secondaryText !== "") {
			this.context.font = `${secondaryFontSize}px 'Roboto Condensed'`;
			
			while (this.context.measureText(this.secondaryText).width > primaryRectangleWidth - 2 * primaryRectanglePaddingSides) {
				secondaryFontSize -= 2;
				
				this.context.font = `${secondaryFontSize}px 'Roboto Condensed'`;
			}
			
			this.context.fillStyle = this.primaryTextColor;
			
			this.context.fillText(
				this.secondaryText,
				this.canvas.width / 2,
				primaryRectanglePosition - secondaryTextPaddingBottom
			);
		}
		
		let classRef = this; // FIXME...? Surely there's a better way to do this.
		
		if (this.terciaryText !== "") {
			let helperCanvas = document.createElement("canvas");
			
			let terciaryLines = [];
			
			const originalTerciaryHeight = (terciaryFontSize + terciaryFontLinePadding) * terciaryTextMaxLines;
			
			do {
				this.context.font = `${terciaryFontSize}px 'Roboto Condensed'`;
				
				terciaryLines = splitStringIntoLines(
					this.context,
					this.terciaryText,
					terciaryTextMaxWidth,
					terciaryTextMaxLines,
					true
				);
				
				if (terciaryLines.length > terciaryTextMaxLines) {
					terciaryFontSize -= 2;
					terciaryFontLinePadding -= 2;
				}
				
				if (((terciaryTextMaxLines + 1) * (terciaryFontSize + terciaryFontLinePadding)) < originalTerciaryHeight) {
					terciaryTextMaxLines += 1;
				}
			} while (terciaryLines.length > terciaryTextMaxLines);
			
			let longestLineWidth = 0;
			
			for (let line of terciaryLines) {
				const length = this.context.measureText(line.join(" ")).width;
				
				if (length > longestLineWidth) {
					longestLineWidth = length;
				}
			}
			
			let totalWidth = Math.ceil(longestLineWidth);
			
			let helperContext = null;
			
			helperCanvas.height = (terciaryFontSize + terciaryFontLinePadding) * (terciaryLines.length + 1);
			helperCanvas.width = totalWidth;
			
			helperContext = helperCanvas.getContext("2d");
			helperContext.font = `${terciaryFontSize}px 'Roboto Condensed'`;
			helperContext.fillStyle = this.terciaryTextColor;
			
			let currentHelperLineY = terciaryFontSize;
			
			for (let line of terciaryLines) {
				helperContext.fillText(
					line.join(" "),
					0,
					currentHelperLineY
				);
				
				currentHelperLineY += terciaryFontSize + terciaryFontLinePadding;
			}
			
			this.context.drawImage(
				helperCanvas,
				(this.canvas.width - helperCanvas.width) / 2, this.canvas.height - terciaryTextOffsetBottom
			);
		}
		
		if (
			this.dateText !== "" ||
			this.timeText !== "" ||
			this.locationText !== ""
		) {
			let helperCanvas = document.createElement("canvas");
			
			let contentWidth = 0;
			
			let dateIcon = null;
			let dateIconWidth = 0;
			let dateTextWidth = 0;
			
			let timeIcon = null;
			let timeIconWidth = 0;
			let timeTextWidth = 0;
			
			let locationIcon = null;
			let locationIconWidth = 0;
			let locationTextWidth = 0;
			
			do {
				contentWidth = 0;

				this.context.font = `${this.primaryFontStyle} ${informationTextFontSize}px ${this.primaryFont}`;
				
				if (this.dateText !== "") {
					dateTextWidth = this.context.measureText(this.dateText).width;
					
					contentWidth += (
						dateTextWidth
						+ informationInnerPadding
					);
					
					let dateIconLoadPromise = new Promise(
						resolve => {
							dateIcon = new Image();
							
							dateIcon.onload = function() {
								dateIconWidth = this.width * (informationIconHeight / this.height);
								
								contentWidth += dateIconWidth;
								
								resolve();
							}
							
							dateIcon.src = "/static/images/mini-icons/calendar.png";
						}
					);
					
					await dateIconLoadPromise;
				}
				
				if (this.timeText !== "") {
					contentWidth += informationInnerOffset;
					
					timeTextWidth = this.context.measureText(this.timeText).width;
					
					contentWidth += (
						timeTextWidth
						+ informationInnerPadding
					);
					
					let timeIconLoadPromise = new Promise(
						resolve => {
							timeIcon = new Image();
							
							timeIcon.onload = function() {
								timeIconWidth = this.width * (informationIconHeight / this.height);
								
								contentWidth += timeIconWidth;
								
								resolve();
							}
							
							timeIcon.src = "/static/images/mini-icons/time.png";
						}
					);
					
					await timeIconLoadPromise;
				}
				
				if (this.locationText !== "") {
					contentWidth += informationInnerOffset;
					
					locationTextWidth = this.context.measureText(this.locationText).width;
					
					contentWidth += (
						locationTextWidth
						+ informationInnerPadding
					);
					
					let locationIconLoadPromise = new Promise(
						resolve => {
							locationIcon = new Image();
							
							locationIcon.onload = function() {
								locationIconWidth = this.width * (informationIconHeight / this.height);
								
								contentWidth += locationIconWidth;
								
								resolve();
							}
							
							locationIcon.src = "/static/images/mini-icons/location.png";
						}
					);
					
					await locationIconLoadPromise;
				}
				
				if (informationMaxWidth < contentWidth) {
					informationIconHeight -= 1.2; // :P
					informationBottomOffset += 2;
					informationInnerPadding -= 0.6;
					informationTextFontSize -= 2;
					
					this.context.font = `${this.primaryFontStyle} ${informationTextFontSize}px ${this.primaryFont}`;
				}
			} while (informationMaxWidth < contentWidth);
			
			helperCanvas.height = informationIconHeight + informationTextFontSize;
			helperCanvas.width = contentWidth;
			let helperContext = helperCanvas.getContext("2d");
			
			let currentHelperLineX = 0;
			
			helperContext.fillStyle = this.informationTextColor;
			helperContext.font = `${this.primaryFontStyle} ${informationTextFontSize}px ${this.primaryFont}`;
			
			const helperCanvasY = helperCanvas.height * 0.1;
			
			const informationTextRGB = hexToRgb(this.informationTextColor);
			
			if (this.dateText !== "") {
				helperContext.drawImage(
					colorizeImage(
						dateIcon,
						dateIconWidth,
						informationIconHeight,
						informationTextRGB.r,
						informationTextRGB.g,
						informationTextRGB.b
					),
					currentHelperLineX, helperCanvasY,
					dateIconWidth, informationIconHeight
				);
				
				currentHelperLineX += dateIconWidth + informationInnerPadding;
				
				helperContext.fillText(
					this.dateText,
					currentHelperLineX, informationIconHeight + helperCanvasY
				);
				
				currentHelperLineX += dateTextWidth;
			}
			
			if (this.timeText !== "") {
				currentHelperLineX += informationInnerOffset;
				
				helperContext.drawImage(
					colorizeImage(
						timeIcon,
						timeIconWidth,
						informationIconHeight,
						informationTextRGB.r,
						informationTextRGB.g,
						informationTextRGB.b
					),
					currentHelperLineX, helperCanvasY,
					timeIconWidth, informationIconHeight
				);
				
				currentHelperLineX += timeIconWidth + informationInnerPadding;
				
				helperContext.fillText(
					this.timeText,
					currentHelperLineX, informationIconHeight + helperCanvasY
				);
				
				currentHelperLineX += timeTextWidth;
			}
			
			if (this.locationText !== "") {
				currentHelperLineX += informationInnerOffset;
				
				helperContext.drawImage(
					colorizeImage(
						locationIcon,
						locationIconWidth,
						informationIconHeight,
						informationTextRGB.r,
						informationTextRGB.g,
						informationTextRGB.b
					),
					currentHelperLineX, helperCanvasY,
					locationIconWidth, informationIconHeight
				);
				
				currentHelperLineX += locationIconWidth + informationInnerPadding;
				
				helperContext.fillText(
					this.locationText,
					currentHelperLineX, informationIconHeight + helperCanvasY
				);
				
				currentHelperLineX += locationTextWidth;
			}
			
			this.context.fillStyle = this.informationTextBackgroundColor;
			
			this.context.fillRect(
				(
					(
						this.canvas.width
						- helperCanvas.width
					) / 2
 					- informationRectanglePaddingSides
				), (
					primaryRectanglePosition
					- primaryRectangleHeight
					- informationRectanglePaddingTopBottom * 2
					- helperCanvas.height
					- informationRectangleOffsetBottom
				),
				(
					helperCanvas.width
 					+ informationRectanglePaddingSides * 2
				), (
					helperCanvas.height
					+ informationRectanglePaddingTopBottom * 2
				)
			);
			
			this.context.drawImage(
				helperCanvas,
				(this.canvas.width - helperCanvas.width) / 2,
				(
					primaryRectanglePosition
					- primaryRectangleHeight
					- helperCanvas.height
					+ informationRectanglePaddingTopBottom
					- informationRectangleOffsetBottom
				)
			);
		}
		
		// Create logo
		// See if we're using the light or dark version
		const backgroundRGB = hexToRgb(this.backgroundColor);
		const backgroundLightness = (
			0.2126 * backgroundRGB.r
			+ 0.7152 * backgroundRGB.g
			+ 0.0722 * backgroundRGB.b
		)

		const useLightLogo = (backgroundLightness < 207);

		function drawLogoImage(image) {
 			let logoWidth = Math.ceil(image.width * (logoHeight / image.height));

			classRef.context.drawImage(
				image,
				(
					classRef.canvas.width
					- logoWidth
				) / 2,
				(
					classRef.canvas.height
					- logoBottomOffset
					- logoHeight
				),
				logoWidth, logoHeight
			);
		}

		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogoImage(this);
						
						resolve();
					}
					
					if (useLightLogo) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogoImage(this.logoImage);
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> secondaryRectangleHeight - (this.canvas.height * 0.03)
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> secondaryRectangleHeight - (this.canvas.height * 0.03)
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.995 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Text
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setTerciaryText(text, skipRedraw = false) {
		this.terciaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setDateText(text, skipRedraw = false) {
		this.dateText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setTimeText(text, skipRedraw = false) {
		this.timeText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setLocationText(text, skipRedraw = false) {
		this.locationText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				this.informationTextColor = "#ffffff";
				this.informationTextBackgroundColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#000000";
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.foregroundColor ="#000000";
				this.backgroundColor ="#ffffff";
				
				break;
			case "forum-black-on-white":
				this.primaryTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				this.informationTextColor = "#962a51";
				this.informationTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#962a51";
				
				break;
			case "forum-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.informationTextColor = "#ffffff";
				this.informationTextBackgroundColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				this.foregroundColor = "#962a51";
				this.backgroundColor = "#ffffff";
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.primaryTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				this.informationTextColor = "#ffffff";
				this.informationTextBackgroundColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#00ad43";
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.informationTextColor = "#ffffff";
				this.informationTextBackgroundColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.foregroundColor = "#00ad43";
				this.backgroundColor = "#ffffff";
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.primaryTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#9796ca";
				this.terciaryTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#21274e";
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#9796ca";
				this.terciaryTextColor = "#ffffff";
				this.foregroundColor = "#21274e";
				this.backgroundColor = "#ffffff";
				
				break;
			case "louny-spolecne-black-on-white":
				this.primaryTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#e2d7a9";
				this.terciaryTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#3e2a5b";
				
				break;
			case "louny-spolecne-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#e2d7a9";
				this.terciaryTextColor = "#ffffff";
				this.foregroundColor = "#3e2a5b";
				this.backgroundColor = "#ffffff";
				
				break;
			case "litomerice-blue-on-white":
				this.primaryTextColor = "#123172";
				this.primaryTextHighlightColor = "#cccccc";
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#cccccc";
				this.terciaryTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#21274e";
				
				break;
			case "litomerice-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#cccccc";
				this.terciaryTextColor = "#ffffff";
				this.foregroundColor = "#21274e";
				this.backgroundColor = "#ffffff";
				
				break;
			case "stranane-gray-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.informationTextColor = "#4d4d4d";
				this.informationTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#4d4d4d";
				this.foregroundColor = "#4d4d4d";
				this.backgroundColor = "#ffd500";
				
				break;
			case "stranane-yellow-on-white":
				this.primaryTextColor = "#4d4d4d";
				this.informationTextColor = "#ffffff";
				this.informationTextBackgroundColor = "#4d4d4d";
				this.terciaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#4d4d4d";
				
				break;
			case "stranane-white-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				this.informationTextColor = "#ffffff";
				this.informationTextBackgroundColor = "#4d4d4d";
				this.terciaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				
				break;
			case "prusanky-black-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffd500";
				
				break;
			case "prusanky-yellow-on-white":
				this.primaryTextColor = "#000000";
				this.informationTextColor = "#ffffff";
				this.informationTextBackgroundColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				this.informationTextColor = "#ffffff";
				this.informationTextBackgroundColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				
				break;
			case "ujezd-green-on-white":
				this.primaryTextColor = "#000000";
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#ffdd55";
				this.terciaryTextColor = "#000000";
				this.foregroundColor = "#8ed4a3";
				this.backgroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "ujezd-white-on-green":
				this.primaryTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#ffdd55";
				this.terciaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#8ed4a3";
				
				break;
			case "cssd-red-on-black":
				this.primaryTextColor = "#ffffff";
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffffff";
				
				break;
			case "cssd-black-on-red":
				this.primaryTextColor = "#ffffff";
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#e63812";
				
				break;
			case "jilemnice-purple-on-black":
				this.primaryTextColor = "#ffffff";
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffffff";
				
				break;
			case "jilemnice-black-on-purple":
				this.primaryTextColor = "#ffffff";
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#6e1646";
				
				break;
			case "novarole-white-on-green":
				this.primaryTextColor = "#000000";
				this.primaryTextHighlightColor = "#a9ce2d";
				
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#a9ce2d";
				
				this.terciaryTextColor = "#000000";
				
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#a9ce2d";
				
				break;
			case "novarole-green-on-white":
				this.primaryTextColor = "#000000";
				
				this.informationTextColor = "#000000";
				this.informationTextBackgroundColor = "#ffffff";
				
				this.terciaryTextColor = "#000000";
				
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#ffffff";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-black":
				this.primaryTextColor = "#000000";
				
				this.informationTextColor = "#ffffff";
				this.informationTextBackgroundColor = "#000000";
				
				this.terciaryTextColor = "#ffffff";
				
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				this.informationTextBackgroundColor = "#fde119";
				this.informationTextColor = "#000000";
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.foregroundColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
