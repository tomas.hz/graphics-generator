class EventTextRight extends Template {
	description = "Určeno pro události na Facebooku.";
	
	changeableAttributes = [
		"logoImage",
		"primaryImage",
		"secondaryImage",
		"primaryText",
		"dateText",
		"timeText",
		"locationText",
		"primaryColorScheme",
		"primaryImagePosition",
		"nameText",
		"secondaryText",
		"terciaryText"
	];

	secondaryImage = null;
	
	secondaryText = "";
	terciaryText = "";
	dateText = "";
	timeText = "";
	locationText = "";
	
	defaultResolution = 4430;
	aspectRatio = 2;

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];
	
	changeableColors = [
		"primaryTextColor",
		"foregroundColor",
		"backgroundColor",
		"primaryTextHighlightColor",
		"requesterTextColor"
	];

	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const backgroundRectangleWidth = Math.ceil(this.canvas.width * 0.32);
		
		const foregroundRectangleOffsetTop = Math.ceil(this.canvas.height * 0.05);
		const foregroundRectangleOffsetSide = Math.ceil(this.canvas.height * 0.05);
		const foregroundRectangleWidth = Math.ceil(this.canvas.width * 0.4);
		const foregroundRectangleHeight = Math.ceil(this.canvas.height * 0.73);
		const foregroundRectanglePaddingInnerSides = Math.ceil(foregroundRectangleWidth * 0.075);
		const foregroundRectanglePaddingTopBottom = Math.ceil(foregroundRectangleHeight * 0.075);
		const foregroundRectangleAngle = Math.ceil(this.canvas.height * 0.0075);
		
		const highlightPaddingSides = Math.ceil(this.canvas.width * 0.01);
		const highlightPaddingBottom = Math.ceil(this.canvas.height * 0.0125);
		const highlightPaddingTop = Math.ceil(this.canvas.height * -0.01);
		
		let nameFontSize = Math.ceil(this.canvas.height * 0.05);
		const nameTextPaddingBottom = Math.ceil(this.canvas.height * 0.01);
		
		let primaryFontSize = Math.ceil(this.canvas.height * 0.12);
		let primaryTextMaxLines = 2;
		
		const logoHeight = Math.ceil(this.canvas.height * 0.09) * this.logoImageZoom;
		const logoOffsetBottom = Math.ceil(this.canvas.height * 0.0675) - (
			logoHeight / this.logoImageZoom * (this.logoImageZoom - 1) / 2
		);
		
		let secondaryFontSize = Math.ceil(this.canvas.height * 0.0425);
		const secondaryTextPaddingTop = Math.ceil(this.canvas.height * 0.025);
		let secondaryTextMaxLines = 5;
		
		let terciaryFontSize = Math.ceil(this.canvas.height * 0.0185);
		const terciaryTextPaddingTop = Math.ceil(this.canvas.height * 0.04);
		let terciaryTextMaxLines = 2;
		
		const secondaryImageWidth = Math.ceil(this.canvas.width * 0.02);
		const secondaryImagePaddingSide = Math.ceil(this.canvas.width * 0.0035);
		
		const separatorHeight = Math.ceil(this.canvas.height * 0.003);
		const separatorBottomOffset = Math.ceil(this.canvas.height * 0.02);
		
		let informationTextFontSize = Math.ceil(this.canvas.height * 0.055);
		let informationIconHeight = Math.ceil(this.canvas.height * 0.04);
		let informationInnerPadding = Math.ceil(this.canvas.width * 0.005);
		
		// Clear the canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Set image
		if (this.primaryImage !== null) {
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D
			
			const imageScaleX = this.canvas.width / this.primaryImage.width;
			const imageScaleY = this.canvas.height / this.primaryImage.height;
			
			const imageScale = Math.max(imageScaleX, imageScaleY) * this.primaryImageZoom;
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - this.primaryImage.width * imageScale) / 2 + this.primaryImageX * this.primaryImageZoom,
				(this.canvas.height - this.primaryImage.height * imageScale) / 2 + this.primaryImageY * this.primaryImageZoom,
			);
			this.context.drawImage(
				this.primaryImage,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
		}
		
		this.context.fillStyle = this.backgroundColor;
		
		this.context.fillRect(
			this.canvas.width - backgroundRectangleWidth, 0,
			backgroundRectangleWidth, this.canvas.height
		);
		
		let currentForegroundRectangleY = foregroundRectangleOffsetTop;
		const foregroundRectangleStartingX = this.canvas.width - foregroundRectangleOffsetSide - foregroundRectangleWidth;
		const foregroundRectangleEndingX = foregroundRectangleStartingX + foregroundRectangleWidth;
		
		this.context.fillStyle = this.foregroundColor;

		this.context.beginPath();
		
		this.context.moveTo(
			foregroundRectangleStartingX,
			currentForegroundRectangleY + foregroundRectangleAngle
		);
		this.context.lineTo(
			foregroundRectangleEndingX,
			currentForegroundRectangleY
		);
		this.context.lineTo(
			foregroundRectangleEndingX,
			currentForegroundRectangleY + foregroundRectangleHeight
		);
		this.context.lineTo(
			foregroundRectangleStartingX,
			currentForegroundRectangleY + foregroundRectangleAngle + foregroundRectangleHeight
		);
		
		this.context.closePath();
		
		this.context.fill();
		
		let primaryLines = [];
		
		const originalPrimaryHeight = (primaryFontSize * primaryTextMaxLines) - foregroundRectanglePaddingTopBottom;
		
		do {
			this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;
			
			primaryLines = splitStringIntoLines(
				this.context,
				this.primaryText,
				foregroundRectangleWidth - 2 * foregroundRectanglePaddingInnerSides,
				primaryTextMaxLines,
				true
			);
			
			if (primaryLines.length > primaryTextMaxLines) {
				primaryFontSize -= 2;
			}
			
			if (((primaryTextMaxLines + 1) * primaryFontSize) < originalPrimaryHeight) {
				primaryTextMaxLines += 1;
			}
		} while (primaryLines.length > primaryTextMaxLines);
		
		this.context.textAlign = "center";
		
		const primaryLineX = foregroundRectangleEndingX - (foregroundRectangleWidth / 2);
		
		// Create name text
		if (this.nameText !== "") {
			this.context.fillStyle = this.primaryTextColor;
			
			currentForegroundRectangleY += foregroundRectanglePaddingTopBottom + nameFontSize;
			
			this.context.font = `${nameFontSize}px 'Roboto Condensed'`;
			
			while (this.context.measureText(this.nameText).width > foregroundRectangleWidth - 2 * foregroundRectanglePaddingInnerSides) {
				nameFontSize -= 2;
				
				this.context.font = `${nameFontSize}px 'Roboto Condensed'`;
			}
			
			this.context.fillText(
				this.nameText,
				primaryLineX,
				currentForegroundRectangleY
			);
		}
		
		currentForegroundRectangleY += primaryFontSize + nameTextPaddingBottom;
		
		// Create primary text
		this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;
		
		const foregroundRGB = hexToRgb(this.foregroundColor);
		
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		)
		
		const useLightHighlightAndUseDarkIcon = (foregroundLightness > 207);
		
		let primaryTextHighlightedColor = null;
		
		const lowercasePrimaryTextHighlightColor = this.primaryTextHighlightColor.toLowerCase();
		const hasColorOverride = (
			lowercasePrimaryTextHighlightColor === "#209a37" ||
			lowercasePrimaryTextHighlightColor === "#e63812"
		);
		
		if (hasColorOverride) {
			if (useLightHighlightAndUseDarkIcon) {
				primaryTextHighlightedColor = this.foregroundColor;
			} else {
				primaryTextHighlightedColor = this.primaryTextColor;
			}
		} else if (!useLightHighlightAndUseDarkIcon) {
			primaryTextHighlightedColor = this.foregroundColor;
		} else {
			primaryTextHighlightedColor = this.primaryTextColor;
		}
		
		this.context.fillStyle = this.primaryTextColor;
		this.context.textAlign = "left";
		
		for (let line of primaryLines) {
			let wordPosition = 0;
			
			for (let word of line) {
				const previousWords = line.slice(0, wordPosition).join(" ");
				const previousWordsWidth = this.context.measureText(
					previousWords
					+ (
						(previousWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				const nextWords = line.slice(wordPosition + 1, line.length).join(" ")
				const nextWordsWidth = this.context.measureText(
					nextWords
					+ (
						(nextWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				let currentWordWidth = this.context.measureText(word).width;
				
				for (const word of line.slice(wordPosition + 1, line.length)) {
					if (word.isHighlighted) {
						currentWordWidth += this.context.measureText(word.toString() + " ").width;
					} else {
						break;
					}
				}
				
				if (word.isHighlighted) {
					if (
						wordPosition === 0 ||
						!line[wordPosition - 1].isHighlighted
					) {
						this.context.fillStyle = this.primaryTextHighlightColor;
						this.context.beginPath();
						
						const startingHighlightLineX = (
							primaryLineX
							+ Math.max(previousWordsWidth / 2)
							- Math.max(nextWordsWidth / 2)
							- Math.max(this.context.measureText(word).width / 2)
						);
						
						this.context.moveTo(
							startingHighlightLineX - highlightPaddingSides,
						  currentForegroundRectangleY + highlightPaddingBottom
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentForegroundRectangleY
								+ highlightPaddingBottom
								- Math.max(
									(currentWordWidth * foregroundRectangleAngle)
									/ foregroundRectangleWidth
								)
							)
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentForegroundRectangleY
								- primaryFontSize
								- highlightPaddingTop
								- Math.max(
									(currentWordWidth * foregroundRectangleAngle)
									/ foregroundRectangleWidth
								)
							)
						);
						this.context.lineTo(
							startingHighlightLineX - highlightPaddingSides,
							(
								currentForegroundRectangleY
								- primaryFontSize
								- highlightPaddingTop
							)
						);
						
						this.context.closePath();
						
						this.context.fill();
					}
					
					this.context.fillStyle = primaryTextHighlightedColor;
				}
				
				this.context.fillText(
					word + " ",
					(
						primaryLineX
						+ Math.ceil(previousWordsWidth / 2)
						- Math.ceil(nextWordsWidth / 2)
						- Math.ceil(this.context.measureText(word).width / 2)
					),
					currentForegroundRectangleY
				);
				
				wordPosition++;
				
				this.context.fillStyle = this.primaryTextColor;
			}
			
			currentForegroundRectangleY += primaryFontSize;
		}
		
		this.context.textAlign = "center";
		
		currentForegroundRectangleY -= primaryFontSize;
		
		currentForegroundRectangleY += secondaryTextPaddingTop;
		
		if (this.secondaryText !== "") {
			currentForegroundRectangleY += secondaryFontSize;
			
			let secondaryLines = [];
			
			const originalSecondaryHeight = (secondaryFontSize * secondaryTextMaxLines) - foregroundRectanglePaddingTopBottom;
			
			do {
				this.context.font = `${secondaryFontSize}px 'Roboto Condensed'`;
				
				secondaryLines = splitStringIntoLines(
					this.context,
					this.secondaryText,
					foregroundRectangleWidth - 2 * foregroundRectanglePaddingInnerSides,
					secondaryTextMaxLines,
					true
				);
				
				if (secondaryLines.length > secondaryTextMaxLines) {
					secondaryFontSize -= 2;
				}
				
				if (((secondaryTextMaxLines + 1) * secondaryFontSize) < originalSecondaryHeight) {
					secondaryTextMaxLines += 1;
				}
			} while (secondaryLines.length > secondaryTextMaxLines);
			
			for (let line of secondaryLines) {
				this.context.fillText(
					line.join(" "),
					primaryLineX,
					currentForegroundRectangleY
				);
				
				currentForegroundRectangleY += secondaryFontSize;
			}
		}
		
		currentForegroundRectangleY -= secondaryFontSize;
		
		if (this.terciaryText !== "") {
			if (currentForegroundRectangleY < this.canvas.height * 0.6) {
				let helperCanvas = document.createElement("canvas");
				
				currentForegroundRectangleY += terciaryTextPaddingTop;
				
				let terciaryLines = [];
				
				const originalTerciaryHeight = (terciaryFontSize * terciaryTextMaxLines) - foregroundRectanglePaddingTopBottom;
				
				do {
					this.context.font = `${terciaryFontSize}px 'Roboto Condensed'`;
					
					terciaryLines = splitStringIntoLines(
						this.context,
						this.terciaryText,
						foregroundRectangleWidth - 2 * foregroundRectanglePaddingInnerSides - (
							(this.secondaryImage !== null) ?
							secondaryImageWidth + secondaryImagePaddingSide :
							0
						),
						terciaryTextMaxLines,
						true
					);
					
					if (terciaryLines.length > terciaryTextMaxLines) {
						terciaryFontSize -= 2;
					}
					
					if (((terciaryTextMaxLines + 1) * terciaryFontSize) < originalTerciaryHeight) {
						terciaryTextMaxLines += 1;
					}
				} while (terciaryLines.length > terciaryTextMaxLines);
				
				let longestLineWidth = 0;
				
				for (let line of terciaryLines) {
					const length = this.context.measureText(line.join(" ")).width;
					
					if (length > longestLineWidth) {
						longestLineWidth = length;
					}
				}
				
				let totalWidth = Math.ceil(longestLineWidth);
				
				let helperContext = null;
				
				if (this.secondaryImage !== null) {
					totalWidth += secondaryImageWidth + secondaryImagePaddingSide;
				
					const secondaryImageHeight = (this.secondaryImage.height * (secondaryImageWidth / this.secondaryImage.width));
					
					helperCanvas.height = Math.ceil(Math.max(terciaryFontSize * (terciaryLines.length + 1), secondaryImageHeight));
					helperCanvas.width = totalWidth;
					
					helperContext = helperCanvas.getContext("2d");
					
					helperContext.drawImage(
						this.secondaryImage,
						0, 0,
						secondaryImageWidth, secondaryImageHeight
					);
				} else {
					helperCanvas.height = terciaryFontSize * (terciaryLines.length + 1);
					helperCanvas.width = totalWidth;
					helperContext = helperCanvas.getContext("2d");
				}
				
				helperContext.font = `${terciaryFontSize}px 'Roboto Condensed'`;
				helperContext.fillStyle = this.primaryTextColor;
				
				let currentHelperLineY = terciaryFontSize;
				
				for (let line of terciaryLines) {
					helperContext.fillText(
						line.join(" "),
						(
							(this.secondaryImage !== null) ?
							secondaryImageWidth + secondaryImagePaddingSide :
							0
						),
						currentHelperLineY
					);
					
					currentHelperLineY += terciaryFontSize;
				}
				
				this.context.drawImage(
					helperCanvas,
					primaryLineX - (helperCanvas.width / 2),
					currentForegroundRectangleY
				);
				
				currentForegroundRectangleY += helperCanvas.height;
			} else {
				alert("Jelikož by se tericární text nevešel, odstranili jsme ho. Tohle můžeš vyřešit zkrácením ostatních textů.");
			}
		}
		
		if (
			this.dateText !== "" ||
			this.timeText !== "" ||
			this.locationText !== ""
		) {
			let helperCanvas = document.createElement("canvas");
			
			let usedInformation = 0;
			
			let contentWidth = 0;
			
			let dateIcon = null;
			let dateIconWidth = 0;
			let dateTextWidth = 0;
			
			let timeIcon = null;
			let timeIconWidth = 0;
			let timeTextWidth = 0;
			
			let locationIcon = null;
			let locationIconWidth = 0;
			let locationTextWidth = 0;
			
			do {
				contentWidth = 0;
				usedInformation = 0;
				
				this.context.font = `${this.primaryFontStyle} ${informationTextFontSize}px ${this.primaryFont}`;
				
				if (this.dateText !== "") {
					usedInformation += 1;
					
					dateTextWidth = this.context.measureText(this.dateText).width;
					
					contentWidth += (
						dateTextWidth
						+ informationInnerPadding
					);
					
					let dateIconLoadPromise = new Promise(
						resolve => {
							dateIcon = new Image();
							
							dateIcon.onload = function() {
								dateIconWidth = this.width * (informationIconHeight / this.height);
								
								contentWidth += dateIconWidth;
								
								resolve();
							}
							
							dateIcon.src = "/static/images/mini-icons/calendar.png";
						}
					);
					
					await dateIconLoadPromise;
				}
				
				if (this.timeText !== "") {
					usedInformation += 1;
					
					timeTextWidth = this.context.measureText(this.timeText).width;
					
					contentWidth += (
						timeTextWidth
						+ informationInnerPadding
					);
					
					let timeIconLoadPromise = new Promise(
						resolve => {
							timeIcon = new Image();
							
							timeIcon.onload = function() {
								timeIconWidth = this.width * (informationIconHeight / this.height);
								
								contentWidth += timeIconWidth;
								
								resolve();
							}
							
							timeIcon.src = "/static/images/mini-icons/time.png";
						}
					);
					
					await timeIconLoadPromise;
				}
				
				if (this.locationText !== "") {
					usedInformation += 1;
					
					locationTextWidth = this.context.measureText(this.locationText).width;
					
					contentWidth += (
						locationTextWidth
						+ informationInnerPadding
					);
					
					let locationIconLoadPromise = new Promise(
						resolve => {
							locationIcon = new Image();
							
							locationIcon.onload = function() {
								locationIconWidth = this.width * (informationIconHeight / this.height);
								
								contentWidth += locationIconWidth;
								
								resolve();
							}
							
							locationIcon.src = "/static/images/mini-icons/location.png";
						}
					);
					
					await locationIconLoadPromise;
				}
				
				if (foregroundRectangleWidth - 2 * foregroundRectanglePaddingInnerSides < contentWidth) {
					informationIconHeight -= 1.2; // :P
					informationInnerPadding -= 0.6;
					informationTextFontSize -= 2;
					
					this.context.font = `${this.primaryFontStyle} ${informationTextFontSize}px ${this.primaryFont}`;
				}
			} while (foregroundRectangleWidth - 2 * foregroundRectanglePaddingInnerSides < contentWidth);
			
			helperCanvas.height = informationIconHeight + informationTextFontSize;
			helperCanvas.width = foregroundRectangleWidth - 2 * foregroundRectanglePaddingInnerSides;
			let helperContext = helperCanvas.getContext("2d");
			
			let informationInnerOffset = (helperCanvas.width - contentWidth) / (usedInformation - 1);
			let currentHelperLineX = 0;
			
			helperContext.fillStyle = this.primaryTextColor;
			helperContext.font = `${this.primaryFontStyle} ${informationTextFontSize}px ${this.primaryFont}`;
			
			const helperCanvasY = helperCanvas.height * 0.1;
			
			const informationTextRGB = hexToRgb(this.primaryTextColor);
			
			if (this.dateText !== "") {
				helperContext.drawImage(
					colorizeImage(
						dateIcon,
						dateIconWidth,
						informationIconHeight,
						informationTextRGB.r,
						informationTextRGB.g,
						informationTextRGB.b
					),
					currentHelperLineX, helperCanvasY,
					dateIconWidth, informationIconHeight
				);
				
				currentHelperLineX += dateIconWidth + informationInnerPadding;
				
				helperContext.fillText(
					this.dateText,
					currentHelperLineX, informationIconHeight + helperCanvasY
				);
				
				currentHelperLineX += dateTextWidth + informationInnerOffset;
			}
			
			if (this.timeText !== "") {
				helperContext.drawImage(
					colorizeImage(
						timeIcon,
						timeIconWidth,
						informationIconHeight,
						informationTextRGB.r,
						informationTextRGB.g,
						informationTextRGB.b
					),
					currentHelperLineX, helperCanvasY,
					timeIconWidth, informationIconHeight
				);
				
				currentHelperLineX += timeIconWidth + informationInnerPadding;
				
				helperContext.fillText(
					this.timeText,
					currentHelperLineX, informationIconHeight + helperCanvasY
				);
				
				currentHelperLineX += timeTextWidth + informationInnerOffset;
			}
			
			if (this.locationText !== "") {
				helperContext.drawImage(
					colorizeImage(
						locationIcon,
						locationIconWidth,
						informationIconHeight,
						informationTextRGB.r,
						informationTextRGB.g,
						informationTextRGB.b
					),
					currentHelperLineX, helperCanvasY,
					locationIconWidth, informationIconHeight
				);
				
				currentHelperLineX += locationIconWidth + informationInnerPadding;
				
				helperContext.fillText(
					this.locationText,
					currentHelperLineX, informationIconHeight + helperCanvasY
				);
				
				currentHelperLineX += locationTextWidth;
			}
			
			this.context.fillRect(
				this.canvas.width - foregroundRectangleOffsetSide - foregroundRectangleWidth + foregroundRectanglePaddingInnerSides,
				foregroundRectangleHeight - foregroundRectanglePaddingTopBottom - separatorBottomOffset,
				foregroundRectangleWidth - 2 * foregroundRectanglePaddingInnerSides, separatorHeight
			);
			
			this.context.drawImage(
				helperCanvas,
				primaryLineX - (helperCanvas.width / 2),
				foregroundRectangleHeight - foregroundRectanglePaddingTopBottom
			);
		}
		
		const backgroundRGB = hexToRgb(this.backgroundColor);
		
		const backgroundLightness = (
			0.2126 * backgroundRGB.r
			+ 0.7152 * backgroundRGB.g
			+ 0.0722 * backgroundRGB.b
		)
		
		const useLightLogo = (backgroundLightness < 207);
		
		let classRef = this;

		function drawLogoImage(image) {
			let logoWidth = Math.ceil(image.width * (logoHeight / image.height));
			
			classRef.context.drawImage(
				image,
				(
					classRef.canvas.width - backgroundRectangleWidth
					+ (backgroundRectangleWidth - logoWidth) / 2
				), classRef.canvas.height - logoHeight - logoOffsetBottom,
				logoWidth, logoHeight
			);
		}
		
		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogoImage(this);
						
						resolve();
					}
					
					if (useLightLogo) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogoImage(this.logoImage);
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.993 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}

	// Text
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setTerciaryText(text, skipRedraw = false) {
		this.terciaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setDateText(text, skipRedraw = false) {
		this.dateText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setTimeText(text, skipRedraw = false) {
		this.timeText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setLocationText(text, skipRedraw = false) {
		this.locationText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffffff";
				
				break;
			case "forum-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#962a51";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "forum-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#962a51";
				this.backgroundColor = "#ffffff";
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#00ad43";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#00ad43";
				this.backgroundColor = "#ffffff";
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#21274e";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#21274e";
				this.backgroundColor = "#ffffff";
				
				break;
			case "louny-spolecne-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#3e2a5b";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "louny-spolecne-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#3e2a5b";
				this.backgroundColor = "#ffffff";
				
				break;
			case "litomerice-blue-on-white":
				this.primaryTextColor = "#123172";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#123172";
				this.primaryTextHighlightColor = "#afe87e";
				
				break;
			case "litomerice-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#123172";
				this.backgroundColor = "#ffffff";
				
				break;
			case "stranane-gray-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#4d4d4d";
				this.backgroundColor = "#ffd500";
				
				break;
			case "stranane-yellow-on-white":
				this.primaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#4d4d4d";
				
				break;
			case "stranane-white-on-yellow":
				this.primaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-black-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffd500";
				
				break;
			case "prusanky-yellow-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "ujezd-green-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#8ed4a3";
				this.backgroundColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "ujezd-white-on-green":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#8ed4a3";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "cssd-red-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#e63812";
				this.backgroundColor = "#000000";
				
				break;
			case "cssd-black-on-red":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#e63812";
				
				break;
			case "jilemnice-purple-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#6e1646";
				this.backgroundColor = "#000000";
				
				break;
			case "jilemnice-black-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#6e1646";
				
				break;
			case "novarole-white-on-green":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#a9ce2d";
				this.primaryTextHighlightColor = "#a9ce2d";
				
				break;
			case "novarole-green-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#ffffff";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-black":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.foregroundColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Secondary image
	async setSecondaryImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.secondaryImage = new Image();
					
					classRef.secondaryImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.secondaryImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
}
