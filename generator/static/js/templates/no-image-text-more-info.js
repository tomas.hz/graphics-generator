class NoImageTextMoreInfo extends Template {
	description = "Určeno pro sociální sítě.";
	
	changeableAttributes = [
		"logoImage",
		"primaryColorScheme",
		"primaryText",
		"nameText",
		"terciaryText",
		"iconText",
		"iconImage"
	];

	iconImage = null;
	iconSource = null;
	iconText = "";
	
	terciaryText = "";
	
	quoteImageSource = "/static/images/quote_nontransparent.png";
	
	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];

	// Colors
	changeableColors = [
		"backgroundColor",
		"foregroundColor",
		"nameBackgroundColor",
		"nameTextColor",
		"primaryTextColor",
		"primaryTextHighlightColor",
		"iconColor",
		"terciaryTextColor",
		"quoteColor"
	];

	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		// Logo
		const logoWidth = Math.ceil(this.canvas.width * 0.17) * this.logoImageZoom;
		const logoOffsetTop = Math.ceil(this.canvas.height * 0.06) * ((3 - this.logoImageZoom) / 2);
		const logoOffsetSide = Math.ceil(this.canvas.width * 0.07);
		
		// Quote
		const quoteOffsetTop = Math.ceil(this.canvas.height * 0.06);
		const quoteOffsetSide = Math.ceil(this.canvas.width * 0.07);
		const quoteWidth = Math.ceil(this.canvas.width * 0.17);
		
		// Name
		let nameTextFontSize = Math.ceil(this.canvas.height * 0.03);
		const nameTextPaddingSides = Math.ceil(this.canvas.width * 0.012);
		const nameTextPaddingTopBottom = nameTextPaddingSides;
		
		const nameRectangleOffsetSide = Math.ceil(this.canvas.width * 0.02);
		const nameRectangleOffsetTop = Math.ceil(this.canvas.width * 0.0235);
		
		const nameTextMaxWidth = Math.ceil(this.canvas.width * 0.45);
		
		// Icon
		const iconWidth = Math.ceil(this.canvas.width * 0.07);
		const iconOffsetSide = Math.ceil(this.canvas.width * 0.07);
		const iconOffsetBottom = iconOffsetSide;
		
		// Icon text
		const iconTextOffsetSide = Math.ceil(this.canvas.width * 0.015);
		const iconTextOffsetBottom = Math.ceil(this.canvas.height * 0.053);
		let iconTextFontSize = Math.ceil(this.canvas.height * 0.03);
		let iconTextMaxLines = 2;
		const iconTextMaxWidth = Math.ceil(this.canvas.width * 0.35);
		
		// Terciary text
		let terciaryTextFontSize = Math.ceil(this.canvas.height * 0.045);
		const terciaryTextMaxWidth = Math.ceil(this.canvas.width * 0.35);
		const terciaryTextOffsetSide = iconOffsetSide;
		const terciaryTextOffsetBottom = Math.ceil(this.canvas.height * 0.035);
		
		// Bottom line
		const bottomLineAngle = Math.ceil(this.canvas.height * 0.01);
		const bottomLineOffset = Math.ceil(this.canvas.height * 0.18);
		const bottomLineThickness = Math.ceil(this.canvas.height * 0.0045);
		
		// Primary text
		let primaryTextFontSize = Math.ceil(this.canvas.height * 0.14);
		let primaryTextMaxLines = 4;
		const primaryTextMaxWidth = Math.ceil(this.canvas.width * 0.8);
		const primaryTextOffsetTop = Math.ceil(this.canvas.height * 0.05);
		
		this.context.fillStyle = this.backgroundColor;
		this.context.fillRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		const classRef = this;
		
		// Create logo
		// See if we're using the light or dark version
		const backgroundRGB = hexToRgb(this.backgroundColor);
		const backgroundLightness = (
			0.2126 * backgroundRGB.r
			+ 0.7152 * backgroundRGB.g
			+ 0.0722 * backgroundRGB.b
		);
		
		const useLightLogo = (backgroundLightness < 207);
		
		function drawLogoImage(image) {
			const logoHeight = (image.height * (logoWidth / image.width));
			
			classRef.context.drawImage(
				image,
				classRef.canvas.width - logoOffsetSide - logoWidth, logoOffsetTop,
				logoWidth, logoHeight
			);
		}
		
		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogoImage(this);
						
						resolve();
					}
					
					if (useLightLogo) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogoImage(this.logoImage);
		}
		
		// Create quote image
		const quoteImageLoadPromise = new Promise(
			resolve => {
				const quoteImage = new Image();
				const quoteRGB = hexToRgb(classRef.quoteColor);
				
				quoteImage.onload = function() {
					const quoteHeight = (this.height * (quoteWidth / this.width));
					
					classRef.context.drawImage(
						colorizeImage(
							this,
							quoteWidth,
							quoteHeight,
							quoteRGB.r,
							quoteRGB.g,
							quoteRGB.b
						),
						quoteOffsetSide, quoteOffsetTop,
						quoteWidth, quoteHeight
					);
					
					resolve();
				}
				
				quoteImage.src = "static/images/quote_nontransparent.png";
			}
		);
		
		await quoteImageLoadPromise;
		
		// Create name text, if there is any
		if (this.nameText !== "") {
			this.context.fillStyle = this.nameBackgroundColor;
			
			let nameTextWidth = 0;
			
			do {
				this.context.font = `${nameTextFontSize}px 'Roboto Condensed'`;
				nameTextWidth = this.context.measureText(this.nameText).width;
				
				if (nameTextWidth > nameTextMaxWidth) {
					nameTextFontSize -= 2;
				}
			} while (nameTextWidth > nameTextMaxWidth);
			
			this.context.fillRect(
				(
					quoteOffsetSide
					+ nameRectangleOffsetSide
				), (
					quoteOffsetTop
					+ nameRectangleOffsetTop
				), (
					nameTextPaddingSides * 2
					+ nameTextWidth
				),
				(
					nameTextPaddingTopBottom * 2
					+ nameTextFontSize
				)
			);
			
			this.context.textBaseline = "ideographic";
			this.context.fillStyle = this.nameTextColor;
			
			this.context.fillText(
				this.nameText,
				quoteOffsetSide + nameRectangleOffsetSide + nameTextPaddingSides,
				quoteOffsetTop + nameRectangleOffsetTop + nameTextPaddingTopBottom + nameTextFontSize
			);
			
			this.context.textBaseline = "alphabetic";
		}
		
		// Create icon, if there is any
		function drawIconImage(image, colorize = true) {
			const iconHeight = (image.height * (iconWidth / image.width));
			
			const iconRGB = hexToRgb(classRef.iconColor);
			
			classRef.context.drawImage(
				(
					(colorize) ?  // Don't colorize when there is a custom icon
					colorizeImage(
						image,
						iconWidth, iconHeight,
						iconRGB.r, iconRGB.g, iconRGB.b
					) : image
				),
				iconOffsetSide, classRef.canvas.height - iconOffsetBottom - iconHeight - (
					(iconWidth > iconHeight) ?
					(
						(
							iconWidth
							- iconHeight
						) / 2
					) : 0
				),
				iconWidth, iconHeight
  			);
		}
		
		function drawIconText() {
			// Draw icon text
			let iconTextLines = null;
			const originalIconTextFontSize = iconTextFontSize;
			
			do {
				classRef.context.font = `${iconTextFontSize}px 'Roboto Condensed'`;
				
				iconTextLines = splitStringIntoLines(
					classRef.context,
					classRef.iconText,
					iconTextMaxWidth,
					iconTextMaxLines,
					true
				)
				
				if (
					iconTextLines.length > iconTextMaxLines
					&& (
						iconTextLines.length * iconTextFontSize
						> iconTextMaxLines * originalIconTextFontSize
					)
				) {
					iconTextFontSize -= 2;
				}
			} while (
				iconTextLines.length > iconTextMaxLines
				&& (
					iconTextLines.length * iconTextFontSize
					> iconTextMaxLines * originalIconTextFontSize
				)
			);
			
			let currentIconLineY = (
				classRef.canvas.height
				- iconTextOffsetBottom
				- iconTextMaxLines * originalIconTextFontSize
			);
			classRef.context.fillStyle = classRef.iconColor;
			
			for (const line of iconTextLines) {
				classRef.context.fillText(
					line.join(" "),
					iconOffsetSide + (
						(classRef.iconImage !== null || classRef.iconSource !== null) ?
						iconWidth + iconTextOffsetSide :
						0
					),
					currentIconLineY
				);
				
				currentIconLineY += iconTextFontSize;
			}
		}
		
		if (this.iconImage !== null) {
			drawIconImage(this.iconImage, false);
		} else if (this.iconSource !== null) {
			const iconImageLoadPromise = new Promise(
				resolve => {
					const iconImage = new Image();
					
					iconImage.onload = function() {
						drawIconImage(this);
						resolve();
					}
					
					iconImage.src = classRef.iconSource;
				}
			);
			
			await iconImageLoadPromise;
		}
		
		if (this.iconText !== "") {
			drawIconText();
		}
		
		// Draw terciary text
		if (this.terciaryText !== "") {
			this.context.fillStyle = this.terciaryTextColor;
			
			do {
				this.context.font = `${this.primaryFontStyle} ${terciaryTextFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.terciaryText).width
					> terciaryTextMaxWidth
				) {
					terciaryTextFontSize -= 2;
				}
			} while (
				this.context.measureText(this.terciaryText).width
				> terciaryTextMaxWidth
			);
			
			this.context.textAlign = "right";
			
			this.context.fillText(
				this.terciaryText,
				this.canvas.width - terciaryTextOffsetSide,
				this.canvas.height - terciaryTextFontSize - terciaryTextOffsetBottom
			);
			
			this.context.textAlign = "left";
		}
		
		// Draw bottom line
		this.context.fillStyle = this.foregroundColor;
		
		this.context.beginPath();
		
		this.context.moveTo(
			iconOffsetSide,
			this.canvas.height - bottomLineOffset
		);
		this.context.lineTo(
			this.canvas.width - terciaryTextOffsetSide,
			this.canvas.height - bottomLineOffset - bottomLineAngle
		);
		this.context.lineTo(
			this.canvas.width - terciaryTextOffsetSide,
			this.canvas.height - bottomLineOffset - bottomLineAngle - bottomLineThickness
		);
		this.context.lineTo(
			iconOffsetSide,
			this.canvas.height - bottomLineOffset - bottomLineThickness
		);
		
		this.context.closePath();
		this.context.fill();
		
		// Draw primary text
		if (this.primaryText !== "") {
			let primaryTextLines = null;
			const originalPrimaryTextFontSize = primaryTextFontSize;
			
			do {
				this.context.font = `${this.primaryFontStyle} ${primaryTextFontSize}px ${this.primaryFont}`;
				
				primaryTextLines = splitStringIntoLines(
					this.context,
					this.primaryText,
					primaryTextMaxWidth,
					primaryTextMaxLines,
					true
				)
				
				if (
					primaryTextLines.length > primaryTextMaxLines
					&& (
						primaryTextLines.length * primaryTextFontSize
						> primaryTextMaxLines * originalPrimaryTextFontSize
					)
				) {
					primaryTextFontSize -= 2;
				}
			} while (
				primaryTextLines.length > primaryTextMaxLines
				&& (
					primaryTextLines.length * primaryTextFontSize
					> primaryTextMaxLines * originalPrimaryTextFontSize
				)
			);
			
			this.context.fillStyle = this.primaryTextColor;
			this.context.textAlign = "center";
			
			let currentPrimaryLineY = (
				(
					this.canvas.height
					- (primaryTextLines.length - 1) * primaryTextFontSize
				) / 2
				+ primaryTextOffsetTop
			);
			
			for (const line of primaryTextLines) {
				this.context.fillText(
					line.join(" "),
					this.canvas.width / 2,
					currentPrimaryLineY
				);
				
				currentPrimaryLineY += primaryTextFontSize;
			}
			
			this.context.textAlign = "left";
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Icon
	async setIconText(text, skipRedraw = false) {
		this.iconText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setIconSource(url, skipRedraw = false) {
		this.iconSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setIconImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.iconImage = new Image();
					
					classRef.iconImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.iconImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
	
	async resetIconImage(skipRedraw = false) {
		this.iconImage = null;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Terciary text
	async setTerciaryText(text, skipRedraw = false) {
		this.terciaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.nameBackgroundColor = "#999999";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.quoteColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "white-on-black":
				this.backgroundColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.nameBackgroundColor = "#999999";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.quoteColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "forum-black-on-white":
				this.backgroundColor = "#962a51";
				this.foregroundColor = "#ffffff";
				this.nameBackgroundColor = "#999999";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.quoteColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "forum-white-on-purple":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#962a51";
				this.nameBackgroundColor = "#999999";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#962a51";
				this.iconColor = "#962a51";
				this.terciaryTextColor = "#962a51";
				this.quoteColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.backgroundColor = "#00ad43";
				this.foregroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.quoteColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#00ad43";
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#00ad43";
				this.iconColor = "#00ad43";
				this.terciaryTextColor = "#00ad43";
				this.quoteColor = "#00ad43";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#21274e";
				this.nameBackgroundColor = "#9796ca";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#21274e";
				this.iconColor = "#21274e";
				this.terciaryTextColor = "#21274e";
				this.quoteColor = "#21274e";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.backgroundColor = "#21274e";
				this.foregroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.quoteColor = "#9796ca";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "louny-spolecne-black-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#e2d7a9";
				this.nameBackgroundColor = "#e2d7a9";
				this.nameTextColor = "#3e2a5b";
				this.primaryTextColor = "#3e2a5b";
				this.iconColor = "#3e2a5b";
				this.terciaryTextColor = "#3e2a5b";
				this.quoteColor = "#3e2a5b";
				this.primaryTextHighlightColor = "#e2d7a9";
				
				break;
			case "louny-spolecne-white-on-purple":
				this.backgroundColor = "#3e2a5b";
				this.foregroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#e2d7a9";
				this.iconColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.quoteColor = "#9796ca";
				this.primaryTextHighlightColor = "#e2d7a9";
				
				break;
			case "litomerice-blue-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#123172";
				this.nameBackgroundColor = "#afe87e";
				this.nameTextColor = "#123172";
				this.primaryTextColor = "#123172";
				this.iconColor = "#123172";
				this.terciaryTextColor = "#123172";
				this.quoteColor = "#123172";
				this.primaryTextHighlightColor = "#afe87e";
				
				break;
			case "litomerice-white-on-blue":
				this.backgroundColor = "#123172";
				this.foregroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#123172";
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.quoteColor = "#afe87e";
				this.primaryTextHighlightColor = "#afe87e";
				
				break;
			case "stranane-gray-on-yellow":
				this.backgroundColor = "#ffd500";
				this.foregroundColor = "#4d4d4d";
				this.nameBackgroundColor = "#4d4d4d";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#4d4d4d";
				this.iconColor = "#4d4d4d";
				this.terciaryTextColor = "#4d4d4d";
				this.quoteColor = "#50c450";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "stranane-yellow-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#ffd500";
				this.nameBackgroundColor = "#ffd500";
				this.nameTextColor = "#4d4d4d";
				this.primaryTextColor = "#ffd500";
				this.iconColor = "#ffd500";
				this.terciaryTextColor = "#ffd500";
				this.quoteColor = "#50c450";
				this.primaryTextHighlightColor = "#4d4d4d";
				
				break;
			case "stranane-white-on-yellow":
				this.backgroundColor = "#ffd500";
				this.foregroundColor = "#ffffff";
				this.nameBackgroundColor = "#4d4d4d";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#4d4d4d";
				this.iconColor = "#4d4d4d";
				this.terciaryTextColor = "#4d4d4d";
				this.quoteColor = "#50c450";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-black-on-yellow":
				this.backgroundColor = "#ffd500";
				this.foregroundColor = "#000000";
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.quoteColor = "#50c450";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-yellow-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#ffd500";
				this.nameBackgroundColor = "#50c450";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#ffd500";
				this.iconColor = "#ffd500";
				this.terciaryTextColor = "#ffd500";
				this.quoteColor = "#ffd500";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.backgroundColor = "#ffd500";
				this.foregroundColor = "#ffffff";
				this.nameBackgroundColor = "#50c450";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#ffffff";
				this.iconColor = "#ffffff";
				this.terciaryTextColor = "#ffffff";
				this.quoteColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "ujezd-green-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#8ed4a3";
				this.nameBackgroundColor = "#ffdd55";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#8ed4a3";
				this.iconColor = "#8ed4a3";
				this.terciaryTextColor = "#8ed4a3";
				this.quoteColor = "#8ed4a3";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "ujezd-white-on-green":
				this.backgroundColor = "#8ed4a3";
				this.foregroundColor = "#000000";
				this.nameBackgroundColor = "#ffdd55";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.quoteColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "cssd-red-on-black":
				this.backgroundColor = "#000000";
				this.foregroundColor = "#e63812";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#e63812";
				this.iconColor = "#e63812";
				this.terciaryTextColor = "#e63812";
				this.quoteColor = "#e63812";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "cssd-black-on-red":
				this.backgroundColor = "#e63812";
				this.foregroundColor = "#000000";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.quoteColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "jilemnice-purple-on-black":
				this.backgroundColor = "#000000";
				this.foregroundColor = "#6e1646";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#6e1646";
				this.iconColor = "#6e1646";
				this.terciaryTextColor = "#6e1646";
				this.quoteColor = "#6e1646";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "jilemnice-black-on-purple":
				this.backgroundColor = "#6e1646";
				this.foregroundColor = "#000000";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.quoteColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-white-on-green":
				this.backgroundColor = "#a9ce2d";
				this.foregroundColor = "#000000";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#000000";
				this.iconColor = "#000000";
				this.terciaryTextColor = "#000000";
				this.quoteColor = "#000000";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-white":
				this.backgroundColor = "#ffffff";
				this.foregroundColor = "#a9ce2d";
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.primaryTextColor = "#a9ce2d";
				this.iconColor = "#a9ce2d";
				this.terciaryTextColor = "#a9ce2d";
				this.quoteColor = "#a9ce2d";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "novarole-green-on-black":
				this.backgroundColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.primaryTextColor = "#a9ce2d";
				this.iconColor = "#a9ce2d";
				this.terciaryTextColor = "#a9ce2d";
				this.quoteColor = "#a9ce2d";
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				this.nameBackgroundColor = "#fde119";
				this.nameTextColor = "#000000";
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.primaryTextColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
