class ZelenaBystrc extends Template {
	description = "Určeno pro sociální sítě.";
	
	changeableAttributes = [
		"primaryText",
		"secondaryText",
		"primaryImage",
		"primaryImagePosition",
		"primaryColorScheme"
	];

	changeableColors = [
	];
	
	primaryColorSchemes = [
		"white-on-black"
	];
	
	primaryText = "";
	secondaryText = "";
	
	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}
		
		this.redrawing = true;
		
		//// Constants
		// Bottom bar
		const bottomBarHeight = this.canvas.height * 0.13;
		const bottomBarLogosWidth = this.canvas.width * 0.85;
		
		// Secondary text bar
		const secondaryTextFontSize = this.canvas.height * 0.05;
		const secondaryTextPadding = this.canvas.height * 0.05;
		const secondaryTextPaddingInner = secondaryTextPadding * 0.4;
		const secondaryTextMaxWidth = this.canvas.width * 0.38;
		const secondaryTextMaxLines = 3;
		const secondaryTextPointImageWidth = this.canvas.width * 0.035;
		const secondaryTextPointImagePaddingSide = this.canvas.width * 0.025;
		
		// Primary text + bar
		let primaryTextFontSize = this.canvas.height * 0.09;
		const primaryTextMaxWidth = this.canvas.width * 0.95;
		const primaryTextBarHeight = this.canvas.height * 0.13;
		
		//// Drawing
		// Clear the canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Create image
		if (this.primaryImage !== null) {
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D

			const imageScaleX = this.canvas.width / this.primaryImage.width;
			const imageScaleY = this.canvas.height / this.primaryImage.height;

			const imageScale = Math.max(imageScaleX, imageScaleY) * this.primaryImageZoom;
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - this.primaryImage.width * imageScale) / 2 + this.primaryImageX * this.primaryImageZoom,
				(this.canvas.height - this.primaryImage.height * imageScale) / 2 + this.primaryImageY * this.primaryImageZoom,
			);
			this.context.drawImage(
				this.primaryImage,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
		}
		
		// Create bottom bar
		this.context.fillStyle = this.bottomBarColor;
		
		this.context.fillRect(
			0, this.canvas.height - bottomBarHeight,
			this.canvas.width, bottomBarHeight
		);
		
		const classRef = this;
		
		const bottomBarLogoLoadPromise = new Promise(
			resolve => {
				const logoImage = new Image();
				
				logoImage.onload = function() {
					const imageHeight = this.height * (bottomBarLogosWidth / this.width);
					
					classRef.context.drawImage(
						this,
						(classRef.canvas.width - bottomBarLogosWidth) / 2, classRef.canvas.height - (bottomBarHeight + imageHeight) / 2,
						bottomBarLogosWidth, imageHeight
					);
					
					resolve();
				}
				
				logoImage.src = "/static/images/bystrc-bottom.png";
			}
		);
		
		await bottomBarLogoLoadPromise;
		
		//// Secondary text
		// Process points
		let secondaryTextBarHeight = 0;
		
		const splitPoints = this.secondaryText.split("\n", 4);
		let points = [];
		let pointHeights = [];
		let pointFontSizes = [];
		const originalPointTextMaxHeight = (secondaryTextFontSize * secondaryTextMaxLines);
		
		for (const point of splitPoints) {
			let pointFontSize = secondaryTextFontSize;
			let pointLines = [];
			let pointMaxLines = secondaryTextMaxLines;
			
			do {
				this.context.font = `${this.primaryFontStyle} ${pointFontSize}px ${this.primaryFont}`;
				
				pointLines = splitStringIntoLines(
					this.context,
					point,
					secondaryTextMaxWidth,
					pointMaxLines,
					true
				);
				
				if (pointLines.length > pointMaxLines) {
					pointFontSize -= 2;
				}
				
				if (((pointMaxLines + 1) * pointFontSize) < originalPointTextMaxHeight) {
					pointMaxLines += 1;
				}
			} while (pointLines.length > pointMaxLines);
			
			if (pointLines[0].length === 0) {
				continue;
			}
			
			let pointHeight = 0;
			
			for (const pointLine of pointLines) {
				pointHeight += pointFontSize;
			}
			
			points.push(pointLines);
			pointHeights.push(pointHeight);
			pointFontSizes.push(pointFontSize);
		}
		
		switch (pointHeights.length) {
			case 0:
				secondaryTextBarHeight = 0;
				break;
			case 1:
				secondaryTextBarHeight = pointHeights[0] + secondaryTextPadding * 2;
				break;
			case 2:
				secondaryTextBarHeight = Math.max(
					(pointHeights[0] + secondaryTextPadding * 2),
					(pointHeights[1] + secondaryTextPadding * 2)
				);
				break;
			case 3:
				secondaryTextBarHeight = pointHeights[0] + pointHeights[1] + secondaryTextPaddingInner + secondaryTextPadding * 2;
				break;
			case 4:
				secondaryTextBarHeight = Math.max(
					(pointHeights[0] + pointHeights[1] + secondaryTextPadding * 2 + secondaryTextPaddingInner),
					(pointHeights[2] + pointHeights[3] + secondaryTextPadding * 2 + secondaryTextPaddingInner)
				);
				break;
		}
		
		// Create background bar
		if (pointHeights.length !== 0) {
			this.context.fillStyle = this.backgroundColor;
			
			this.context.fillRect(
				0, this.canvas.height - bottomBarHeight - secondaryTextBarHeight,
				this.canvas.width, secondaryTextBarHeight
			);
			
			// Draw text
			let pointPosition = 0;
			let pointY = this.canvas.height;
			
			let pointImage = new Image();
			let pointImageHeight = 0;
			const pointImageRGB = hexToRgb(this.foregroundColor);
			
			const pointImageLoadPromise = new Promise(
				resolve => {
					pointImage.onload = function() {
						pointImageHeight = this.height * (secondaryTextPointImageWidth / this.width);
						
						pointImage = colorizeImage(
							pointImage,
							secondaryTextPointImageWidth, pointImageHeight,
							pointImageRGB.r, pointImageRGB.g, pointImageRGB.b
						);
						
						resolve();
					}
					
					pointImage.src = "/static/images/bystrc-point.png";
				}
			);
			
			await pointImageLoadPromise;
			
			this.context.fillStyle = this.secondaryTextColor;
			
			for (const point of points) {
				let pointImageX = 0;
				let pointY = 0;
				
				switch (pointPosition) {
					case 0: {
						pointImageX = secondaryTextPointImagePaddingSide;
						
						if (points.length === 2 && pointHeights[1] > pointHeights[pointPosition]) {
							pointHeights[pointPosition] = pointHeights[1];
						}
						
						pointY = (
							this.canvas.height
							- bottomBarHeight
							- pointHeights[pointPosition]
							- secondaryTextPadding
						);
						
						if (points.length > 2) {
							pointY -= pointHeights[pointPosition + 1] + secondaryTextPaddingInner;
						}
						
						break;
					}
					case 1: {
						if (points.length === 2) {
							pointY = (
								this.canvas.height
								- bottomBarHeight
								- pointHeights[pointPosition - 1]
								- secondaryTextPadding
							);
							
							pointImageX = (
								this.canvas.width
								- secondaryTextMaxWidth
								- secondaryTextPointImagePaddingSide * 2
								- secondaryTextPointImageWidth
							);
						} else {
							pointImageX = secondaryTextPointImagePaddingSide;
							
							pointY = (
								this.canvas.height
								- bottomBarHeight
								- pointHeights[pointPosition]
								- secondaryTextPadding
							);
						}
						
						break;
					}
					case 2: {
						pointImageX = (
							this.canvas.width
							- secondaryTextMaxWidth
							- secondaryTextPointImagePaddingSide * 2
							- secondaryTextPointImageWidth
						);
						
						pointY = (
							this.canvas.height
							- bottomBarHeight
							- secondaryTextBarHeight
							+ secondaryTextPadding
						);
						
						break;
					}
					case 3: {
						pointImageX = (
							this.canvas.width
							- secondaryTextMaxWidth
							- secondaryTextPointImagePaddingSide * 2
							- secondaryTextPointImageWidth
						);
						
						pointY = (
							this.canvas.height
							- bottomBarHeight
							- secondaryTextBarHeight
							+ secondaryTextPadding
							+ pointHeights[pointPosition - 1]
							+ secondaryTextPaddingInner
						);
					}
				}
				
				this.context.drawImage(
					pointImage,
					pointImageX, pointY,
					secondaryTextPointImageWidth, pointImageHeight
				);
				
				this.context.textBaseline = "top";
				this.context.textAlign = "left";
				
				for (const line of point) {
					this.context.font = `${this.primaryFontStyle} ${pointFontSizes[pointPosition]}px ${this.primaryFont}`;
					
					this.context.fillText(
						line.join(" "),
						(
							pointImageX
							+ secondaryTextPointImageWidth
							+ secondaryTextPointImagePaddingSide
						),
						pointY
					);
					
					pointY += pointFontSizes[pointPosition];
				}
				
				pointPosition++;
			}
		}
		
		// Create primary text bar
		this.context.fillStyle = this.foregroundColor;
		
		this.context.fillRect(
			0, this.canvas.height - bottomBarHeight - secondaryTextBarHeight - primaryTextBarHeight,
			this.canvas.width, primaryTextBarHeight
		);
		
		// Create primary text
		do {
			this.context.font = `${primaryTextFontSize}px 'Bebas Neue'`;
			
			if ((this.context.measureText(this.primaryText).width) > primaryTextMaxWidth) {
				primaryTextFontSize -= 2;
			}
		} while ((this.context.measureText(this.primaryText).width) > primaryTextMaxWidth);
		
		this.context.fillStyle = this.primaryTextColor;
		
		this.context.shadowOffsetX =
		this.context.shadowOffsetY = primaryTextFontSize * 0.075;
		
		this.context.shadowColor = this.primaryTextShadeColor;
		this.context.shadowBlur = primaryTextFontSize * 0;
		this.context.textAlign = "center";
		this.context.textBaseline = "middle";
		
		this.context.fillText(
			this.primaryText,
			this.canvas.width / 2,
			(
				this.canvas.height
				- bottomBarHeight
				- secondaryTextBarHeight
				- primaryTextBarHeight / 2.3
			)
		);
		
		this.context.textBaseline = "alphabetic";
		
		this.context.shadowColor = "rgba(0, 0, 0, 0)";
		
		// Create requester text
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();
			
			this.context.translate(this.canvas.width - 1, 0);
			
			this.context.rotate(3 * Math.PI / 2);
			
			let requesterFontSize = Math.ceil(this.canvas.width * 0.0175);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;
			
			this.context.textAlign = "left";
			
			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		this.primaryTextColor = "#ffffff";
		this.primaryTextShadeColor = "#08291c";
		this.foregroundColor = "#2d7850";
		
		this.pointColor = this.foregroundColor;
		this.secondaryTextColor = "#231f20";
		this.backgroundColor = "#ffffff";
		
		this.terciaryTextColor = "#ffffff";
		this.bottomBarColor = "#231f20";
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
