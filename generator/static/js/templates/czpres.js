class CzPres extends Template {
	description = "Určeno pro sociální sítě.";
	
	changeableAttributes = [
		"iconImage",
		"dateText",
		"primaryText",
		"secondaryText",
		"terciaryText",
		"primaryColorScheme"
	];

	changeableColors = [
		"backgroundGradientColor1",
		"backgroundGradientColor2",
		"dateTextColor",
		"primaryTextColor",
		"secondaryTextColor",
		"terciaryTextColor",
		"iconColor",
		"urlTextColor"
	];
	
	primaryColorSchemes = [
		"white-on-black"
	];
	
	
	dateText = "";
	secondaryText = "";
	terciaryText = "";
	
	iconSource = null;
	iconImage = null;
	
	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}
		
		this.redrawing = true;
		
		//// Constants
		// Date text
		let dateTextFontSize = this.canvas.height * 0.042;
		const dateTextOffsetTop = this.canvas.height * 0.07;
		
		// Date rectangle
		const dateRectanglePadding = this.canvas.width * 0.014;
		
		// Primary text
		let primaryTextFontSize = this.canvas.height * 0.13;
		const primaryTextOffsetSide = this.canvas.width * 0.07;
		const primaryTextOffsetTop = this.canvas.height * 0;
		const primaryTextMaxWidth = this.canvas.width * 0.55;
		
		// Secondary text
		let secondaryTextFontSize = this.canvas.height * 0.08;
		const secondaryTextOffsetTop = 0;
		
		// Icon
		const iconWidth = this.canvas.width * 0.45;
		const iconInnerOffset = this.canvas.width * 0.1;
		
		/// Points
		// Text
		const pointTextFontSize = this.canvas.height * 0.043;
		const pointTextMaxLines = 2;
		// Icons
		const pointIconWidthHeight = this.canvas.height * 0.05;
		const pointIconPaddingSide = this.canvas.width * 0.03;
		// Shared
		const pointOffsetTop = this.canvas.height * 0.055;
		const pointMaxCount = 5;
		
		// Logo
		const logoWidth = this.canvas.width * 0.35;
		const logoOffsetTop = this.canvas.height * 0.07;
		const logoOffsetSide = this.canvas.width * 0.09;
		
		//// Drawing
		// Background gradient
		const backgroundGradient = this.context.createLinearGradient(
			this.canvas.width / 2, 0,
			this.canvas.width / 2, this.canvas.height * 0.85
		);
		
		backgroundGradient.addColorStop(0, this.backgroundGradientColor1);
		backgroundGradient.addColorStop(1, this.backgroundGradientColor2);
		
		this.context.fillStyle = backgroundGradient;
		
		this.context.fillRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		const classRef = this;
		
		// Icon
		function drawIcon(image) {
			const iconHeight = image.height * (iconWidth / image.width);
			const iconRGB = hexToRgb(classRef.iconColor);
			
			classRef.context.drawImage(
				colorizeImage(
					image,
					iconWidth, iconHeight,
					iconRGB.r, iconRGB.g, iconRGB.b
				),
				classRef.canvas.width - iconWidth + iconInnerOffset,
				-iconInnerOffset,
				iconWidth, iconHeight
			);
		}
		
		if (this.iconImage !== null) {
			drawIcon(this.iconImage);
		} else {
			if (this.iconSource === null) {
				this.iconSource = "static/images/czpres-flower.png";
			}
			
			const iconImageLoadPromise = new Promise(
				resolve => {
					const iconImage = new Image();
					
					iconImage.onload = function() {
						drawIcon(this);
						resolve();
					}
					
					iconImage.src = classRef.iconSource;
				}
			);
			
			await iconImageLoadPromise;
		}
		
		// Date text
		if (this.dateText !== "") {
			const helperCanvas = document.createElement("canvas");
			
			do {
				this.context.font = `${dateTextFontSize}px 'Bebas Neue'`;
				
				if (this.context.measureText(this.dateText).width > primaryTextMaxWidth) {
					dateTextFontSize -= 2;
				}
			} while (this.context.measureText(this.dateText).width > primaryTextMaxWidth);
			
			helperCanvas.width = (
				this.context.measureText(this.dateText).width
				+ 2 * dateRectanglePadding
			);
			helperCanvas.height = (
				dateTextFontSize
				+ 2 * dateRectanglePadding
			);
			
			const helperContext = helperCanvas.getContext("2d");
			
			helperContext.fillStyle = this.dateTextColor;
			helperContext.font = `${dateTextFontSize}px 'Bebas Neue'`;
			
			helperContext.fillRect(
				0, 0,
				helperCanvas.width, helperCanvas.height
			);
			
			helperContext.globalCompositeOperation = "xor";
			helperContext.textAlign = "center";
			
			helperContext.fillText(
				this.dateText,
				helperCanvas.width / 2,
				helperCanvas.height - dateRectanglePadding * 1.5
			);
			
			this.context.drawImage(
				helperCanvas,
				primaryTextOffsetSide, dateTextOffsetTop,
			);
		}
		
		// Primary text
		if (this.primaryText !== "") {
			do {
				this.context.font = `${primaryTextFontSize}px 'Bebas Neue'`;
				
				if (this.context.measureText(this.primaryText).width > primaryTextMaxWidth) {
					primaryTextFontSize -= 2;
				}
			} while (this.context.measureText(this.primaryText).width > primaryTextMaxWidth);
			
			const primaryTextY = (
				dateTextOffsetTop
				+ (
					(this.dateText !== "") ?
					(
						dateTextFontSize
						+ dateRectanglePadding * 2
						+ primaryTextOffsetTop
					) : 0
				)
				+ primaryTextFontSize
			);
			const pointTextLineX = (
				primaryTextOffsetSide
				+ pointIconWidthHeight
				+ pointIconPaddingSide
			);
			
			this.context.fillStyle = this.primaryTextColor;
			
			this.context.fillText(
				this.primaryText,
				primaryTextOffsetSide,
				primaryTextY
			);
			
			// Secondary text
			if (this.secondaryText !== "") {
				do {
					this.context.font = `${secondaryTextFontSize}px 'Bebas Neue'`;
					
					if (this.context.measureText(this.secondaryText).width > primaryTextMaxWidth) {
						secondaryTextFontSize -= 2;
					}
				} while (this.context.measureText(this.secondaryText).width > primaryTextMaxWidth);
				
				this.context.fillStyle = this.secondaryTextColor;
				
				this.context.fillText(
					this.secondaryText,
					primaryTextOffsetSide,
					(
						primaryTextY
						+ secondaryTextOffsetTop
						+ secondaryTextFontSize
					)
				);
			}
			
			// Points
			if (this.terciaryText !== "") {
				const iconRGB = hexToRgb(this.backgroundGradientColor1);
				const points = this.terciaryText.split(/\n|\r\n/, pointMaxCount);
				
				let currentPointTextLineY = (
					primaryTextY
					+ (
						(classRef.secondaryText !== "") ?
						(
							secondaryTextOffsetTop
							+ secondaryTextFontSize
						) : 0
					)
					+ pointOffsetTop
				);
				const pointTextMaxWidth = (
					this.canvas.width
					- pointIconWidthHeight
					- pointIconPaddingSide
					- primaryTextOffsetSide * 1.5
				);
				
				for (const point of points) {
					const pointIconLoadPromise = new Promise(
						resolve => {
							const icon = new Image();
							
							icon.onload = function() {
								classRef.context.drawImage(
									colorizeImage(
										this,
										pointIconWidthHeight, pointIconWidthHeight,
										iconRGB.r, iconRGB.g, iconRGB.b
									),
									primaryTextOffsetSide, currentPointTextLineY,
									pointIconWidthHeight, pointIconWidthHeight
								);
								
								resolve();
							}
							
							icon.src = "static/images/icons/Fajfka-orig.png";
						}
					);
					
					await pointIconLoadPromise;
					
					let pointTextLines = null;
					const originalPointTextFontSize = pointTextFontSize;
					let currentPointTextFontSize = pointTextFontSize;
					
					do {
						this.context.font = `${currentPointTextFontSize}px 'Anton'`;
						
						pointTextLines = splitStringIntoLines(
							this.context,
							point,
							pointTextMaxWidth,
							pointTextMaxLines,
							true
						)
						
						if (
							pointTextLines.length > pointTextMaxLines
							&& (
								pointTextLines.length * currentPointTextFontSize
								> pointTextMaxLines * originalPointTextFontSize
							)
						) {
							currentPointTextFontSize -= 2;
						}
					} while (
						pointTextLines.length > pointTextMaxLines
						&& (
							pointTextLines.length * currentPointTextFontSize
							> pointTextMaxLines * originalPointTextFontSize
						)
					);
					
					this.context.fillStyle = this.terciaryTextColor;
					this.context.textBaseline = "top";
					
					currentPointTextLineY += currentPointTextFontSize * 0.25;
					
					for (const line of pointTextLines) {
						this.context.fillText(
							line.join(" "),
							pointTextLineX,
							currentPointTextLineY
						);
						
						currentPointTextLineY += currentPointTextFontSize * 1.2;
					}
					
					if (pointTextLines.length === 1) {
						currentPointTextLineY += currentPointTextFontSize * 0.2
					}
					
					this.context.textBaseline = "alphabetic";
				}
			}
		}
		
		// Presidency logo
		const presidencyLogoLoadPromise = new Promise(
			resolve => {
				const logo = new Image();
				
				logo.onload = function() {
					const logoHeight = this.height * (logoWidth / this.width);
					const logoRGB = hexToRgb(classRef.urlTextColor);
					
					classRef.context.drawImage(
						colorizeImage(
							this,
							logoWidth, logoHeight,
							logoRGB.r, logoRGB.g, logoRGB.b
						),
						classRef.canvas.width - logoOffsetSide - logoWidth, classRef.canvas.height - logoOffsetTop - logoHeight,
						logoWidth, logoHeight
					);
					
					resolve();
				}
				
				logo.src = "static/images/czpres-logo.png";
			}
		);
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();
			
			this.context.translate(this.canvas.width - 1, 0);
			
			this.context.rotate(3 * Math.PI / 2);
			
			let requesterFontSize = Math.ceil(this.canvas.width * 0.0175);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;
			
			this.context.textAlign = "left";
			
			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Text
	async setDateText(text, skipRedraw = false) {
		this.dateText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setTerciaryText(text, skipRedraw = false) {
		this.terciaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Icon
	async setIconSource(url, skipRedraw = false) {
		this.iconSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setIconImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.iconImage = new Image();
					
					classRef.iconImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.iconImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
	
	async resetIconImage(skipRedraw = false) {
		this.iconImage = null;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		this.backgroundGradientColor1 = "#3db7ac";
		this.backgroundGradientColor2 = "#000000";
		
		this.iconColor = "#26746d";
		
		this.dateTextColor = "#000000";
		
		this.primaryTextColor = "#ffffff";
		this.secondaryTextColor = "#000000";
		this.terciaryTextColor = "#ffffff";
		
		this.urlTextColor = "#3db7ac";
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
