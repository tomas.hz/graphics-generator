class IvanShortClaim extends Template {
	description = "Určeno pro sociální sítě.";
	
	changeableAttributes = [
		"primaryImage",
		"iconImage",
		"primaryText",
		"secondaryText",
		"nameText",
		"underNameText",
		"primaryImagePosition",
		"primaryColorScheme"
	];

	changeableColors = [
		"primaryTextColor",
		"requesterTextColor",
		"secondaryTextColor",
		"nameBackgroundColor",
		"underNameTextColor",
		"iconColor",
		"quoteColor"
	];
	
	primaryColorSchemes = [
		"white-on-black"
	];
	
	secondaryText = "";
	nameText = "";
	underNameText = "";
	
	iconSource = null;
	iconImage = null;
	
	imageFlipped = false;
	
	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		//// Constants
		// Quote
		const quoteOffsetSide = this.canvas.width * 0.05;
		const quoteOffsetTop = this.canvas.height * 0.15;
		const quoteWidth = this.canvas.width * 0.12;
		
		// Primary text
		let primaryTextFontSize = this.canvas.height * 0.088;
		const primaryTextMaxLines = 2;
		const primaryTextOffsetTop = this.canvas.height * 0.35;
		const primaryTextOffsetSide = this.canvas.width * 0.13;
		const primaryTextMaxWidth = this.canvas.width * 0.5;
		
		// Secondary text
		let secondaryTextFontSize = this.canvas.height * 0.06;
		const secondaryTextMaxLines = 2;
		const secondaryTextOffsetTop = this.canvas.height * 0.11;
		
		// Icon image
		const iconImageMaxWidthHeight = this.canvas.width * 0.2;
		const iconImageOffsetBottom = this.canvas.height * 0.1;
		
		// Name text
		let nameTextFontSize = this.canvas.height * 0.085;
		const nameTextOffsetBottom = this.canvas.height * 0.16;
		const nameTextOffsetSide = this.canvas.width * 0.1;
		const nameTextMaxWidth = this.canvas.width * 0.32;
		
		// Name rectangle
		const nameRectanglePaddingSides = this.canvas.width * 0.03;
		const nameRectanglePaddingTopBottom = this.canvas.height * 0.005;
		
		// Under name text
		let underNameTextFontSize = this.canvas.height * 0.02;
		const underNameTextOffsetBottom = this.canvas.height * 0.13;
		const underNameTextMaxLines = 3;
		
		// Clear canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		const classRef = this;
		
		// Create image
		if (this.primaryImage !== null) {
			const helperCanvas = document.createElement("canvas");
			
			helperCanvas.width = this.primaryImage.width;
			helperCanvas.height = this.primaryImage.height;
			
			const helperContext = helperCanvas.getContext("2d");
			
			if (this.imageFlipped) {
				helperContext.scale(-1, 1);
				
				helperContext.drawImage(
					this.primaryImage,
					-this.primaryImage.width, 0,
					this.primaryImage.width, this.primaryImage.height
				);
			} else {
				helperContext.drawImage(
					this.primaryImage,
					0, 0,
					this.primaryImage.width, this.primaryImage.height
				);
			}
			
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D
			
			const imageScaleX = this.canvas.width / helperCanvas.width;
			const imageScaleY = this.canvas.height / helperCanvas.height;
			
			const imageScale = Math.max(imageScaleX, imageScaleY) * this.primaryImageZoom;
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - helperCanvas.width * imageScale) / 2 + this.primaryImageX * this.primaryImageZoom,
				(this.canvas.height - helperCanvas.height * imageScale) / 2 + this.primaryImageY * this.primaryImageZoom,
			);
			
			this.context.drawImage(
				helperCanvas,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
		}
		
		// Create gradients
		const gradientLeftRight = this.context.createLinearGradient(
			0, this.canvas.height / 2,
			this.canvas.width, this.canvas.height / 2
		);
		
		gradientLeftRight.addColorStop(0, this.backgroundGradientColor);
		gradientLeftRight.addColorStop(1, "rgba(0, 0, 0, 0)");
		
		this.context.fillStyle = gradientLeftRight;
		this.context.fillRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		const gradientBottomTop = this.context.createLinearGradient(
			this.canvas.width / 2, this.canvas.height,
			this.canvas.width / 2, 0
		);
		
		gradientBottomTop.addColorStop(0, this.backgroundGradientColor + "7f"); // Semi-transparent
		gradientBottomTop.addColorStop(1, "rgba(0, 0, 0, 0)");
		
		this.context.fillStyle = gradientBottomTop;
		this.context.fillRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Create quote
		const quoteImageLoadPromise = new Promise(
			resolve => {
				const quoteImage = new Image();
				
				quoteImage.onload = function() {
					const quoteHeight = this.height * (quoteWidth / this.width);
					
					const quoteRGB = hexToRgb(classRef.quoteColor);
					
					classRef.context.drawImage(
						colorizeImage(
							this,
							quoteWidth, quoteHeight,
							quoteRGB.r,
							quoteRGB.g,
							quoteRGB.b
						),
						quoteOffsetSide, quoteOffsetTop,
						quoteWidth, quoteHeight
					);
					
					resolve();
				}
				
				quoteImage.src = "static/images/quote_nontransparent_short.png";
			}
		);
		
		await quoteImageLoadPromise;
		
		// Primary text
		if (this.primaryText !== "") {
			let primaryTextLines = null;
			const originalPrimaryTextFontSize = primaryTextFontSize;
			
			do {
				this.context.font = `${primaryTextFontSize}px 'Anton'`;
				
				primaryTextLines = splitStringIntoLines(
					this.context,
					this.primaryText,
					primaryTextMaxWidth,
					primaryTextMaxLines,
					true
				)
				
				if (
					primaryTextLines.length > primaryTextMaxLines
					&& (
						primaryTextLines.length * primaryTextFontSize
						> primaryTextMaxLines * originalPrimaryTextFontSize
					)
				) {
					primaryTextFontSize -= 2;
				}
			} while (
				primaryTextLines.length > primaryTextMaxLines
				&& (
					primaryTextLines.length * primaryTextFontSize
					> primaryTextMaxLines * originalPrimaryTextFontSize
				)
			);
			
			this.context.fillStyle = this.primaryTextColor;
			let currentPrimaryLineY = primaryTextOffsetTop;
			
			for (const line of primaryTextLines) {
				this.context.fillText(
					line.join(" "),
					primaryTextOffsetSide,
					currentPrimaryLineY
				);
				
				currentPrimaryLineY += primaryTextFontSize * 1.15;
			}
			
			currentPrimaryLineY -= primaryTextFontSize * 1.15;
			
			// Secondary text
			if (this.secondaryText !== "") {
				let secondaryTextLines = null;
				const originalSecondaryTextFontSize = secondaryTextFontSize;
				
				do {
					this.context.font = `${secondaryTextFontSize}px 'Bebas Neue'`;
					
					secondaryTextLines = splitStringIntoLines(
						this.context,
						this.secondaryText,
						primaryTextMaxWidth,
						secondaryTextMaxLines,
						true
					)
					
					if (
						secondaryTextLines.length > secondaryTextMaxLines
						&& (
							secondaryTextLines.length * secondaryTextFontSize
							> secondaryTextMaxLines * originalSecondaryTextFontSize
						)
					) {
						secondaryTextFontSize -= 2;
					}
				} while (
					secondaryTextLines.length > secondaryTextMaxLines
					&& (
						secondaryTextLines.length * secondaryTextFontSize
						> secondaryTextMaxLines * originalSecondaryTextFontSize
					)
				);
				
				this.context.fillStyle = this.secondaryTextColor;
				let currentSecondaryLineY = currentPrimaryLineY + secondaryTextOffsetTop;
				
				for (const line of secondaryTextLines) {
					this.context.fillText(
						line.join(" "),
						primaryTextOffsetSide,
						currentSecondaryLineY
					);
					
					currentSecondaryLineY += secondaryTextFontSize;
				}
			}
		}
		
		// Name text
		if (this.nameText !== "") {
			do {
				this.context.font = `${nameTextFontSize}px 'Bebas Neue'`;
				
				if (this.context.measureText(this.nameText).width > nameTextMaxWidth) {
					nameTextFontSize -= 2;
				}
			} while (this.context.measureText(this.nameText).width > nameTextMaxWidth);
			
			const helperCanvas = document.createElement("canvas");
			helperCanvas.width = (
				nameRectanglePaddingSides * 2
				+ nameTextMaxWidth
			);
			helperCanvas.height = (
				nameRectanglePaddingTopBottom * 2
				+ nameTextFontSize
			);
			
			const helperContext = helperCanvas.getContext("2d");
			
			helperContext.fillStyle = classRef.nameBackgroundColor;
			
			helperContext.fillRect(
				0, 0,
				helperCanvas.width, helperCanvas.height
			);
			
			helperContext.font = `${nameTextFontSize}px 'Bebas Neue'`;
			helperContext.textAlign = "center";
			helperContext.globalCompositeOperation = "xor";
			
			helperContext.fillText(
				classRef.nameText,
				helperContext.canvas.width / 2,
				helperContext.canvas.height - nameRectanglePaddingTopBottom * 3
			);
			
			classRef.context.drawImage(
				helperCanvas,
				classRef.canvas.width - nameTextOffsetSide - helperCanvas.width + nameRectanglePaddingSides,
				classRef.canvas.height - nameTextOffsetBottom - helperCanvas.height
			);
		}
		
		// Under name text
		if (this.underNameText !== "") {
			let underNameTextLines = null;
			const originalUnderNameTextMaxFontSize = underNameTextFontSize;
			
			do {
				this.context.font = `${underNameTextFontSize}px 'Roboto Condensed'`;
				
				underNameTextLines = splitStringIntoLines(
					this.context,
					this.underNameText,
					nameTextMaxWidth,
					underNameTextMaxLines,
					true
				)
				
				if (
					underNameTextLines.length > underNameTextMaxLines
					&& (
						underNameTextLines.length * underNameTextFontSize
						> underNameTextMaxLines * originalUnderNameTextMaxFontSize
					)
				) {
					underNameTextFontSize -= 2;
				}
			} while (
				underNameTextLines.length > underNameTextMaxLines
				&& (
					underNameTextLines.length * underNameTextFontSize
					> underNameTextMaxLines * originalUnderNameTextMaxFontSize
				)
			);
			
			let currentUnderNameTextLineY = this.canvas.height - underNameTextOffsetBottom;
			this.context.fillStyle = this.underNameTextColor;
			
			this.context.textAlign = "center";
			
			for (const line of underNameTextLines) {
				this.context.fillText(
					line.join(" "),
					this.canvas.width - nameTextOffsetSide - nameTextMaxWidth / 2,
					currentUnderNameTextLineY
				);
				
				currentUnderNameTextLineY += underNameTextFontSize;
			}
			
			this.context.textAlign = "left";
		}
		
		// Icon
		function drawIcon(image) {
			let currentIconHeight = iconImageMaxWidthHeight;
			
			let iconWidth = (image.width * (currentIconHeight / image.height));
			
			if (iconWidth > currentIconHeight) {
				currentIconHeight = (image.height * (iconImageMaxWidthHeight / image.width));
				iconWidth = iconImageMaxWidthHeight;
			}
			
			const iconRGB = hexToRgb(classRef.iconColor);
			
			classRef.context.drawImage(
				colorizeImage(
					image,
					iconWidth, currentIconHeight,
					iconRGB.r,
					iconRGB.g,
					iconRGB.b
				),
				primaryTextOffsetSide + (iconImageMaxWidthHeight - iconWidth) / 2,
				classRef.canvas.height - iconImageOffsetBottom - currentIconHeight - (iconImageMaxWidthHeight - currentIconHeight) / 2
			);
		}
		
		if (this.iconImage !== null) {
			drawIcon(this.iconImage);
		} else if (this.iconSource !== null) {
			const iconSourceLoadPromise = new Promise(
				resolve => {
					const iconImage = new Image();
					
					iconImage.onload = function() {
						drawIcon(this);
						resolve();
					}
					
					iconImage.src = classRef.iconSource;
				}
			);
			
			await iconSourceLoadPromise;
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.0175);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Icon
	async setIconSource(url, skipRedraw = false) {
		this.iconSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setIconImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.iconImage = new Image();
					
					classRef.iconImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.iconImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
	
	async resetIconImage(skipRedraw = false) {
		this.iconImage = null;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Text
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setNameText(text, skipRedraw = false) {
		this.nameText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setUnderNameText(text, skipRedraw = false) {
		this.underNameText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		this.primaryTextColor =
		this.secondaryTextColor =
		this.nameBackgroundColor =
		this.underNameTextColor =
		this.requesterTextColor = "#ffffff";
		
		this.backgroundGradientColor = "#000000";
		
		this.quoteColor =
		this.iconColor = "#3ab8ac";
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
