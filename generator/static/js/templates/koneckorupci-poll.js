class KonecKorupciPoll extends Template {
	description = "Určeno pro ankety na Fejsbůčku.";
	
	changeableAttributes = [
		"logoImage",
		"primaryImage",
		"secondaryImage",
		"primaryColorScheme",
		"iconImage",
		"twoReactionSet"
	];

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];

	primaryTextHighlightColorSchemes = [
		"black-on-gold"
	];
	
	changeableColors = [
		"foregroundColor",
		"reactionTextColor",
		"requesterTextColor"
	];
	
	reactions = [
		{
			source: "",
			text: ""
		},
		{
			source: "",
			text: ""
		}
	];

	iconSource = null;
	iconImage = null;
	secondaryImage = null;
	
	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const primaryRectangleHeight = Math.ceil(this.canvas.height * 0.45);

		const logoHeight = Math.ceil(this.canvas.height * 0.065) * this.logoImageZoom;
		const logoSideOffset = Math.ceil(this.canvas.height * 0.1);
		const logoBottomOffset = Math.ceil(this.canvas.height * 0.05) - (
			logoHeight / this.logoImageZoom * (this.logoImageZoom - 1) / 2
		);
		
		const reactionImageOffsetSide = Math.ceil(this.canvas.width * 0.15);
		const reactionImageOffsetTop = Math.ceil(this.canvas.height * -0.115);
		const reactionImagePaddingInner = Math.ceil(this.canvas.width * 0.03);
		const reactionImageWidthHeight = Math.ceil(this.canvas.width * 0.23);
		
		let reactionTextFontSize = Math.ceil(this.canvas.height * 0.09);
		const reactionTextMaxWidth = Math.ceil(this.canvas.width * 0.3);
		const reactionTextOffsetTop = Math.ceil(this.canvas.height * 0.01);
		const reactionTextMaxLines = 2;
		
		const iconImageOffset = Math.ceil(this.canvas.height * 0.03);
		const iconOpacity = 0.125;

		const themeImageHeight = Math.ceil(this.canvas.height * 0.15);
		const themeBottomOffset = Math.ceil(this.canvas.height * 0.02);
		const themeSideOffset = themeBottomOffset * 2;
		
		const backgroundGradientHeight = Math.ceil(this.canvas.height * 0.2);
		
		// Clear the canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Set primary image
		if (this.primaryImage !== null) {
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D

			const imageScaleX = this.canvas.width / 2 / this.primaryImage.width;
			const imageScaleY = (this.canvas.height - primaryRectangleHeight) / this.primaryImage.height;

			const imageScale = Math.max(imageScaleX, imageScaleY);
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				0, 0
			);
			
			this.context.filter = "grayscale(1)";
			
			this.context.drawImage(
				this.primaryImage,
				0, 0
			);
			
			this.context.filter = "none";
			
			this.context.setTransform(); // Reset transformation
		}
		
		// Set secondary image
		if (this.secondaryImage !== null) {
			const imageScaleX = this.canvas.width / 2 / this.secondaryImage.width;
			const imageScaleY = (this.canvas.height - primaryRectangleHeight) / this.secondaryImage.height;

			const imageScale = Math.max(imageScaleX, imageScaleY);
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				this.canvas.width / 2, 0
			);
			
			this.context.drawImage(
				this.secondaryImage,
				0, 0
			);
			
			this.context.setTransform(); // Reset transformation
		}
		
		const classRef = this;
		
		// Create background gradient
		const backgroundGradientLoadPromise = new Promise(
			resolve => {
				const gradient = new Image();
				
				gradient.onload = function() {
					classRef.context.drawImage(
						this,
						0, classRef.canvas.height - primaryRectangleHeight - backgroundGradientHeight,
						classRef.canvas.width, backgroundGradientHeight
					);
					
					resolve();
				}
				
				gradient.src = "static/images/gradient.png";
			}
		);
		
		await backgroundGradientLoadPromise;
		
		const foregroundRGB = hexToRgb(this.foregroundColor);
		
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		);
		
		const primaryRectangleStartingX = 0;
		const primaryRectangleEndingX = this.canvas.width;
		
		this.context.fillStyle = this.foregroundColor;
		
		this.context.fillRect(
			0, this.canvas.height - primaryRectangleHeight,
			this.canvas.width, primaryRectangleHeight
		);
		
		function drawIconImage(image) {
			const iconHeight = primaryRectangleHeight - 2 * iconImageOffset;
			const iconWidth = (image.width * (iconHeight / image.height));
			
			classRef.context.globalAlpha = iconOpacity;
			
			const iconRGB = hexToRgb(classRef.reactionTextColor);
			
			classRef.context.drawImage(
				colorizeImage(
					image,
					iconWidth, iconHeight,
					iconRGB.r,
					iconRGB.g,
					iconRGB.b
				),
				(
					classRef.canvas.height
					- iconWidth
					- iconImageOffset
				), (
					classRef.canvas.height
					- iconImageOffset
					- iconHeight
				),
				iconWidth, iconHeight
			);
			
			classRef.context.globalAlpha = 1;
		}
		
		// Create icon, if there is one
		if (this.iconImage !== null) {
			drawIconImage(this.iconImage);
		} else if (this.iconSource !== null) {
			const iconImageLoadPromise = new Promise(
				resolve => {
					const iconImage = new Image();
					
					iconImage.onload = function() {
						drawIconImage(this);
						
						resolve();
					}
					
					iconImage.src = this.iconSource;
				}
			);
			
			await iconImageLoadPromise;
		}

		// Create logo
		const useLightLogoAndIcon = (foregroundLightness < 207);

		function drawLogoImage(image) {
			const logoWidth = Math.ceil(image.width * (logoHeight / image.height));
			const logoX = classRef.canvas.width - logoWidth - logoSideOffset;

			classRef.context.drawImage(
				image,
				logoX, classRef.canvas.height - logoHeight - logoBottomOffset,
				logoWidth, logoHeight
			);
		}

		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogoImage(this);
						
						resolve();
					}
					
					if (useLightLogoAndIcon) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogoImage(this.logoImage);
		}

		const themeImageLoadPromise = new Promise(
			resolve => {
				const themeImage = new Image();
				
				themeImage.onload = function() {
					const themeImageWidth = Math.ceil(themeImage.width * (themeImageHeight / themeImage.height));
					
					classRef.context.drawImage(
						this,
						themeSideOffset, classRef.canvas.height - themeBottomOffset - themeImageHeight,
						themeImageWidth, themeImageHeight
					);
					
					resolve();
				}
				
				themeImage.src = "static/images/koneckorupci.png";
			}
		);
		
		await themeImageLoadPromise;
		
		const foregroundRectangleTop = this.canvas.height - primaryRectangleHeight;
		
		if (
			this.reactions[0].source !== "" &&
			this.reactions[0].text !== "" &&
			this.reactions[1].source !== "" &&
			this.reactions[1].text !== ""
		) {
			this.context.beginPath();
			this.context.fillStyle = this.foregroundColor;

			this.context.arc(
				reactionImageOffsetSide + (reactionImageWidthHeight / 2),
				foregroundRectangleTop + (reactionImageWidthHeight / 2) + reactionImageOffsetTop,
				reactionImageWidthHeight / 2,
				0,
				2 * Math.PI,
				false
			);
			
			this.context.arc(
				this.canvas.width - reactionImageOffsetSide - (reactionImageWidthHeight / 2),
				foregroundRectangleTop + (reactionImageWidthHeight / 2) + reactionImageOffsetTop,
				reactionImageWidthHeight / 2,
				0,
				2 * Math.PI,
				false
			);
			
			this.context.closePath();
			
			this.context.fill();
			
			const firstReactionImage = new Image();
			
			firstReactionImage.onload = function() {
				classRef.context.drawImage(
					this,
					(
						reactionImageOffsetSide
						+ (reactionImagePaddingInner / 2)
					), (
						foregroundRectangleTop
						+ reactionImageOffsetTop
						+ (reactionImagePaddingInner / 2)
					),
					(
						reactionImageWidthHeight
						- reactionImagePaddingInner
					), (
						reactionImageWidthHeight
						- reactionImagePaddingInner
					)
				);
			}
			
			firstReactionImage.src = this.reactions[0].source.replace("reactions", "dark-reactions");
			
			const secondReactionImage = new Image();
			
			secondReactionImage.onload = function() {
				classRef.context.drawImage(
					this,
					(
						classRef.canvas.width
						- reactionImageWidthHeight
						- reactionImageOffsetSide
						+ (reactionImagePaddingInner / 2)
					), (
						foregroundRectangleTop
						+ reactionImageOffsetTop
						+ (reactionImagePaddingInner / 2)
					),
					(
						reactionImageWidthHeight
						- reactionImagePaddingInner
					), (
						reactionImageWidthHeight
						- reactionImagePaddingInner
					)
				);
			}
			
			secondReactionImage.src = this.reactions[1].source;
			
			const originalReactionFontSize = reactionTextFontSize;
			
			this.context.fillStyle = this.reactionTextColor;
			this.context.textAlign = "center";
			this.context.textBaseline = "bottom";
			
			let reactionLines = null;
			
			do {
				this.context.font = `${this.primaryFontStyle} ${reactionTextFontSize}px ${this.primaryFont}`;
				
				reactionLines = splitStringIntoLines(
					this.context,
					this.reactions[0].text,
					reactionTextMaxWidth,
					reactionTextMaxLines,
					true
				);
				
				if (
					reactionLines.length > reactionTextMaxLines
					&& (
						reactionLines.length * reactionTextFontSize
						> reactionTextMaxLines * originalReactionFontSize
					)
				) {
					reactionTextFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${reactionTextFontSize}px ${this.primaryFont}`;
				}
			} while (
				reactionLines.length > reactionTextMaxLines
				&& (
					reactionLines.length * reactionTextFontSize
					> reactionTextMaxLines * originalReactionFontSize
				)
			);
			
			let currentReactionLineY = (
				foregroundRectangleTop
				+ reactionImageOffsetTop
				+ reactionImageWidthHeight
				+ reactionTextOffsetTop
				+ reactionTextFontSize
			);
			
			for (const line of reactionLines) {
				this.context.fillText(
					line.join(" "),
					(
						reactionImageOffsetSide
						+ reactionImageWidthHeight / 2
					),
					currentReactionLineY
				);
				
				currentReactionLineY += reactionTextFontSize;
			}
			
			reactionTextFontSize = originalReactionFontSize;
			
			reactionLines = null;
			
			do {
				this.context.font = `${this.primaryFontStyle} ${reactionTextFontSize}px ${this.primaryFont}`;
				
				reactionLines = splitStringIntoLines(
					this.context,
					this.reactions[1].text,
					reactionTextMaxWidth,
					reactionTextMaxLines,
					true
				);
				
				if (
					reactionLines.length > reactionTextMaxLines
					&& (
						reactionLines.length * reactionTextFontSize
						> reactionTextMaxLines * originalReactionFontSize
					)
				) {
					reactionTextFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${reactionTextFontSize}px ${this.primaryFont}`;
				}
			} while (
				reactionLines.length > reactionTextMaxLines
				&& (
					reactionLines.length * reactionTextFontSize
					> reactionTextMaxLines * originalReactionFontSize
				)
			);
			
			currentReactionLineY = (
				foregroundRectangleTop
				+ reactionImageOffsetTop
				+ reactionImageWidthHeight
				+ reactionTextOffsetTop
				+ reactionTextFontSize
			);
			
			for (const line of reactionLines) {
				this.context.fillText(
					line.join(" "),
					(
						this.canvas.width
						- reactionImageOffsetSide
					) - reactionImageWidthHeight / 2,
					currentReactionLineY
				);
				
				currentReactionLineY += reactionTextFontSize;
			}
			
			this.context.textAlign = "left";
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> primaryRectangleHeight - (this.canvas.height * 0.03)
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> primaryRectangleHeight - (this.canvas.height * 0.03)
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.9925 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Icon
	async setIconText(text, skipRedraw = false) {
		this.iconText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setIconSource(url, skipRedraw = false) {
		this.iconSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setIconImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.iconImage = new Image();
					
					classRef.iconImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.iconImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
	
	async resetIconImage(skipRedraw = false) {
		this.iconImage = null;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Location
	async setLocationSource(url, skipRedraw = false) {
		this.locationSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.reactionTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				
				break;
			case "white-on-black":
				this.reactionTextColor = "#ffffff";
				this.foregroundColor ="#000000";
				
				break;
			case "forum-black-on-white":
				this.reactionTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				
				break;
			case "forum-white-on-purple":
				this.reactionTextColor = "#ffffff";
				this.foregroundColor = "#962a51";
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.reactionTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.reactionTextColor = "#ffffff";
				this.foregroundColor = "#00ad43";
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.reactionTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.reactionTextColor = "#ffffff";
				this.foregroundColor = "#21274e";
				
				break;
			case "louny-spolecne-black-on-white":
				this.reactionTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				
				break;
			case "louny-spolecne-white-on-purple":
				this.reactionTextColor = "#ffffff";
				this.foregroundColor = "#3e2a5b";
				
				break;
			case "litomerice-blue-on-white":
				this.reactionTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				
				break;
			case "litomerice-white-on-blue":
				this.reactionTextColor = "#ffffff";
				this.foregroundColor = "#123172";
				
				break;
			case "stranane-gray-on-yellow":
				this.reactionTextColor = "#ffffff";
				this.foregroundColor = "#4d4d4d";
				
				break;
			case "stranane-yellow-on-white":
				this.reactionTextColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				
				break;
			case "stranane-white-on-yellow":
				this.reactionTextColor = "#4d4d4d";
				this.foregroundColor = "#ffffff";
				this.reactionTextColor = "#4d4d4d";
				
				break;
			case "prusanky-black-on-yellow":
				this.reactionTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				
				break;
			case "prusanky-yellow-on-white":
				this.reactionTextColor = "#000000";
				this.foregroundColor = "#ffd500";
				
				break;
			case "prusanky-white-on-yellow":
				this.reactionTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.reactionTextColor = "#000000";
				
				break;
			case "ujezd-green-on-white":
				this.reactionTextColor = "#000000";
				this.foregroundColor = "#8ed4a3";
				
				break;
			case "ujezd-white-on-green":
				this.reactionTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				
				break;
			case "cssd-red-on-black":
				this.reactionTextColor = "#ffffff";
				this.foregroundColor = "#e63812";
				
				break;
			case "cssd-black-on-red":
				this.reactionTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				
				break;
			case "jilemnice-purple-on-black":
				this.reactionTextColor = "#ffffff";
				this.foregroundColor = "#6e1646";
				
				break;
			case "jilemnice-black-on-purple":
				this.reactionTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				
				break;
			case "novarole-white-on-green":
				this.reactionTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				
				break;
			case "novarole-green-on-white":
				this.reactionTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-black":
				this.reactionTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				this.nameBackgroundColor = "#fde119";
				this.nameTextColor = "#000000";
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.reactionTextColor;
	}
	
	// Secondary image
	async setSecondaryImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.secondaryImage = new Image();
					
					classRef.secondaryImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.secondaryImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
	
	// Logo
	async setLogoIsCenter(isCenter, skipRedraw = false) {
		this.logoIsCenter = isCenter;

		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
