class PosterBottomSloganTemplateNoIcon extends Template {
	description = "Není určeno pro tisk na plakáty.";
	
	changeableAttributes = [
		"logoImage",
		"primaryImage",
		"primaryText",
		"nameText",
		"primaryColorScheme",
		"terciaryColorScheme",
		"primaryImagePosition",
		"locationImage",
		"underNameText",
		"terciaryText",
		"qrCode"
	];

	locationSource = null;
	iconSource = null;
	
	iconImage = null;

	underNameText = "";
	terciaryText = "";
	
	qrCodeUri = "";
	
	defaultResolution = 4430;
	aspectRatio = 0.67720090293;

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];
	
	terciaryColorSchemes = [
		"black-on-gold",
		"white-on-black",
		"black-on-white"
	];

	primaryTextHighlightColorSchemes = [
		"black-on-gold"
	];
	
	changeableColors = [
		"primaryTextColor",
		"foregroundColor",
		"backgroundColor",
		"nameTextColor",
		"primaryTextHighlightColor",
		"terciaryTextColor",
		"terciaryTextBackgroundColor",
		"qrCodeColor",
		"requesterTextColor"
	];

	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const primaryRectangleWidth = Math.ceil(this.canvas.width * 0.875);
		const primaryRectangleAngle = Math.ceil(this.canvas.height * 0.01);
		const primaryRectanglePosition = Math.ceil(this.canvas.height * 0.83);
		const primaryRectanglePaddingTop = Math.ceil(this.canvas.height * 0.0125);
		const primaryRectangleAdditionalPaddingWithDiacritics = Math.ceil(this.canvas.height * 0.0075);
		const primaryRectanglePaddingBottomWithoutTerciary = Math.ceil(this.canvas.height * 0.02);
		const primaryRectanglePaddingBottomWithTerciary = Math.ceil(this.canvas.height * 0.03);
		const primaryRectanglePaddingSides = Math.ceil(this.canvas.width * 0.04);

		const highlightPaddingSides = Math.ceil(this.canvas.width * 0.01);
		const highlightPaddingBottom = Math.ceil(this.canvas.height * 0.0125);
		const highlightPaddingTop = Math.ceil(this.canvas.height * -0.01); // It is, it's how Roboto works.

		const nameRectangleOffsetX = Math.ceil(this.canvas.width * 0.02);
		const nameRectangleOffsetY = Math.ceil(this.canvas.height * 0.01);
		const nameRectanglePaddingTopBottom = Math.ceil(this.canvas.height * 0.01);
		const nameRectanglePaddingSides = Math.ceil(this.canvas.width * 0.05);
		
		const locationWidth = Math.ceil(this.canvas.width * 0.350);
		const locationOffsetTop = locationWidth * 0.04;
		
		const logoHeight = Math.ceil(this.canvas.height * 0.055) * this.logoImageZoom;
		const logoSideOffset = Math.ceil(this.canvas.width * 0.03);
		const logoBottomOffset = Math.ceil(this.canvas.height * 0.065) - (
			logoHeight / this.logoImageZoom * (this.logoImageZoom - 1) / 2
		);
		
		let primaryFontSize = Math.ceil(this.canvas.height * 0.09);
		const primaryFontLinePadding = 0;
		const primaryTextMaxLines = 3;
		
		let nameFontSize = Math.ceil(this.canvas.height * 0.033);
		const nameTextOffsetSide = Math.ceil(this.canvas.height * 0.02);
		let nameTextOffsetBottom = Math.ceil(this.canvas.height * 0.0629);
		const nameTextMaxWidth = Math.ceil(this.canvas.width * 0.3);
		const nameTextInnerPadding = Math.ceil(this.canvas.height * 0.0075);
		
		let underNameTextFontSize = Math.ceil(this.canvas.height * 0.02);
		
		const terciaryTextFontSize = Math.ceil(this.canvas.height * 0.02);
		const terciaryRectanglePaddingTopBottom = Math.ceil(this.canvas.height * 0.01);
		const terciaryRectanglePaddingSides = Math.ceil(this.canvas.width * 0.02);
		const terciaryRectangleOffsetTop = Math.ceil(this.canvas.height * 0.01);
		const terciaryRectangleOffsetSide = Math.ceil(this.canvas.width * 0.015);
		
		const qrCodeWidth = Math.ceil(this.canvas.width * 0.125);
		const qrCodeBottomOffset = Math.ceil(this.canvas.height * 0.04);

		// Get primary text split into lines, no more than ``primaryTextMaxLines`` of them
		
		let primaryTextLines = null;
		
		do {
			this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;

			primaryTextLines = splitStringIntoLines(
				this.context,
				this.primaryText,
				primaryRectangleWidth - primaryRectanglePaddingSides,
				primaryTextMaxLines,
				true
			).reverse();

			if (primaryTextLines.length > primaryTextMaxLines) {
				primaryFontSize -= 2;
			}
		} while (primaryTextLines.length > primaryTextMaxLines);

		// Clear the canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Set image
		if (this.primaryImage !== null) {
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D

			const imageScaleX = this.canvas.width / this.primaryImage.width;
			const imageScaleY = this.canvas.height / this.primaryImage.height;

			const imageScale = Math.max(imageScaleX, imageScaleY) * this.primaryImageZoom;
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - this.primaryImage.width * imageScale) / 2 + this.primaryImageX * this.primaryImageZoom,
				(this.canvas.height - this.primaryImage.height * imageScale) / 2 + this.primaryImageY * this.primaryImageZoom,
			);
			this.context.drawImage(
				this.primaryImage,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
		}
		
		// Change bottom rectangle width based on the text height
		const secondaryRectangleHeight = (
			(
				(primaryTextLines.length * (primaryFontSize + primaryFontLinePadding))
				< this.canvas.height / 4.5
			) ?
			Math.ceil(this.canvas.height / 4) : Math.ceil(this.canvas.height / 2.757)
		);
		
		// Fill bottom rectangle
		this.context.fillStyle = this.backgroundColor;
		this.context.fillRect(
			0, this.canvas.height - secondaryRectangleHeight,
			this.canvas.width, secondaryRectangleHeight
		);
		
		const firstPrimaryLine = primaryTextLines[primaryTextLines.length - 1].join(" ");
		
		// Create rectangle behind the primary text
		const primaryRectangleHeight = (
			primaryTextLines.length * (primaryFontSize + primaryFontLinePadding)
			+ primaryRectanglePaddingTop
			+ (
				(this.terciaryText !== "") ?
				primaryRectanglePaddingBottomWithTerciary : primaryRectanglePaddingBottomWithoutTerciary
			)
			+ (
				(
					firstPrimaryLine.replace(/[a-zA-Z0-9À-ž]+/g, "").length
					!== firstPrimaryLine.replace(/[a-zA-Z0-9]+/g, "").length
				) ?
				primaryRectangleAdditionalPaddingWithDiacritics : 0
			)
		);
		
		const foregroundRGB = hexToRgb(this.foregroundColor);
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		)
		
		this.context.beginPath();
		
		const primaryRectangleStartingX = (this.canvas.width - primaryRectangleWidth) / 2;
		const primaryRectangleEndingX = primaryRectangleWidth + primaryRectangleStartingX
		
		this.context.fillStyle = this.foregroundColor;
		
		this.context.moveTo(
			primaryRectangleStartingX,
			primaryRectanglePosition + primaryRectangleAngle
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePosition
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			primaryRectanglePosition - primaryRectangleHeight
		);
		this.context.lineTo(
			primaryRectangleStartingX,
			primaryRectanglePosition + primaryRectangleAngle - primaryRectangleHeight
		);
		
		this.context.closePath();
		
		this.context.fill();
		
		// Create primary text
		this.context.textAlign = "left";
		
		const useLightHighlight = (foregroundLightness > 207);
		
		const primaryLineX = this.canvas.width / 2;
		let currentPrimaryLineY = (
			primaryRectanglePosition
			- (
				(this.terciaryText !== "") ?
				primaryRectanglePaddingBottomWithTerciary : primaryRectanglePaddingBottomWithoutTerciary
			)
			- primaryFontLinePadding
		);
		
		let primaryTextHighlightedColor = null;
		
		const lowercasePrimaryTextHighlightColor = this.primaryTextHighlightColor.toLowerCase();
		const hasColorOverride = (
			lowercasePrimaryTextHighlightColor === "#209a37" ||
			lowercasePrimaryTextHighlightColor === "#e63812"
		);
		
		if (hasColorOverride) {
			if (useLightHighlight) {
				primaryTextHighlightedColor = this.foregroundColor;
			} else {
				primaryTextHighlightedColor = this.primaryTextColor;
			}
		} else if (!useLightHighlight) {
			primaryTextHighlightedColor = this.foregroundColor;
		} else {
			primaryTextHighlightedColor = this.primaryTextColor;
		}
		
		this.context.fillStyle = this.primaryTextColor;

		for (let line of primaryTextLines) {
			let wordPosition = 0;
			
			for (let word of line) {
				const previousWords = line.slice(0, wordPosition).join(" ");
				const previousWordsWidth = this.context.measureText(
					previousWords
					+ (
						(previousWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				const nextWords = line.slice(wordPosition + 1, line.length).join(" ")
				const nextWordsWidth = this.context.measureText(
					nextWords
					+ (
						(nextWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				let currentWordWidth = this.context.measureText(word).width;
				
				for (const word of line.slice(wordPosition + 1, line.length)) {
					if (word.isHighlighted) {
						currentWordWidth += this.context.measureText(word.toString() + " ").width;
					} else {
						break;
					}
				}
				
				if (word.isHighlighted) {
					if (
						wordPosition === 0 ||
						!line[wordPosition - 1].isHighlighted
					) {
						this.context.fillStyle = this.primaryTextHighlightColor;
						this.context.beginPath();

						const startingHighlightLineX = (
							primaryLineX
							+ Math.max(previousWordsWidth / 2)
							- Math.max(nextWordsWidth / 2)
							- Math.max(this.context.measureText(word).width / 2)
						);

						this.context.moveTo(
							startingHighlightLineX - highlightPaddingSides,
							currentPrimaryLineY + highlightPaddingBottom
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								+ highlightPaddingBottom
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ primaryRectangleWidth
								)
							)
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ primaryRectangleWidth
								)
							)
						);
						this.context.lineTo(
							startingHighlightLineX - highlightPaddingSides,
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
							)
						);
						
						this.context.closePath();
						
						this.context.fill();
					}
					
					this.context.fillStyle = primaryTextHighlightedColor;
				}
				
				this.context.fillText(
					word + " ",
					(
						primaryLineX
						+ Math.ceil(previousWordsWidth / 2)
						- Math.ceil(nextWordsWidth / 2)
						- Math.ceil(this.context.measureText(word).width / 2)
					),
					currentPrimaryLineY
				);
				
				wordPosition++;
				
				this.context.fillStyle = this.primaryTextColor;
			}
			
			currentPrimaryLineY -= (primaryFontSize + primaryFontLinePadding);
		}
		
		this.context.textAlign = "center";
		
		let classRef = this;
		
		// Create location image, if there is one
		if (this.locationSource !== null) {
			const locLoadPromise = new Promise(
				resolve => {
					let locationImage = new Image();
					
					locationImage.onload = function() {
						const locationHeight = Math.ceil(this.height * (locationWidth / this.width));
						
						const imageStartingY = (
							primaryRectanglePosition
							- primaryRectangleHeight
							- locationHeight
							+ (
								primaryRectangleAngle
								* (locationWidth / primaryRectangleWidth)
								* 1.15 // I don't know if this is imprecise floating point numbers or my broken math.
							)
							+ locationOffsetTop
						)
						
						let temporaryCanvas = document.createElement("canvas");
						
						temporaryCanvas.width = locationWidth;
						temporaryCanvas.height = locationHeight;
						
						let temporaryContext = temporaryCanvas.getContext("2d");
						
						temporaryContext.drawImage(
							this,
							0, 0,
							locationWidth, locationHeight
						);
						
						let pixels = temporaryContext.getImageData(
							0, 0,
							locationWidth, locationHeight
						);
						
						for (let pixelPosition = 0; pixelPosition < pixels.data.length; pixelPosition += 4) {
							pixels.data[pixelPosition] = pixels.data[pixelPosition] / 255 * foregroundRGB.r;
							pixels.data[pixelPosition + 1] = pixels.data[pixelPosition + 1] / 255 * foregroundRGB.g;
							pixels.data[pixelPosition + 2] = pixels.data[pixelPosition + 2] / 255 * foregroundRGB.b;
						}
						
						temporaryContext.putImageData(
							pixels,
							0, 0
						);
						
						classRef.context.drawImage(
							temporaryCanvas,
							primaryRectangleEndingX - locationWidth, imageStartingY
						);
						
						resolve();
					}
					
					locationImage.src = this.locationSource;
				}
			);
			
			await locLoadPromise;
		}
		
		// Create name, if not empty
		if (this.nameText !== "") {
			this.context.textAlign = "left";
			
			this.context.font = `bold ${nameFontSize}px 'Roboto Condensed'`;
			
			// Make the font smaller if necessary
			while (this.context.measureText(this.nameText).width > nameTextMaxWidth) {
				nameFontSize -= 2;
				nameTextOffsetBottom += 2;
				
				this.context.font = `bold ${nameFontSize}px 'Roboto Condensed'`;
			}
			
			// Correct offset for primary text
			this.context.font = `${underNameTextFontSize}px 'Roboto Condensed'`;
			
			while (this.context.measureText(this.underNameText).width > nameTextMaxWidth) {
				underNameTextFontSize -= 2;
				
				this.context.font = `${underNameTextFontSize}px 'Roboto Condensed'`;
			}
			
			// Fill both text spaces
			this.context.font = `bold ${nameFontSize}px 'Roboto Condensed'`;
			this.context.fillStyle = this.nameTextColor;

			this.context.fillText(
				this.nameText,
				(this.canvas.width - primaryRectangleWidth) / 2 + nameTextOffsetSide,
				this.canvas.height - nameTextOffsetBottom - underNameTextFontSize - nameTextInnerPadding
			);
			
			this.context.font = `${underNameTextFontSize}px 'Roboto Condensed'`;
			this.context.fillStyle = this.nameTextColor;
			
			this.context.fillText(
				this.underNameText,
				(this.canvas.width - primaryRectangleWidth) / 2 + nameTextOffsetSide,
				this.canvas.height - nameTextOffsetBottom
			);
		}
		
		this.context.textAlign = "center";

		// Create logo
		// See if we're using the light or dark version
		const backgroundRGB = hexToRgb(this.backgroundColor);
		const backgroundLightness = (
			0.2126 * backgroundRGB.r
			+ 0.7152 * backgroundRGB.g
			+ 0.0722 * backgroundRGB.b
		)

		const useLightLogoAndIcon = (backgroundLightness < 207);

		if (this.terciaryText !== "") {
			this.context.font = `${terciaryTextFontSize}px 'Roboto Condensed'`;
			this.context.fillStyle = this.terciaryTextBackgroundColor;
			
			const terciaryTextWidth = this.context.measureText(this.terciaryText).width;
			this.context.textBaseline = "bottom";
			
			this.context.fillRect(
				(
					this.canvas.width
					- ((this.canvas.width - primaryRectangleWidth) / 2)
					- terciaryRectangleOffsetSide
					- terciaryTextWidth
					- 2 * terciaryRectanglePaddingSides
				), (
					primaryRectanglePosition
					- terciaryRectangleOffsetTop
				),
				(
					terciaryTextWidth
					+ 2 * terciaryRectanglePaddingSides
				),
				(
					terciaryTextFontSize
					+ 2 * terciaryRectanglePaddingTopBottom
				)
			);
			
			this.context.fillStyle = this.terciaryTextColor;

			this.context.fillText(
				this.terciaryText,
				(
					this.canvas.width
					- ((this.canvas.width - primaryRectangleWidth) / 2)
					- terciaryRectangleOffsetSide
					- (
						terciaryTextWidth
						+ terciaryRectanglePaddingSides * 2
					) / 2
				),
				(
					primaryRectanglePosition
					- terciaryRectangleOffsetTop
					+ terciaryRectanglePaddingTopBottom
					+ terciaryTextFontSize
				)
			);
			
			this.context.textBaseline = "alphabetic";
		}
		
		if (this.qrCodeURI !== "") {
			const qrCodeImage = new Image();
			
			const code = new QRCode(
				qrCodeImage,
				{
					"text": this.qrCodeURI,
					"width": qrCodeWidth,
					"height": qrCodeWidth,
					"colorDark": this.qrCodeColor,
					"colorLight": this.backgroundColor,
					"correctLevel": QRCode.CorrectLevel.H
				}
			);
			
			this.context.drawImage(
				code._oDrawing._elCanvas, // Ugly, but the fastest way to get the canvas
				(this.canvas.width - qrCodeWidth) / 2, this.canvas.height - qrCodeWidth - qrCodeBottomOffset,
				qrCodeWidth, qrCodeWidth
			);
		}
		
		function drawLogoImage(image) {
			let logoWidth = Math.ceil(image.width * (logoHeight / image.height));
			let logoX = (
				primaryRectangleEndingX
				- logoWidth
				- logoSideOffset
			);
			
			classRef.context.drawImage(
				image,
				logoX, classRef.canvas.height - logoHeight - logoBottomOffset,
				logoWidth, logoHeight
			);
		}

		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogoImage(this);
						
						resolve();
					}
					
					if (useLightLogoAndIcon) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogoImage(this.logoImage);
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> secondaryRectangleHeight - (this.canvas.height * 0.007)
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> secondaryRectangleHeight - (this.canvas.height * 0.007)
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.993, -this.canvas.width * 0.993 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Icon
	async setIconText(text, skipRedraw = false) {
		this.iconText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setIconSource(url, skipRedraw = false) {
		this.iconSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setIconImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.iconImage = new Image();
					
					classRef.iconImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.iconImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
	
	async resetIconImage(skipRedraw = false) {
		this.iconImage = null;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Text
	async setUnderNameText(text, skipRedraw = false) {
		this.underNameText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setTerciaryText(text, skipRedraw = false) {
		this.terciaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// Location
	async setLocationSource(url, skipRedraw = false) {
		this.locationSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.qrCodeColor = "#ffffff";
				this.backgroundColor = "#000000";
				
				this.setTerciaryColorScheme("black-on-gray", true);
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor ="#000000";
				this.qrCodeColor = "#000000";
				this.backgroundColor ="#ffffff";
				
				this.setTerciaryColorScheme("black-on-gray", true);
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "forum-black-on-white":
				this.primaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.qrCodeColor = "#ffffff";
				this.backgroundColor = "#962a51";
				
				this.setTerciaryColorScheme("white-on-black", true);
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "forum-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#962a51";
				this.qrCodeColor = "#000000";
				this.backgroundColor = "#ffffff";
				
				this.setTerciaryColorScheme("white-on-black", true);
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.primaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.qrCodeColor = "#000000";
				this.backgroundColor = "#00ad43";
				
				this.setTerciaryColorScheme("black-on-most", true);
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#00ad43";
				this.qrCodeColor = "#ffffff";
				this.backgroundColor = "#ffffff";
				
				this.setTerciaryColorScheme("black-on-most", true);
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.primaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.qrCodeColor = "#ffffff";
				this.backgroundColor = "#21274e";
				
				this.setTerciaryColorScheme("pirati-spolecne", true);
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#21274e";
				this.qrCodeColor = "#21274e";
				this.backgroundColor = "#ffffff";
				
				this.setTerciaryColorScheme("pirati-spolecne", true);
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "louny-spolecne-black-on-white":
				this.primaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.qrCodeColor = "#ffffff";
				this.backgroundColor = "#3e2a5b";
				this.terciaryTextBackgroundColor = "#e2d7a9";
				this.terciaryTextColor = "#000000";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "louny-spolecne-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#3e2a5b";
				this.qrCodeColor = "#3e2a5b";
				this.backgroundColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#3e2a5b";
				this.terciaryTextColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "litomerice-blue-on-white":
				this.primaryTextColor = "#123172";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.qrCodeColor = "#ffffff";
				this.backgroundColor = "#123172";
				
				this.setTerciaryColorScheme("blue-on-litomerice", true);
				this.setPrimaryTextHighlightColorScheme("litomerice", true);
				
				break;
			case "litomerice-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#123172";
				this.qrCodeColor = "#000000";
				this.backgroundColor = "#ffffff";
				
				this.setTerciaryColorScheme("blue-on-litomerice", true);
				
				break;
			case "stranane-gray-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#4d4d4d";
				this.qrCodeColor = "#4d4d4d";
				this.backgroundColor = "#ffd500";
				this.terciaryTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#4d4d4d";
				
				break;
			case "stranane-yellow-on-white":
				this.primaryTextColor = "#4d4d4d";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.qrCodeColor = "#4d4d4d";
				this.backgroundColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#4d4d4d";
				this.terciaryTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#4d4d4d";
				
				break;
			case "stranane-white-on-yellow":
				this.primaryTextColor = "#4d4d4d";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.qrCodeColor = "#4d4d4d";
				this.backgroundColor = "#ffd500";
				this.terciaryTextBackgroundColor = "#4d4d4d";
				this.terciaryTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-black-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#000000";
				this.qrCodeColor = "#000000";
				this.backgroundColor = "#ffd500";
				this.terciaryTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				
				break;
			case "prusanky-yellow-on-white":
				this.primaryTextColor = "#000000";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.qrCodeColor = "#000000";
				this.backgroundColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.primaryTextColor = "#000000";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.qrCodeColor = "#000000";
				this.backgroundColor = "#ffd500";
				this.terciaryTextBackgroundColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "ujezd-green-on-white":
				this.primaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#8ed4a3";
				this.qrCodeColor = "#000000";
				this.backgroundColor = "#ffffff";
				this.terciaryTextBackgroundColor = "#ffdd55";
				this.terciaryTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "ujezd-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.nameTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.qrCodeColor = "#000000";
				this.backgroundColor = "#8ed4a3";
				this.terciaryTextBackgroundColor = "#ffdd55";
				this.terciaryTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "cssd-red-on-black":
				this.primaryTextColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#e63812";
				this.qrCodeColor = "#ffffff";
				this.backgroundColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				
				break;
			case "cssd-black-on-red":
				this.primaryTextColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.qrCodeColor = "#ffffff";
				this.backgroundColor = "#e63812";
				this.terciaryTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				
				break;
			case "jilemnice-purple-on-black":
				this.primaryTextColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#6e1646";
				this.qrCodeColor = "#ffffff";
				this.backgroundColor = "#000000";
				this.terciaryTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				
				break;
			case "jilemnice-black-on-purple":
				this.primaryTextColor = "#ffffff";
				this.nameTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.qrCodeColor = "#ffffff";
				this.backgroundColor = "#6e1646";
				this.terciaryTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				
				break;
			case "novarole-white-on-green":
				this.primaryTextColor = "#000000";
				this.nameTextColor = "#000000";
				
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#a9ce2d";
				
				this.qrCodeColor = "#000000";
				
				this.terciaryTextBackgroundColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				
				this.primaryTextHighlightColor = "#a9ce2d";
				
				break;
			case "novarole-green-on-white":
				this.primaryTextColor = "#000000";
				this.nameTextColor = "#000000";
				
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#ffffff";
				
				this.qrCodeColor = "#000000";
				
				this.terciaryTextBackgroundColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-black":
				this.primaryTextColor = "#000000";
				this.nameTextColor = "#ffffff";
				
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#000000";
				
				this.qrCodeColor = "#ffffff";
				
				this.terciaryTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				this.terciaryTextBackgroundColor = "#fde119";
				this.terciaryTextColor = "#000000";
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.foregroundColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setTerciaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-gold":
				this.terciaryTextBackgroundColor = "#ffeda5";
				this.terciaryTextColor = "#000000";
				break;
			case "black-on-gray":
				this.terciaryTextBackgroundColor = "#999999";
				this.terciaryTextColor = "#000000";
				break;
			case "white-on-black":
				this.terciaryTextBackgroundColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				break;
			case "black-on-white":
				this.terciaryTextBackgroundColor = "#ffffff";
				this.terciaryTextColor = "#000000";
				break;
			case "pirati-spolecne":
				this.terciaryTextBackgroundColor = "#9796ca";
				this.terciaryTextColor = "#000000";
				break;
			case "black-on-most":
				this.terciaryTextBackgroundColor = "#000000";
				this.terciaryTextColor = "#ffffff";
				break;
			case "blue-on-litomerice":
				this.terciaryTextBackgroundColor = "#123172";
				this.terciaryTextColor = "#000000";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setPrimaryTextHighlightColorScheme(scheme, skipRedraw = false) {
		switch(scheme) {
			case "gold":
				this.primaryTextHighlightColor = "#ffcc00";
				break;
			case "litomerice":
				this.primaryTextHighlightColor = "#afe87e";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Colors
	async seticonColor(color, skipRedraw = false) {
		this.iconColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setNameBackgroundColor(color, skipRedraw = false) {
		this.nameBackgroundColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setNameTextColor(color, skipRedraw = false) {
		this.nameTextColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setPrimaryTextHighlightColor(color, skipRedraw = false) {
		this.primaryTextHighlightColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	// QR code
	async setQrCodeURI(uri, skipRedraw = false) {
		this.qrCodeURI = uri;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
