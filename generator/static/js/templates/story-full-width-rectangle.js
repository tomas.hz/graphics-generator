class StoryWithFullWidthRectangleTemplate extends Template {
	description = "Určeno pro story na sociálních sítích.";
	
	changeableAttributes = [
		"logoImage",
		"primaryImage",
		"primaryText",
		"nameText",
		"primaryColorScheme",
		"nameColorScheme",
		"primaryImagePosition"
	];

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];
	
	nameColorSchemes = [
		"white-on-black",
		"black-on-white"
	];

	primaryTextHighlightColorSchemes = [
		"black-on-gold"
	];
	
	changeableColors = [
		"primaryTextColor",
		"foregroundColor",
		"nameBackgroundColor",
		"nameTextColor",
		"primaryTextHighlightColor",
		"requesterTextColor"
	];
	
	aspectRatio = 0.5625;
	defaultResolution = 1920;

	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const primaryRectangleAngle = Math.ceil(this.canvas.height * 0.015);
		const primaryRectanglePaddingBottom = Math.ceil(this.canvas.height * 0.08);
		const primaryRectanglePaddingTopWithName = Math.ceil(this.canvas.height * 0.035);
		const primaryRectanglePaddingTopWithoutName = Math.ceil(this.canvas.height * 0.03);
		const primaryRectangleAdditionalPaddingWithDiacritics = Math.ceil(this.canvas.height * 0.01);
		const primaryRectanglePaddingSides = Math.ceil(this.canvas.width * 0.04);

		const highlightPaddingSides = Math.ceil(this.canvas.width * 0.01);
		const highlightPaddingBottom = Math.ceil(this.canvas.height * 0.0125);
		const highlightPaddingTop = Math.ceil(this.canvas.height * -0.01); // It is, it's how Roboto works.

		const nameRectangleOffsetX = Math.ceil(this.canvas.width * 0.1);
		const nameRectangleOffsetY = Math.ceil(this.canvas.height * 0.01);
		const nameRectanglePaddingTopBottom = Math.ceil(this.canvas.height * 0.01);
		const nameRectanglePaddingSides = Math.ceil(this.canvas.width * 0.032);
		
		const logoHeight = Math.ceil(this.canvas.height * 0.04) * this.logoImageZoom;
		const logoBottomOffset = Math.ceil(this.canvas.height * 0.125) - (
			logoHeight / this.logoImageZoom * (this.logoImageZoom - 1) / 2
		);
		
		let primaryFontSize = Math.ceil(this.canvas.height * 0.09);
		const primaryFontLinePadding = 0;
		const primaryTextMaxLines = 3;
		const primaryTextPaddingBottom = Math.ceil(this.canvas.height * 0.16);
		
		const nameFontSize = Math.ceil(this.canvas.height * 0.025);

		const backgroundGradientHeight = Math.ceil(this.canvas.height * 0.2);
		
		// Get primary text split into lines, no more than ``primaryTextMaxLines`` of them
		
		let primaryTextLines = null;
		
		do {
			this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;

			primaryTextLines = splitStringIntoLines(
				this.context,
				this.primaryText,
				this.canvas.width - primaryRectanglePaddingSides,
				primaryTextMaxLines,
				true
			).reverse();

			if (primaryTextLines.length > primaryTextMaxLines) {
				primaryFontSize -= 2;
			}
		} while (primaryTextLines.length > primaryTextMaxLines);

		// Clear the canvas
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Set image
		if (this.primaryImage !== null) {
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D

			const imageScaleX = this.canvas.width / this.primaryImage.width;
			const imageScaleY = this.canvas.height / this.primaryImage.height;

			const imageScale = Math.max(imageScaleX, imageScaleY) * this.primaryImageZoom;
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - this.primaryImage.width * imageScale) / 2 + this.primaryImageX * this.primaryImageZoom,
				(this.canvas.height - this.primaryImage.height * imageScale) / 2 + this.primaryImageY * this.primaryImageZoom
			);
			this.context.drawImage(
				this.primaryImage,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
		}
		
		const firstPrimaryLine = primaryTextLines[primaryTextLines.length - 1].join(" ");
		
		// Create rectangle behind the primary text
		const primaryRectangleHeight = (
			primaryTextLines.length * (primaryFontSize + primaryFontLinePadding)
			+ (
				(this.nameText !== "") ?
				primaryRectanglePaddingTopWithName : primaryRectanglePaddingTopWithoutName
			)
			+ primaryRectanglePaddingBottom
			+ primaryTextPaddingBottom
			+ (
				(
					firstPrimaryLine.replace(/[a-zA-Z0-9À-ž]+/g, "").length
					!== firstPrimaryLine.replace(/[a-zA-Z0-9]+/g, "").length
				) ?
				primaryRectangleAdditionalPaddingWithDiacritics :
				0
			)
		);
		
		const classRef = this;
		
		// Create background gradient
		const backgroundGradientLoadPromise = new Promise(
			resolve => {
				const gradient = new Image();
				
				gradient.onload = function() {
					classRef.context.drawImage(
						this,
						0, classRef.canvas.height - primaryRectangleHeight + primaryRectangleAngle - backgroundGradientHeight,
						classRef.canvas.width, backgroundGradientHeight
					);
					
					resolve();
				}
				
				gradient.src = "static/images/gradient.png";
			}
		);
		
		await backgroundGradientLoadPromise;
		
		const foregroundRGB = hexToRgb(this.foregroundColor);
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		)
		
		this.context.beginPath();
		
		const primaryRectangleStartingX = 0;
		const primaryRectangleEndingX = this.canvas.width;
		
		this.context.fillStyle = this.foregroundColor;
		
		this.context.moveTo(
			0,
			this.canvas.height
		);
		this.context.lineTo(
			this.canvas.width,
			this.canvas.height
		);
		this.context.lineTo(
			this.canvas.width,
			this.canvas.height - primaryRectangleHeight
		);
		this.context.lineTo(
			0,
			this.canvas.height + primaryRectangleAngle - primaryRectangleHeight
		);
		
		this.context.closePath();
		
		this.context.fill();
		
		// Create primary text
		this.context.textAlign = "left";
		
		const useLightHighlightAndUseDarkLogo = (foregroundLightness > 207);
		
		const primaryLineX = this.canvas.width / 2;
		let currentPrimaryLineY = (
			this.canvas.height
			- primaryRectanglePaddingBottom
			- primaryFontLinePadding
			- primaryTextPaddingBottom
		);
		
		let primaryTextHighlightedColor = null;
		
		const lowercasePrimaryTextHighlightColor = this.primaryTextHighlightColor.toLowerCase();
		const hasColorOverride = (
			lowercasePrimaryTextHighlightColor === "#209a37" ||
			lowercasePrimaryTextHighlightColor === "#e63812"
		);
		
		if (hasColorOverride) {
			if (useLightHighlightAndUseDarkLogo) {
				primaryTextHighlightedColor = this.foregroundColor;
			} else {
				primaryTextHighlightedColor = this.primaryTextColor;
			}
		} else if (!useLightHighlightAndUseDarkLogo) {
			primaryTextHighlightedColor = this.foregroundColor;
		} else {
			primaryTextHighlightedColor = this.primaryTextColor;
		}
		
		this.context.fillStyle = this.primaryTextColor;

		for (let line of primaryTextLines) {
			let wordPosition = 0;
			
			for (let word of line) {
				const previousWords = line.slice(0, wordPosition).join(" ");
				const previousWordsWidth = this.context.measureText(
					previousWords
					+ (
						(previousWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				const nextWords = line.slice(wordPosition + 1, line.length).join(" ")
				const nextWordsWidth = this.context.measureText(
					nextWords
					+ (
						(nextWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				let currentWordWidth = this.context.measureText(word).width;
				
				for (const word of line.slice(wordPosition + 1, line.length)) {
					if (word.isHighlighted) {
						currentWordWidth += this.context.measureText(word.toString() + " ").width;
					} else {
						break;
					}
				}
				
				if (word.isHighlighted) {
					if (
						wordPosition === 0 ||
						!line[wordPosition - 1].isHighlighted
					) {
						this.context.fillStyle = this.primaryTextHighlightColor;
						this.context.beginPath();

						const startingHighlightLineX = (
							primaryLineX
							+ Math.max(previousWordsWidth / 2)
							- Math.max(nextWordsWidth / 2)
							- Math.max(this.context.measureText(word).width / 2)
						);

						this.context.moveTo(
							startingHighlightLineX - highlightPaddingSides,
							currentPrimaryLineY + highlightPaddingBottom
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								+ highlightPaddingBottom
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ this.canvas.width
								)
							)
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ this.canvas.width
								)
							)
						);
						this.context.lineTo(
							startingHighlightLineX - highlightPaddingSides,
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
							)
						);
						
						this.context.closePath();
						
						this.context.fill();
					}
					
					this.context.fillStyle = primaryTextHighlightedColor;
				}

				this.context.fillText(
					word + " ",
					(
						primaryLineX
						+ Math.ceil(previousWordsWidth / 2)
						- Math.ceil(nextWordsWidth / 2)
						- Math.ceil(this.context.measureText(word).width / 2)
					),
					currentPrimaryLineY
				);
				
				wordPosition++;
				
				this.context.fillStyle = this.primaryTextColor;
			}

			currentPrimaryLineY -= (primaryFontSize + primaryFontLinePadding);
		}
		
		// Create name, if not empty

		if (this.nameText !== "") {
			// Create rectangle for name text
			this.context.font = `${nameFontSize}px 'Roboto Condensed'`;
			this.context.textAlign = "center";
			
			const nameRectangleStartingX = nameRectangleOffsetX;
			const nameRectangleStartingY = (
				this.canvas.height
				- primaryRectangleHeight
				- nameRectangleOffsetY
			);
			
			const nameRectangleTextWidth = this.context.measureText(this.nameText).width;
			
			this.context.fillStyle = this.nameBackgroundColor;
			this.context.fillRect(
				nameRectangleStartingX, nameRectangleStartingY,
				nameRectangleTextWidth + nameRectanglePaddingSides * 2, nameFontSize + nameRectanglePaddingTopBottom * 2
			);
			
			// Create name text itself
			this.context.fillStyle = this.nameTextColor;
			this.context.fillText(
				this.nameText,
				nameRectangleStartingX + nameRectanglePaddingSides + Math.ceil(nameRectangleTextWidth / 2),
				nameRectangleStartingY + nameFontSize + nameRectanglePaddingTopBottom / 2
			);
		}

		function drawLogoImage(image) {
			const logoWidth = Math.ceil(image.width * (logoHeight / image.height));

			classRef.context.drawImage(
				image,
				(classRef.canvas.width - logoWidth)/2, classRef.canvas.height - logoHeight - logoBottomOffset,
				logoWidth, logoHeight
			);
		}

		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogoImage(this);
						
						resolve();
					}
					
					if (!useLightHighlightAndUseDarkLogo) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogoImage(this.logoImage);
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.0175);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> primaryRectangleHeight - (this.canvas.height * 0.03)
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> primaryRectangleHeight - (this.canvas.height * 0.03)
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.989, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("white-on-black", true);
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor ="#000000";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("black-on-white", true);
				
				break;
			case "forum-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("white-on-black", true);
				
				break;
			case "forum-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#962a51";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("black-on-white", true);
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("black-on-most", true);
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#00ad43";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("black-on-most", true);
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("pirati-spolecne", true);
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#21274e";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("pirati-spolecne", true);
				
				break;
			case "louny-spolecne-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				
				this.primaryTextHighlightColor = "#e2d7a9";
				
				this.nameBackgroundColor = "#e2d7a9";
				this.nameTextColor = "#000000";
				
				break;
			case "louny-spolecne-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#3e2a5b";
				
				this.primaryTextHighlightColor = "#e2d7a9";
				
				this.nameBackgroundColor = "#e2d7a9";
				this.nameTextColor = "#000000";
				
				break;
			case "litomerice-blue-on-white":
				this.primaryTextColor = "#123172";
				this.foregroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("litomerice", true);
				this.setNameColorScheme("blue-on-litomerice", true);
				
				break;
			case "litomerice-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#123172";
				
				this.setPrimaryTextHighlightColorScheme("litomerice", true);
				this.setNameColorScheme("blue-on-litomerice", true);
				
				break;
			case "stranane-gray-on-yellow":
				this.primaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				this.nameBackgroundColor = "#50c450";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "stranane-yellow-on-white":
				this.primaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffd500";
				this.nameBackgroundColor = "#50c450";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#4d4d4d";
				
				break;
			case "stranane-white-on-yellow":
				this.primaryTextColor = "#4d4d4d";
				this.foregroundColor = "#ffffff";
				this.nameBackgroundColor = "#50c450";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-black-on-yellow":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.nameBackgroundColor = "#50c450";
				this.nameTextColor = "#000000";
// 				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "prusanky-yellow-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffd500";
				this.nameBackgroundColor = "#50c450";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.nameBackgroundColor = "#50c450";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "ujezd-green-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#8ed4a3";
				this.nameBackgroundColor = "#ffdd55";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "ujezd-white-on-green":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffdd55";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "cssd-red-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#e63812";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "cssd-black-on-red":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			 case "jilemnice-purple-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#6e1646";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "jilemnice-black-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "novarole-white-on-green":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.nameBackgroundColor = "#a9ce2d";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#a9ce2d";
				
				break;
			case "novarole-green-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.nameBackgroundColor = "#a9ce2d";
				this.nameTextColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				this.nameBackgroundColor = "#fde119";
				this.nameTextColor = "#000000";
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.primaryTextColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setNameColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-blue":
				this.nameBackgroundColor = "#18c1df";
				this.nameTextColor = "#000000";
				break;
			case "black-on-gray":
				this.nameBackgroundColor = "#999999";
				this.nameTextColor = "#000000";
				break;
			case "black-on-white":
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				break;
			case "white-on-black":
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				break;
			case "pirati-spolecne":
				this.nameBackgroundColor = "#9796ca";
				this.nameTextColor = "#000000";
				break;
			case "black-on-most":
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				break;
			case "blue-on-litomerice":
				this.nameBackgroundColor = "#cccccc";
				this.nameTextColor = "#123172";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setPrimaryTextHighlightColorScheme(scheme, skipRedraw = false) {
		switch(scheme) {
			case "gold":
				this.primaryTextHighlightColor = "#ffcc00";
				break;
			case "litomerice":
				this.primaryTextHighlightColor = "#afe87e";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Colors	
	async setNameBackgroundColor(color, skipRedraw = false) {
		this.nameBackgroundColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setNameTextColor(color, skipRedraw = false) {
		this.nameTextColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setPrimaryTextHighlightColor(color, skipRedraw = false) {
		this.primaryTextHighlightColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
