class NoImageQuote extends Template {
	description = "Určeno pro sociální sítě.";
	
	changeableAttributes = [
		"logoImage",
		"primaryText",
		"nameText",
		"primaryColorScheme",
		"nameColorScheme",
		"iconImage",
		"backgroundHasPattern"
	];

	iconImage = null;
	iconSource = null;
	backgroundHasPattern = true;
	
	backgroundPatternSource = "/static/images/background_pattern.svg";

	quoteImageDarkSource = "/static/images/quote_light.png";
	quoteImageLightSource = "/static/images/quote_dark.png";

	primaryColorSchemes = [
		"black-on-white",
		"white-on-black"
	];
	
	nameColorSchemes = [
		"black-on-gray",
		"black-on-gold",
		"white-on-black",
		"black-on-white"
	];

	primaryTextHighlightColorSchemes = [
		"black-on-gold"
	];
	
	// Colors
	changeableColors = [
		"primaryTextColor",
		"foregroundColor",
		"backgroundColor",
		"primaryTextHighlightColor",
		"nameTextColor",
		"nameBackgroundColor",
		"requesterTextColor"
	];

	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const quoteImageOffsetTop = Math.ceil(this.canvas.height * 0.03);
		const quoteImageOffsetSide = Math.ceil(this.canvas.width * 0.03);
		const quoteImageHeight = Math.ceil(this.canvas.height * 0.275);
		
		const iconHeight = Math.ceil(this.canvas.height * 0.13);
		const iconImageOffsetTop = Math.ceil(this.canvas.height * 0.08);

		let primaryTextMaxLines = 3;
		let primaryFontSize = Math.ceil(this.canvas.height * 0.175);
		const primaryTextPaddingSides = Math.ceil(this.canvas.width * 0.05);
		const primaryTextCenterOffset = Math.ceil(this.canvas.height * -0.025); // More Roboto hacks
		
		const primaryRectangleWidth = Math.ceil(this.canvas.width * 0.8);
		const primaryRectangleAngle = Math.ceil(this.canvas.height * 0.01);
		const primaryRectanglePaddingTop = Math.ceil(this.canvas.height * 0.02);
		const primaryRectanglePaddingBottomWithoutName = Math.ceil(this.canvas.height * 0.05);
		const primaryRectanglePaddingBottomWithName = Math.ceil(this.canvas.height * 0.07);
		
		const logoHeight = Math.ceil(this.canvas.height * 0.06) * this.logoImageZoom;
		const logoOffsetBottom = Math.ceil(this.canvas.height * 0.09) - (
			logoHeight / this.logoImageZoom * (this.logoImageZoom - 1) / 2
		);
		
		const highlightPaddingSides = Math.ceil(this.canvas.width * 0.01);
		const highlightPaddingBottom = Math.ceil(this.canvas.height * 0.0125);
		const highlightPaddingTop = Math.ceil(this.canvas.height * -0.01); // It is, it's how Roboto works.
		
		const nameFontSize = Math.ceil(this.canvas.height * 0.03);
		
		const nameRectangleOffsetX = Math.ceil(this.canvas.width * 0.02);
		const nameRectangleOffsetY = Math.ceil(this.canvas.height * -0.01);
		const nameRectanglePaddingTopBottom = Math.ceil(this.canvas.height * 0.01);
		const nameRectanglePaddingSides = Math.ceil(this.canvas.width * 0.032);

		// Clear the canvas
		this.context.fillStyle = this.backgroundColor;
		this.context.fillRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		const backgroundRGB = hexToRgb(this.backgroundColor);
		const backgroundLightness = (
			0.2126 * backgroundRGB.r
			+ 0.7152 * backgroundRGB.g
			+ 0.0722 * backgroundRGB.b
		)
		
		const useDarkQuoteAndLightLogo = (backgroundLightness < 207);
		
		let classRef = this;
		
		if (this.backgroundHasPattern) {
			// https://stackoverflow.com/a/56341485
			// Thanks to Kimbatt!
			let backgroundImage = null;
			
			const bgImageLoadPromise = new Promise(
				resolve => {
					backgroundImage = new Image();
					
					backgroundImage.onload = function() {
						classRef.context.drawImage(
							this,
							0, 0,
							classRef.canvas.width, classRef.canvas.height
						);
						
						resolve();
					}
					
					backgroundImage.src = this.backgroundPatternSource;
				}
			);
			
			await bgImageLoadPromise;
		}
		
		let quoteImage = null;
		
		const quoteImageLoadPromise = new Promise(
			resolve => {
				quoteImage = new Image();
				
				quoteImage.onload = function() {
					classRef.context.drawImage(
						this,
						quoteImageOffsetSide, quoteImageOffsetTop,
						quoteImageHeight, Math.ceil(this.width * (quoteImageHeight / this.height))
					);
					
					resolve();
				}
				
				quoteImage.src = (
					(useDarkQuoteAndLightLogo) ?
					this.quoteImageLightSource :
					this.quoteImageDarkSource
				);
			}
			
		);
		
		await quoteImageLoadPromise;

		let primaryTextLines = [];
		
		const foregroundRGB = hexToRgb(this.foregroundColor);
		const foregroundLightness = (
			0.2126 * foregroundRGB.r
			+ 0.7152 * foregroundRGB.g
			+ 0.0722 * foregroundRGB.b
		)
		
		const useLightHighlight = (foregroundLightness > 207);
		const originalMaxHeight = primaryFontSize * primaryTextMaxLines;
		
		do {
			this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;
			
			primaryTextLines = splitStringIntoLines(
				this.context,
				this.primaryText,
				primaryRectangleWidth - (primaryTextPaddingSides * 2),
				primaryTextMaxLines,
				true
			);
			
			if (primaryTextLines.length > primaryTextMaxLines) {
				primaryFontSize -= 2;
			}
			
			if (originalMaxHeight > primaryFontSize * (primaryTextMaxLines + 1)) {
				primaryTextMaxLines += 1;
			}
		} while (primaryTextLines.length > primaryTextMaxLines);
		
		let currentPrimaryLineY = (
			(
				this.canvas.height
				- (primaryTextLines.length * primaryFontSize)
			) / 2
			+ primaryFontSize
			+ primaryTextCenterOffset
		);
		
		// Foreground rectangle
		this.context.beginPath();
		
		const primaryRectangleStartingX = (this.canvas.width - primaryRectangleWidth) / 2;
		const primaryRectangleEndingX = primaryRectangleWidth + primaryRectangleStartingX
		
		this.context.fillStyle = this.foregroundColor;
		
		this.context.moveTo(
			primaryRectangleStartingX,
			currentPrimaryLineY - primaryFontSize - primaryRectanglePaddingTop
		);
		this.context.lineTo(
			primaryRectangleEndingX,
			(
				currentPrimaryLineY
				- primaryRectangleAngle
				- primaryFontSize
				- primaryRectanglePaddingTop
			)
		);
		
		this.context.lineTo(
			primaryRectangleEndingX,
			(
				currentPrimaryLineY
				+ (primaryTextLines.length * primaryFontSize)
				- primaryFontSize
				+ (
					(this.nameText === "") ?
					primaryRectanglePaddingBottomWithoutName :
					primaryRectanglePaddingBottomWithName
				)
			)
		);
		this.context.lineTo(
			primaryRectangleStartingX,
			(
				currentPrimaryLineY
				+ (primaryTextLines.length * primaryFontSize)
				+ primaryRectangleAngle
				- primaryFontSize
				+ (
					(this.nameText === "") ?
					primaryRectanglePaddingBottomWithoutName :
					primaryRectanglePaddingBottomWithName
				)
			)
		);
		
		this.context.closePath();
		
		this.context.fill();
		
		this.context.textAlign = "left";
		this.context.font = `${this.primaryFontStyle} ${primaryFontSize}px ${this.primaryFont}`;
		
		const primaryLineX = this.canvas.width / 2;
		let primaryTextHighlightedColor = null;
		
		const lowercasePrimaryTextHighlightColor = this.primaryTextHighlightColor.toLowerCase();
		const hasColorOverride = (
			lowercasePrimaryTextHighlightColor === "#209a37" ||
			lowercasePrimaryTextHighlightColor === "#e63812"
		);
		
		if (hasColorOverride) {
			if (useLightHighlight) {
				primaryTextHighlightedColor = this.foregroundColor;
			} else {
				primaryTextHighlightedColor = this.primaryTextColor;
			}
		} else if (!useLightHighlight) {
			primaryTextHighlightedColor = this.foregroundColor;
		} else {
			primaryTextHighlightedColor = this.primaryTextColor;
		}
		
		this.context.fillStyle = this.primaryTextColor;
		
		for (let line of primaryTextLines) {
			let wordPosition = 0;
			
			for (let word of line) {
				const previousWords = line.slice(0, wordPosition).join(" ");
				const previousWordsWidth = this.context.measureText(
					previousWords
					+ (
						(previousWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				const nextWords = line.slice(wordPosition + 1, line.length).join(" ")
				const nextWordsWidth = this.context.measureText(
					nextWords
					+ (
						(nextWords.length !== 0) ?
						" " : ""
					)
				).width;
				
				let currentWordWidth = this.context.measureText(word).width;
				
				for (const word of line.slice(wordPosition + 1, line.length)) {
					if (word.isHighlighted) {
						currentWordWidth += this.context.measureText(word.toString() + " ").width;
					} else {
						break;
					}
				}
				
				if (word.isHighlighted) {
					if (
						wordPosition === 0 ||
						!line[wordPosition - 1].isHighlighted
					) {
						this.context.fillStyle = this.primaryTextHighlightColor;
						this.context.beginPath();
						
						const startingHighlightLineX = (
							primaryLineX
							+ Math.max(previousWordsWidth / 2)
							- Math.max(nextWordsWidth / 2)
							- Math.max(this.context.measureText(word).width / 2)
						);
						
						this.context.moveTo(
							startingHighlightLineX - highlightPaddingSides,
							currentPrimaryLineY + highlightPaddingBottom
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								+ highlightPaddingBottom
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ primaryRectangleWidth
								)
							)
						);
						this.context.lineTo(
							(
								startingHighlightLineX
								+ currentWordWidth
								+ highlightPaddingSides
							),
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
								- Math.max(
									(currentWordWidth * primaryRectangleAngle)
									/ primaryRectangleWidth
								)
							)
						);
						this.context.lineTo(
							startingHighlightLineX - highlightPaddingSides,
							(
								currentPrimaryLineY
								- primaryFontSize
								- highlightPaddingTop
							)
						);
						
						this.context.closePath();
						
						this.context.fill();
					}
					
					this.context.fillStyle = primaryTextHighlightedColor;
				}
				
				this.context.fillText(
					word + " ",
					(
						primaryLineX
						+ Math.ceil(previousWordsWidth / 2)
						- Math.ceil(nextWordsWidth / 2)
						- Math.ceil(this.context.measureText(word).width / 2)
					),
					currentPrimaryLineY
				);
				
				wordPosition++;
				
				this.context.fillStyle = this.primaryTextColor;
			}
			
			currentPrimaryLineY += primaryFontSize;
		}
		
		function drawLogo(image) {
			let logoWidth = Math.ceil(image.width * (logoHeight / image.height));
			
			classRef.context.drawImage(
				image,
				(classRef.canvas.width - logoWidth)/2, classRef.canvas.height - logoOffsetBottom - logoHeight,
				logoWidth, logoHeight
			);
		}
		
		if (this.logoImage === null) {
			const logoImageLoadPromise = new Promise(
				resolve => {
					let logoImage = new Image();
					
					logoImage.onload = function() {
						drawLogo(this);
						
						resolve();
					}
					
					if (useDarkQuoteAndLightLogo) {
						logoImage.src = classRef.lightLogoDefaultSource;
					} else {
						logoImage.src = classRef.darkLogoDefaultSource;
					}
				}
			);
			
			await logoImageLoadPromise;
		} else {
			drawLogo(this.logoImage);
		}

		// Create name
		if (this.nameText !== "") {
			// Create rectangle for name text
			this.context.font = `${nameFontSize}px 'Roboto Condensed'`;
			this.context.textAlign = "center";
			
			const nameRectangleStartingX = (
				primaryRectangleStartingX
				+ nameRectangleOffsetX
			);
			const nameRectangleStartingY = (
				currentPrimaryLineY
				- primaryFontSize
				+ primaryRectanglePaddingBottomWithName
				+ nameRectangleOffsetY
			);
			
			const nameRectangleTextWidth = this.context.measureText(this.nameText).width;
			
			this.context.fillStyle = this.nameBackgroundColor;
			this.context.fillRect(
				nameRectangleStartingX, nameRectangleStartingY,
				nameRectangleTextWidth + nameRectanglePaddingSides * 2, nameFontSize + nameRectanglePaddingTopBottom * 2
			);
			
			// Create name text itself
			this.context.fillStyle = this.nameTextColor;
			this.context.fillText(
				this.nameText,
				nameRectangleStartingX + nameRectanglePaddingSides + Math.ceil(nameRectangleTextWidth / 2),
				nameRectangleStartingY + nameFontSize + nameRectanglePaddingTopBottom / 2
			);
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.97
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.97
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.985, -this.canvas.width * 0.985 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Icon
	async setIconSource(url, skipRedraw = false) {
		this.iconSource = url;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setIconImageFromInput(imageInput, skipRedraw = false) {
		if (imageInput.files.length == 0) {
			return;
		}
		
		const readPromise = new Promise(
			resolve => {
				const fileReader = new FileReader();
				
				let classRef = this;
				
				fileReader.onloadend = function(event) {
					classRef.iconImage = new Image();
					
					classRef.iconImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.iconImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(imageInput.files[0]);
			}
		);
		
		await readPromise;
	}
	
	async resetIconImage(skipRedraw = false) {
		this.iconImage = null;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Color schemes
	async setPrimaryColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#000000";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("black-on-gray", true);
				
				break;
			case "white-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor ="#000000";
				this.backgroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("black-on-gray", true);
				
				break;
			case "forum-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#962a51";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("white-on-black", true);
				
				break;
			case "forum-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#962a51";
				this.backgroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("white-on-black", true);
				
				break;
			case "zeleni-volary-bystrc-most-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#00ad43";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("black-on-most", true);
				
				break;
			case "zeleni-volary-bystrc-most-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#00ad43";
				this.backgroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("black-on-most", true);
				
				break;
			case "spolecne-s-piraty-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#21274e";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("pirati-spolecne", true);
				
				break;
			case "spolecne-s-piraty-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#21274e";
				this.backgroundColor = "#ffffff";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				this.setNameColorScheme("pirati-spolecne", true);
				
				break;
			case "louny-spolecne-black-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#3e2a5b";
				this.nameBackgroundColor = "#e2d7a9";
				this.nameTextColor = "#000000";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "louny-spolecne-white-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#3e2a5b";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#e2d7a9";
				this.nameTextColor = "#000000";
				
				this.setPrimaryTextHighlightColorScheme("gold", true);
				
				break;
			case "litomerice-blue-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#123172";
				
				this.setPrimaryTextHighlightColorScheme("litomerice", true);
				this.setNameColorScheme("blue-on-litomerice", true);
				
				break;
			case "litomerice-white-on-blue":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#123172";
				this.backgroundColor = "#ffffff";
				
				this.setNameColorScheme("blue-on-litomerice", true);
				
				break;
			case "stranane-gray-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#4d4d4d";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#4d4d4d";
				
				break;
			case "stranane-yellow-on-white":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#4d4d4d";
				this.nameTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#4d4d4d";
				
				break;
			case "stranane-white-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#4d4d4d";
				this.nameTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "prusanky-black-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "prusanky-yellow-on-white":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#ffd500";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#000000";
				
				break;
			case "prusanky-white-on-yellow":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#ffd500";
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				this.primaryTextHighlightColor = "#ffd500";
				
				break;
			case "ujezd-green-on-white":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#8ed4a3";
				this.backgroundColor = "#ffffff";
				this.nameBackgroundColor = "#ffdd55";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "ujezd-white-on-green":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#8ed4a3";
				this.nameBackgroundColor = "#ffdd55";
				this.nameTextColor = "#000000";
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "cssd-red-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#e63812";
				this.backgroundColor = "#000000";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "cssd-black-on-red":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#e63812";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "jilemnice-purple-on-black":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#6e1646";
				this.backgroundColor = "#000000";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "jilemnice-black-on-purple":
				this.primaryTextColor = "#ffffff";
				this.foregroundColor = "#000000";
				this.backgroundColor = "#6e1646";
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				break;
			case "novarole-white-on-green":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#ffffff";
				this.backgroundColor = "#a9ce2d";
				
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				
				this.primaryTextHighlightColor = "#ffdd55";
				
				break;
			case "novarole-green-on-white":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#ffffff";
				
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "novarole-green-on-black":
				this.primaryTextColor = "#000000";
				this.foregroundColor = "#a9ce2d";
				this.backgroundColor = "#000000";
				
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				
				this.primaryTextHighlightColor = "#ffcc00";
				
				break;
			case "zeleni-melnik-yellow-name-rect":
				await this.setPrimaryColorScheme("white-on-black", true);
				
				this.nameBackgroundColor = "#fde119";
				this.nameTextColor = "#000000";
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		this.requesterTextColor = this.foregroundColor;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setNameColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-gray":
				this.nameBackgroundColor = "#999999";
				this.nameTextColor = "#000000";
				break;
			case "black-on-gold":
				this.nameBackgroundColor = "#ffeda5";
				this.nameTextColor = "#000000";
				break;
			case "white-on-black":
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				break;
			case "black-on-white":
				this.nameBackgroundColor = "#ffffff";
				this.nameTextColor = "#000000";
				break;
			case "pirati-spolecne":
				this.nameBackgroundColor = "#9796ca";
				this.nameTextColor = "#000000";
				break;
			case "black-on-most":
				this.nameBackgroundColor = "#000000";
				this.nameTextColor = "#ffffff";
				break;
			case "black-on-litomerice":
				this.nameBackgroundColor = "#123172";
				this.nameTextColor = "#000000";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	async setPrimaryTextHighlightColorScheme(scheme, skipRedraw = false) {
		switch(scheme) {
			case "gold":
				this.primaryTextHighlightColor = "#ffcc00";
				break;
			case "litomerice":
				this.primaryTextHighlightColor = "#afe87e";
				break;
			default:
				throw new Error("This scheme does not exist.");
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}

	// Colors
	async setPrimaryTextHighlightColor(color, skipRedraw = false) {
		this.primaryTextHighlightColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setNameBackgroundColor(color, skipRedraw = false) {
		this.nameBackgroundColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setNameTextColor(color, skipRedraw = false) {
		this.nameTextColor = color;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setBackgroundHasPattern(hasPattern, skipRedraw = false) {
		this.backgroundHasPattern = hasPattern;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
