class ProUstiSloganBubble extends Template {
	description = "";
	
	changeableAttributes = [
		"primaryText",
		"secondaryText",
		"proUstiColorScheme"
	];

	defaultResolution = 2000;
	aspectRatio = 1;
	
	changeableColors = [
		"primaryTextColor",
		"foregroundColor",
		"backgroundColor",
		"iconColor",
		"secondaryTextColor"
	];
	
	secondaryText = "";
	
	// Canvas
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}

		this.redrawing = true;
		
		const foregroundRectangleAngle = Math.ceil(this.canvas.height * 0.15);
		
		const foregroundRectangleNippleWidth = Math.ceil(this.canvas.width * 0.15);
		const foregroundRectangleNippleOffsetBottom = Math.ceil(foregroundRectangleAngle * -0.1);
		
		// ---
		
		const logoOffsetSide = Math.ceil(this.canvas.width * 0.1);
		const logoBigWidth = Math.ceil(this.canvas.width * 0.17);
		const logoBigOffsetBottom = Math.ceil(this.canvas.height * 0.05);
		
		// ---
		
		let primaryTextFontSize = Math.ceil(this.canvas.height * 0.089);
		let primaryTextMaxLines = 6;
		const primaryTextMaxWidth = Math.ceil(this.canvas.width * 0.8);
		const primaryTextOffsetTopBottom = Math.ceil(this.canvas.height * 0.06);
		
		// ---
		
		let secondaryTextFontSize = Math.ceil(this.canvas.height * 0.035);
		const secondaryTextMaxWidth = Math.ceil(this.canvas.width * 0.45);
		
		// -----------------------------------------------------
		
		
		// Background
		this.context.fillStyle = this.backgroundColor;
		this.context.fillRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		// Get primary lines & calculate height
		let primaryTextLines = [];
		let foregroundRectangleHeight = Math.ceil(this.canvas.height * 0.2);
		
		if (this.primaryText !== "") {
			const originalPrimaryTextFontSize = primaryTextFontSize;
			
			do {
				this.context.font = `${primaryTextFontSize}px 'Raleway Black'`;
				
				primaryTextLines = splitStringIntoLines(
					this.context,
					this.primaryText,
					primaryTextMaxWidth,
					primaryTextMaxLines,
					true
				)
				
				if (
					primaryTextLines.length > primaryTextMaxLines
					&& (
						primaryTextLines.length * primaryTextFontSize
						> primaryTextMaxLines * originalPrimaryTextFontSize
					)
				) {
					primaryTextFontSize -= 2;
				}
			} while (
				primaryTextLines.length > primaryTextMaxLines
				&& (
					primaryTextLines.length * primaryTextFontSize
					> primaryTextMaxLines * originalPrimaryTextFontSize
				)
			);
			
			foregroundRectangleHeight = (
				primaryTextLines.length * primaryTextFontSize
				+ foregroundRectangleAngle
				+ primaryTextOffsetTopBottom * 2
			);
		}
		
		// Foreground rectangle
		this.context.fillStyle = this.foregroundColor;
		this.context.beginPath();
		
		this.context.moveTo(0, 0);
		this.context.lineTo(this.canvas.width, 0);
		this.context.lineTo(
			this.canvas.width,
			foregroundRectangleHeight - foregroundRectangleAngle
		);
		this.context.lineTo(
			0,
			foregroundRectangleHeight
		);
		this.context.closePath();
		
		this.context.fill();
		
		// Foreground rectangle nipple
		this.context.beginPath();
		this.context.moveTo(
			(this.canvas.width - foregroundRectangleNippleWidth) / 2,
			foregroundRectangleHeight - foregroundRectangleAngle
		);
		this.context.lineTo(
			this.canvas.width / 2,
			foregroundRectangleHeight - foregroundRectangleNippleOffsetBottom
		);
		this.context.lineTo(
			(this.canvas.width + foregroundRectangleNippleWidth) / 2,
			foregroundRectangleHeight - foregroundRectangleAngle
		);
		this.context.closePath();
		
		this.context.fill();
		
		// Big logo image
		const classRef = this;
		const secondaryTextRGB = hexToRgb(this.secondaryTextColor);
		
		const logoBigImageLoadPromise = new Promise(
			resolve => {
				const logoImage = new Image();
				
				logoImage.onload = function() {
					const logoBigHeight = Math.ceil(this.height * (logoBigWidth / this.width));
					
					classRef.context.drawImage(
						colorizeImage(
							this,
							logoBigWidth,
							logoBigHeight,
							secondaryTextRGB.r,
							secondaryTextRGB.g,
							secondaryTextRGB.b
						),
						classRef.canvas.width - logoOffsetSide - logoBigWidth, (
							classRef.canvas.height
							- logoBigHeight
							- logoBigOffsetBottom
						),
						logoBigWidth, logoBigHeight
					);
					
					resolve();
				}
				
				logoImage.src = "static/images/pro-usti/logo-big.png";
			}
		);
		
		await logoBigImageLoadPromise;
		
		// Primary text
		if (this.primaryText !== "") {
			this.context.fillStyle = this.primaryTextColor;
			
			let currentPrimaryLineY = (
				+ primaryTextOffsetTopBottom
				+ primaryTextFontSize
			);
			
			for (const line of primaryTextLines) {
				this.context.fillText(
					line.join(" "),
					(this.canvas.width - primaryTextMaxWidth) / 2,
					currentPrimaryLineY
				);
				
				currentPrimaryLineY += primaryTextFontSize;
			}
		}
		
		// Secondary text
		if (this.secondaryText !== "") {
			this.context.fillStyle = this.secondaryTextColor;
			
			do {
				this.context.font = `${secondaryTextFontSize}px 'Raleway Black'`;
				
				if (this.context.measureText(this.secondaryText).width > secondaryTextMaxWidth) {
					secondaryTextFontSize -= 2;
				}
			} while (this.context.measureText(this.secondaryText).width > secondaryTextMaxWidth);
			
			this.context.textBaseline = "bottom";
			
			this.context.fillText(
				this.secondaryText,
				logoOffsetSide,
				(
					this.canvas.height
					- logoBigOffsetBottom
				)
			);
			
			this.context.textBaseline = "alphabetic";
		}
		
		if (this.requesterText !== "") {
			// https://newspaint.wordpress.com/2014/05/22/writing-rotated-text-on-a-javascript-canvas/
			// Thanks to newspaint!
			
			this.context.save();

			this.context.translate(this.canvas.width - 1, 0);

			this.context.rotate(3 * Math.PI / 2);

			let requesterFontSize = Math.ceil(this.canvas.width * 0.015);
			
			do {
				this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				
				if (
					this.context.measureText(this.requesterText).width
					> this.canvas.height * 0.986
				) {
					requesterFontSize -= 2;
					this.context.font = `${this.primaryFontStyle} ${requesterFontSize}px ${this.primaryFont}`;
				}
			} while (
				this.context.measureText(this.requesterText).width
				> this.canvas.height * 0.986
			);
			
			this.context.fillStyle = this.requesterTextColor;

			this.context.textAlign = "left";

			this.context.globalAlpha = 0.6;
			this.context.fillText(
				this.requesterText,
				-this.canvas.height * 0.993, -this.canvas.width * 0.993 + requesterFontSize
			);
			this.context.globalAlpha = 1;
			
			this.context.restore();
		}
		this.finalDrawHook();
		this.stickerDrawHook();
		
		this.redrawing = false;
	}
	
	// Secondary text
	async setSecondaryText(text, skipRedraw = false) {
		this.secondaryText = text;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async resetIconImage(skipRedraw = false) {
		this.iconImage = null;
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
	
	async setProUstiColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "blue-on-orange":
				this.primaryTextColor = "#133134";
				
				this.foregroundColor = "#51c7bc";
				this.backgroundColor = "#ff6f34";
				
				this.iconColor = "#000000";
				
				this.secondaryTextColor = "#231f20";
				
				this.requesterTextColor = "#000000";
				
				break;
			case "purple-on-pink":
				this.primaryTextColor = "#fdbedb";
				
				this.foregroundColor = "#812b4c";
				this.backgroundColor = "#fdbedb";
				
				this.iconColor = "#af5778";
				
				this.secondaryTextColor = "#812b4c";
				
				this.requesterTextColor = "#000000";
				
				break;
			case "orange-on-brown":
				this.primaryTextColor = "#341202";
				
				this.foregroundColor = "#ff6f34";
				this.backgroundColor = "#341202";
				
				this.iconColor = "#8e4120";
				
				this.secondaryTextColor = "#ff6f34";
				
				this.requesterTextColor = "#ffffff";
				
				break;
			case "pink-on-orange":
				this.primaryTextColor = "#ffa82d";
				
				this.foregroundColor = "#a71e42";
				this.backgroundColor = "#ffa82d";
				
				this.iconColor = "#c9501c";
				
				this.secondaryTextColor = "#a71e42";
				
				this.requesterTextColor = "#000000";
				
				break;
			case "blue-on-white":
				this.primaryTextColor = "#cfebf4";
				
				this.foregroundColor = "#1a79b1";
				this.backgroundColor = "#d2e9f2";
				
				this.iconColor = "#5a9ac1";
				
				this.secondaryTextColor = "#1a79b1";
				
				this.requesterTextColor = "#000000";
				
				break;
			case "green-on-pink":
				this.primaryTextColor = "#ff9d71";
				
				this.foregroundColor = "#1c705e";
				this.backgroundColor = "#ff9d71";
				
				this.iconColor = "#d1512e";
				
				this.secondaryTextColor = "#1c705e";
				
				this.requesterTextColor = "#000000";
				
				break;
			default:
				throw new Error("This scheme does not exist.");
				
				break;
		}
		
		if (!skipRedraw) {
			await this.redrawCanvas();
		}
	}
}
