const colorPickers = {
	"localizationColor": "localization-color-wrapper"
}
const specialNumberFunctions = {
	"jihlava": async function(template) {},
	"kladno": async function(template) {},
	"louny": async function(template) {},
	"most": async function(template) {},
	"olomouc": async function(template) {}
}

// Color picker colors
$(".colorPickSelector").html(`
	<option>Žádná barva</option>
	<option
		value=\"#FFFFFF\"
	>Bílá</option>
	<option
		value=\"#000000\"
	>Černá</option>
	<option
		value=\"#CCCCCC\"
	>Šedá 1</option>
	<option
		value=\"#B3B3B3\"
	>Šedá 2</option>
	<option
		value=\"#868A89\"
	>Šedá 3</option>
	<option
		value=\"#4D4D4D\"
	>Šedá 4</option>
	<option
		value=\"#999999\"
	>Šedá 5</option>
	<option
		value=\"#838383\"
	>Šedá 6</option>
	<option
		value=\"#962A51\"
	>Rudá 1</option>
	<option
		value=\"#712C45\"
	>Rudá 2</option>
	<option
		value=\"#4E1E2E\"
	>Rudá 3</option>
	<option
		value=\"#E63812\"
	>Rudá 4</option>
	<option
		value=\"#BE1E2D\"
	>Rudá 5</option>
	<option
		value=\"#DE2B58\"
	>Růžová 1</option>
	<option
		value=\"#B8284C\"
	>Růžová 2</option>
	<option
		value=\"#4E1E2F\"
	>Růžová 3</option>
	<option
		value=\"#DE8787\"
	>Růžová 4</option>
	<option
		value=\"#D35F5F\"
	>Růžová 5</option>
	<option
		value=\"#A02C2C\"
	>Růžová 6</option>
	<option
		value=\"#E17A85\"
	>Růžová 7</option>
	<option
		value=\"#AA5C65\"
	>Růžová 8</option>
	<option
		value=\"#864B52\"
	>Růžová 9</option>
	<option
		value=\"#FDA972\"
	>Pleťová 1</option>
	<option
		value=\"#ED8B4B\"
	>Pleťová 2</option>
	<option
		value=\"#B46C3D\"
	>Pleťová 3</option>
	<option
		value=\"#FFCC00\"
	>Žlutá 1</option>
	<option
		value=\"#D7B320\"
	>Žlutá 2</option>
	<option
		value=\"#A88D1C\"
	>Žlutá 3</option>
	<option
		value=\"#FFEDA5\"
	>Žlutá 4</option>
	<option
		value=\"#D8CC8E\"
	>Žlutá 5</option>
	<option
		value=\"#A8A573\"
	>Žlutá 6</option>
	<option
		value=\"#E2D7A9\"
	>Žlutá 7</option>
	<option
		value=\"#DAC465\"
	>Žlutá 8</option>
	<option
		value=\"#D7B31C\"
	>Žlutá 9</option>
	<option
		value=\"#FFD500\"
	>Žlutá 10</option>
	<option
		value=\"#FFDD55\"
	>Žlutá 11</option>
	<option
		value=\"#CDDE87\"
	>Limetková 1</option>
	<option
		value=\"#ABC837\"
	>Limetková 2</option>
	<option
		value=\"#89A02C\"
	>Limetková 3</option>
	<option
		value=\"#D5FFD5\"
	>Limetková 4</option>
	<option
		value=\"#41DE87\"
	>Zelená 1</option>
	<option
		value=\"#209A37\"
	>Zelená 2</option>
	<option
		value=\"#206537\"
	>Zelená 3</option>
	<option
		value=\"#78BE43\"
	>Zelená 4</option>
	<option
		value=\"#AFE87E\"
	>Zelená 5</option>
	<option
		value=\"#50C450\"
	>Zelená 6</option>
	<option
		value=\"#8ED4A3\"
	>Zelená 7</option>
	<option
		value=\"#A9CE2D\"
	>Zelená 8</option>
	<option
		value=\"#5ECCC9\"
	>Tyrkysová 1</option>
	<option
		value=\"#3DB6AC\"
	>Tyrkysová 2</option>
	<option
		value=\"#199887\"
	>Tyrkysová 3</option>
	<option
		value=\"#52D7CB\"
	>Tyrkysová 4</option>
	<option
		value=\"#5FBFB4\"
	>Tyrkysová 5</option>
	<option
		value=\"#32948B\"
	>Tyrkysová 6</option>
	<option
		value=\"#26A4B7\"
	>Modrá 1</option>
	<option
		value=\"#0A4E5B\"
	>Modrá 2</option>
	<option
		value=\"#083F49\"
	>Modrá 3</option>
	<option
		value=\"#18C1DF\"
	>Modrá 4</option>
	<option
		value=\"#14A0B9\"
	>Modrá 5</option>
	<option
		value=\"#307684\"
	>Modrá 6</option>
	<option
		value=\"#123172\"
	>Modrá 7</option>
	<option
		value=\"#00173C\"
	>Modrá 8</option>
	<option
		value=\"#0038D1\"
	>Modrá 9</option>
	<option
		value=\"#AAEEFF\"
	>Světle modrá 1</option>
	<option
		value=\"#87CDDE\"
	>Světle modrá 2</option>
	<option
		value=\"#2C89A0\"
	>Světle modrá 3</option>
	<option
		value=\"#9796CA\"
	>Fialová 1</option>
	<option
		value=\"#585B88\"
	>Fialová 2</option>
	<option
		value=\"#21274E\"
	>Fialová 3</option>
	<option
		value=\"#DE87CD\"
	>Fialová 4</option>
	<option
		value=\"#D35FBC\"
	>Fialová 5</option>
	<option
		value=\"#782167\"
	>Fialová 6</option>
	<option
		value=\"#7B367C\"
	>Fialová 7</option>
	<option
		value=\"#5B305C\"
	>Fialová 8</option>
	<option
		value=\"#422A42\"
	>Fialová 9</option>
	<option
		value=\"#6E1646\"
	>Fialová 10</option>
	<option
		value=\"#3E2A5B\"
	>Fialová 11</option>
	<option
		value=\"#9796CA\"
	>Fialová 12</option>
	
	<option
		value=\"#6AA82E\"
	>Zelená - Zelená Koalice 1</option>
	<option
		value=\"#87CF32\"
	>Zelená - Zelená Koalice 2</option>
	<option
		value=\"#B5E179\"
	>Zelená - Zelená Koalice 3</option>
	<option
		value=\"#00AD43\"
	>Zelená - Zelená Koalice 4</option>
	<option
		value=\"#095C41\"
	>Zelená - Zelená Koalice 5</option>
	<option
		value=\"#9BA56B\"
	>Zelená - Zelená Koalice 6</option>
	
	<option
		value=\"#D5C980\"
	>Béžová - Zelená Koalice 1</option>
	<option
		value=\"#FEE993\"
	>Béžová - Zelená Koalice 2</option>
	<option
		value=\"#E1D298\"
	>Béžová - Zelená Koalice 3</option>
	<option
		value=\"#D7C256\"
	>Béžová - Zelená Koalice 4</option>
	
	<option
		value=\"#DBB01A\"
	>Žlutá - Zelená Koalice 1</option>
	<option
		value=\"#A0891F\"
	>Žlutá - Zelená Koalice 2</option>
	<option
		value=\"#DBB01C\"
	>Žlutá - Zelená Koalice 3</option>
	<option
		value=\"#FBC317\"
	>Žlutá - Zelená Koalice 4</option>
	
	<option
		value=\"#F7946F\"
	>Růžová - Zelená Koalice 1</option>
	<option
		value=\"#F57E49\"
	>Růžová - Zelená Koalice 2</option>
	<option
		value=\"#C5623B\"
	>Růžová - Zelená Koalice 3</option>
	
	<option
		value=\"#8E3E53\"
	>Rudá - Zelená Koalice 1</option>
	<option
		value=\"#BA4B6A\"
	>Rudá - Zelená Koalice 2</option>
	<option
		value=\"#F36287\"
	>Rudá - Zelená Koalice 3</option>
	<option
		value=\"#B52733\"
	>Rudá - Zelená Koalice 4</option>
	<option
		value=\"#F14B61\"
	>Rudá - Zelená Koalice 5</option>
	<option
		value=\"#F57386\"
	>Rudá - Zelená Koalice 6</option>
	<option
		value=\"#EE2462\"
	>Rudá - Zelená Koalice 7</option>
	<option
		value=\"#D71E55\"
	>Rudá - Zelená Koalice 8</option>
	<option
		value=\"#871F3F\"
	>Rudá - Zelená Koalice 9</option>
	<option
		value=\"#4F1B2D\"
	>Rudá - Zelená Koalice 10</option>
	<option
		value=\"#752245\"
	>Rudá - Zelená Koalice 11</option>
	<option
		value=\"#A72157\"
	>Rudá - Zelená Koalice 12</option>
	
	<option
		value=\"#831F6D\"
	>Fialová - Zelená Koalice 1</option>
	<option
		value=\"#D94EAE\"
	>Fialová - Zelená Koalice 2</option>
	<option
		value=\"#E772C0\"
	>Fialová - Zelená Koalice 3</option>
	<option
		value=\"#842F89\"
	>Fialová - Zelená Koalice 4</option>
	<option
		value=\"#5B2C5F\"
	>Fialová - Zelená Koalice 5</option>
	<option
		value=\"#3B2641\"
	>Fialová - Zelená Koalice 6</option>
	
	<option
		value=\"#8E8FD0\"
	>Modrá - Zelená Koalice 1</option>
	<option
		value=\"#4B5C92\"
	>Modrá - Zelená Koalice 2</option>
	<option
		value=\"#1E2D56\"
	>Modrá - Zelená Koalice 3</option>
	
	<option
		value=\"#1692AA\"
	>Tyrkysová - Zelená Koalice 1</option>
	<option
		value=\"#66CCD8\"
	>Tyrkysová - Zelená Koalice 2</option>
	<option
		value=\"#8FDAE9\"
	>Tyrkysová - Zelená Koalice 3</option>
	<option
		value=\"#1C7A8D\"
	>Tyrkysová - Zelená Koalice 4</option>
	<option
		value=\"#09AAC2\"
	>Tyrkysová - Zelená Koalice 5</option>
	<option
		value=\"#14B2CF\"
	>Tyrkysová - Zelená Koalice 6</option>
	<option
		value=\"#0F404C\"
	>Tyrkysová - Zelená Koalice 7</option>
	<option
		value=\"#115161\"
	>Tyrkysová - Zelená Koalice 8</option>
	<option
		value=\"#0DAEC0\"
	>Tyrkysová - Zelená Koalice 9</option>
	<option
		value=\"#38BCBB\"
	>Tyrkysová - Zelená Koalice 10</option>
	<option
		value=\"#1AB2AA\"
	>Tyrkysová - Zelená Koalice 11</option>
	<option
		value=\"#07A693\"
	>Tyrkysová - Zelená Koalice 12</option>
	<option
		value=\"#1AB2AA\"
	>Tyrkysová - Zelená Koalice 13</option>
	<option
		value=\"0DAC89\"
	>Tyrkysová - Zelená Koalice 14</option>
	<option
		value=\"#107460\"
	>Tyrkysová - Zelená Koalice 15</option>
	
	<option
		value=\"#C4C5C5\"
	>Šedá - Zelená Koalice 1</option>
	<option
		value=\"#A9AEAF\"
	>Šedá - Zelená Koalice 2</option>
	<option
		value=\"#798789\"
	>Šedá - Zelená Koalice 3</option>
	<option
		value=\"#3E494D\"
	>Šedá - Zelená Koalice 4</option>
	<option
		value=\"#273234\"
	>Šedá - Zelená Koalice 5</option>
	<option
		value=\"#1D272C\"
	>Šedá - Zelená Koalice 6</option>
	
	<option
		value=\"#133134\"
	>PRO! Ústí - Modrá 1</option>
	<option
		value=\"#51C7BC\"
	>PRO! Ústí - Modrá 2</option>
	<option
		value=\"#CFEBF4\"
	>PRO! Ústí - Modrá 3</option>
	<option
		value=\"#D2E9F2\"
	>PRO! Ústí - Modrá 4</option>
	<option
		value=\"#1A79B1\"
	>PRO! Ústí - Modrá 5</option>
	<option
		value=\"#5A9AC1\"
	>PRO! Ústí - Modrá 6</option>
	<option
		value=\"#BBD5E6\"
	>PRO! Ústí - Modrá 7</option>
	
	<option
		value=\"#FF6F34\"
	>PRO! Ústí - Oranžová 1</option>
	<option
		value=\"#DD5027\"
	>PRO! Ústí - Oranžová 2</option>
	<option
		value=\"#FF6F34\"
	>PRO! Ústí - Oranžová 3</option>
	<option
		value=\"#D1512E\"
	>PRO! Ústí - Oranžová 4</option>
	
	<option
		value=\"#FFA82D\"
	>PRO! Ústí - Žlutá 1</option>
	<option
		value=\"#FD9529\"
	>PRO! Ústí - Žlutá 2</option>
	
	<option
		value=\"#2C2E35\"
	>PRO! Ústí - Šedá 1</option>
	
	<option
		value=\"#FDBEDB\"
	>PRO! Ústí - Růžová 1</option>
	<option
		value=\"#812B4C\"
	>PRO! Ústí - Růžová 2</option>
	<option
		value=\"#EDA7C5\"
	>PRO! Ústí - Růžová 3</option>
	<option
		value=\"#AF5778\"
	>PRO! Ústí - Růžová 4</option>
	<option
		value=\"#A71E42\"
	>PRO! Ústí - Růžová 5</option>
	<option
		value=\"#FF9D71\"
	>PRO! Ústí - Růžová 6</option>
	<option
		value=\"#FF8F62\"
	>PRO! Ústí - Růžová 7</option>
	
	<option
		value=\"#341202\"
	>PRO! Ústí - Hnědá 1</option>
	<option
		value=\"#261005\"
	>PRO! Ústí - Hnědá 2</option>
	<option
		value=\"#8E4120\"
	>PRO! Ústí - Hnědá 3</option>
	<option
		value=\"#C9501C\"
	>PRO! Ústí - Hnědá 4</option>
	<option
		value=\"#231F20\"
	>PRO! Ústí - Hnědá 5</option>
	
	<option
		value=\"#1C705E\"
	>PRO! Ústí - Zelená 1</option>`);

// https://stackoverflow.com/a/50300880
// Thanks to Ulf Aslak!
$("#download").on(
	"click",
	function(){
		let link = document.createElement('a');
		
		link.download = "Ted uz to fakt vyhrajem!.png";
		link.href = document.getElementById("graphicsCanvas").toDataURL()
		
		link.click();
	}
);


$("#image").on(
	"change",
	function(event) { template.loadImageFromElement(event.target); }
);


$("#reset-image").on(
	"click",
	function(event) {
		template.resetImage();
		
		$("#image").val(null);
	}
);


//// Localization
// Own localization
$("#own-icon-image").on(
	"change",
	function(event) { template.loadIconFromElement(event.target); }
);

$("#reset-own-icon").on(
	"click",
	function(event) {
		template.resetIconImage();
		
		$("#own-icon-image").val(null);
	}
);

// Selected localization
$("#localization-selection").select2({
	templateSelection: formatSelect2ImageData,
	templateResult: formatSelect2ImageData,
	selectionCssClass: "select2-container-medium-images",
	dropdownCssClass: "select2-container-medium-images",
	width: "100%"
});

$("#localization-selection").on(
	"select2:select",
	function(event) {
		const data = event.params.data.element.dataset;
		let source = data.imageLocationSource;
		
		if (source === undefined || source === null) {
			source = "";
		}
		
		template.setLocalizationSource(source);
		
		if (data.url !== undefined && data.url !== null) {
			$("#localization-permalink-wrapper").css("display", "block");
			
			let url = data.url;
			
			if (
				$("#color-scheme-selection").select2("data").length !== 0 &&
				$("#color-scheme-selection").select2("data")[0].element.dataset.colorScheme === "white-on-black"
			) {
				url += "?tmava=ano";
			}
			
			$("#localization-permalink").attr("href", url);
			$("#localization-permalink").html(url);
		} else {
			$("#localization-permalink-wrapper").css("display", "none");
		}
	}
);

// Icons
$("#icon-selection").select2({
	templateSelection: formatSelect2ImageData,
	templateResult: formatSelect2ImageData,
	selectionCssClass: "select2-container-medium-images",
	dropdownCssClass: "select2-container-medium-images",
	width: "100%"
});


$("#icon-selection").on(
	"select2:select",
	function(event) {
		const element = event.params.data.element;
		
		const value = element.value;
		const dataset = element.dataset;
		
		template.darkIconDefaultSource = dataset.iconDarkSource;
		template.lightIconDefaultSource = dataset.iconLightSource;
		
		template.iconHeightMultiplier = (
			(ICON_HEIGHT_SPECIAL_MULTIPLIERS[value] !== undefined) ?
			ICON_HEIGHT_SPECIAL_MULTIPLIERS[value] :
			1
		);
		
		template.iconOffsetBottomMultiplier = (
			(ICON_OFFSET_BOTTOM_SPECIAL_MULTIPLIERS[value] !== undefined) ?
			ICON_OFFSET_BOTTOM_SPECIAL_MULTIPLIERS[value] :
			1
		);
		
		if (dataset.url !== undefined && dataset.url !== null) {
			let url = dataset.url;
			
			if (
				$("#color-scheme-selection").select2("data").length !== 0 &&
				$("#color-scheme-selection").select2("data")[0].element.dataset.colorScheme === "white-on-black"
			) {
				url += "?tmava=ano";
			}
			
			$("#icon-permalink").attr("href", url);
			$("#icon-permalink").html(url);
		} else {
			$("#icon-permalink").attr("href", defaultIconLink)
			$("#icon-permalink").html(defaultIconLink);;
		}
		
		template.redrawCanvas();
	}
);

// Text things
$("#text").on(
	"input",
	function(event) { template.setText(event.currentTarget.value); }
);

$("#set-pirate-candidate").on(
	"click",
	async function(event) {
		// Make absolutely sure Bebas Neue is loaded.
		// Quick fix
		await (
			fetch("/static/fonts/bebas-neue/BebasNeue.woff")
			.then(resp => resp.arrayBuffer())
			.then(font => {
				const fontFace = new FontFace("Bebas Neue", font);
				document.fonts.add(fontFace);
			})
		);
		
		$("#text").val("Kandiduju za Piráty!");
		$("#text").trigger("input");
	}
);


$("#color-scheme-selection").select2({
	templateSelection: formatSelect2ImageData,
	templateResult: formatSelect2ImageData,
	selectionCssClass: "select2-container-small-images",
	dropdownCssClass: "select2-container-small-images",
	width: "100%"
});


$("#color-scheme-selection").on(
	"select2:select",
	function(event) {
		const colorScheme = event.params.data.element.dataset.colorScheme;
		
		template.setColorScheme(colorScheme);
		
		for (const permalink of $("#icon-permalink,#localization-permalink")) {
			if (colorScheme === "white-on-black") {
				permalink.innerHTML += "?tmava=ano";
			} else {
				permalink.innerHTML = permalink.innerHTML.replace("?tmava=ano", "");
			}
			
			permalink.href = permalink.innerHTML;
		}
	}
);

$("#show-gradient").on(
	"change",
	function(event) {
		template.setGradient(event.currentTarget.checked);
	}
);


$("#move-image-left").on(
	"click",
	function(event) { template.moveImageLeft(); }
);

$("#move-image-up").on(
	"click",
	function(event) { template.moveImageUp(); }
);

$("#move-image-right").on(
	"click",
	function(event) { template.moveImageRight(); }
);

$("#move-image-down").on(
	"click",
	function(event) { template.moveImageDown(); }
);

$("#move-image-reset").on(
	"click",
	function(event) { template.resetImagePosition(); }
);


// Filters
$("#filter-color").select2({
	templateSelection: formatSelect2ColorData,
	templateResult: formatSelect2ColorData,
	selectionCssClass: "select2-container-small-images",
	dropdownCssClass: "select2-container-small-images",
	width: "100%"
});

$("#filter-color").on(
	"select2:select",
	function(event) {
		const value = event.params.data.element.value;
		
		if (value === undefined || value === null || value === "Žádná barva") {
			template.resetFilterColor();
			return;
		}
		
		template.setFilterColor(value);
	}
);


$("#localization-color").select2({
	templateSelection: formatSelect2ColorData,
	templateResult: formatSelect2ColorData,
	selectionCssClass: "select2-container-small-images",
	dropdownCssClass: "select2-container-small-images",
	width: "100%"
});


$("#localization-color").on(
	"select2:select",
	function(event) {
		const value = event.params.data.element.value;
		
		if (value === undefined || value === null || value === "Žádná barva") {
			template.resetLocalizationColor();
			return;
		}
		
		template.setLocalizationColor(value);
	}
);

// https://stackoverflow.com/a/15037102
// Thanks to markE!

let canvas = document.getElementById("graphicsCanvas");
let canvasOffset = $("#graphicsCanvas").offset();
let canvasBoundingClientRect = canvas.getBoundingClientRect();
let offsetX = canvasOffset.left;
let offsetY = canvasOffset.top;
let canvasWidth = canvas.width;
let canvasHeight = canvas.height;
let isDragging = false;

$(window).on("resize", function(){
	canvasOffset = $("#graphicsCanvas").offset();
	offsetX = canvasOffset.left;
	offsetY = canvasOffset.top;
	
	canvasBoundingClientRect = canvas.getBoundingClientRect();
});

$("#graphicsCanvas").on(
	"mousedown",
	function(event){
		canMouseX=parseInt(event.clientX-offsetX)
		canMouseY=parseInt(event.clientY-offsetY);
		// set the drag flag
		isDragging=true;
	}
);

$("#graphicsCanvas").on(
	"mousemove",
	function(event){
		canMouseX=parseInt(event.clientX-offsetX);
		canMouseY=parseInt(event.clientY-offsetY);
		// if the drag flag is set, clear the canvas and draw the image
		
		if (isDragging) {
			template.imageX = (
				(
					canvas.width
					* (canMouseX / canvasBoundingClientRect.width)
				)
				- canvas.width / 2
			);
			template.imageY = (
				(
					canvas.height
					* (canMouseY / canvasBoundingClientRect.height)
				)
				- canvas.height / 2
			);
			
			template.redrawCanvas();
		}
	}
);

$("#graphicsCanvas").on(
	"mouseup",
	function(event){
		canMouseX=parseInt(event.clientX-offsetX);
		canMouseY=parseInt(event.clientY-offsetY);
		// clear the drag flag
		isDragging=false;
	}
);

$("#graphicsCanvas").on(
	"mouseout",
	function(event){
		canMouseX=parseInt(event.clientX-offsetX);
		canMouseY=parseInt(event.clientY-offsetY);
		// user has left the canvas, so clear the drag flag
		isDragging=false;
	}
);

// Zoom

$("#zoom-image-input").on(
	"input",
	function(event) { template.setImageZoom(parseInt(event.target.value) * 0.01); }
);

$("#zoom-image-reset").on(
	"click",
	function(event) { template.resetImageZoom(); }
);


$(window).ready(
	async function() {
		template = new AvatarTemplate("graphicsCanvas");
		
		const imageElement = document.getElementById("image");
		
		if (imageElement.files.length !== 0) {
			await template.loadImageFromElement(imageElement, true);
		}
		
		const localizationElement = document.getElementById("own-icon-image");
		
		if (localizationElement.files.length !== 0) {
			await template.loadIconFromElement(localizationElement, true);
		} else if (
			$("#localization-selection").select2("data").length !== 0 &&
			$("#localization-selection").select2("data")[0].element.dataset.imageLocationSource !== undefined
		) {
			template.setLocalizationSource(
				$("#localization-selection").select2("data")[0].element.dataset.imageLocationSource,
				true
			);
		}
		
		const iconSelectionSelect2 = $("#icon-selection").select2("data");
		
		if (
			iconSelectionSelect2.length !== 0 &&
			iconSelectionSelect2[0].element.dataset.iconDarkSource !== undefined &&
			iconSelectionSelect2[0].element.dataset.iconLightSource !== undefined
		) {
			template.darkIconDefaultSource = iconSelectionSelect2[0].element.dataset.iconDarkSource;
			template.lightIconDefaultSource = iconSelectionSelect2[0].element.dataset.iconLightSource;
			
			template.iconHeightMultiplier = (
				(ICON_HEIGHT_SPECIAL_MULTIPLIERS[iconSelectionSelect2[0].element.value] !== undefined) ?
				ICON_HEIGHT_SPECIAL_MULTIPLIERS[iconSelectionSelect2[0].element.value] :
				1
			);
			
			template.iconOffsetBottomMultiplier = (
				(ICON_OFFSET_BOTTOM_SPECIAL_MULTIPLIERS[iconSelectionSelect2[0].element.value] !== undefined) ?
				ICON_OFFSET_BOTTOM_SPECIAL_MULTIPLIERS[iconSelectionSelect2[0].element.value] :
				1
			);
		}
		
		template.setText($("#text").val(), true);
		template.setNumber($("#number").val(), true);
		
		if (
			$("#color-scheme-selection").select2("data").length !== 0 &&
			$("#color-scheme-selection").select2("data")[0].element.dataset.colorScheme !== undefined
		) {
			template.setColorScheme($("#color-scheme-selection").select2("data")[0].element.dataset.colorScheme, true);
		}
		
		if ($("#filter-color").select2("data").length !== 0) {
			const value = $("#filter-color").select2("data")[0].element.value;
			
			if (value !== undefined && value !== null && value !== "Žádná barva") {
				template.setFilterColor(value);
			}
		}
		
		template.setGradient($("#show-gradient").is(":checked"), true);
		
		template.redrawCanvas();
	}
);
