// https://stackoverflow.com/a/5624139
// Thanks to Tim Down!
function hexToRgb(hex) {
	// Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
	var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
	
	hex = hex.replace(shorthandRegex, function(m, r, g, b) {
		return r + r + g + g + b + b;
	});
	
	var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	return result ? {
		r: parseInt(result[1], 16),
		g: parseInt(result[2], 16),
		b: parseInt(result[3], 16)
	} : null;
}

$(window).ready(
	async function() {
		// https://stackoverflow.com/a/50300880
		// Thanks to Ulf Aslak!
		// Image downloading
		$("#download").on(
			"click",
			function(){
				let link = document.createElement('a');
				
				link.download = "Ted uz to fakt vyhrajem!.png";
				link.href = document.getElementById("avatar-canvas").toDataURL()
				
				link.click();
			}
		);
		
		// Image uploading - hidden input
		$("#upload-image").on(
			"change",
			function(event) {
				if (event.target.files.length !== 0) {
					$("#image-controls").css("display", "flex");
				}
				
				template.loadImageFromElement(event.target);
			}
		);
		
		// Image uploading - visible input
		$("#upload-image-button").on(
			"click",
			function(event) {
				template.resetImage();
				
				$("#upload-image").val(null);
				$("#image-controls").css("display", "none");
				$("#upload-image").click();
			}
		);
		
		// Image positioning
		$("#move-image-left").on(
			"click",
			function(event) { template.moveImageLeft(); }
		);
		
		$("#move-image-up").on(
			"click",
			function(event) { template.moveImageUp(); }
		);
		
		$("#move-image-right").on(
			"click",
			function(event) { template.moveImageRight(); }
		);
		
		$("#move-image-down").on(
			"click",
			function(event) { template.moveImageDown(); }
		);
		
		$("#move-image-reset").on(
			"click",
			function(event) { template.resetImagePosition(); }
		);
		
		// Image zooming
		$("#zoom-image-input").on(
			"input",
			function(event) { template.setImageZoom(parseInt(event.target.value) * 0.01); }
		);
		
		$("#zoom-image-reset").on(
			"click",
			function(event) { template.resetImageZoom(); }
		);
		
		// https://stackoverflow.com/a/15037102
		// Thanks to markE!
		let canvas = document.getElementById("avatar-canvas");
		let canvasOffset = $("#avatar-canvas").offset();
		let canvasBoundingClientRect = canvas.getBoundingClientRect();
		let offsetX = canvasOffset.left;
		let offsetY = canvasOffset.top;
		let canvasWidth = canvas.width;
		let canvasHeight = canvas.height;
		let isDragging = false;
		
		$(window).on("resize", function(){
			canvasOffset = $("#avatar-canvas").offset();
			offsetX = canvasOffset.left;
			offsetY = canvasOffset.top;
			
			canvasBoundingClientRect = canvas.getBoundingClientRect();
		});
		
		$("#avatar-canvas").on(
			"mousedown",
			function(event){
				canMouseX=parseInt(event.clientX-offsetX)
				canMouseY=parseInt(event.clientY-offsetY);
				// set the drag flag
				isDragging=true;
			}
		);
		
		$("#avatar-canvas").on(
			"mousemove",
			function(event){
				canMouseX=parseInt(event.clientX-offsetX);
				canMouseY=parseInt(event.clientY-offsetY);
				// if the drag flag is set, clear the canvas and draw the image
				
				if (isDragging) {
					template.imageX = (
						(
							canvas.width
							* (canMouseX / canvasBoundingClientRect.width)
						)
						- canvas.width / 2
					);
					template.imageY = (
						(
							canvas.height
							* (canMouseY / canvasBoundingClientRect.height)
						)
						- canvas.height / 2
					);
					
					template.redrawCanvas();
				}
			}
		);
		
		$("#avatar-canvas").on(
			"mouseup",
			function(event){
				canMouseX=parseInt(event.clientX-offsetX);
				canMouseY=parseInt(event.clientY-offsetY);
				// clear the drag flag
				isDragging=false;
			}
		);
		
		$("#avatar-canvas").on(
			"mouseout",
			function(event){
				canMouseX=parseInt(event.clientX-offsetX);
				canMouseY=parseInt(event.clientY-offsetY);
				// user has left the canvas, so clear the drag flag
				isDragging=false;
			}
		);
		
		console.log(template);
		
		// Load template
		template = new AvatarTemplate("avatar-canvas");
		
		const imageElement = document.getElementById("upload-image");
		
		if (imageElement.files.length !== 0) {
			$("#image-controls").css("display", "flex");
			await template.loadImageFromElement(imageElement, true);
		} else {
			$("#image-controls").css("display", "none");
		}
		
		await init();
	}
);
