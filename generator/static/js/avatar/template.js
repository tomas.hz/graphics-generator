class AvatarTemplate {
	constructor(canvasId) {
		this.canvas = document.getElementById(canvasId);
		this.context = this.canvas.getContext("2d");
	}
	
	darkIconDefaultSource = "/static/images/badges/default-dark.png";
	lightIconDefaultSource = "/static/images/badges/default-light.png";
	
	text = "";
	number = "";
	
	localizationSource = "";
	iconImage = null;
	
	primaryFontStyle = "";
	primaryFont = "'Bebas Neue'";
	
	image = null;
	imageX = 0;
	imageY = 0;
	imageZoom = 1;
	
	localizationColor = "#ffffff";
	filterColor = null;
	
	iconHeightMultiplier = 1;
	iconOffsetBottomMultiplier = 1;

	hasGradient = false;

	redrawing = false;
	
	specialNumberFunction = null;
	
	async redrawCanvas() {
		if (this.redrawing) {
			return;
		}
		
		this.redrawing = true;
		
		const localizationWidth = Math.ceil(this.canvas.width * 0.5);
		const localizationAngle = Math.ceil(this.canvas.height * 0.01);
		const localizationBottomOffset = Math.ceil(this.canvas.height * 0.0075);
		const localizationRectangleHeightDivider = 1.5;
		
		const textMaxWidth = Math.ceil(this.canvas.width * 0.35);
		const textOffsetSide = Math.ceil(this.canvas.width * 0.025);
		let textFontSize = Math.ceil(this.canvas.height * 0.11);
		let textMaxLines = 2;
		let textOffsetTop = 0;
		
		const gradientHeightDivider = 1;
		
		const iconHeight = Math.ceil(this.canvas.height * 0.25 * this.iconHeightMultiplier);
		const iconOffsetBottom = Math.ceil(this.canvas.height * 0.05 * this.iconOffsetBottomMultiplier);
		
		this.context.clearRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		if (this.image !== null) {
			let image = this.image;
			
			let filterColorRGB = null;
			
			if (this.filterColor !== null) {
				filterColorRGB = hexToRgb(this.filterColor);
				
				const filterColorLightness = (
					0.2126 * filterColorRGB.r
					+ 0.7152 * filterColorRGB.g
					+ 0.0722 * filterColorRGB.b
				);
				
				const contrastDifference = Math.max(100, (filterColorLightness / -255) * 150);
				
				this.context.filter = `grayscale(1) contrast(${contrastDifference}%)`;
			} else {
				this.context.filter = "none";
			}
			
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D

			const imageScaleX = this.canvas.width / image.width;
			const imageScaleY = this.canvas.height / image.height;

			const imageScale = Math.max(imageScaleX, imageScaleY) * this.imageZoom;
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			this.context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(this.canvas.width - image.width * imageScale) / 2 + this.imageX * this.imageZoom,
				(this.canvas.height - image.height * imageScale) / 2 + this.imageY * this.imageZoom,
			);
			this.context.drawImage(
				image,
				0, 0
			);
			this.context.setTransform(); // Reset transformation
			
			if (this.filterColor !== null) {
				colorTextGignac(
					this.canvas,
					this.context,
					filterColorRGB.r,
					filterColorRGB.g,
					filterColorRGB.b,
					this.canvas.width,
					this.canvas.height
				);
				
				this.context.filter = "none";
			}
		}
		
		const classRef = this;
		
		const localizationRGB = hexToRgb(classRef.localizationColor);
		
		const localizationLightness = (
			0.2126 * localizationRGB.r
			+ 0.7152 * localizationRGB.g
			+ 0.0722 * localizationRGB.b
		);
		
		let textLines = [];
		let maxTextWidth = 0;
		
		if (this.text !== "") {
			const originalHeight = textFontSize * textMaxLines;
			
			do {
				this.context.font = `${this.primaryFontStyle} ${textFontSize}px ${this.primaryFont}`;
				
				textLines = splitStringIntoLines(
					this.context,
					this.text,
					textMaxWidth,
					textMaxLines,
					true
				);
				
				if (textLines.length > textMaxLines) {
					textFontSize -= 2;
					textOffsetTop += 2;
				}
				
				if (((textMaxLines + 1) * textFontSize) < originalHeight) {
					textMaxLines += 1;
					textOffsetTop -= textFontSize / 2;
				}
				
				for (let line of textLines) {
					const lineWidth = this.context.measureText(line.join(" ")).width;
					
					if (lineWidth > maxTextWidth) {
						maxTextWidth = lineWidth;
					}
				}
			} while (textLines.length > textMaxLines);
		}
		
		const iconLoadPromise = new Promise(
			resolve => {
				let icon = new Image();
				
				icon.onload = async function() {
					await drawIcon(this);
				}
				
				async function drawIcon(image) {
					const iconWidth = Math.ceil(image.width * (iconHeight / image.height))
					
					let usingLocalization = false;
					
					if (classRef.localizationSource !== "") {
						usingLocalization = true;
						
						const locImageLoadPromise = new Promise(
							resolve => {
								let localizationImage = new Image();
								
								localizationImage.onload = function() {
									let temporaryCanvas = document.createElement("canvas");
									
									const localizationHeight = (
										this.height
										* (localizationWidth / this.width)
									);
									
									temporaryCanvas.width = localizationWidth;
									temporaryCanvas.height = localizationHeight;
									
									let temporaryContext = temporaryCanvas.getContext("2d");
									
									temporaryContext.drawImage(
										this,
										0, 0,
										localizationWidth, localizationHeight
									);
									
									let pixels = temporaryContext.getImageData(
										0, 0,
										localizationWidth, localizationHeight
									);
									
									for (let pixelPosition = 0; pixelPosition < pixels.data.length; pixelPosition += 4) {
										pixels.data[pixelPosition] = pixels.data[pixelPosition] / 255 * localizationRGB.r;
										pixels.data[pixelPosition + 1] = pixels.data[pixelPosition + 1] / 255 * localizationRGB.g;
										pixels.data[pixelPosition + 2] = pixels.data[pixelPosition + 2] / 255 * localizationRGB.b;
									}
									
									temporaryContext.putImageData(
										pixels,
										0, 0
									);
									
									
									classRef.context.beginPath();
									
									classRef.context.fillStyle = classRef.localizationColor;
									
									classRef.context.moveTo(0, classRef.canvas.height);
									classRef.context.lineTo(classRef.canvas.width, classRef.canvas.height);
									classRef.context.lineTo(classRef.canvas.width, (
										classRef.canvas.height
										- iconOffsetBottom
										- iconHeight / localizationRectangleHeightDivider
										- localizationAngle
									));
									classRef.context.lineTo(0, (
										classRef.canvas.height
										- iconOffsetBottom
										- iconHeight / localizationRectangleHeightDivider
									));
									
									classRef.context.closePath();
									classRef.context.fill();
									
									classRef.context.drawImage(
										temporaryCanvas,
										(
											classRef.canvas.width
											- localizationWidth
										), (
											classRef.canvas.height
											- iconOffsetBottom
											- iconHeight / localizationRectangleHeightDivider
											- localizationHeight
											+ localizationBottomOffset
										)
									);
									
									resolve();
								}
								
								localizationImage.src = classRef.localizationSource;
							}
						);
						
						await locImageLoadPromise;
					}
					
					let temporaryCanvas = document.createElement("canvas");
					
					temporaryCanvas.height = iconHeight * 1.2;
					temporaryCanvas.width = iconWidth + (
						(textLines.length !== 0) ?
						(
							+ textOffsetSide
							+ maxTextWidth
						) : 0
					);
					
					let temporaryContext = temporaryCanvas.getContext("2d");
					
					temporaryContext.drawImage(
						image,
						0, 0,
						iconWidth, iconHeight
					);
					
					temporaryContext.font = `${classRef.primaryFontStyle} ${textFontSize}px ${classRef.primaryFont}`;
					temporaryContext.fillStyle = (
						(classRef.localizationSource !== "") ?
						classRef.textColor : classRef.localizationColor
					);
					
					if (textLines.length !== 0) {
						let currentTextLineY = textFontSize + textOffsetTop;
						temporaryContext.textAlign = "left";
						
						for (let line of textLines) {
							temporaryContext.fillText(
								line.join(" "),
								(
									iconWidth
									+ textOffsetSide
								),
								 currentTextLineY
							);
							
							currentTextLineY += textFontSize;
						}
					}
					
					if (!usingLocalization && classRef.hasGradient) {
						const gradientLoadPromise = new Promise(
							resolve => {
								const gradient = new Image();
								
								gradient.onload = function() {
									const height = (
										iconOffsetBottom
										+ iconHeight / gradientHeightDivider
									);
									
									classRef.context.drawImage(
										colorizeImage(
											this,
											this.width,
											this.height,
											localizationRGB.r,
											localizationRGB.g,
											localizationRGB.b
										),
										0, classRef.canvas.height - height,
										classRef.canvas.width, height
									);
									
									resolve();
								}
								
								gradient.src = "/static/images/gradient.png";
							}
						);
						
						await gradientLoadPromise;
					}
					
					classRef.context.drawImage(
						temporaryCanvas,
						(classRef.canvas.width - temporaryCanvas.width)/2,
						classRef.canvas.height - iconOffsetBottom - iconHeight,
						temporaryCanvas.width, temporaryCanvas.height
					);
					
					resolve();
				}
				
				if (classRef.iconImage !== null) {
					drawIcon(classRef.iconImage);
				} else if (localizationLightness < 127.5) {
					icon.src = classRef.darkIconDefaultSource;
				} else {
					icon.src = classRef.lightIconDefaultSource;
				}
			}
		);
		
		await iconLoadPromise;
		
		if (this.number !== "") {
			if (this.specialNumberFunction !== null) {
				await this.specialNumberFunction();
			} else {
				// TODO
			}
		}
		
		this.redrawing = false;
	}
	
	setColorScheme(scheme, skipRedraw = false) {
		switch (scheme) {
			case "black-on-white":
				this.localizationColor = "#ffffff";
				this.textColor = "#000000";
				break;
			case "white-on-black":
				this.localizationColor = "#000000";
				this.textColor = "#ffffff";
				break;
			default:
				throw new Error(`Inexistent color scheme: ${scheme}`);
				break;
		}
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	setLocalizationColor(color, skipRedraw = false) {
		this.localizationColor = color;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	resetLocalizationColor(skipRedraw = false) {
		template.localizationColor = "#ffffff";
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	setFilterColor(color, skipRedraw = false) {
		this.filterColor = color;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	resetFilterColor(skipRedraw = false) {
		template.filterColor = null;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	async loadIconFromElement(element, skipRedraw = false) {
		const fileReader = new FileReader();
		
		let classRef = this;
		
		const readPromise = new Promise(
			resolve => {
				fileReader.onloadend = function(event) {
					classRef.iconImage = new Image();
					
					classRef.iconImage.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.iconImage.src = event.target.result;
				}
				
				fileReader.readAsDataURL(element.files[0]);
			}
		);
		
		await readPromise;
	}
	
	setLocalizationSource(source, skipRedraw = false) {
		this.localizationSource = source;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	resetIconImage(skipRedraw = false) {
		this.iconImage = null;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	async loadImageFromElement(element, skipRedraw = false) {
		const fileReader = new FileReader();
		
		let classRef = this;
		
		const readPromise = new Promise(
			resolve => {
				fileReader.onloadend = function(event) {
					classRef.image = new Image();
					
					classRef.image.onload = function() {
						if (!skipRedraw) {
							classRef.redrawCanvas();
						}
						
						resolve();
					}
					
					classRef.image.src = event.target.result;
				}
				
				fileReader.readAsDataURL(element.files[0]);
			}
		);
		
		await readPromise;
	}
	
	resetImage(skipRedraw = false) {
		this.image = null;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	moveImageUp(skipRedraw = false) {
		this.imageY -= this.canvas.height / 100;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	moveImageDown(skipRedraw = false) {
		this.imageY += this.canvas.height / 100;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	moveImageLeft(skipRedraw = false) {
		this.imageX -= this.canvas.width / 100;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	moveImageRight(skipRedraw = false) {
		this.imageX += this.canvas.width / 100;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	resetImagePosition(skipRedraw = false) {
		this.imageX = 0;
		this.imageY = 0;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	zoomImageIn(skipRedraw = false) {
		this.imageZoom += 0.05;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	zoomImageOut(skipRedraw = false) {
		this.imageZoom -= 0.05;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	setImageZoom(zoom, skipRedraw = false) {
		this.imageZoom = zoom;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	resetImageZoom(skipRedraw = false) {
		this.imageZoom = 1;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	setText(text, skipRedraw = false) {
		this.text = text;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	setNumber(number, skipRedraw = false) {
		this.number = number;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	setGradient(hasGradient, skipRedraw = false) {
		this.hasGradient = hasGradient;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	async loadData(
		localizationSource = "",
		iconImage = null,
		image = null,
		colorScheme = "black-on-white",
		text = "",
		hasGradient = false
	) {
		this.localizationSource = localizationSource;
		this.iconImage = iconImage;
		this.image = image;
		this.setColorScheme(colorScheme, true);
		this.setText(text, true);
		this.setGradient(hasGradient, true);
		
		this.redrawCanvas();
	}
}


var template = null;
