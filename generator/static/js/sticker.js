$(window).ready(
	function() {
		let stickerCanMouseX = 0;
		let stickerCanMouseY = 0;
		
		let stickerX = null;
		let stickerY = null;
		
		let adjustedStickerWidth = 0;
		let stickerWidth = 0;
		
		const widthScale = 4;
		const stickerSizeScale = 0.16;
		
		let redrawing = false;
		
		const positionCanvas = document.getElementById("sticker-position");
		let positionContext = positionCanvas.getContext("2d");
		
		function redrawCanvas() {
			if (redrawing) {
				return;
			}
			
			redrawing = true;
			
			positionContext.clearRect(
				0, 0,
				positionCanvas.width, positionCanvas.height
			);
			
			positionContext.drawImage(
				template.canvas,
				0, 0,
				template.canvas.width / widthScale, template.canvas.height / widthScale
			);
			
			positionContext.fillStyle = "#ff0000";
			
			positionContext.beginPath();
			positionContext.arc(
				stickerCanMouseX + (adjustedStickerWidth / 2),
				stickerCanMouseY + (adjustedStickerWidth / 2),
				adjustedStickerWidth / 2, // Radius
				0,
				2 * Math.PI,
				false
			);
			positionContext.closePath();
			
			positionContext.fill();
			
			redrawing = false;
		}
		
		let stickerCanvasBoundingClientRect = positionCanvas.getBoundingClientRect();
		let stickerOffsetX = stickerCanvasBoundingClientRect.left;
		let stickerOffsetY = stickerCanvasBoundingClientRect.top;
		let stickerCanvasWidth = positionCanvas.width;
		let stickerCanvasHeight = positionCanvas.height;
		let stickerIsDragging = false;
		
		function reloadOffset() {
			stickerCanvasBoundingClientRect = positionCanvas.getBoundingClientRect();
			
			stickerOffsetX = stickerCanvasBoundingClientRect.left;
			stickerOffsetY = stickerCanvasBoundingClientRect.top;
		}
		
		$(window).on("resize", reloadOffset);
		
		// Moving - buttons 
		
		$("#add-sticker-move-left").on(
			"click",
			function() {
				stickerCanMouseX -= (positionCanvas.width / 50);
				
				redrawCanvas();
			}
		);
		$("#add-sticker-move-right").on(
			"click",
			function() {
				stickerCanMouseX += (positionCanvas.width / 50);
				
				redrawCanvas();
			}
		);
		$("#add-sticker-move-top").on(
			"click",
			function() {
				stickerCanMouseY -= (positionCanvas.width / 50);
				
				redrawCanvas();
			}
		);
		$("#add-sticker-move-down").on(
			"click",
			function() {
				stickerCanMouseY += (positionCanvas.width / 50);
				
				redrawCanvas();
			}
		);
		
		// Moving - mouse
		
		$(positionCanvas).on(
			"mousedown",
			function(event){
				stickerCanMouseX = parseInt(event.clientX - stickerOffsetX)
				stickerCanMouseY = parseInt(event.clientY - stickerOffsetY);
				// set the drag flag
				stickerIsDragging = true;
			}
		);
		
		$(positionCanvas).on(
			"mousemove",
			function(event){
				reloadOffset();
				
				if (stickerIsDragging) {
					stickerCanMouseX = parseInt(event.clientX - stickerOffsetX);
					stickerCanMouseY = parseInt(event.clientY - stickerOffsetY);
					// if the drag flag is set, clear the canvas and draw the image
					
					stickerX = (
						(
							positionCanvas.width
							* (stickerCanMouseX / stickerCanvasBoundingClientRect.width)
						)
						- positionCanvas.width / 2
					);
					stickerY = (
						(
							positionCanvas.height
							* (stickerCanMouseY / stickerCanvasBoundingClientRect.height * template.aspectRatio)
						)
						- positionCanvas.height / 2
					);
					
					redrawCanvas();
				}
			}
		);
		
		$(positionCanvas).on(
			"mouseup",
			function(event){
				if (stickerIsDragging) {
					stickerCanMouseX = parseInt(event.clientX - stickerOffsetX);
					stickerCanMouseY = parseInt(event.clientY - stickerOffsetY);
					// clear the drag flag
					stickerIsDragging = false;
				}
			}
		);
		
		$(positionCanvas).on(
			"mouseout",
			function(event){
				if (stickerIsDragging) {
					stickerCanMouseX = parseInt(event.clientX - stickerOffsetX);
					stickerCanMouseY = parseInt(event.clientY - stickerOffsetY);
					// clear the drag flag
					stickerIsDragging = false;
				}
			}
		);
		
		// Control buttons
		$("#add-sticker").on(
			"click",
			function() {
				const positionCanvas = document.getElementById("sticker-position");
				
				positionCanvas.width = template.canvas.width / widthScale;
				positionCanvas.height = template.canvas.height / widthScale;
				adjustedStickerWidth = Math.max(
					positionCanvas.width * stickerSizeScale,
					positionCanvas.height * stickerSizeScale
				);
				stickerWidth = Math.max(
					template.canvas.width * stickerSizeScale,
					template.canvas.height * stickerSizeScale
				);
				
				positionContext = positionCanvas.getContext("2d");
				
				positionContext.drawImage(
					template.canvas,
					0, 0,
					template.canvas.width / widthScale, template.canvas.height / widthScale
				);
				
				stickerCanMouseX = positionCanvas.width / 2 - adjustedStickerWidth / 2;
				stickerCanMouseY = positionCanvas.height / 2 - adjustedStickerWidth / 2;
				
				redrawCanvas();
			}
		);
		
		$("#remove-sticker").on(
			"click",
			function() {
				template.stickerDrawHook = function() {}
				
				template.redrawCanvas();
				
				$(this).css("display", "none");
				$("#add-sticker").css("display", "inline-block");
			}
		);
		
		// Modal stuff
		$("#create-sticker").on(
			"click",
			function() {
				template.stickerDrawHook = async function() {
					// Create sticker
					const helperCanvas = document.createElement("canvas");
					
					helperCanvas.width = stickerWidth;
					helperCanvas.height = stickerWidth;
					
					helperContext = helperCanvas.getContext("2d");
					
					const stickerTemplate = new StickerNumber(helperCanvas, helperContext);
					
					stickerTemplate.setPrimaryText(
						$("#sticker-primary-text").val(),
						true
					);
					
					stickerTemplate.setSecondaryText(
						$("#sticker-secondary-text").val(),
						true
					);
					
					stickerTemplate.setShowNumberLabel(
						$("#sticker-show-number-label").is(":checked"),
						true
					); 
					
					if (document.getElementById("sticker-show-localization").checked) {
						stickerTemplate.setLocationSource(
							(
								(
									$("#location-image-selection").select2("data").length !== 0 &&
									$("#location-image-selection").select2("data")[0].element.dataset.imageLocationSource !== undefined
								) ?
								$("#location-image-selection").select2("data")[0].element.dataset.imageLocationSource :
								null
							),
							true
						);
					}
					
					const primaryColorSchemeData = $("#primary-color-scheme-selection").select2("data");
					
					if (primaryColorSchemeData.length !== 0) {
						stickerTemplate.setPrimaryColorScheme(primaryColorSchemeData[0].element.dataset.colorScheme, true);
					}
					
					if (coalitionName !== null) {
						stickerTemplate.redrawing = true;
						
						setCoalition(coalitionName, stickerTemplate);
						
						stickerTemplate.redrawing = false;
					}
					
					await stickerTemplate.redrawCanvas();
					
					// Draw sticker on
					template.context.shadowColor = "#000000";
					template.context.shadowBlur = Math.max(
						this.canvas.width / 32,
						this.canvas.height / 32
					);
					template.context.shadowOffsetX = 0;
					template.context.shadowOffsetY = 0;
					
					template.context.drawImage(
						helperCanvas,
						stickerCanMouseX * widthScale,
						stickerCanMouseY * widthScale,
						stickerWidth, stickerWidth
					);
					
					this.context.shadowColor = "rgba(0, 0, 0, 0)";
				}
				
				template.redrawCanvas();
				
				$("#add-sticker").css("display", "none");
				$("#remove-sticker").css("display", "inline-block");
			}
		);
	}
);
