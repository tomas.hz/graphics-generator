var currentConcept = null;

function generateFetchAuthorizationHeader() {
	if (document.cookie === "") {
		// temporary fallback, since we're *still* not using the pirate ID
		
		return "";
	}
	
	return (
		"Bearer "
		+ (
			document.cookie
			.split("; ")
			.find(row => row.startsWith("token="))
			.split("=")[1]
		)
	);
}

// https://stackoverflow.com/a/36281449
// Thanks to Dmitri Pavlutin!
async function getBase64(file) {
	const reader = new FileReader();
	reader.readAsDataURL(file);
	let result = null;
	
	const readPromise = new Promise(
		resolve => {
			reader.onload = function () {
				result = reader.result;
				
				resolve();
			};
			reader.onerror = function (error) {
				throw new Error(`Error converting to base64: ${error}`);
				
				resolve();
			};
		}
	);
	
	await readPromise;
	return result;
}

$("#concept-name").on(
	"keypress",
	function(event) {
		if (event.key === "Enter") {
			event.preventDefault();
			$("#concept-save").click();
		}
	}
);

$("#concept-selection").select2({
	width: "100%"
});

$("#concept-selection").on(
	"select2:select",
	async function(event) {
		const element = event.params.data.element;
		
		if (element.value === "Žádný koncept") {
			$("#concept-settings-wrapper").css("display", "none");
			currentConcept = null;
			$("#concept-name").val("");
			
			window.location.reload(true);
			
			return;
		}
		
		currentConcept = {
			name: element.dataset.name,
			id: element.value
		};
		
		$("#concept-name").val(currentConcept.name);
		
		let decodedJSON = JSON.parse(unescapeHTML(element.dataset.json));
		
		const canvasData = decodedJSON.canvasData;
		let binaryData = {};
		
		if (decodedJSON.binaryData !== undefined) {
			binaryData = decodedJSON.binaryData;
		}
		
		templateName = canvasData.templateName;
		template = new templateTypes[templateName]("graphicsCanvas");
		
		template.redrawing = true;
		
		$("#template-selection").val(canvasData.templateHumanName);
		$("#template-selection").trigger("change");
		
		if (template.changeableAttributes.includes("primaryText")) {
			$("#primary-text").val(canvasData.primaryText);
			$("#primary-text").trigger("change");
		}
		
		if (template.changeableAttributes.includes("secondaryText")) {
			$("#secondary-text").val(canvasData.secondaryText);
			$("#secondary-text").trigger("change");
		}
		
		if (template.changeableAttributes.includes("terciaryText")) {
			$("#terciary-text").val(canvasData.terciaryText);
			$("#terciary-text").trigger("change");
		}
		
		if (template.changeableAttributes.includes("urlText")) {
			$("#url-text").val(canvasData.urlText);
			$("#url-text").trigger("change");
		}
		
		if (template.changeableAttributes.includes("underNameText")) {
			$("#under-name-text").val(canvasData.underNameText);
			$("#under-name-text").trigger("change");
		}
		
		if (template.changeableAttributes.includes("dateText")) {
			$("#date-text").val(canvasData.dateText);
			$("#date-text").trigger("change");
		}
		
		if (template.changeableAttributes.includes("timeText")) {
			$("#time-text").val(canvasData.timeText);
			$("#time-text").trigger("change");
		}
		
		if (template.changeableAttributes.includes("locationText")) {
			$("#location-text").val(canvasData.locationText);
			$("#location-text").trigger("change");
		}
		
		if (template.changeableAttributes.includes("nameText")) {
			$("#name-text").val(canvasData.nameText);
			$("#name-text").trigger("change");
		}
		
		if (template.changeableAttributes.includes("iconText")) {
			$("#icon-text").val(canvasData.iconText);
			$("#icon-text").trigger("change");
		}
		
		$("#requester-text").val(canvasData.requesterText);
		$("#requester-text").trigger("change");
	
		if (canvasData.showRequesterText) {
			$("#requester-text").css("display", "block");
			$("#show-requester").prop("checked", true);
		} else {
			$("#requester-text").css("display", "none");
			$("#show-requester").prop("checked", false);
		}
		
		$("#show-requester").trigger("change");
		
		if (template.changeableAttributes.includes("qrCode")) {
			$("#qr-code-source").val(canvasData.qrCodeSource);
			$("#qr-code-source").trigger("change");
		}
		
		// TODO: Color schemes
		
		if (template.changeableAttributes.includes("logoIsCenter")) {
			if (canvasData.logoIsCenter) {
				$("#logo-is-center").prop("checked", true);
			} else {
				$("#logo-is-center").prop("checked", false);
			}
			$("#logo-is-center").trigger("change");
		}
		
		if (template.changeableAttributes.includes("locationImageToTop")) {
			if (canvasData.locationImageToTop) {
				$("#location-to-top").prop("checked", true);
			} else {
				$("#location-to-top").prop("checked", false);
			}
			$("#location-to-top").trigger("change");
		}
		
		if (template.changeableAttributes.includes("gradient")) {
			if (canvasData.enableGradient) {
				$("#show-gradient").prop("checked", true);
			} else {
				$("#show-gradient").prop("checked", false);
			}
			$("#show-gradient").trigger("change");
		}
		
		if (template.changeableAttributes.includes("backgroundHasPattern")) {
			if (canvasData.backgroundHasPattern) {
				$("#background-has-pattern").prop("checked", true);
			} else {
				$("#background-has-pattern").prop("checked", false);
			}
			$("#background-has-pattern").trigger("change");
		}
		
		if (template.changeableAttributes.includes("fourIconSet")) {
			for (let num of Array(4).keys()) {
				if (canvasData.fourIcons[num].name !== undefined) {
					const sourceElement = $(`#4-icon-group-image-${num + 1}-selection`);
					
					sourceElement.val(canvasData.fourIcons[num].name);
					sourceElement.trigger("change");
				}
				
				if (canvasData.fourIcons[num].text !== undefined) {
					const textElement = $(`#4-icon-group-text-${num + 1}`);
					
					textElement.val(canvasData.fourIcons[num].text);
					textElement.trigger("change");
				}
			}
		}
		
		if (template.changeableAttributes.includes("fivePointSet")) {
			for (const num of Array(5).keys()) {
				if (canvasData.fivePointSet[num] !== undefined) {
					$(`#5-point-set-text-${num}`).val(canvasData.fivePointSet[num]);
				}
			}
		}
		
		if (template.changeableAttributes.includes("fiveIconSet")) {
			for (let num of Array(5).keys()) {
				if (canvasData.fiveIcons[num].name !== undefined) {
					const sourceElement = $(`#5-icon-group-image-${num + 1}-selection`);
					
					sourceElement.val(canvasData.fiveIcons[num].name);
					sourceElement.trigger("change");
				}
				
				if (canvasData.fiveIcons[num].text) {
					const textElement = $(`#5-icon-group-text-${num + 1}`);
					
					textElement.val(canvasData.fiveIcons[num].text);
					textElement.trigger("change");
				}
			}
		}
		
		if (template.changeableAttributes.includes("twoReactionSet")) {
			for (let num of Array(2).keys()) {
				const sourceElement = $(`#2-reaction-set-image-${num + 1}-selection`);
				
				sourceElement.val(canvasData.twoReactions[num].name);
				sourceElement.trigger("change");
				
				const textElement = $(`#2-reaction-set-text-${num + 1}`);
				
				textElement.val(canvasData.twoReactions[num].text);
				textElement.trigger("change");
			}
		}
		
		if (template.changeableAttributes.includes("iconImage")) {
			if (binaryData.iconImage !== undefined) {
				template.iconImage = new Image();
				
				const iconImageLoadPromise = new Promise(
					resolve => {
						template.iconImage.onload = function() {
							resolve();
						}
						
						template.iconImage.src = binaryData.iconImage;
					}
				);
				
				await iconImageLoadPromise;
			} else if (canvasData.iconName !== undefined) {
				$("#icon-image-selection").val(canvasData.iconName);
				$("#icon-image-selection").trigger("change");
			}
		}
		
		if (
			template.changeableAttributes.includes("selectableRollupBackground") &&
			canvasData.rollupBackground !== undefined
		) {
			template.setBackgroundSource(canvasData.rollupBackground);
		}
		
		if (
			template.changeableAttributes.includes("showNumberLabel") &&
			canvasData.showNumberLabel !== undefined
		) {
			template.setShowNumberLabel(canvasData.showNumberLabel);
		}
		
		if (binaryData.logoAdditionalImage !== undefined) {
			template.logoImage = new Image();
			
			const logoImageLoadPromise = new Promise(
				resolve => {
					template.logoImage.onload = function() {
						resolve();
					}
					
					template.logoImage.src = binaryData.logoAdditionalImage;
				}
			);
			
			await logoImageLoadPromise;
		}
		
		
		if (
			template.changeableAttributes.includes("primaryImage") &&
			binaryData.primaryImage !== undefined
		) {
			template.primaryImage = new Image();
			const primaryImageLoadPromise = new Promise(
				resolve => {
					template.primaryImage.onload = function() {
						resolve();
					}
					
					template.primaryImage.src = binaryData.primaryImage;
				}
			);
			
			await primaryImageLoadPromise;
			
			if (canvasData.primaryImageX !== undefined) { // All others must exist
				template.primaryImageX = canvasData.primaryImageX;
				template.primaryImageY = canvasData.primaryImageY;
				template.primaryImageZoom = canvasData.primaryImageZoom;
			}
		}
		
// 		if (
// 			template.changeableAttributes.includes("additionalPrimaryImages3") &&
// 			binaryData.additionalPrimaryImages.length !== 0
// 		) {
// 			for (const num of Array(3).keys()) {
// 				template.additionalPrimaryImages[num] = new Image();
// 				
// 				const additionalImageLoadPromise = new Promise(
// 					resolve => {
// 						template.additionalPrimaryImages[num].onload = function() {
// 							resolve();
// 						}
// 						
// 						template.additionalPrimaryImages[num].src = binaryData.additionalPrimaryImages[num];
// 					}
// 				);
// 				
// 				await additionalImageLoadPromise;
// 			}
// 		}
		
		if (
			template.changeableAttributes.includes("secondaryImage") &&
			binaryData.secondaryImage !== undefined
		) {
			template.secondaryImage = new Image();
			
			const secondaryImageLoadPromise = new Promise(
				resolve => {
					template.secondaryImage.onload = function() {
						resolve();
					}
					
					template.secondaryImage.src = binaryData.secondaryImage;
				}
			);
			
			await secondaryImageLoadPromise;
		}
		
		if (
			template.changeableAttributes.includes("locationImage") &&
			canvasData.locationName !== undefined
		) {
			$("#location-image-selection").val(canvasData.locationName);
			$("#location-image-selection").trigger("change");
		}
		
		if (canvasData.coalition !== undefined) {
			$("#coalition-selection").val(canvasData.coalition);
			$("#coalition-selection").trigger("change");
		}
		
		template.redrawing = false;
		
		await initTemplate(templateName, true);
		
		template.redrawing = true;
		
		for (const colorKey of Object.keys(canvasData.colors)) {
			$(`#${colorPickers[colorKey].replace("-wrapper", "")}`).val(canvasData.colors[colorKey].toUpperCase());
			$(`#${colorPickers[colorKey].replace("-wrapper", "")}`).trigger("change");
			template[colorKey] = canvasData.colors[colorKey];
		}
		
		template.redrawing = false;
		
		template.redrawCanvas();
		
		$("#concept-settings-wrapper").css("display", "inline");
	}
);

$("#concept-delete").on(
	"click",
	async function () {
		$("#concept-save,#concept-delete").prop("disabled", true);
		
		await (
			fetch(
				`/concepts/${currentConcept.id}`,
				{
					"method": "DELETE",
					"headers": {
						"Authorization": generateFetchAuthorizationHeader(),
						"Content-Type": "application/json"
					}
				}
			).
			then(response => {
				if (!response.ok) {
					$("#concept-save,#concept-delete").prop("disabled", false);
					throw new Error(
						`Failed to delete concept. The response code was ${response.code}.`
					);
				}
			})
		);
		
		await loadConcepts();
		
		$("#concept-settings-wrapper").css("display", "none");
		currentConcept = null;
		
		$("#concept-name").val("");
		
		$("#concept-selection").val("Žádný koncept");
		$("#concept-selection").trigger("change");
		$("#concept-save,#concept-delete").prop("disabled", false);
	}
);

async function getConcept() {
	let canvasData = {
		name: (
			($("#concept-name").val() !== "") ?
			$("#concept-name").val() : null
		),
		templateName: templateName,
		templateHumanName: $("#template-selection").select2("data")[0].text,
		primaryText: $("#primary-text").val(),
		secondaryText: $("#secondary-text").val(),
		terciaryText: $("#terciary-text").val(),
		urlText: $("#url-text").val(),
		underNameText: $("#under-name-text").val(),
		dateText: $("#date-text").val(),
		timeText: $("#time-text").val(),
		locationText: $("#location-text").val(),
		locationImageToTop: $("#location-to-top").is(":checked"),
		nameText: $("#name-text").val(),
		iconText: $("#icon-text").val(),
		requesterText: $("#requester-text").val(),
		showRequesterText: ($("#requester-text").css("display") === "block"),
		qrCodeSource: $("#qr-code-source").val(),
		colors: {},
		logoIsCenter: $("#logo-is-center").is(":checked"),
		enableGradient: ($("#show-gradient").is(":checked")),
		backgroundHasPattern: ($("#background-has-pattern").is(":checked")),
		showNumberLabel: ($("#show-number-label").is(":checked")),
		fourIcons: [],
		fiveIcons: [],
		fivePointSet: [],
		twoReactions: []
	};
	
	let binaryData = {};
	
	if (document.getElementById("logo-additional-image").files.length !== 0) {
		binaryData.logoAdditionalImage = await getBase64(document.getElementById("logo-additional-image").files[0]);
	}
	
	if (document.getElementById("primary-image").files.length !== 0) {
		if (template.primaryImageX !== undefined) { // Assume everything else exists too
			canvasData.primaryImageX = template.primaryImageX;
			canvasData.primaryImageY = template.primaryImageY;
			canvasData.primaryImageZoom = template.primaryImageZoom;
		}
		
		binaryData.primaryImage = await getBase64(document.getElementById("primary-image").files[0]);
	}
	
// 	for (const num of Array(3).keys()) {
// 		const sourceElement = document.getElementById(`additional-primary-image-${num}`);
// 		
// 		if (sourceElement.files.length === 0) {
// 			continue;
// 		}
// 		
// 		binaryData.additionalPrimaryImages.push(
// 			await getBase64(sourceElement.files[0])
// 		);
// 	}
	
	if (document.getElementById("secondary-image").files.length !== 0) {
		binaryData.secondaryImage = await getBase64(document.getElementById("secondary-image").files[0]);
	}
	
	if (
		$("#location-image-selection").select2("data").length !== 0 &&
		$("#location-image-selection").select2("data")[0].element.dataset.imageLocationSource !== undefined
	) {
		canvasData.locationName = $("#location-image-selection").select2("data")[0].element.value;
	}
	
	if (
		$("#selectable-rollup-background").select2("data").length !== 0
	) {
		canvasData.rollupBackground = $("#selectable-rollup-background").select2("data")[0].element.dataset.imageSource;
	}
	
	if (document.getElementById("icon-image-input").files.length !== 0) {
		binaryData.iconImage = await getBase64(document.getElementById("icon-image-input").files[0]);
	} else if (
		$("#icon-image-selection").select2("data").length !== 0 &&
		$("#icon-image-selection").select2("data")[0].element.dataset.imageIconSource !== undefined
	) {
		canvasData.iconName = $("#icon-image-selection").select2("data")[0].element.value;
	}
	
	for (let num of Array(4).keys()) {
		const sourceElement = $(`#4-icon-group-image-${num + 1}-selection`);
		
		let fourIconValue = {};
		
		if (
			sourceElement.select2("data").length !== 0 &&
			sourceElement.select2("data")[0].element.dataset.imageIconSource !== undefined
		) {
			fourIconValue.name = sourceElement.val();
		}
		
		const textElementValue = $(`#4-icon-group-text-${num + 1}`).val();
		
		if (textElementValue !== "") {
			fourIconValue.text = textElementValue;
		}
		
		canvasData.fourIcons.push(fourIconValue);
	}
	
	for (const num of Array(5).keys()) {
		const value = $(`#5-point-set-text-${num}`).val();
		
		if (
			value !== undefined &&
			value !== null &&
			value !== ""
		) {
			canvasData.fivePointSet.push(value);
		}
	}
	
	for (let num of Array(5).keys()) {
		const sourceElement = $(`#5-icon-group-image-${num + 1}-selection`);
		
		let fiveIconValue = {};
		
		if (
			sourceElement.select2("data").length !== 0 &&
			sourceElement.select2("data")[0].element.dataset.imageIconSource !== undefined
		) {
			fiveIconValue.name = sourceElement.val();
		}
		
		const textElementValue = $(`#5-icon-group-text-${num + 1}`).val();
		
		if (textElementValue !== "") {
			fiveIconValue.text = textElementValue;
		}
		
		canvasData.fiveIcons.push(fiveIconValue);
	}
	
	for (let num of Array(2).keys()) {
		const sourceElement = $(`#2-reaction-set-image-${num + 1}-selection`);
		
		let twoReactionValue = {};
		
		if (
			sourceElement.select2("data").length !== 0 &&
			sourceElement.select2("data")[0].element.dataset.imageReactionSource !== undefined
		) {
			twoReactionValue.name = sourceElement.val();
		}
		
		const textElementValue = $(`#2-reaction-set-text-${num + 1}`).val();
		
		if (textElementValue !== "") {
			twoReactionValue.text = textElementValue;
		}
		
		canvasData.twoReactions.push(twoReactionValue);
	}
	
	for (const colorAttr of template.changeableColors) {
		canvasData.colors[colorAttr] = template[colorAttr];
	}
	
	if (
		$("#coalition-selection").select2("data").length !== 0 &&
		$("#coalition-selection").select2("data")[0].id !== undefined &&
		$("#coalition-selection").select2("data")[0].id !== "no-coalition"
	) {
		canvasData.coalition = $("#coalition-selection").select2("data")[0].id;
	}
	
	return {
		"canvasData": canvasData,
		"binaryData": binaryData
	};
}

$("#concept-save").on(
	"click",
	async function () {
		$("#concept-save,#concept-delete").prop("disabled", true);
		
		const concept = await getConcept();
		
		if (concept.canvasData.name === null) {
			alert("Koncept musí mít jméno.");
			return;
		}
		
		if (currentConcept === null) {
			const id = await (
				fetch(
					`/concepts`,
					{
						"method": "POST",
						"headers": {
							"Authorization": generateFetchAuthorizationHeader(),
							"Content-Type": "application/json"
						},
						"body": JSON.stringify(concept)
					}
				).
				then(response => {
					if (!response.ok) {
						$("#concept-save,#concept-delete").prop("disabled", false);
						
						throw new Error(
							`Failed to create concept. The response code was ${response.code}.`
						);
					}
					
					return response.json();
				})
			);
			
			await loadConcepts();
			
			$("#concept-selection").val(id);
			$("#concept-selection").trigger("change");
			
			currentConcept = {
				id: id,
				name: concept.canvasData.name
			}
		} else {
			await (
				fetch(
					`/concepts/${currentConcept.id}`,
					{
						"method": "PUT",
						"headers": {
							"Authorization": generateFetchAuthorizationHeader(),
							"Content-Type": "application/json"
						},
						"body": JSON.stringify(concept)
					}
				).
				then(response => {
					if (!response.ok) {
						$("#concept-save,#concept-delete").prop("disabled", false);
						
						throw new Error(
							`Failed to create concept. The response code was ${response.code}.`
						);
					}
				})
			);
			
			await loadConcepts();
			
			$("#concept-selection").val(currentConcept.id);
			$("#concept-selection").trigger("change");
		}
		
		$("#concept-save,#concept-delete").prop("disabled", false);
	}
);

async function loadConcepts() {
	const concepts = await (
		fetch(
			`/concepts`,
			{
				"method": "GET",
				"headers": {
					"Authorization": generateFetchAuthorizationHeader()
				}
			}
		).
		then(response => {
			if (!response.ok) {
				throw new Error(
					`Failed to get concepts. The response code was ${response.code}.`
				);
			}
			
			return response.json();
		})
	);
	
	$("#concept-selection").html("<option>Žádný koncept</option>");
	
	for (let concept of concepts) {
		const option = new Option(concept.data.canvasData.name, concept.id, false, false);
		option.dataset.name = concept.data.canvasData.name;
		option.dataset.json = JSON.stringify(concept.data);
		
		document.getElementById("concept-selection").appendChild(option);
	}
}

// Load existing concepts
$(window).ready(loadConcepts);
