function splitStringIntoLines(context, text, maxLength, maxLines, skipOverflowCheck = false) {
	let words = text.split(/( |\n|\r\n)/g);

	let currentLine = [];
	let lines = [[]];
	let linePos = 0;
	let breakCount = 0;
	let currentlyHighlighted = false;
	
	for (word of words) {
		if (word == " " || word == "") {
			continue;
		}
		
		// Ignore !, ? and such
		const sanitizedWord = word.replace(/(!|\.|\?)+/, "");
		
		if (word == "\n" || word == "\n\r") {
			if (breakCount !== maxLines - 1) {
				breakCount++;
				linePos++;
				currentlyHighlighted = false;
				
				currentLine = [];

				lines.push([]);
			}

			continue;
		}
		
		let wordObject = {
			word: word,
			isHighlighted: (
				(
					sanitizedWord.startsWith("*")
					|| currentlyHighlighted
				) &&
				sanitizedWord.length != 0
			),
			toString: function() {
				return this.word;
			}
		}

		if (wordObject.isHighlighted) {
			if (sanitizedWord.endsWith("*")) {
				currentlyHighlighted = false;
			} else {
				currentlyHighlighted = true;
			}
			
			wordObject.word = wordObject.word.replace(/(?=(!|\?|\.|^))\*/, "");
			wordObject.word = wordObject.word.replace(/\*(?=(!|\?|\.|$))/, "");
		}
		
		currentLine.push(wordObject);
		
		if (
			context.measureText(currentLine.join(" ")).width
			> maxLength
		) {
			let wordTooLongOnItsOwn = false;
			
			if (!skipOverflowCheck) {
				while (
					context.measureText(wordObject.toString()).width
					> maxLength
				) {
					wordTooLongOnItsOwn = true;
				
					wordObject.word = wordObject.word.substring(0, wordObject.word.length - 4) + "...";
				}
			}
			
			if (!wordTooLongOnItsOwn) {
				linePos++;
				
				currentLine = [wordObject];
			}
		}
		 
		if (lines.length - 1 < linePos) {
			lines.push([]);
		}
		 
		lines[linePos].push(wordObject);
	}
	
	return lines;
}

function checkMissingImage(template) {
	if (!template.changeableAttributes.includes("primaryImage")) {
		return;
	}
	
	const pixels = template.context.getImageData(
		0, 0,
		template.canvas.width, template.canvas.height
	);
	
	for (let pixelPosition = 0; pixelPosition < pixels.data.length; pixelPosition += 4) {
		if (pixels.data[pixelPosition + 3] === 0) {
			alert("Pozor, primární obrázek nezakrývá celou plochu, kterou může. Pokud to není absolutně potřebné, pokus se to prosím opravit.");
			return;
		}
	}
}

// https://stackoverflow.com/a/5624139
// Thanks to Tim Down!
function hexToRgb(hex) {
	// Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
	var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
	
	hex = hex.replace(shorthandRegex, function(m, r, g, b) {
		return r + r + g + g + b + b;
	});
	
	var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	return result ? {
		r: parseInt(result[1], 16),
		g: parseInt(result[2], 16),
		b: parseInt(result[3], 16)
	} : null;
}

// https://stackoverflow.com/a/61375162
// Thanks to Abbos Tajimov!
function snakeToCamel(name) {
	return (
		name.toLowerCase().replace(
			/([-_][a-z])/g,
			group =>
			group
			.toUpperCase()
			.replace('-', '')
			.replace('_', '')
		)
	);
}

// https://learnbatta.com/blog/how-to-add-image-in-select-options-html-93/
// https://stackoverflow.com/a/32866520
// Thanks to Jay Rizzi and learnBATTA!
function formatSelect2ImageData(data){
	if (data.disabled || data.loading || data.element.dataset.imageSource === undefined) {
		return data.text;
	}
	
	return $(`<span class="select2-image-wrapper"><img src="${data.element.dataset.imageSource}" class="select2-image" alt="${data.text}"><span class="select2-text">${data.text}</span></span>`);
}

function formatSelect2ColorData(data){
	if (data.disabled || data.loading || data.element.value === undefined || data.element.value === "none") {
		return data.text;
	}
	
	return $(`<span class="select2-image-wrapper"><div style="background:${data.element.value}" class="select2-color" alt="${data.text}"></div><span class="select2-text">${data.text}</span></span>`);
}

function colorizeImage(image, width, height, r, g, b) {
	let temporaryCanvas = document.createElement("canvas");
	
	if (width < 1 || height < 1) {
		// Just return a canvas, don't do anything with it
		return temporaryCanvas;
	}
	
	temporaryCanvas.width = width;
	temporaryCanvas.height = height;
	
	let temporaryContext = temporaryCanvas.getContext("2d");
	
	temporaryContext.drawImage(
		image,
		0, 0,
		width, height
	);
	
	let pixels = temporaryContext.getImageData(
		0, 0,
		width, height
	);
	
	for (let pixelPosition = 0; pixelPosition < pixels.data.length; pixelPosition += 4) {
		pixels.data[pixelPosition] = pixels.data[pixelPosition] / 255 * r;
		pixels.data[pixelPosition + 1] = pixels.data[pixelPosition + 1] / 255 * g;
		pixels.data[pixelPosition + 2] = pixels.data[pixelPosition + 2] / 255 * b;
	}
	
	temporaryContext.putImageData(
		pixels,
		0, 0
	);
	
	return temporaryCanvas;
}

// https://stackoverflow.com/a/22706073
// Thanks to Vitim.us!
function escapeHTML(str){
	var p = document.createElement("p");
	p.appendChild(document.createTextNode(str));
	return p.innerHTML;
}

// https://stackoverflow.com/a/34064434
// Thanks to Wladimir Palant and vsync!
function unescapeHTML(input) {
	var doc = new DOMParser().parseFromString(input, "text/html");
	return doc.documentElement.textContent;
}

// https://stackoverflow.com/a/56348573
// Thanks to ashleedawg and V. Rubinetti!
function blendColors(colorA, colorB, amount) {
	const [rA, gA, bA] = colorA.match(/\w\w/g).map((c) => parseInt(c, 16));
	const [rB, gB, bB] = colorB.match(/\w\w/g).map((c) => parseInt(c, 16));
	const r = Math.round(rA + (rB - rA) * amount).toString(16).padStart(2, '0');
	const g = Math.round(gA + (gB - gA) * amount).toString(16).padStart(2, '0');
	const b = Math.round(bA + (bB - bA) * amount).toString(16).padStart(2, '0');
	return '#' + r + g + b;
}

// https://stackoverflow.com/a/27328841
// Thanks to Moustafa Elqabbany (and, of course, John-Paul Gignac!)
function colorTextGignac(canvas, context, r, g, b, w, h) {
	var imageData = context.getImageData(0, 0, canvas.width, canvas.height);
	var pixels = imageData.data;
	for (var x = 0; x < w; x++) {
		for (var y = 0; y < h; y++) {
			var redIndex = ((y - 1) * (canvas.width * 4)) + ((x - 1) * 4);
			var greenIndex = redIndex + 1;
			var blueIndex = redIndex + 2;
			var alphaIndex = redIndex + 3;
			pixels[redIndex] = r + (255-r)*pixels[redIndex]/255;
			pixels[greenIndex] = g + (255-g)*pixels[greenIndex]/255;
			pixels[blueIndex] = b + (255-b)*pixels[blueIndex]/255;
			//c1_r = f1_r + (b1_r-f1_r)*c0_r/255
		}
	}
	context.clearRect(0, 0, canvas.width, canvas.height);
	context.putImageData(imageData, 0, 0);
}

// https://html5graphics.blogspot.com/2015/03/html5-canvas-rounded-text.html
// Thanks to James Alford!
function getCircularText(
	text,
	diameter,
	startAngle,
	align,
	textInside,
	inwardFacing,
	fName,
	fSize,
	fontColor,
	kerning
) {
	// text:         The text to be displayed in circular fashion
	// diameter:     The diameter of the circle around which the text will
	//               be displayed (inside or outside)
	// startAngle:   In degrees, Where the text will be shown. 0 degrees
	//               if the top of the circle
	// align:        Positions text to left right or center of startAngle
	// textInside:   true to show inside the diameter. False to show outside
	// inwardFacing: true for base of text facing inward. false for outward
	// fName:        name of font family. Make sure it is loaded
	// fSize:        size of font family. Don't forget to include units
	// fontColor:    The color of the text to use.
	// kearning:     0 for normal gap between letters. positive or
	//               negative number to expand/compact gap in pixels
	//------------------------------------------------------------------------
	
	// declare and intialize canvas, reference, and useful variables
	align = align.toLowerCase();
	var mainCanvas = document.createElement('canvas');
	var ctxRef = mainCanvas.getContext('2d');
	var clockwise = align == "right" ? 1 : -1; // draw clockwise for aligned right. Else Anticlockwise
	startAngle = startAngle * (Math.PI / 180); // convert to radians
	
	// calculate height of the font. Many ways to do this
	// you can replace with your own!
	var div = document.createElement("div");
	div.innerHTML = text;
	div.style.position = 'absolute';
	div.style.top = '-10000px';
	div.style.left = '-10000px';
	div.style.fontFamily = fName;
	div.style.fontSize = fSize;
	document.body.appendChild(div);
	var textHeight = div.offsetHeight;
	document.body.removeChild(div);
	
	// in cases where we are drawing outside diameter,
	// expand diameter to handle it
	if (!textInside) diameter += textHeight * 2;
	
	mainCanvas.width = diameter;
	mainCanvas.height = diameter;
	// omit next line for transparent background
	ctxRef.fillStyle = fontColor;
	ctxRef.font = fSize + ' ' + fName;
	
	// Reverse letters for align Left inward, align right outward 
	// and align center inward.
	if (((["left", "center"].indexOf(align) > -1) && inwardFacing) || (align == "right" && !inwardFacing)) text = text.split("").reverse().join(""); 
	
	// Setup letters and positioning
	ctxRef.translate(diameter / 2, diameter / 2); // Move to center
	startAngle += (Math.PI * !inwardFacing); // Rotate 180 if outward
	ctxRef.textBaseline = 'middle'; // Ensure we draw in exact center
	ctxRef.textAlign = 'center'; // Ensure we draw in exact center
	
	// rotate 50% of total angle for center alignment
	if (align == "center") {
		for (var j = 0; j < text.length; j++) {
			var charWid = ctxRef.measureText(text[j]).width;
			startAngle += ((charWid + (j == text.length-1 ? 0 : kerning)) / (diameter / 2 - textHeight)) / 2 * -clockwise;
		}
	}
	
	// Phew... now rotate into final start position
	ctxRef.rotate(startAngle);
	
	// Now for the fun bit: draw, rotate, and repeat
	for (var j = 0; j < text.length; j++) {
		var charWid = ctxRef.measureText(text[j]).width; // half letter
		// rotate half letter
		ctxRef.rotate((charWid/2) / (diameter / 2 - textHeight) * clockwise); 
		// draw the character at "top" or "bottom" 
		// depending on inward or outward facing
		ctxRef.fillText(text[j], 0, (inwardFacing ? 1 : -1) * (0 - diameter / 2 + textHeight / 2));
		
		ctxRef.rotate((charWid/2 + kerning) / (diameter / 2 - textHeight) * clockwise); // rotate half letter
	}
	
	// Return it
	return (mainCanvas);
}
