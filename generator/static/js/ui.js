var template = null;

const templateTypes = {
	"bottom-slogan": BottomSloganTemplate,
	"bottom-slogan-gradient": BottomSloganGradient,
	"bottom-slogan-gradient-icon": BottomSloganGradientIcon,
	"bottom-slogan-gradient-vertical-logo": BottomSloganGradientVerticalLogo,
	"bottom-slogan-person": BottomSloganPerson,
	"bottom-slogan-quote": BottomSloganQuote,
	"bottom-slogan-full-width": BottomSloganFullWidth,
	"bottom-slogan-vertical-logo": BottomSloganVerticalLogo,
	"left-long-text": LeftLongTextTemplate,
	"right-long-text": RightLongTextTemplate,
	"right-big-text": RightBigTextTemplate,
	"left-corner-slogan": LeftCornerSloganTemplate,
	"story-with-rectangle": StoryWithRectangleTemplate,
	"story-with-full-width-rectangle": StoryWithFullWidthRectangleTemplate,
	"no-image-icon-text": NoImageIconText,
	"no-image-text-more-info": NoImageTextMoreInfo,
	"no-image-quote": NoImageQuote,
	"no-image-big-text-icon-with-terciary": NoImageBigTextIconWithTerciary,
	"no-image-big-text-icon": NoImageBigTextIcon,
	"no-image-text-icon-vertical-logo": NoImageTextIconVerticalLogo,
	"poster-bottom-slogan": PosterBottomSloganTemplate,
	"poster-bottom-slogan-no-icon": PosterBottomSloganTemplateNoIcon,
	"poster-center-slogan-no-image": PosterCenterSloganNoImage,
	"poster-center-slogan-icons-no-image": PosterCenterSloganIconsNoImage,
	"poster-event": PosterEvent,
	"poster-bottom-slogan-printable": PosterBottomSloganTemplatePrintable,
	"poster-bottom-slogan-no-icon-printable": PosterBottomSloganTemplateNoIconPrintable,
	"poster-center-slogan-no-image-printable": PosterCenterSloganNoImagePrintable,
	"poster-center-slogan-icons-no-image-printable": PosterCenterSloganIconsNoImagePrintable,
	"banner-name-left": BannerNameLeft,
	"banner-name-right": BannerNameRight,
	"billboard": Billboard,
	"cover": Cover,
	"cover-static-name-and-logo": CoverStaticNameAndLogo,
	"cover-static-name-and-logo-left": CoverStaticNameAndLogoLeft,
	"poll": Poll,
	"koneckorupci-poll": KonecKorupciPoll,
	"event-text-right": EventTextRight,
	"event-text-bottom": EventTextBottom,
	"eu-image-slogan": EuImageSlogan,
	"eu-big-text-image": EuBigTextImage,
	"eu-icons-image": EuIconsImage,
	"nalodeni": Nalodeni,
	"nalodeni-story": NalodeniStory,
	"komise": Komise,
	"komise-story": KomiseStory,
	"rollup-big-logo-short-text": RollupBigLogoShortText,
	"rollup-points": RollupPoints,
	"rollup-short-and-long-text": RollupShortAndLongText,
	"humans": Humans,
	"humans-no-quote": HumansNoQuote,
	"pro-usti-poster": ProUstiPoster,
	"pro-usti-slogan-icon-bubble": ProUstiSloganIconBubble,
	"pro-usti-slogan-bubble": ProUstiSloganBubble,
	"zelena-bystrc": ZelenaBystrc,
	"sticker-number": StickerNumber,
	"ivan-short-claim": IvanShortClaim,
	"ivan-points": IvanPoints,
	"czpres": CzPres
}

const attributeElements = {
	"primaryImage": "#primary-image-wrapper, #primary-image-position-wrapper-lower",
	"additionalPrimaryImages3": "#additional-3-primary-images-wrapper",
	"locationImage": "#location-image-wrapper",
	"iconImage": "#icon-image-wrapper",
	"primaryText": "#primary-text-wrapper",
	"secondaryText": "#secondary-text-wrapper",
	"terciaryText": "#terciary-text-wrapper",
	"urlText": "#url-text-wrapper",
	"underNameText": "#under-name-text-wrapper",
	"secondaryNameText": "#secondary-name-text-wrapper",
	"secondaryUnderNameText": "#secondary-under-name-text-wrapper",
	"nameText": "#name-text-wrapper",
	"iconText": "#icon-text-wrapper",
	"primaryColorScheme": "#primary-color-scheme-wrapper",
	"nameColorScheme": "#name-color-scheme-wrapper",
	"secondaryColorScheme": "#secondary-color-scheme-wrapper",
	"terciaryColorScheme": "#terciary-color-scheme-wrapper",
	"primaryImagePosition": "#primary-image-position-wrapper",
	"logoIsCenter": "#logo-is-center-wrapper",
	"backgroundHasPattern": "#background-has-pattern-wrapper",
	"qrCode": "#qr-code-wrapper",
	"fourIconSet": "#4-icon-group-wrapper",
	"fiveIconSet": "#5-icon-group-wrapper",
	"fivePointSet": "#5-point-set-wrapper",
	"twoReactionSet": "#2-reaction-set-wrapper",
	"dateText": "#date-text-wrapper",
	"timeText": "#time-text-wrapper",
	"locationText": "#location-text-wrapper",
	"secondaryImage": "#secondary-image-wrapper",
	"gradient": "#gradient-wrapper",
	"locationImageToTop": "#location-to-top-wrapper",
	"selectableRollupBackground": "#selectable-rollup-background-wrapper",
	"proUstiColorScheme": "#pro-usti-color-scheme-wrapper",
	"proUstiIcon": "#pro-usti-icon-image-wrapper",
	"logoImage": "#logo-image-wrapper",
	"showNumberLabel": "#show-number-label-wrapper"
}

const colorPickers = {
	"primaryTextColor": "primary-text-color-wrapper",
	"iconColor": "icon-color-wrapper",
	"backgroundColor": "background-color-wrapper",
	"foregroundColor": "foreground-color-wrapper",
	"nameBackgroundColor": "name-background-color-wrapper",
	"nameTextColor": "name-text-color-wrapper",
	"dateTextColor": "date-text-color-wrapper",
	"urlTextColor": "url-text-color-wrapper",
	"primaryTextHighlightColor": "primary-text-highlight-color-wrapper",
	"secondaryTextBackgroundColor": "secondary-text-background-color-wrapper",
	"secondaryTextColor": "secondary-text-color-wrapper",
	"secondaryTextHighlightColor": "secondary-text-highlight-color-wrapper",
	"terciaryTextColor": "terciary-text-color-wrapper",
	"terciaryTextBackgroundColor": "terciary-text-background-color-wrapper",
	"reactionTextColor": "reaction-text-color-wrapper",
	"underNameTextColor": "under-name-text-color-wrapper",
	"qrCodeColor": "qr-code-color-wrapper",
	"requesterTextColor": "requester-text-color-wrapper",
	"quoteColor": "quote-color-wrapper",
	"backgroundGradientColor1": "background-gradient-color1-wrapper",
	"backgroundGradientColor2": "background-gradient-color2-wrapper"
}

function reloadColorPalette() {
	const colorScheme = $("#primary-color-scheme-selection").select2("data")[0].element.value;
	
	for (let [attribute, elementId] of Object.entries(colorPickers)) {
		if (template.changeableColors.includes(attribute) && template[attribute] !== undefined) {
			$(`#${elementId}`).css("display", "flex");
			$(`#${elementId.replace("-wrapper", "")}`).val(template[attribute].toUpperCase());
			$(`#${elementId.replace("-wrapper", "")}`).trigger("change");
		} else {
			$(`#${elementId}`).css("display", "none");
		}
	}
}

async function initTemplate(templateName, skipSetting = false) {
	if (templateName.startsWith("sticker")) {
		$("#sticker-wrapper").css("display", "none");
	} else {
		$("#sticker-wrapper").css("display", "block");
	}
	
	if (templateName.startsWith("cover")) {
		$(".guide-left").css("display", "block");
		$(".guide-top").css("display", "block");
		
		if (!templateName.endsWith("left")) {
			document.getElementById("guide-top-image").src = "static/images/cover_guide_top.png";
		} else {
			document.getElementById("guide-top-image").src = "static/images/cover_guide_top-left.png";
		}
	} else {
		$(".guide-left").css("display", "none");
		$(".guide-top").css("display", "none");
	}
	
	if (!skipSetting) {
		template = new templateTypes[templateName]("graphicsCanvas");
	}

	$("#template-description").html(template.description);
	
	for (let [attribute, elementSelector] of Object.entries(attributeElements)) {
		if (template.changeableAttributes.includes(attribute)) {
			$(`${elementSelector}`).css("display", "block");
		} else {
			$(`${elementSelector}`).css("display", "none");
		}
	}
	
	if (template.isPrintable) {
		$("#printing-wrapper").css("display", "block");
	} else {
		$("#printing-wrapper").css("display", "none");
	}
	
	if (document.getElementById("logo-additional-image").files.length !== 0) {
		template.setLogoImageFromInput(document.getElementById("logo-additional-image"));
	}
	
	await template.loadData(
		(
			(template.primaryImage === null) ?
			document.getElementById("primary-image") : null
		),
		$("#primary-text").val(),
		$("#name-text").val(),
		template.primaryColorSchemes[0],
		(
 			(parseInt($("#resolution").val()) === NaN || parseInt($("#resolution").val()) < 750) ?
			parseInt($("#resolution").val()) :
			template.defaultResolution
		),
		true
	);
	
	if (template.changeableAttributes.includes("locationImage")) {
		template.setLocationSource(
			(
				(
					$("#location-image-selection").select2("data").length !== 0 &&
					$("#location-image-selection").select2("data")[0].element.dataset.imageLocationSource !== undefined
				) ?
				$("#location-image-selection").select2("data")[0].element.dataset.imageLocationSource :
				null
			),
			true
		);
	}
	
	if (template.changeableAttributes.includes("locationImageToTop")) {
		template.setLocationToTop($("#location-to-top").is(":checked"), true);
	}
	
	if (template.changeableAttributes.includes("iconImage")) {
		if ($("#icon-image-input").val() !== "") {
			await template.setIconImageFromInput(document.getElementById("icon-image-input"));
		} else {
			template.setIconSource(
				(
					(
						$("#icon-image-selection").select2("data").length !== 0 &&
						$("#icon-image-selection").select2("data")[0].element.dataset.imageIconSource !== undefined
					) ?
					$("#icon-image-selection").select2("data")[0].element.dataset.imageIconSource :
					null
				),
				true
			);
		}
	}
	
	if (template.changeableAttributes.includes("iconText")) {
		template.setIconText($("#icon-text").val(), true);
	}
	
	if (template.changeableAttributes.includes("nameColorScheme")) {
		for (let child of $("#name-color-scheme-selection").children()) {
			if (template.nameColorSchemes.includes(child.value)) {
				$(child).removeProp("disabled").removeAttr("disabled");
			} else {
				$(child).prop("disabled", true);
			}
		}
		
		$("#name-color-scheme-selection").trigger("change.select2");
		
		$("#name-color-scheme-selection").val(template.nameColorSchemes[0]);
		$("#name-color-scheme-selection").trigger("change");
	}
	
	if (template.changeableAttributes.includes("secondaryColorScheme")) {
		for (let child of $("#secondary-color-scheme-selection").children()) {
			if (template.secondaryColorSchemes.includes(child.value)) {
				$(child).removeProp("disabled").removeAttr("disabled");
			} else {
				$(child).prop("disabled", true);
			}
		}
		
		$("#secondary-color-scheme-selection").trigger("change.select2");
		
		$("#secondary-color-scheme-selection").val(template.secondaryColorSchemes[0]);
		$("#secondary-color-scheme-selection").trigger("change");
	}
	
	if (template.changeableAttributes.includes("terciaryColorScheme")) {
		for (let child of $("#terciary-color-scheme-selection").children()) {
			if (template.terciaryColorSchemes.includes(child.value)) {
				$(child).removeProp("disabled").removeAttr("disabled");
			} else {
				$(child).prop("disabled", true);
			}
		}
		
		$("#terciary-color-scheme-selection").trigger("change.select2");
		
		$("#terciary-color-scheme-selection").val(template.terciaryColorSchemes[0]);
		$("#terciary-color-scheme-selection").trigger("change");
	}

	if (template.changeableAttributes.includes("secondaryImage")) {
		await template.setSecondaryImageFromInput(document.getElementById("secondary-image"), true);
	}
	
	if (template.changeableAttributes.includes("logoIsCenter")) {
		template.setLogoIsCenter(
			(
				($("#logo-is-center").is(":checked")) ?
				true : false
			),
			true
		);
	}
	
	if (template.changeableAttributes.includes("gradient")) {
		template.setGradient($("#show-gradient").is(":checked"));
	}
	
	if (template.changeableAttributes.includes("backgroundHasPattern")) {
		template.setBackgroundHasPattern(
			(
				($("#background-has-pattern").is(":checked")) ?
				true : false
			),
			true
		);
	}
	
	if (template.changeableAttributes.includes("secondaryText")) {
		template.setSecondaryText(
			$("#secondary-text").val(),
			true
		);
	}
	
	if (template.changeableAttributes.includes("terciaryText")) {
		template.setTerciaryText(
			$("#terciary-text").val(),
			true
		);
	}
	
	if (template.changeableAttributes.includes("urlText")) {
		template.setUrlText(
			$("#url-text").val(),
			true
		);
	}
	
	if (template.changeableAttributes.includes("underNameText")) {
		template.setUnderNameText(
			$("#under-name-text").val(),
			true
		);
	}
	
	if (template.changeableAttributes.includes("secondaryNameText")) {
		template.setSecondaryNameText(
			$("#secondary-name-text").val(),
			true
		);
	}
	
	if (template.changeableAttributes.includes("secondaryUnderNameText")) {
		template.setSecondaryUnderNameText(
			$("#secondary-under-name-text").val(),
			true
		);
	}
	
	if (template.changeableAttributes.includes("dateText")) {
		template.setDateText(
			$("#date-text").val(),
			true
		);
	}
	
	if (template.changeableAttributes.includes("timeText")) {
		template.setTimeText(
			$("#time-text").val(),
			true
		);
	}
	
	if (template.changeableAttributes.includes("locationText")) {
		template.setLocationText(
			$("#location-text").val(),
			true
		);
	}
	
	if (template.changeableAttributes.includes("selectableRollupBackground")) {
		const sourceElementData = $("#selectable-rollup-background").select2("data");
		
		if (sourceElementData.length !== 0) {
			template.setBackgroundSource(sourceElementData[0].element.dataset.imageSource, true);
			template.setPrimaryColorScheme(sourceElementData[0].element.dataset.colorScheme, true);
		}
	}
	
	if (template.changeableAttributes.includes("proUstiColorScheme")) {
		const selectElementData = $("#pro-usti-color-scheme-selection").select2("data");
		
		if (selectElementData.length !== 0) {
			template.setProUstiColorScheme(selectElementData[0].element.dataset.colorScheme, true);
		}
	}
	
	if (template.changeableAttributes.includes("proUstiIcon")) {
		if ($("#pro-usti-icon-image-input").val() !== "") {
			await template.setIconImageFromInput(document.getElementById("pro-usti-icon-image-input"));
		} else {
			template.setIconSource(
				(
					(
						$("#pro-usti-icon-image-selection").select2("data").length !== 0 &&
						$("#pro-usti-icon-image-selection").select2("data")[0].element.dataset.imageIconSource !== undefined
					) ?
					$("#pro-usti-icon-image-selection").select2("data")[0].element.dataset.imageIconSource :
					null
				),
				true
			);
		}
	}
	
	if ($("#requester-text").css("display") === "block") {
		template.setRequesterText($("#requester-text").val(), true);
	}
	
	if (template.changeableAttributes.includes("qrCode")) {
		template.setQrCodeURI($("#qr-code-source").val(), true);
	}
	
	if (template.changeableAttributes.includes("additionalPrimaryImages3")) {
		for (const num of Array(3).keys()) {
			const sourceElement = $(`#additional-primary-image-${num}`);
			
			$(`#reset-additional-primary-image-${num}`).on(
				"click",
				function() {
					sourceElement.val(null);
					template.additionalPrimaryImages[num] = null;
					template.redrawCanvas();
				}
			)
			
			sourceElement.on(
				"change",
				function (event) {
					if (event.target.files.length === 0) {
						return;
					}
					
					const uploadedImage = new Image();
					
					uploadedImage.onload = function() {
						if (this.width < 1080 || this.height < 1080) {
							alert("Prosím, nahraj obrázek s minimálním rozlišením 1080 pixelů na výšku i šířku.");
							$(event.target).val(null);
							
							return;
						}
						
						template.additionalPrimaryImages[num] = this;
						template.redrawCanvas();
					}
					
					uploadedImage.src = window.URL.createObjectURL(event.target.files[0]);
				}
			);
			
			if (sourceElement[0].files.length !== 0) {
				await template.setAdditionalPrimaryImageFromInput(
					num,
					sourceElement[0],
					true
				);
			}
		}
	}
	
	if (template.changeableAttributes.includes("fourIconSet")) {
		for (const num of Array(4).keys()) {
			// Check input first
			const inputElement = $(`#4-icon-group-image-${num + 1}-input`);
			
			if (inputElement[0].files.length !== 0) {
				inputReadPromise = new Promise(
					resolve => {
						const reader = new FileReader();
						
						reader.onloadend = function() {
							template.iconSet[num].source = reader.result;
							
							resolve();
						};
						
						reader.readAsDataURL(inputElement[0].files[0]);
					}
				);
				
				await inputReadPromise;
				return;
			}
			
			// Selections
			const selectionElement = $(`#4-icon-group-image-${num + 1}-selection`);
			
			if (
				selectionElement.select2("data").length !== 0 &&
				selectionElement.select2("data")[0].element.dataset.imageIconSource !== undefined
			) {
				template.iconSet[num].source = selectionElement.select2("data")[0].element.dataset.imageIconSource;
			}
			
			const textElementValue = $(`#4-icon-group-text-${num + 1}`).val();
			
			if (textElementValue !== "") {
				template.iconSet[num].text = textElementValue;
			}
		}
	}
	
	if (template.changeableAttributes.includes("fivePointSet")) {
		for (const num of Array(5).keys()) {
			const value = $(`#5-point-set-text-${num + 1}`).val();
			
			if (value !== "" && value !== undefined && value !== null) {
				template.pointSet[num] = value;
			}
		}
	}
	
	if (template.changeableAttributes.includes("fiveIconSet")) {
		for (let num of Array(5).keys()) {
			const sourceElement = $(`#5-icon-group-image-${num + 1}-selection`);
			
			if (
				sourceElement.select2("data").length !== 0 &&
				sourceElement.select2("data")[0].element.dataset.imageIconSource !== undefined
			) {
				template.iconSet[num].source = sourceElement.select2("data")[0].element.dataset.imageIconSource;
			}
			
			const textElementValue = $(`#5-icon-group-text-${num + 1}`).val();
			
			if (textElementValue !== "") {
				template.iconSet[num].text = textElementValue;
			}
		}
	}
	
	if (template.changeableAttributes.includes("twoReactionSet")) {
		for (let num of Array(2).keys()) {
			const sourceElement = $(`#2-reaction-set-image-${num + 1}-selection`);
			
			if (
				sourceElement.select2("data").length !== 0 &&
				sourceElement.select2("data")[0].element.dataset.imageReactionSource !== undefined
			) {
				template.reactions[num].source = sourceElement.select2("data")[0].element.dataset.imageReactionSource;
			}
			
			const textElementValue = $(`#2-reaction-set-text-${num + 1}`).val();
			
			if (textElementValue !== "") {
				template.reactions[num].text = textElementValue;
			}
		}
	}
	
	if (template.changeableAttributes.includes("showNumberLabel")) {
		template.setShowNumberLabel(
			$("#show-number-label").is(":checked"),
			true
		);
	}
	
	if (
		$("#coalition-selection").select2("data").length !== 0 &&
		$("#coalition-selection").select2("data")[0].id !== undefined &&
		$("#coalition-selection").select2("data")[0].id !== "no-coalition"
	) {
		await setCoalition($("#coalition-selection").select2("data")[0].id, template);
	} else {
		await template.redrawCanvas();
	}
	
	reloadColorPalette();
}

$(".icon-selection").html(`
<option>Žádná ikona</option>
<option
	data-image-source="static/images/icons/Bezpecnost-1-dark.png"
	data-image-icon-source="static/images/icons/Bezpecnost-1.png"
>Bezpečnost 1</option>
<option
	data-image-source="static/images/icons/Bezpecnost-2-dark.png"
	data-image-icon-source="static/images/icons/Bezpecnost-2.png"
>Bezpečnost 2</option>
<option
	data-image-source="static/images/icons/Bydleni-1-dark.png"
	data-image-icon-source="static/images/icons/Bydleni-1.png"
>Bydlení 1</option>
<option
	data-image-source="static/images/icons/Bydleni-2-dark.png"
	data-image-icon-source="static/images/icons/Bydleni-2.png"
>Bydlení 2</option>
<option
	data-image-source="static/images/icons/Cestovni-ruch-1-dark.png"
	data-image-icon-source="static/images/icons/Cestovni-ruch-1.png"
>Cestovní ruch 1</option>
<option
	data-image-source="static/images/icons/Cestovni-ruch-2-dark.png"
	data-image-icon-source="static/images/icons/Cestovni-ruch-2.png"
>Cestovní ruch 2</option>
<option
	data-image-source="static/images/icons/Digitalizace-1-dark.png"
	data-image-icon-source="static/images/icons/Digitalizace-1.png"
>Digitalizace 1</option>
<option
	data-image-source="static/images/icons/Digitalizace-2-dark.png"
	data-image-icon-source="static/images/icons/Digitalizace-2.png"
>Digitalizace 2</option>
<option
	data-image-source="static/images/icons/Doprava-a-logistika-1-dark.png"
	data-image-icon-source="static/images/icons/Doprava-a-logistika-1.png"
>Doprava a logistika 1</option>
<option
	data-image-source="static/images/icons/Doprava-a-logistika-2-dark.png"
	data-image-icon-source="static/images/icons/Doprava-a-logistika-2.png"
>Doprava a logistika 2</option>
<option
	data-image-source="static/images/icons/Energetika-1-dark.png"
	data-image-icon-source="static/images/icons/Energetika-1.png"
>Energetika 1</option>
<option
	data-image-source="static/images/icons/Energetika-2-dark.png"
	data-image-icon-source="static/images/icons/Energetika-2.png"
>Energetika 2</option>
<option
	data-image-source="static/images/icons/Fajfka-dark.png"
	data-image-icon-source="static/images/icons/Fajfka.png"
>Fajfka</option>
<option
	data-image-source="static/images/icons/Finance-1-dark.png"
	data-image-icon-source="static/images/icons/Finance-1.png"
>Finance 1</option>
<option
	data-image-source="static/images/icons/Finance-2-dark.png"
	data-image-icon-source="static/images/icons/Finance-2.png"
>Finance 2</option>
<option
	data-image-source="static/images/icons/Jistoty-1-dark.png"
	data-image-icon-source="static/images/icons/Jistoty-1.png"
>Jistoty 1</option>
<option
	data-image-source="static/images/icons/Jistoty-2-dark.png"
	data-image-icon-source="static/images/icons/Jistoty-2.png"
>Jistoty 2</option>
<option
	data-image-source="static/images/icons/Kontrola-1-dark.png"
	data-image-icon-source="static/images/icons/Kontrola-1.png"
>Kontrola 1</option>
<option
	data-image-source="static/images/icons/Kontrola-2-dark.png"
	data-image-icon-source="static/images/icons/Kontrola-2.png"
>Kontrola 2</option>
<option
	data-image-source="static/images/icons/Kultura-1-dark.png"
	data-image-icon-source="static/images/icons/Kultura-1.png"
>Kultura 1</option>
<option
	data-image-source="static/images/icons/Kultura-2-dark.png"
	data-image-icon-source="static/images/icons/Kultura-2.png"
>Kultura 2</option>
<option
	data-image-source="static/images/icons/MHD-1-dark.png"
	data-image-icon-source="static/images/icons/MHD-1.png"
>MHD 1</option>
<option
	data-image-source="static/images/icons/MHD-2-dark.png"
	data-image-icon-source="static/images/icons/MHD-2.png"
>MHD 2</option>
<option
	data-image-source="static/images/icons/Mezinarodni-vztahy-1-dark.png"
	data-image-icon-source="static/images/icons/Mezinarodni-vztahy-1.png"
>Mezinarodní vztahy 1</option>
<option
	data-image-source="static/images/icons/Mezinarodni-vztahy-2-dark.png"
	data-image-icon-source="static/images/icons/Mezinarodni-vztahy-2.png"
>Mezinarodní vztahy 2</option>
<option
	data-image-source="static/images/icons/Moderni-mesto-1-dark.png"
	data-image-icon-source="static/images/icons/Moderni-mesto-1.png"
>Moderní město 1</option>
<option
	data-image-source="static/images/icons/Moderni-mesto-2-dark.png"
	data-image-icon-source="static/images/icons/Moderni-mesto-2.png"
>Moderní město 2</option>
<option
	data-image-source="static/images/icons/Modernizace-1-dark.png"
	data-image-icon-source="static/images/icons/Modernizace-1.png"
>Modernizace 1</option>
<option
	data-image-source="static/images/icons/Modernizace-2-dark.png"
	data-image-icon-source="static/images/icons/Modernizace-2.png"
>Modernizace 2</option>
<option
	data-image-source="static/images/icons/Newsletter-1-dark.png"
	data-image-icon-source="static/images/icons/Newsletter-1.png"
>Newsletter 1</option>
<option
	data-image-source="static/images/icons/Newsletter-2-dark.png"
	data-image-icon-source="static/images/icons/Newsletter-2.png"
>Newsletter 2</option>
<option
	data-image-source="static/images/icons/Obrana-1-dark.png"
	data-image-icon-source="static/images/icons/Obrana-1.png"
>Obrana 1</option>
<option
	data-image-source="static/images/icons/Obrana-2-dark.png"
	data-image-icon-source="static/images/icons/Obrana-2.png"
>Obrana 2</option>
<option
	data-image-source="static/images/icons/Odpadove-hospodarstvi-1-dark.png"
	data-image-icon-source="static/images/icons/Odpadove-hospodarstvi-1.png"
>Odpadové hospodářství 1</option>
<option
	data-image-source="static/images/icons/Odpadove-hospodarstvi-2-dark.png"
	data-image-icon-source="static/images/icons/Odpadove-hospodarstvi-2.png"
>Odpadové hospodářství 2</option>
<option
	data-image-source="static/images/icons/Otevrena-radnice-1-dark.png"
	data-image-icon-source="static/images/icons/Otevrena-radnice-1.png"
>Otevřená radnice 1</option>
<option
	data-image-source="static/images/icons/Otevrena-radnice-2-dark.png"
	data-image-icon-source="static/images/icons/Otevrena-radnice-2.png"
>Otevřená radnice 2</option>
<option
	data-image-source="static/images/icons/Pece-a-rodina-1-dark.png"
	data-image-icon-source="static/images/icons/Pece-a-rodina-1.png"
>Péče a rodina 1</option>
<option
	data-image-source="static/images/icons/Pece-a-rodina-2-dark.png"
	data-image-icon-source="static/images/icons/Pece-a-rodina-2.png"
>Péče a rodina 2</option>
<option
	data-image-source="static/images/icons/Prace-a-socialni-veci-1-dark.png"
	data-image-icon-source="static/images/icons/Prace-a-socialni-veci-1.png"
>Práce a sociální věci 1</option>
<option
	data-image-source="static/images/icons/Prace-a-socialni-veci-2-dark.png"
	data-image-icon-source="static/images/icons/Prace-a-socialni-veci-2.png"
>Práce a sociální věci 2</option>
<option
	data-image-source="static/images/icons/Program-1-dark.png"
	data-image-icon-source="static/images/icons/Program-1.png"
>Program 1</option>
<option
	data-image-source="static/images/icons/Program-2-dark.png"
	data-image-icon-source="static/images/icons/Program-2.png"
>Program 2</option>
<option
	data-image-source="static/images/icons/Prumysl-1-dark.png"
	data-image-icon-source="static/images/icons/Prumysl-1.png"
>Průmysl 1</option>
<option
	data-image-source="static/images/icons/Prumysl-2-dark.png"
	data-image-icon-source="static/images/icons/Prumysl-2.png"
>Průmysl 2</option>
<option
	data-image-source="static/images/icons/Sport-1-dark.png"
	data-image-icon-source="static/images/icons/Sport-1.png"
>Sport 1</option>
<option
	data-image-source="static/images/icons/Sport-2-dark.png"
	data-image-icon-source="static/images/icons/Sport-2.png"
>Sport 2</option>
<option
	data-image-source="static/images/icons/Spravedlnost-1-dark.png"
	data-image-icon-source="static/images/icons/Spravedlnost-1.png"
>Spravedlnost 1</option>
<option
	data-image-source="static/images/icons/Spravedlnost-2-dark.png"
	data-image-icon-source="static/images/icons/Spravedlnost-2.png"
>Spravedlnost 2</option>
<option
	data-image-source="static/images/icons/Srdce-1-dark.png"
	data-image-icon-source="static/images/icons/Srdce-1.png"
>Srdce 1</option>
<option
	data-image-source="static/images/icons/Srdce-2-dark.png"
	data-image-icon-source="static/images/icons/Srdce-2.png"
>Srdce 2</option>
<option
	data-image-source="static/images/icons/Svoboda-1-dark.png"
	data-image-icon-source="static/images/icons/Svoboda-1.png"
>Svoboda 1</option>
<option
	data-image-source="static/images/icons/Svoboda-2-dark.png"
	data-image-icon-source="static/images/icons/Svoboda-2.png"
>Svoboda 2</option>
<option
	data-image-source="static/images/icons/Transparentnost-1-dark.png"
	data-image-icon-source="static/images/icons/Transparentnost-1.png"
>Transparentnost 1</option>
<option
	data-image-source="static/images/icons/Transparentnost-2-dark.png"
	data-image-icon-source="static/images/icons/Transparentnost-2.png"
>Transparentnost 2</option>
<option
	data-image-source="static/images/icons/Uzemni-planovani-1-dark.png"
	data-image-icon-source="static/images/icons/Uzemni-planovani-1.png"
>Územní plánování 1</option>
<option
	data-image-source="static/images/icons/Uzemni-planovani-2-dark.png"
	data-image-icon-source="static/images/icons/Uzemni-planovani-2.png"
>Územní plánování 2</option>
<option
	data-image-source="static/images/icons/Vzdelavani-a-skolstvi-1-dark.png"
	data-image-icon-source="static/images/icons/Vzdelavani-a-skolstvi-1.png"
>Vzdělávání a školství 1</option>
<option
	data-image-source="static/images/icons/Vzdelavani-a-skolstvi-2-dark.png"
	data-image-icon-source="static/images/icons/Vzdelavani-a-skolstvi-2.png"
>Vzdělávání a školství 2</option>
<option
	data-image-source="static/images/icons/Worklife-balance-1-dark.png"
	data-image-icon-source="static/images/icons/Worklife-balance-1.png"
>Worklife balance 1</option>
<option
	data-image-source="static/images/icons/Worklife-balance-2-dark.png"
	data-image-icon-source="static/images/icons/Worklife-balance-2.png"
>Worklife balance 2</option>
<option
	data-image-source="static/images/icons/Zdravotnictvi-1-dark.png"
	data-image-icon-source="static/images/icons/Zdravotnictvi-1.png"
>Zdravotnictví 1</option>
<option
	data-image-source="static/images/icons/Zdravotnictvi-2-dark.png"
	data-image-icon-source="static/images/icons/Zdravotnictvi-2.png"
>Zdravotnictví 2</option>
<option
	data-image-source="static/images/icons/Zemedelstvi-1-dark.png"
	data-image-icon-source="static/images/icons/Zemedelstvi-1.png"
>Zemědělství 1</option>
<option
	data-image-source="static/images/icons/Zemedelstvi-2-dark.png"
	data-image-icon-source="static/images/icons/Zemedelstvi-2.png"
>Zemědělství 2</option>
<option
	data-image-source="static/images/icons/Zivotni-prostredi-1-dark.png"
	data-image-icon-source="static/images/icons/Zivotni-prostredi-1.png"
>Životní prostředí 1</option>
<option
	data-image-source="static/images/icons/Zivotni-prostredi-2-dark.png"
	data-image-icon-source="static/images/icons/Zivotni-prostredi-2.png"
>Životní prostředí 2</option>
`);

// PDF Exporting

const { jsPDF } = window.jspdf;

$("#pdf-a4").on(
	"click",
	async function(event) {
		await template.setResolution(Math.ceil(1122.52 * 3));

		const pdf = new jsPDF({
			orientation: "portrait",
			unit: "pt",
			format: "a4"
		});
		
		pdf.addImage(document.getElementById("graphicsCanvas"), "JPEG", 0, 0, 595.28, 841.89);
		
		pdf.save("VyhrajemA4.pdf");
		
		await template.setResolution(template.defaultResolution);
	}
);

$("#pdf-a3").on(
	"click",
	async function(event) {
		await template.setResolution(Math.ceil(1122.52 * 4));

		const pdf = new jsPDF({
			orientation: "portrait",
			unit: "pt",
			format: "a3"
		});
		
		pdf.addImage(document.getElementById("graphicsCanvas"), "JPEG", 0, 0, 841.89, 1190.55);
		
		pdf.save("VyhrajemA3.pdf");
		
		await template.setResolution(template.defaultResolution);
	}
);

$("#pdf-a2").on(
	"click",
	async function(event) {
		await template.setResolution(Math.ceil(1122.52 * 5));

		const pdf = new jsPDF({
			orientation: "portrait",
			unit: "pt",
			format: "a2"
		});
		
		pdf.addImage(document.getElementById("graphicsCanvas"), "JPEG", 0, 0, 1190.55, 1683.78);
		
		pdf.save("VyhrajemA2.pdf");
		
		await template.setResolution(template.defaultResolution);
	}
);

$("#pdf-a1").on(
	"click",
	async function(event) {
		await template.setResolution(Math.ceil(1122.52 * 6));

		const pdf = new jsPDF({
			orientation: "portrait",
			unit: "pt",
			format: "a1"
		});
		
		pdf.addImage(document.getElementById("graphicsCanvas"), "JPEG", 0, 0, 1683.78, 2383.94);
		
		pdf.save("VyhrajemA1.pdf");
		
		await template.setResolution(template.defaultResolution);
	}
);

$("#pdf-a0").on(
	"click",
	async function(event) {
		await template.setResolution(Math.ceil(1122.52 * 7));

		const pdf = new jsPDF({
			orientation: "portrait",
			unit: "pt",
			format: "a0"
		});
		
		pdf.addImage(document.getElementById("graphicsCanvas"), "JPEG", 0, 0, 2383.94, 3370.39);
		
		pdf.save("VyhrajemA0.pdf");
		
		await template.setResolution(template.defaultResolution);
	}
);

// Text

$("#primary-text").on(
	"input",
	async function(event) {
		await template.setPrimaryText(event.target.value);
	}
);

$("#secondary-text").on(
	"input",
	async function(event) {
		await template.setSecondaryText(event.target.value);
	}
);

$("#terciary-text").on(
	"input",
	async function(event) {
		await template.setTerciaryText(event.target.value);
	}
);

$("#url-text").on(
	"input",
	async function(event) {
		await template.setUrlText(event.target.value);
	}
);

$("#under-name-text").on(
	"input",
	async function(event) {
		await template.setUnderNameText(event.target.value);
	}
);

$("#secondary-under-name-text").on(
	"input",
	async function(event) {
		await template.setSecondaryUnderNameText(event.target.value);
	}
);

$("#date-text").on(
	"input",
	async function(event) {
		await template.setDateText(event.target.value);
	}
);

$("#time-text").on(
	"input",
	async function(event) {
		await template.setTimeText(event.target.value);
	}
);

$("#location-text").on(
	"input",
	async function(event) {
		await template.setLocationText(event.target.value);
	}
);

$("#name-text").on(
	"input",
	async function(event) {
		await template.setNameText(event.target.value);
	}
);

$("#secondary-name-text").on(
	"input",
	async function(event) {
		await template.setSecondaryNameText(event.target.value);
	}
);

// Location to top
$("#location-to-top").on(
	"change",
	async function(event) {
		await template.setLocationToTop(event.currentTarget.checked);
	}
);

// Show/hide requester text

$("#show-requester").on(
	"change",
	function(event) {
		if (event.currentTarget.checked) {
			$("#requester-text").css("display", "block");
			$("#requester-text").trigger("input");
		} else {
			$("#requester-text").css("display", "none");
			template.setRequesterText("");
		}
	}
);

$("#show-gradient").on(
	"change",
	async function(event) {
		await template.setGradient(event.currentTarget.checked);
	}
);

$("#requester-text").on(
	"input",
	async function(event) {
		await template.setRequesterText(event.target.value);
	}
);

$("#icon-text").on(
	"input",
	async function(event) {
		await template.setIconText(event.target.value);
	}
);

// QR codes
$("#qr-code-source").on(
	"input",
	function(event) {
		template.setQrCodeURI(event.target.value);
	}
);

// Image control

$("#resolution").on(
	"input",
	function(event) {
		let resolution = parseInt(event.target.value);
		
		if (Object.is(resolution, NaN) || resolution < 750) {
			resolution = 2000;
		}
		
		template.setResolution(resolution);
	}
);

// Images

$("#primary-image").on(
	"change",
	function(event) {
		if (event.target.files.length === 0) {
			return;
		}
		
		let uploadedImage = new Image();
		
		uploadedImage.onload = function() {
			if (this.width < 1080 || this.height < 1080) {
				alert("Prosím, nahraj obrázek s minimálním rozlišením 1080 pixelů na výšku i šířku.");
				$(event.target).val(null);
				
				return;
			}
			
			template.primaryImage = this;
			template.redrawCanvas();
		}
		
		uploadedImage.src = window.URL.createObjectURL(event.target.files[0]);
	}
);

$("#secondary-image").on(
	"change",
	function(event) {
		if (event.target.files.length === 0) {
			return;
		}
		
		let uploadedImage = new Image();
		
		uploadedImage.onload = function() {
			template.secondaryImage = this;
			template.redrawCanvas();
		}
		
		uploadedImage.src = window.URL.createObjectURL(event.target.files[0]);
	}
);

$("#logo-additional-image-reset").on(
	"click",
	function(event) {
		template.resetLogoImage();
		
		$("#logo-additional-image").val(null);
	}
);

$("#logo-additional-image").on(
	"change",
	function(event) {
		template.setLogoImageFromInput(event.target);
	}
);

$("#icon-image-input").on(
	"change",
	function(event) {
		template.setIconImageFromInput(event.target);
	}
);

$("#icon-image-reset").on(
	"click",
	function(event) {
		template.resetIconImage();
		
		$("#icon-image-input").val(null);
	}
);

$("#icon-image-selection").on(
	"select2:select",
	function(event) {
		let source = event.params.data.element.dataset.imageIconSource;
		
		if (source === undefined) {
			source = null;
		}
		
		template.setIconSource(source);
	}
);

// PRO! Ústí icons
$("#pro-usti-icon-image-input").on(
	"change",
	function(event) {
		template.setIconImageFromInput(event.target);
	}
);

$("#pro-usti-icon-image-reset").on(
	"click",
	function(event) {
		template.resetIconImage();
		
		$("#pro-usti-icon-image-input").val(null);
	}
);

$("#pro-usti-icon-image-selection").on(
	"select2:select",
	function(event) {
		let source = event.params.data.element.dataset.imageIconSource;
		
		if (source === undefined) {
			source = null;
		}
		
		template.setIconSource(source);
	}
);

// Point set
for (const num of Array(5).keys()) {
	$(`#5-point-set-text-${num + 1}`).on(
		"input",
		async function(event) {
			template.pointSet[num] = event.target.value;
			await template.redrawCanvas();
		}
	);
}

// Icon groups
for (let num of Array(4).keys()) {
	$(`#4-icon-group-image-${num + 1}-selection`).select2({
		templateSelection: formatSelect2ImageData,
		templateResult: formatSelect2ImageData,
		selectionCssClass: "select2-container-small-images",
		dropdownCssClass: "select2-container-small-images",
		width: "100%"
	});
	
	$(`#4-icon-group-image-${num + 1}-selection`).on(
		"select2:select",
		function(event) {
			let source = event.params.data.element.dataset.imageIconSource;
			
			if (source === undefined) {
				source = "";
			}
			
			template.iconSet[num].source = source;
			
			template.redrawCanvas();
		}
	);
	
	$(`#4-icon-group-image-${num + 1}-input`).on(
		"change",
		function(event) {
			if (event.target.files.length === 0) {
				template.iconSet[num].source = "";
				return;
			}
			
			const file = event.target.files[0];
			
			const reader = new FileReader();
			
			reader.onloadend = function() {
				template.iconSet[num].source = reader.result;
				template.redrawCanvas();
			};
			
			reader.readAsDataURL(file);
		}
	);
	
	$(`#4-icon-group-text-${num + 1}`).on(
		"input",
		function(event) {
			template.iconSet[num].text = event.target.value;
			
			template.redrawCanvas();
		}
	);
}

for (let num of Array(5).keys()) {
	$(`#5-icon-group-image-${num + 1}-selection`).select2({
		templateSelection: formatSelect2ImageData,
		templateResult: formatSelect2ImageData,
		selectionCssClass: "select2-container-small-images",
		dropdownCssClass: "select2-container-small-images",
		width: "100%"
	});
	
	$(`#5-icon-group-image-${num + 1}-selection`).on(
		"select2:select",
		function(event) {
			let source = event.params.data.element.dataset.imageIconSource;
			
			if (source === undefined) {
				source = "";
			}
			
			template.iconSet[num].source = source;
			
			template.redrawCanvas();
		}
	);
	
	$(`#5-icon-group-text-${num + 1}`).on(
		"input",
		function(event) {
			template.iconSet[num].text = event.target.value;
			
			template.redrawCanvas();
		}
	);
}

// Reaction groups
for (let num of Array(2).keys()) {
	$(`#2-reaction-set-image-${num + 1}-selection`).select2({
		templateSelection: formatSelect2ImageData,
		templateResult: formatSelect2ImageData,
		selectionCssClass: "select2-container-small-images",
		dropdownCssClass: "select2-container-small-images",
		width: "100%"
	});
	
	$(`#2-reaction-set-image-${num + 1}-selection`).on(
		"select2:select",
		function(event) {
			let source = event.params.data.element.dataset.imageReactionSource;
			
			if (source === undefined) {
				source = "";
			}
			
			template.reactions[num].source = source;
			
			template.redrawCanvas();
		}
	);
	
	$(`#2-reaction-set-text-${num + 1}`).on(
		"input",
		function(event) {
			template.reactions[num].text = event.target.value;
			
			template.redrawCanvas();
		}
	);
}

$("#location-image-selection").on(
	"select2:select",
	function(event) {
		let source = event.params.data.element.dataset.imageLocationSource;
		
		if (source === undefined) {
			source = null;
		}
		
		if (template.changeableAttributes.includes("locationImage")) {
			template.setLocationSource(source);
		}
	}
);

$("#location-image-selection").on(
	"change",
	function(event) {
		let source = $("#location-image-selection").select2("data")[0].element.dataset.imageLocationSource;
		
		if (source === undefined) {
			source = null;
		}
		
		if (template.changeableAttributes.includes("locationImage")) {
			template.setLocationSource(source);
		}
	}
);

$("#logo-is-center").on(
	"change",
	function(event) {
		template.setLogoIsCenter(event.target.checked);
	}
);

$("#background-has-pattern").on(
	"change",
	function(event) {
		template.setBackgroundHasPattern(event.target.checked);
	}
);

$("#location-image-selection").select2({
	templateSelection: formatSelect2ImageData,
	templateResult: formatSelect2ImageData,
	selectionCssClass: "select2-container-medium-images",
	dropdownCssClass: "select2-container-medium-images",
	width: "100%"
});

$("#icon-image-selection").select2({
	templateSelection: formatSelect2ImageData,
	templateResult: formatSelect2ImageData,
	selectionCssClass: "select2-container-small-images",
	dropdownCssClass: "select2-container-small-images",
	width: "100%"
});

$("#pro-usti-icon-image-selection").select2({
	templateSelection: formatSelect2ImageData,
	templateResult: formatSelect2ImageData,
	selectionCssClass: "select2-container-small-images",
	dropdownCssClass: "select2-container-small-images",
	width: "100%"
});

$("#selectable-rollup-background").select2({
	templateSelection: formatSelect2ImageData,
	templateResult: formatSelect2ImageData,
	selectionCssClass: "select2-container-medium-images",
	dropdownCssClass: "select2-container-medium-images",
	width: "100%"
});

// Rollup backgrounds
$("#selectable-rollup-background").on(
	"select2:select",
	async function (event) {
		const dataset = event.params.data.element.dataset;
		
		await template.setPrimaryColorScheme(dataset.colorScheme, true);
		await template.setBackgroundSource(dataset.imageSource);
	}
);

// Images - positions & zoom

// Positions

$(".move-primary-image-left").on(
	"click",
	function(event) { template.movePrimaryImageLeft(); }
);

$(".move-primary-image-up").on(
	"click",
	function(event) { template.movePrimaryImageUp(); }
);

$(".move-primary-image-right").on(
	"click",
	function(event) { template.movePrimaryImageRight(); }
);

$(".move-primary-image-down").on(
	"click",
	function(event) { template.movePrimaryImageDown(); }
);

$(".move-primary-image-reset").on(
	"click",
	function(event) { template.resetPrimaryImagePosition(); }
);

// https://stackoverflow.com/a/15037102
// Thanks to markE!

let canvas = document.getElementById("graphicsCanvas");
let canvasOffset = $("#graphicsCanvas").offset();
let canvasBoundingClientRect = canvas.getBoundingClientRect();
let offsetX = canvasOffset.left;
let offsetY = canvasOffset.top;
let canvasWidth = canvas.width;
let canvasHeight = canvas.height;
let isDragging = false;

$(window).on("resize", function(){
	canvasOffset = $("#graphicsCanvas").offset();
	offsetX = canvasOffset.left;
	offsetY = canvasOffset.top;
	
	canvasBoundingClientRect = canvas.getBoundingClientRect();
});

$("#graphicsCanvas").on(
	"mousedown",
	function(event){
		canMouseX=parseInt(event.clientX-offsetX)
		canMouseY=parseInt(event.clientY-offsetY);
		// set the drag flag
		isDragging=true;
	}
);

$("#graphicsCanvas").on(
	"mousemove",
	function(event){
		canMouseX=parseInt(event.clientX-offsetX);
		canMouseY=parseInt(event.clientY-offsetY);
		// if the drag flag is set, clear the canvas and draw the image
		
		// Ignore movement when there is no image to move
		if (template === null || !template.changeableAttributes.includes("primaryImagePosition")) {
			return;
		}
		
		if (isDragging) {
			template.primaryImageX = (
				(
					canvas.width
					* (canMouseX / canvasBoundingClientRect.width)
				)
				- canvas.width / 2
			);
			template.primaryImageY = (
				(
					canvas.height
					* (canMouseY / canvasBoundingClientRect.height * template.aspectRatio)
				)
				- canvas.height / 2
			);
			
			template.redrawCanvas();
		}
	}
);

$("#graphicsCanvas").on(
	"mouseup",
	function(event){
		if (isDragging) {
			canMouseX=parseInt(event.clientX-offsetX);
			canMouseY=parseInt(event.clientY-offsetY);
			// clear the drag flag
			isDragging=false;
		}
	}
);

$("#graphicsCanvas").on(
	"mouseout",
	function(event){
		if (isDragging) {
			canMouseX=parseInt(event.clientX-offsetX);
			canMouseY=parseInt(event.clientY-offsetY);
			// clear the drag flag
			isDragging=false;
		}
	}
);

// Zoom

$(".zoom-primary-image-input").on(
	"input",
	function(event) { template.setPrimaryImageZoom(parseInt(event.target.value) * 0.01); }
);

$(".zoom-primary-image-reset").on(
	"click",
	function(event) {
		template.resetPrimaryImageZoom();
		$(".zoom-primary-image-input").val("100");
	}
);

$("#logo-image-zoom").on(
	"input",
	function(event) { template.setLogoImageZoom(parseInt(event.target.value) * 0.01); }
);

$("#logo-image-zoom-reset").on(
	"click",
	function(event) {
		template.resetLogoImageZoom();
		$("#logo-image-zoom").val("100");
	}
);

// Sticker stuff
$("#show-number-label").on(
	"click",
	function(event) {
		template.setShowNumberLabel(event.currentTarget.checked);
	}
);

// Colors
$(".colorPickSelector").html(`<option
		value=\"#FFFFFF\"
	>Bílá</option>
	<option
		value=\"#000000\"
	>Černá</option>
	<option
		value=\"#CCCCCC\"
	>Šedá 1</option>
	<option
		value=\"#B3B3B3\"
	>Šedá 2</option>
	<option
		value=\"#868A89\"
	>Šedá 3</option>
	<option
		value=\"#4D4D4D\"
	>Šedá 4</option>
	<option
		value=\"#999999\"
	>Šedá 5</option>
	<option
		value=\"#838383\"
	>Šedá 6</option>
	<option
		value=\"#1D1D1B\"
	>Šedá 7</option>
	<option
		value=\"#962A51\"
	>Rudá 1</option>
	<option
		value=\"#712C45\"
	>Rudá 2</option>
	<option
		value=\"#4E1E2E\"
	>Rudá 3</option>
	<option
		value=\"#E63812\"
	>Rudá 4</option>
	<option
		value=\"#BE1E2D\"
	>Rudá 5</option>
	<option
		value=\"#DE2B58\"
	>Růžová 1</option>
	<option
		value=\"#B8284C\"
	>Růžová 2</option>
	<option
		value=\"#4E1E2F\"
	>Růžová 3</option>
	<option
		value=\"#DE8787\"
	>Růžová 4</option>
	<option
		value=\"#D35F5F\"
	>Růžová 5</option>
	<option
		value=\"#A02C2C\"
	>Růžová 6</option>
	<option
		value=\"#E17A85\"
	>Růžová 7</option>
	<option
		value=\"#AA5C65\"
	>Růžová 8</option>
	<option
		value=\"#864B52\"
	>Růžová 9</option>
	<option
		value=\"#FDA972\"
	>Pleťová 1</option>
	<option
		value=\"#ED8B4B\"
	>Pleťová 2</option>
	<option
		value=\"#B46C3D\"
	>Pleťová 3</option>
	<option
		value=\"#FFCC00\"
	>Žlutá 1</option>
	<option
		value=\"#D7B320\"
	>Žlutá 2</option>
	<option
		value=\"#A88D1C\"
	>Žlutá 3</option>
	<option
		value=\"#FFEDA5\"
	>Žlutá 4</option>
	<option
		value=\"#D8CC8E\"
	>Žlutá 5</option>
	<option
		value=\"#A8A573\"
	>Žlutá 6</option>
	<option
		value=\"#E2D7A9\"
	>Žlutá 7</option>
	<option
		value=\"#DAC465\"
	>Žlutá 8</option>
	<option
		value=\"#D7B31C\"
	>Žlutá 9</option>
	<option
		value=\"#FFD500\"
	>Žlutá 10</option>
	<option
		value=\"#FFDD55\"
	>Žlutá 11</option>
	<option
		value=\"#F6CB03\"
	>Žlutá 12</option>
	<option
		value=\"#CDDE87\"
	>Limetková 1</option>
	<option
		value=\"#ABC837\"
	>Limetková 2</option>
	<option
		value=\"#89A02C\"
	>Limetková 3</option>
	<option
		value=\"#D5FFD5\"
	>Limetková 4</option>
	<option
		value=\"#41DE87\"
	>Zelená 1</option>
	<option
		value=\"#209A37\"
	>Zelená 2</option>
	<option
		value=\"#206537\"
	>Zelená 3</option>
	<option
		value=\"#78BE43\"
	>Zelená 4</option>
	<option
		value=\"#AFE87E\"
	>Zelená 5</option>
	<option
		value=\"#50C450\"
	>Zelená 6</option>
	<option
		value=\"#8ED4A3\"
	>Zelená 7</option>
	<option
		value=\"#A9CE2D\"
	>Zelená 8</option>
	<option
		value=\"2DAD50\"
	>Zelená 9</option>
	<option
		value=\"#5ECCC9\"
	>Tyrkysová 1</option>
	<option
		value=\"#3DB6AC\"
	>Tyrkysová 2</option>
	<option
		value=\"#199887\"
	>Tyrkysová 3</option>
	<option
		value=\"#52D7CB\"
	>Tyrkysová 4</option>
	<option
		value=\"#5FBFB4\"
	>Tyrkysová 5</option>
	<option
		value=\"#32948B\"
	>Tyrkysová 6</option>
	<option
		value=\"#27756E\"
	>Tyrkysová 7</option>
	<option
		value=\"#3DB7AC\"
	>Tyrkysová 8</option>
	<option
		value=\"#26A4B7\"
	>Modrá 1</option>
	<option
		value=\"#0A4E5B\"
	>Modrá 2</option>
	<option
		value=\"#083F49\"
	>Modrá 3</option>
	<option
		value=\"#18C1DF\"
	>Modrá 4</option>
	<option
		value=\"#14A0B9\"
	>Modrá 5</option>
	<option
		value=\"#307684\"
	>Modrá 6</option>
	<option
		value=\"#123172\"
	>Modrá 7</option>
	<option
		value=\"#00173C\"
	>Modrá 8</option>
	<option
		value=\"#0038D1\"
	>Modrá 9</option>
	<option
		value=\"#AAEEFF\"
	>Světle modrá 1</option>
	<option
		value=\"#87CDDE\"
	>Světle modrá 2</option>
	<option
		value=\"#2C89A0\"
	>Světle modrá 3</option>
	<option
		value=\"#9796CA\"
	>Fialová 1</option>
	<option
		value=\"#585B88\"
	>Fialová 2</option>
	<option
		value=\"#21274E\"
	>Fialová 3</option>
	<option
		value=\"#DE87CD\"
	>Fialová 4</option>
	<option
		value=\"#D35FBC\"
	>Fialová 5</option>
	<option
		value=\"#782167\"
	>Fialová 6</option>
	<option
		value=\"#7B367C\"
	>Fialová 7</option>
	<option
		value=\"#5B305C\"
	>Fialová 8</option>
	<option
		value=\"#422A42\"
	>Fialová 9</option>
	<option
		value=\"#6E1646\"
	>Fialová 10</option>
	<option
		value=\"#3E2A5B\"
	>Fialová 11</option>
	<option
		value=\"#9796CA\"
	>Fialová 12</option>
	
	<option
		value=\"#6AA82E\"
	>Zelená - Zelená Koalice 1</option>
	<option
		value=\"#87CF32\"
	>Zelená - Zelená Koalice 2</option>
	<option
		value=\"#B5E179\"
	>Zelená - Zelená Koalice 3</option>
	<option
		value=\"#00AD43\"
	>Zelená - Zelená Koalice 4</option>
	<option
		value=\"#095C41\"
	>Zelená - Zelená Koalice 5</option>
	<option
		value=\"#9BA56B\"
	>Zelená - Zelená Koalice 6</option>
	
	<option
		value=\"#D5C980\"
	>Béžová - Zelená Koalice 1</option>
	<option
		value=\"#FEE993\"
	>Béžová - Zelená Koalice 2</option>
	<option
		value=\"#E1D298\"
	>Béžová - Zelená Koalice 3</option>
	<option
		value=\"#D7C256\"
	>Béžová - Zelená Koalice 4</option>
	
	<option
		value=\"#DBB01A\"
	>Žlutá - Zelená Koalice 1</option>
	<option
		value=\"#A0891F\"
	>Žlutá - Zelená Koalice 2</option>
	<option
		value=\"#DBB01C\"
	>Žlutá - Zelená Koalice 3</option>
	<option
		value=\"#FBC317\"
	>Žlutá - Zelená Koalice 4</option>
	
	<option
		value=\"#F7946F\"
	>Růžová - Zelená Koalice 1</option>
	<option
		value=\"#F57E49\"
	>Růžová - Zelená Koalice 2</option>
	<option
		value=\"#C5623B\"
	>Růžová - Zelená Koalice 3</option>
	
	<option
		value=\"#8E3E53\"
	>Rudá - Zelená Koalice 1</option>
	<option
		value=\"#BA4B6A\"
	>Rudá - Zelená Koalice 2</option>
	<option
		value=\"#F36287\"
	>Rudá - Zelená Koalice 3</option>
	<option
		value=\"#B52733\"
	>Rudá - Zelená Koalice 4</option>
	<option
		value=\"#F14B61\"
	>Rudá - Zelená Koalice 5</option>
	<option
		value=\"#F57386\"
	>Rudá - Zelená Koalice 6</option>
	<option
		value=\"#EE2462\"
	>Rudá - Zelená Koalice 7</option>
	<option
		value=\"#D71E55\"
	>Rudá - Zelená Koalice 8</option>
	<option
		value=\"#871F3F\"
	>Rudá - Zelená Koalice 9</option>
	<option
		value=\"#4F1B2D\"
	>Rudá - Zelená Koalice 10</option>
	<option
		value=\"#752245\"
	>Rudá - Zelená Koalice 11</option>
	<option
		value=\"#A72157\"
	>Rudá - Zelená Koalice 12</option>
	
	<option
		value=\"#831F6D\"
	>Fialová - Zelená Koalice 1</option>
	<option
		value=\"#D94EAE\"
	>Fialová - Zelená Koalice 2</option>
	<option
		value=\"#E772C0\"
	>Fialová - Zelená Koalice 3</option>
	<option
		value=\"#842F89\"
	>Fialová - Zelená Koalice 4</option>
	<option
		value=\"#5B2C5F\"
	>Fialová - Zelená Koalice 5</option>
	<option
		value=\"#3B2641\"
	>Fialová - Zelená Koalice 6</option>
	
	<option
		value=\"#8E8FD0\"
	>Modrá - Zelená Koalice 1</option>
	<option
		value=\"#4B5C92\"
	>Modrá - Zelená Koalice 2</option>
	<option
		value=\"#1E2D56\"
	>Modrá - Zelená Koalice 3</option>
	
	<option
		value=\"#1692AA\"
	>Tyrkysová - Zelená Koalice 1</option>
	<option
		value=\"#66CCD8\"
	>Tyrkysová - Zelená Koalice 2</option>
	<option
		value=\"#8FDAE9\"
	>Tyrkysová - Zelená Koalice 3</option>
	<option
		value=\"#1C7A8D\"
	>Tyrkysová - Zelená Koalice 4</option>
	<option
		value=\"#09AAC2\"
	>Tyrkysová - Zelená Koalice 5</option>
	<option
		value=\"#14B2CF\"
	>Tyrkysová - Zelená Koalice 6</option>
	<option
		value=\"#0F404C\"
	>Tyrkysová - Zelená Koalice 7</option>
	<option
		value=\"#115161\"
	>Tyrkysová - Zelená Koalice 8</option>
	<option
		value=\"#0DAEC0\"
	>Tyrkysová - Zelená Koalice 9</option>
	<option
		value=\"#38BCBB\"
	>Tyrkysová - Zelená Koalice 10</option>
	<option
		value=\"#1AB2AA\"
	>Tyrkysová - Zelená Koalice 11</option>
	<option
		value=\"#07A693\"
	>Tyrkysová - Zelená Koalice 12</option>
	<option
		value=\"#1AB2AA\"
	>Tyrkysová - Zelená Koalice 13</option>
	<option
		value=\"0DAC89\"
	>Tyrkysová - Zelená Koalice 14</option>
	<option
		value=\"#107460\"
	>Tyrkysová - Zelená Koalice 15</option>
	
	<option
		value=\"#C4C5C5\"
	>Šedá - Zelená Koalice 1</option>
	<option
		value=\"#A9AEAF\"
	>Šedá - Zelená Koalice 2</option>
	<option
		value=\"#798789\"
	>Šedá - Zelená Koalice 3</option>
	<option
		value=\"#3E494D\"
	>Šedá - Zelená Koalice 4</option>
	<option
		value=\"#273234\"
	>Šedá - Zelená Koalice 5</option>
	<option
		value=\"#1D272C\"
	>Šedá - Zelená Koalice 6</option>
	
	<option
		value=\"#133134\"
	>PRO! Ústí - Modrá 1</option>
	<option
		value=\"#51C7BC\"
	>PRO! Ústí - Modrá 2</option>
	<option
		value=\"#CFEBF4\"
	>PRO! Ústí - Modrá 3</option>
	<option
		value=\"#D2E9F2\"
	>PRO! Ústí - Modrá 4</option>
	<option
		value=\"#1A79B1\"
	>PRO! Ústí - Modrá 5</option>
	<option
		value=\"#5A9AC1\"
	>PRO! Ústí - Modrá 6</option>
	<option
		value=\"#BBD5E6\"
	>PRO! Ústí - Modrá 7</option>
	
	<option
		value=\"#FF6F34\"
	>PRO! Ústí - Oranžová 1</option>
	<option
		value=\"#DD5027\"
	>PRO! Ústí - Oranžová 2</option>
	<option
		value=\"#FF6F34\"
	>PRO! Ústí - Oranžová 3</option>
	<option
		value=\"#D1512E\"
	>PRO! Ústí - Oranžová 4</option>
	
	<option
		value=\"#FFA82D\"
	>PRO! Ústí - Žlutá 1</option>
	<option
		value=\"#FD9529\"
	>PRO! Ústí - Žlutá 2</option>
	
	<option
		value=\"#2C2E35\"
	>PRO! Ústí - Šedá 1</option>
	
	<option
		value=\"#FDBEDB\"
	>PRO! Ústí - Růžová 1</option>
	<option
		value=\"#812B4C\"
	>PRO! Ústí - Růžová 2</option>
	<option
		value=\"#EDA7C5\"
	>PRO! Ústí - Růžová 3</option>
	<option
		value=\"#AF5778\"
	>PRO! Ústí - Růžová 4</option>
	<option
		value=\"#A71E42\"
	>PRO! Ústí - Růžová 5</option>
	<option
		value=\"#FF9D71\"
	>PRO! Ústí - Růžová 6</option>
	<option
		value=\"#FF8F62\"
	>PRO! Ústí - Růžová 7</option>
	
	<option
		value=\"#341202\"
	>PRO! Ústí - Hnědá 1</option>
	<option
		value=\"#261005\"
	>PRO! Ústí - Hnědá 2</option>
	<option
		value=\"#8E4120\"
	>PRO! Ústí - Hnědá 3</option>
	<option
		value=\"#C9501C\"
	>PRO! Ústí - Hnědá 4</option>
	<option
		value=\"#231F20\"
	>PRO! Ústí - Hnědá 5</option>
	
	<option
		value=\"#1C705E\"
	>PRO! Ústí - Zelená 1</option>`);

$(".colorPickSelector").select2({
	templateSelection: formatSelect2ColorData,
	templateResult: formatSelect2ColorData,
	selectionCssClass: "select2-container-small-images",
	dropdownCssClass: "select2-container-small-images",
	width: "100%"
});

$(".colorPickSelector").on(
	"select2:select",
	function(event) {
		const attrName = snakeToCamel(event.target.id);
		const value = event.params.data.element.value;
		
		if (
			(attrName === "backgroundColor" && template.foregroundColor !== undefined && template.foregroundColor.toLowerCase() === value.toLowerCase()) ||
			(attrName === "foregroundColor" && template.backgroundColor !== undefined && template.backgroundColor.toLowerCase() === value.toLowerCase())
		) {
			alert("Silně nedoporučujeme používat stejnou barvu pro pozadí i popředí, šablony na to nejsou dělané. Nezastavíme tě, ale zkus si to prosím ještě promyslet.");
		}
		
		template[attrName] = value;
		template.redrawCanvas();
	}
);

$("#primary-color-scheme-selection").select2({
	templateSelection: formatSelect2ImageData,
	templateResult: formatSelect2ImageData,
	selectionCssClass: "select2-container-small-images",
	dropdownCssClass: "select2-container-small-images",
	width: "100%"
});

$("#primary-color-scheme-selection").on(
	"select2:select",
	function(event) {
		template.setPrimaryColorScheme(event.params.data.element.dataset.colorScheme);
		
		reloadColorPalette();
	}
);

$("#pro-usti-color-scheme-selection").select2({
	templateSelection: formatSelect2ImageData,
	templateResult: formatSelect2ImageData,
	selectionCssClass: "select2-container-small-images",
	dropdownCssClass: "select2-container-small-images",
	width: "100%"
});

$("#pro-usti-color-scheme-selection").on(
	"select2:select",
	function(event) {
		template.setProUstiColorScheme(event.params.data.element.dataset.colorScheme);
		
		reloadColorPalette();
	}
);

$("#name-color-scheme-selection").select2({
	templateSelection: formatSelect2ImageData,
	templateResult: formatSelect2ImageData,
	selectionCssClass: "select2-container-small-images",
	dropdownCssClass: "select2-container-small-images",
	width: "100%"
});

$("#name-color-scheme-selection").on(
	"select2:select",
	function(event) {
		template.setNameColorScheme(event.params.data.element.dataset.colorScheme);
		
		reloadColorPalette();
	}
);

$("#secondary-color-scheme-selection").select2({
	templateSelection: formatSelect2ImageData,
	templateResult: formatSelect2ImageData,
	selectionCssClass: "select2-container-small-images",
	dropdownCssClass: "select2-container-small-images",
	width: "100%"
});

$("#secondary-color-scheme-selection").on(
	"select2:select",
	function(event) {
		template.setSecondaryTextColorScheme(event.params.data.element.dataset.colorScheme);
		
		reloadColorPalette();
	}
);

$("#terciary-color-scheme-selection").select2({
	templateSelection: formatSelect2ImageData,
	templateResult: formatSelect2ImageData,
	selectionCssClass: "select2-container-small-images",
	dropdownCssClass: "select2-container-small-images",
	width: "100%"
});

$("#terciary-color-scheme-selection").on(
	"select2:select",
	function(event) {
		template.setTerciaryColorScheme(event.params.data.element.dataset.colorScheme);
		
		reloadColorPalette();
	}
);


$("#coalition-selection").select2({
	templateSelection: formatSelect2ImageData,
	templateResult: formatSelect2ImageData,
	selectionCssClass: "select2-container-medium-images",
	dropdownCssClass: "select2-container-medium-images",
	width: "100%"
});

async function setLitomericeCornerType(color, skipRedraw = false) {
	const colorRGB = hexToRgb(color);
	
	switch (templateName) {
		case "bottom-slogan":
		case "right-long-text":
		case "right-big-text":
		case "story-with-rectangle":
		case "story-with-full-width-rectangle":
		case "poll":
		case "cover":
		case "no-image-big-text-icon":
		case "poster-bottom-slogan":
		case "poster-bottom-slogan-no-icon":
		case "poster-center-slogan-no-image":
		case "poster-event":
		case "banner-name-left":
		case "banner-name-right":
		case "billboard":
		case "event-text-right": {
			template.finalDrawHook = async function() {
				const imageWidthMultiplier = 0.075;
				
				const eventImgLoadPromise = new Promise(
					resolve => {
						const eventImage = new Image();
						
						eventImage.onload = function() {
							let imageWidth = 0;
							
							// Use whichever dimension is smaller
							if (template.aspectRatio <= 1) {
								imageWidth = template.canvas.width * imageWidthMultiplier;
							} else {
								imageWidth = template.canvas.height * imageWidthMultiplier;
							}
							
							template.context.drawImage(
								colorizeImage(
									this,
									imageWidth,
									imageWidth,
									colorRGB.r,
									colorRGB.g,
									colorRGB.b
								),
								0, 0
							);
							
							resolve();
						}
						
						eventImage.src = "static/images/special/litomerice-corner.png";
					}
				);
				
				await eventImgLoadPromise;
			}
			
			if (!skipRedraw) {
				await template.redrawCanvas();
			}
			
			break;
		}
		
		case "left-long-text": {
			template.finalDrawHook = async function() {
				const imageWidthMultiplier = 0.075;
				
				const eventImgLoadPromise = new Promise(
					resolve => {
						const eventImage = new Image();
						
						eventImage.onload = function() {
							let imageWidth = 0;
							
							// Use whichever dimension is smaller
							if (template.aspectRatio <= 1) {
								imageWidth = template.canvas.width * imageWidthMultiplier;
							} else {
								imageWidth = template.canvas.height * imageWidthMultiplier;
							}
							
							// https://stackoverflow.com/a/13208013
							// Thanks to elclanrs!
							template.context.translate(template.canvas.width, 0);
							template.context.scale(-1, 1);
							template.context.drawImage(
								colorizeImage(
									this,
									imageWidth,
									imageWidth,
									colorRGB.r,
									colorRGB.g,
									colorRGB.b
								),
								0, 0
							);
							template.context.setTransform(1, 0, 0, 1, 0, 0);
							
							resolve();
						}
						
						eventImage.src = "static/images/special/litomerice-corner.png";
					}
				);
				
				await eventImgLoadPromise;
			}
			
			if (!skipRedraw) {
				await template.redrawCanvas();
			}
			
			break;
		}
	}
}

$("#litomerice-corner-type-selection").select2({
	templateSelection: formatSelect2ColorData,
	templateResult: formatSelect2ColorData,
	selectionCssClass: "select2-container-small-images",
	dropdownCssClass: "select2-container-small-images",
	width: "100%"
});

$("#litomerice-corner-type-selection").on(
	"select2:select",
	function(event) {
		if (event.params.data.element.value === "none") {
			template.finalDrawHook = function() {}
			template.redrawCanvas();
		}
		
		setLitomericeCornerType(event.params.data.element.value);
	}
);

let coalitionName = null;


async function setCoalition(coalition, template) {
	if (coalition === null || coalition === undefined || coalition === "no-coalition") {
		coalitionName = null;
		
		// reset
		template.setLightLogoSource(template.lightDefaultLogoSource, true);
		template.setDarkLogoSource(template.darkDefaultLogoSource, true);
		
		$("#location-image-selection").val("Žádná lokalizace");
		$("#location-image-selection").trigger("change");
		
		// if we have a sticker hook
		const stickerHook = template.stickerDrawHook;
		
		initTemplate($("#template-selection").select2("data")[0].element.dataset.templateType);
		
		template.stickerDrawHook = stickerHook;
		
		return;
	}
	
	coalitionName = coalition;
	
	function setGlobalSpecifics(coalition) {
		switch (coalition) {
			case "forum-jihlava":
				template.setDarkLogoSource("/static/images/logos/forum_jihlava-dark.png", true);
				template.setLightLogoSource("/static/images/logos/forum_jihlava.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Jihlava");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "zeleni-most":
				template.setDarkLogoSource("/static/images/logos/zeleni_most-dark.png", true);
				template.setLightLogoSource("/static/images/logos/zeleni_most.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Most");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "zeleni-bystrc":
				template.setDarkLogoSource("/static/images/logos/bystrc-dark.png", true);
				template.setLightLogoSource("/static/images/logos/bystrc.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Žádná lokalizace");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "zeleni-melnik":
				template.setDarkLogoSource("/static/images/logos/melnik-dark.png", true);
				template.setLightLogoSource("/static/images/logos/melnik.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Mělník");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "zeleni-praha13":
				template.setDarkLogoSource("/static/images/logos/zeleni_praha13-dark.png", true);
				template.setLightLogoSource("/static/images/logos/zeleni_praha13.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Praha 13");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "zeleni-praha14":
				template.setDarkLogoSource("/static/images/logos/zeleni_praha14-dark.png", true);
				template.setLightLogoSource("/static/images/logos/zeleni_praha14.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Praha 14");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "volary":
				template.setDarkLogoSource("/static/images/logos/volary-dark.png", true);
				template.setLightLogoSource("/static/images/logos/volary.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Volary");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "louny-spolecne":
				if (template.changeableAttributes.includes("logoImage")) {
					template.setDarkLogoSource("/static/images/logos/louny_spolecne-dark.png", true);
					template.setLightLogoSource("/static/images/logos/louny_spolecne.png", true);
				} else if (template.changeableAttributes.includes("verticalLogoImage")) {
					template.setDarkLogoSource("/static/images/vertical-logos/Louny-dark.png", true);
					template.setLightLogoSource("/static/images/vertical-logos/Louny-light.png", true);
				}
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Louny");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "spolecne-s-piraty":
				template.setDarkLogoSource("/static/images/logos/pirati_spolecne-dark.png", true);
				template.setLightLogoSource("/static/images/logos/pirati_spolecne.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Žádná lokalizace");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "vice-litomerice":
				$("#litomerice-corner").css("display", "block");
				
				if (
					$("#litomerice-corner-type-selection").select2("data").length !== 0 &&
					$("#litomerice-corner-type-selection").select2("data")[0].element.value !== "none"
				) {
					setLitomericeCornerType($("#litomerice-corner-type-selection").select2("data")[0].element.value, true);
				}
				
				template.setDarkLogoSource("/static/images/logos/vice_litomerice-dark.png", true);
				template.setLightLogoSource("/static/images/logos/vice_litomerice.png", true);
				
				template.primaryFont = "'Roboto Condensed'";
				template.primaryFontStyle = "bold";
				template.primaryTextTransformationHook = function(text) { return text.toUpperCase(); }
				template.setPrimaryText(template.primaryText, true); // reload transformation
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Žádná lokalizace");
					$("#location-image-selection").trigger("change");
				}
			
				break;
			case "stranane":
				template.setDarkLogoSource("static/images/logos/stranane-dark.png", true);
				template.setLightLogoSource("static/images/logos/stranane.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Žádná lokalizace");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "stranane-base":
				template.setDarkLogoSource("static/images/logos/stranane-dark.png", true);
				template.setLightLogoSource("static/images/logos/stranane_base.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Žádná lokalizace");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "prusanky":
				template.setDarkLogoSource("static/images/logos/prusanky-dark.png", true);
				template.setLightLogoSource("static/images/logos/prusanky.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Žádná lokalizace");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "prusanky-base":
				template.setDarkLogoSource("static/images/logos/prusanky-dark.png", true);
				template.setLightLogoSource("static/images/logos/prusanky_base.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Žádná lokalizace");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "ujezd":
				template.setDarkLogoSource("static/images/logos/ujezd-dark.png", true);
				template.setLightLogoSource("static/images/logos/ujezd.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Žádná lokalizace");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "ujezd-base":
				template.setDarkLogoSource("static/images/logos/ujezd.png", true);
				template.setLightLogoSource("static/images/logos/ujezd-dark.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Žádná lokalizace");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "cssd":
				template.setDarkLogoSource("static/images/logos/cssd_rychnov-dark.png", true);
				template.setLightLogoSource("static/images/logos/cssd_rychnov.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Rychnov");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "jilemnice":
				template.setDarkLogoSource("static/images/logos/jilemnice-dark.png", true);
				template.setLightLogoSource("static/images/logos/jilemnice.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Jílemnice");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "novarole-green": // :P
				template.setDarkLogoSource("static/images/logos/novarole-dark.png", true);
				template.setLightLogoSource("static/images/logos/novarole-dark.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Nová Role");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "novarole-dark":
				template.setDarkLogoSource("static/images/logos/novarole-dark.png", true);
				template.setLightLogoSource("static/images/logos/novarole.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Nová Role");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "nezavisli":
				template.setDarkLogoSource("static/images/logos/nezavisli-dark.png", true);
				template.setLightLogoSource("static/images/logos/nezavisli.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Žádná lokalizace");
					$("#location-image-selection").trigger("change");
				}
				
				break;
			case "balcarova":
				template.setDarkLogoSource("static/images/logos/dana_balcarova-dark.png", true);
				template.setLightLogoSource("static/images/logos/dana_balcarova.png", true);
				
				if (template.changeableAttributes.includes("locationImage")) {
					$("#location-image-selection").val("Žádná lokalizace");
					$("#location-image-selection").trigger("change");
				}
				
				break;
		}
	}
	
	template.primaryFont = "'Bebas Neue'";
	template.primaryFontStyle = "";
	template.primaryTextTransformationHook = function(text) { return text; }
	$("#litomerice-corner").css("display", "none");

	switch(coalition) {
		case "forum-jihlava-purple-dark":
			await template.setPrimaryColorScheme("forum-black-on-white", true);

			if (template.nameColorSchemes !== undefined) {
				template.setNameColorScheme("white-on-black", true);
			}
			
			setGlobalSpecifics("forum-jihlava");
			
			reloadColorPalette();
			
			break;
		case "forum-jihlava-purple-light":
			await template.setPrimaryColorScheme("forum-white-on-purple", true);
			
			if (template.nameColorSchemes !== undefined) {
				template.setNameColorScheme("white-on-black", true);
			}
			
			setGlobalSpecifics("forum-jihlava");
			
			reloadColorPalette();
			
			break;
		case "forum-jihlava-dark":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			if (template.nameColorSchemes !== undefined) {
				template.setNameColorScheme("white-on-black", true);
			}
			
			setGlobalSpecifics("forum-jihlava");
			
			reloadColorPalette();
			
			break;
		case "zeleni-most-green-dark":
			await template.setPrimaryColorScheme("zeleni-volary-bystrc-most-black-on-white", true);
			
			setGlobalSpecifics("zeleni-most");
			
			reloadColorPalette();
			
			break;
		case "zeleni-most-green-light":
			await template.setPrimaryColorScheme("zeleni-volary-bystrc-most-white-on-green", true);
			
			setGlobalSpecifics("zeleni-most");
			
			reloadColorPalette();
			
			break;
		case "zeleni-most-dark":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			if (template.nameColorSchemes !== undefined) {
				template.setNameColorScheme("white-on-black", true);
			}
			
			setGlobalSpecifics("zeleni-most");
			
			reloadColorPalette();
			
			break;
		case "zeleni-melnik-base":
			await template.setPrimaryColorScheme("white-on-black", true);
			
			setGlobalSpecifics("zeleni-melnik");
			
			reloadColorPalette();
			
			break;
		case "zeleni-melnik-yellow-rect":
			await template.setPrimaryColorScheme("zeleni-melnik-yellow-name-rect", true);
			
			setGlobalSpecifics("zeleni-melnik");
			
			reloadColorPalette();
			
			break;
		case "zeleni-melnik-dark":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			if (template.nameColorSchemes !== undefined) {
				template.setNameColorScheme("white-on-black", true);
			}
			
			setGlobalSpecifics("zeleni-melnik");
			
			reloadColorPalette();
			
			break;
		case "zeleni-bystrc-green-dark":
			await template.setPrimaryColorScheme("zeleni-volary-bystrc-most-black-on-white", true);
			
			setGlobalSpecifics("zeleni-bystrc");
			
			reloadColorPalette();
			
			break;
		case "zeleni-bystrc-green-light":
			await template.setPrimaryColorScheme("zeleni-volary-bystrc-most-white-on-green", true);
			
			setGlobalSpecifics("zeleni-bystrc");
			
			reloadColorPalette();
			
			break;
		case "zeleni-bystrc-dark":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			if (template.nameColorSchemes !== undefined) {
				template.setNameColorScheme("white-on-black", true);
			}
			
			setGlobalSpecifics("zeleni-bystrc");
			
			reloadColorPalette();
			
			break;
		case "volary-green-dark":
			await template.setPrimaryColorScheme("zeleni-volary-bystrc-most-black-on-white", true);
			
			setGlobalSpecifics("volary");
			
			reloadColorPalette();
			
			break;
		case "volary-green-light":
			await template.setPrimaryColorScheme("zeleni-volary-bystrc-most-white-on-green", true);
			
			setGlobalSpecifics("volary");
			
			reloadColorPalette();
			
			break;
		case "volary-dark":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			if (template.nameColorSchemes !== undefined) {
				template.setNameColorScheme("white-on-black", true);
			}
			
			setGlobalSpecifics("volary");
			
			reloadColorPalette();
			
			break;
		case "zeleni-praha13-green-dark":
			await template.setPrimaryColorScheme("zeleni-volary-bystrc-most-black-on-white", true);
			
			setGlobalSpecifics("zeleni-praha13");
			
			reloadColorPalette();
			
			break;
		case "zeleni-praha13-green-light":
			await template.setPrimaryColorScheme("zeleni-volary-bystrc-most-white-on-green", true);
			
			setGlobalSpecifics("zeleni-praha13");
			
			reloadColorPalette();
			
			break;
		case "zeleni-praha13-dark":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			if (template.nameColorSchemes !== undefined) {
				template.setNameColorScheme("white-on-black", true);
			}
			
			setGlobalSpecifics("zeleni-praha13");
			
			reloadColorPalette();
			
			break;
		case "zeleni-praha14-green-dark":
			await template.setPrimaryColorScheme("zeleni-volary-bystrc-most-black-on-white", true);
			
			setGlobalSpecifics("zeleni-praha14");
			
			reloadColorPalette();
			
			break;
		case "zeleni-praha14-green-light":
			await template.setPrimaryColorScheme("zeleni-volary-bystrc-most-white-on-green", true);
			
			setGlobalSpecifics("zeleni-praha14");
			
			reloadColorPalette();
			
			break;
		case "zeleni-praha14-dark":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			if (template.nameColorSchemes !== undefined) {
				template.setNameColorScheme("white-on-black", true);
			}
			
			setGlobalSpecifics("zeleni-praha14");
			
			reloadColorPalette();
			
			break;
		case "louny-spolecne-purple-dark":
			await template.setPrimaryColorScheme("louny-spolecne-white-on-purple", true);
			
			setGlobalSpecifics("louny-spolecne");
			
			reloadColorPalette();
			
			break;
		case "louny-spolecne-purple-light":
			await template.setPrimaryColorScheme("louny-spolecne-black-on-white", true);
			
			setGlobalSpecifics("louny-spolecne");
			
			reloadColorPalette();
			
			break;
		case "louny-spolecne-dark":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			setGlobalSpecifics("louny-spolecne");
			
			reloadColorPalette();
			
			break;
		case "spolecne-s-piraty-blue-dark":
			await template.setPrimaryColorScheme("spolecne-s-piraty-black-on-white", true);
			
			setGlobalSpecifics("spolecne-s-piraty");
			
			reloadColorPalette();
			
			break;
		case "spolecne-s-piraty-blue-light":
			await template.setPrimaryColorScheme("spolecne-s-piraty-white-on-blue", true);
			
			setGlobalSpecifics("spolecne-s-piraty");
			
			reloadColorPalette();
			
			break;
		case "spolecne-s-piraty-dark":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			setGlobalSpecifics("spolecne-s-piraty");
			
			reloadColorPalette();
			
			break;
		case "vice-litomerice-blue-dark":
			await template.setPrimaryColorScheme("litomerice-blue-on-white", true);
			
			setGlobalSpecifics("vice-litomerice");
			
			reloadColorPalette();
			
			break;
		case "vice-litomerice-blue-light":
			await template.setPrimaryColorScheme("litomerice-white-on-blue", true);
			
			setGlobalSpecifics("vice-litomerice");
			
			reloadColorPalette();
			
			break;
		case "vice-litomerice-dark":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			setGlobalSpecifics("vice-litomerice");
			
			reloadColorPalette();
			
			break;
		case "stranane-yellow-white":
			await template.setPrimaryColorScheme("stranane-yellow-on-white", true);
			
			setGlobalSpecifics("stranane");
			
			reloadColorPalette();
			
			break;
		case "stranane-white-yellow":
			await template.setPrimaryColorScheme("stranane-white-on-yellow", true);
			
			setGlobalSpecifics("stranane");
			
			reloadColorPalette();
			
			break;
		case "stranane-gray-yellow":
			await template.setPrimaryColorScheme("stranane-gray-on-yellow", true);
			
			setGlobalSpecifics("stranane");
			
			reloadColorPalette();
			
			break;
		case "stranane-base":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			setGlobalSpecifics("stranane-base");
			
			reloadColorPalette();
			
			break;
		case "prusanky-yellow-white":
			await template.setPrimaryColorScheme("prusanky-yellow-on-white", true);
			
			setGlobalSpecifics("prusanky");
			
			reloadColorPalette();
			
			break;
		case "prusanky-white-yellow":
			await template.setPrimaryColorScheme("prusanky-white-on-yellow", true);
			
			setGlobalSpecifics("prusanky");
			
			reloadColorPalette();
			
			break;
		case "prusanky-black-yellow":
			await template.setPrimaryColorScheme("prusanky-black-on-yellow", true);
			
			setGlobalSpecifics("prusanky");
			
			reloadColorPalette();
			
			break;
		case "prusanky-base":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			setGlobalSpecifics("prusanky-base");
			
			reloadColorPalette();
			
			break;
		case "ujezd-green-white":
			await template.setPrimaryColorScheme("ujezd-green-on-white", true);
			
			setGlobalSpecifics("ujezd");
			
			reloadColorPalette();
			
			break;
		case "ujezd-white-green":
			await template.setPrimaryColorScheme("ujezd-white-on-green", true);
			
			setGlobalSpecifics("ujezd");
			
			reloadColorPalette();
			
			break;
		case "ujezd-base":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			setGlobalSpecifics("ujezd-base");
			
			reloadColorPalette();
			
			break;
		case "cssd-rychnov-red-black":
			await template.setPrimaryColorScheme("cssd-red-on-black", true);
			
			setGlobalSpecifics("cssd");
			
			reloadColorPalette();
			
			break;
		case "cssd-rychnov-black-red":
			await template.setPrimaryColorScheme("cssd-black-on-red", true);
			
			setGlobalSpecifics("cssd");
			
			reloadColorPalette();
			
			break;
		case "cssd-rychnov-base":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			setGlobalSpecifics("cssd");
			
			reloadColorPalette();
			
			break;
		case "jilemnice-purple-black":
			await template.setPrimaryColorScheme("jilemnice-purple-on-black", true);
			
			setGlobalSpecifics("jilemnice");
			
			reloadColorPalette();
			
			break;
		case "jilemnice-black-purple":
			await template.setPrimaryColorScheme("jilemnice-black-on-purple", true);
			
			setGlobalSpecifics("jilemnice");
			
			reloadColorPalette();
			
			break;
		case "jilemnice-base":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			setGlobalSpecifics("jilemnice");
			
			reloadColorPalette();
			
			break;
		case "novarole-white-green":
			await template.setPrimaryColorScheme("novarole-white-on-green", true);
			
			setGlobalSpecifics("novarole-green");
			
			reloadColorPalette();
			
			break;
		case "novarole-green-white":
			await template.setPrimaryColorScheme("novarole-green-on-white", true);
			
			setGlobalSpecifics("novarole-green");
			
			reloadColorPalette();
			
			break;
		case "novarole-green-black":
			await template.setPrimaryColorScheme("novarole-green-on-black", true);
			
			setGlobalSpecifics("novarole-dark");
			
			reloadColorPalette();
			
			break;
		case "novarole-base":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			setGlobalSpecifics("novarole-dark");
			
			reloadColorPalette();
			
			break;
		case "nezavisli-dark":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			setGlobalSpecifics("nezavisli");
			
			break;
		case "nezavisli-light":
			await template.setPrimaryColorScheme("white-on-black", true);
			
			setGlobalSpecifics("nezavisli");
			
			break;
		case "balcarova-dark":
			await template.setPrimaryColorScheme("black-on-white", true);
			
			setGlobalSpecifics("balcarova");
			
			break;
		case "balcarova-light":
			await template.setPrimaryColorScheme("white-on-black", true);
			
			setGlobalSpecifics("balcarova");
			
			break;
		default:
			throw new Error(`Possible broken coalition lookup: ${coalition}.`);
	}
	
	await template.redrawCanvas();
}

$("#coalition-selection").on(
	"select2:select",
	function(event) {
		setCoalition(event.params.data.id, template);
	}
);

$("#coalition-selection").on(
	"change",
	function(event) {
		setCoalition(event.currentTarget.value, template);
	}
);

// Templates

$("#template-selection").select2({
	templateSelection: formatSelect2ImageData,
	templateResult: formatSelect2ImageData,
	selectionCssClass: "select2-container-medium-images",
	dropdownCssClass: "select2-container-medium-images",
	width: "100%"
});

$("#template-selection").on(
	"select2:select",
	function(event) {
		templateName = event.params.data.element.dataset.templateType;
		initTemplate(event.params.data.element.dataset.templateType);
	}
);

// https://stackoverflow.com/a/50300880
// Thanks to Ulf Aslak!
$("#download").on(
	"click",
	function(){
		checkMissingImage(template);
		
		let link = document.createElement('a');

		link.download = "Vyhrajem.png";
		link.href = document.getElementById("graphicsCanvas").toDataURL()

		link.click();
	}
);

// Ivan-specific
$("#flip").on(
	"click",
	function() {
		template.imageFlipped = !template.imageFlipped;
		template.redrawCanvas();
	}
);

var templateName = null;

// Run
$(window).ready(
	async function () {
		// Make absolutely sure Bebas Neue is loaded.
		// Quick fix
		await (
			fetch("/static/fonts/bebas-neue/BebasNeue.woff")
			.then(resp => resp.arrayBuffer())
			.then(font => {
				const fontFace = new FontFace("Bebas Neue", font);
				document.fonts.add(fontFace);
			})
		);
		
		templateName = $("#template-selection").select2("data")[0].element.dataset.templateType;
		
		if ($("#show-requester").is(":checked")) {
			$("#requester-text").css("display", "block");
		}

		initTemplate(templateName);
	}
);
