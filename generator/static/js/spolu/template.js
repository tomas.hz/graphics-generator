class Template {
	// The base class for all templates, including some abstract properties.
	
	constructor(canvas, context = null) {
		if (typeof(canvas) === "string") {
			this.canvas = document.getElementById(canvas);
		} else {
			this.canvas = canvas;
		}
		
		if (context === null) {
			this.context = this.canvas.getContext("2d");
		} else {
			this.context = context;
		}
	}
	
	changeableAttributes = [];
	colors = {};
	
	async redrawCanvas() {
		throw new Error("This function is not implemented.");
	}
}


class PrimaryLocalizationTextBackgroundImageTemplate extends Template {
	changeableAttributes = [
		"primaryText",
		"localizationText",
		"backgroundImageSource"
	];
	
	primaryText = "";
	localizationText = "";
	
	backgroundImageSources = [
		"/static/images/spolu/backgrounds/1.png",
		
	];
	backgroundImage = null;
	
	async setPrimaryText(text, redraw = true) {
		this.primaryText = true;
		
		if (redraw) await this.redrawCanvas();
	}
	
	async setLocalizationText(text, redraw = true) {
		this.localizationText = true;
		
		if (redraw) await this.redrawCanvas();
	}
	
	async setBackgroundImageSource(source, redraw = true) {
		this.backgroundImage = new Image();
		
		const backgroundImageLoadPromise = new Promise(
			resolve => {
				this.backgroundImage.onload = function() {
					resolve();
				}
			}
		);
		
		this.backgroundImage.src = source;
		
		if (redraw) await this.redrawCanvas();
	}
	
	async pickRandomBackgroundImage() {
		// https://stackoverflow.com/a/4550514
		// Thanks to Jacob Relkin and jakanz!
		
		await this.setBackgroundImageSource(
			this.backgroundImageSources[Math.floor(Math.random() * this.backgroundImageSources.length)]
		);
	}
}


class BasicClaimTemplate extends PrimaryLocalizationTextBackgroundImageTemplate {
	colors = {
		headerBackground: "#fed929",
		header: "#03034d",
		primaryTextBackground: "#ffffff",
		primaryText: "#03034d",
		localizationText: "#ffffff"
	};
	
	async redrawCanvas() {
		
	}
}

class PersonQuoteTemplate extends PrimaryLocalizationTextBackgroundImageTemplate {
	changeableAttributes = PrimaryLocalizationTextBackgroundImageTemplate.changeableAttributes.concat([
		"nameText"
	]);
	
	colors = {
		primaryTextBackground: "#ffffff",
		primaryText: "#03034d",
		nameBackground: "#fed929",
		nameText: "#03034d",
		localizationText: "#ffffff"
	};
	
	nameText = "";
	
	async redrawCanvas() {
		
	}
	
	async setNameText(text, redraw = true) {
		this.nameText = true;
		
		if (redraw) await this.redrawCanvas();
	}
}
