import functools
import typing

import authlib.jose
import flask
import werkzeug

__all__ = ["authenticate_via_jwt"]

def authenticate_via_jwt(
	func: typing.Callable
) -> typing.Callable:
	"""Checks that the current user is authorized to use the wrapped view. This
	depends on them having a valid ``token`` cookie. Otherwise, they're
	redirected to ``oidc.login``.
	"""

	@functools.wraps(func)
	def decorator(*args, **kwargs) -> typing.Any:
		if "token" not in flask.request.cookies:
			return flask.redirect(
				flask.url_for("oidc.login")
			)

		try:
			claims = authlib.jose.jwt.decode(
				flask.request.cookies["token"],
				flask.current_app.config["JWT_SECRET_KEY"]
			)
		except authlib.jose.JoseError as exception:
			raise werkzeug.exceptions.BadRequest from exception

		try:
			claims.validate()
		except authlib.jose.JoseError as exception:
			flask.current_app.logger.debug(
				"Claim validation failed: %s",
				exception
			)

			return flask.redirect(
				flask.url_for("oidc.login")
			)

		flask.g.user_id = claims["sub"]

		return func(*args, **kwargs)
	return decorator
