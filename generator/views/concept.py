"""Concept API views."""

import base64
import binascii
import http.client
import json
import mimetypes
import os
import typing
import uuid

import werkzeug.exceptions
import werkzeug.utils
import flask
import sqlalchemy

from .. import authentication, database

__all__ = [
	"concept_blueprint",
	"list_",
	"delete",
	"edit",
	"create"
]

concept_blueprint = flask.Blueprint(
	"concept",
	__name__,
	url_prefix="/concepts"
)


def get_concept_path(concept_id: str) -> str:
	binary_path = os.environ.get(
		"CONCEPT_BINARY_LOCATION",
		os.path.join(os.getcwd(), "concepts-binary")
	)

	return os.path.join(
		binary_path,
		concept_id
	)


def write_binary_data(concept_id: str) -> None:
	if "binaryData" in flask.request.json:
		if not isinstance(flask.request.json["binaryData"], dict):
			raise werkzeug.exceptions.BadRequest

		for key, content in flask.request.json["binaryData"].items():
			filename = werkzeug.utils.secure_filename(key).replace(".", "")

			concept_path = get_concept_path(concept_id)

			if not os.path.exists(concept_path):
				os.mkdir(concept_path)

			split_content = content.split(";base64")

			if len(split_content) != 2:
				raise werkzeug.exceptions.BadRequest

			split_header = split_content[0].split("data:")

			if len(split_header) != 2:
				raise werkzeug.exceptions.BadRequest

			extension = mimetypes.guess_extension(split_header[1])

			if extension != None:
				filename += extension

			with open(
				os.path.join(concept_path, filename),
				"wb"
			) as binary_file:
				try:
					binary_file.write(
						base64.b64decode(split_content[1].encode("utf-8"))
					)
				except (binascii.Error, ValueError) as e:
					raise werkzeug.exceptions.BadRequest


def delete_binary_data(
	concept_id: str,
	include_directory: bool = False
) -> None:
	concept_path = get_concept_path(concept_id)

	if not os.path.exists(concept_path):
		return

	for existing_binary_file in os.listdir(concept_path):
		os.remove(
			os.path.join(
				concept_path,
				existing_binary_file
			)
		)

	if include_directory:
		os.rmdir(concept_path)


@concept_blueprint.route("", methods=["GET"])
@authentication.authenticate_via_jwt
def list_() -> typing.Tuple[flask.Response, int]:
	result = []
	
	for concept in (
		flask.g.sa_session.execute(
			sqlalchemy.select(database.Concept).
			where(database.Concept.user_id == flask.g.user_id)
		).scalars().all()
	):
		concept_data = {
			"id": concept.id,
			"data": {
				"canvasData": json.loads(concept.data),
				"binaryData": {}
			}
		}

		concept_path = get_concept_path(str(concept.id))

		if os.path.exists(concept_path):
			for binary_file_name in os.listdir(concept_path):
				split_filename = binary_file_name.split(".")

				if len(split_filename) == 1:
					concept_data["data"]["binaryData"][binary_file_name] = (
						"data:application/octet-stream;base64,"
					)
				else:
					concept_data["data"]["binaryData"][split_filename[0]] = (
						"data:"
						# Assume mimetypes knows this extension, since it generated it
						+ mimetypes.guess_type(binary_file_name)[0]
						+ ";base64,"
					)

				with open(
					os.path.join(concept_path, binary_file_name),
					"rb"
				) as binary_file:
					concept_data["data"]["binaryData"][split_filename[0]] += base64.b64encode(
						binary_file.read()
					).decode("utf-8")

		result.append(concept_data)

	return flask.jsonify(result), http.client.OK


# Completely ignore input validation where possible, since our instance isn't
# going to be public and input validation for the amount of data we have would
# cost tons of time we simply don't have. Can implement later on once the first
# release rolls out.
@concept_blueprint.route("", methods=["POST"])
@authentication.authenticate_via_jwt
def create() -> typing.Tuple[flask.Response, int]:
	if (
		flask.request.json is None
		or "canvasData" not in flask.request.json
	):
		raise werkzeug.exceptions.BadRequest

	concept = database.Concept(
		user_id=flask.g.user_id,
		data=json.dumps(flask.request.json["canvasData"])
	)

	flask.g.sa_session.add(concept)
	flask.g.sa_session.commit()

	write_binary_data(str(concept.id))

	return flask.jsonify(concept.id), http.client.CREATED


@concept_blueprint.route("/<uuid:id_>", methods=["PUT"])
@authentication.authenticate_via_jwt
def edit(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	if (
		flask.request.json is None
		or "canvasData" not in flask.request.json
	):
		raise werkzeug.exceptions.BadRequest

	concept = flask.g.sa_session.execute(
		sqlalchemy.select(database.Concept).
		where(database.Concept.id == id_)
	).scalars().one_or_none()

	if concept is None:
		raise werkzeug.exceptions.NotFound

	concept.data = json.dumps(flask.request.json["canvasData"])
	flask.g.sa_session.commit()

	delete_binary_data(str(concept.id))
	write_binary_data(str(concept.id))

	return flask.jsonify(None), http.client.NO_CONTENT


@concept_blueprint.route("/<uuid:id_>", methods=["DELETE"])
@authentication.authenticate_via_jwt
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	concept = flask.g.sa_session.execute(
		sqlalchemy.select(database.Concept).
		where(database.Concept.id == id_)
	).scalars().one_or_none()

	if concept is None:
		raise werkzeug.exceptions.NotFound

	delete_binary_data(str(concept.id), include_directory=True)

	flask.g.sa_session.delete(concept)
	flask.g.sa_session.commit()

	return flask.jsonify(None), http.client.NO_CONTENT
