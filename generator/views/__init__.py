"""Views."""

from .concept import concept_blueprint
from .generator import generator_blueprint
from .oidc import oidc_blueprint 


__all__ = [
	"concept_blueprint",
	"generator_blueprint",
	"oidc_blueprint"
]
