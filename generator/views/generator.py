"""Generator views."""

import http.client
import json
import typing

import werkzeug.exceptions
import flask

from .. import authentication

__all__ = [
	"generator_blueprint",
	"view",
	"view_avatar_generator"
]

generator_blueprint = flask.Blueprint(
	"generator",
	__name__
)


ICON_HEIGHT_SPECIAL_MULTIPLIERS = {
	"kladno": 1.2,
	"kralovec": 1.2,
	"louny": 2,
	"louny-bila-na-fialove": 2,
	"louny-zlata-na-fialove": 2,
	"louny-fialova-na-bile": 2,
	"louny-fialova-na-zlate": 2,
	"martin-kakora": 4,
	"spolecne-s-piraty-zelena": 2,
	"spolecne-s-piraty-oranzova": 2,
	"spolecne-s-piraty-fialova": 2,
	"melnik": 4,
	"olomouc-volim": 4,
	"olomouc-kandidat": 4,
	"pro-usti": 4,
	"zelezny-brod": 1.2
}

ICON_OFFSET_BOTTOM_SPECIAL_MULTIPLIERS = {
	"jihlava": 2.5,
	"kladno": 0,
	"louny": 0,
	"louny-bila-na-fialove": 0,
	"louny-zlata-na-fialove": 0,
	"louny-fialova-na-bile": 0,
	"louny-fialova-na-zlate": 0,
	"martin-kakora": 0,
	"spolecne-s-piraty-zelena": 0,
	"spolecne-s-piraty-oranzova": 0,
	"spolecne-s-piraty-fialova": 0,
	"melnik": 0,
	"olomouc-volim": 0,
	"olomouc-kandidat": 0,
	"pro-usti": 0
}


@generator_blueprint.route("/")
@authentication.authenticate_via_jwt
def view() -> typing.Tuple[flask.Response, int]:
	return flask.render_template("generator.html", ivan=False), http.client.OK


@generator_blueprint.route("/ivan")
@authentication.authenticate_via_jwt
def view_with_ivan() -> typing.Tuple[flask.Response, int]:
	return flask.render_template("generator.html", ivan=True), http.client.OK


@generator_blueprint.route("/avatar")
@authentication.authenticate_via_jwt
def view_avatar_generator() -> typing.Tuple[flask.Response, int]:
	return flask.render_template(
		"avatar_internal.html",
		ICON_HEIGHT_SPECIAL_MULTIPLIERS=json.dumps(ICON_HEIGHT_SPECIAL_MULTIPLIERS),
		ICON_OFFSET_BOTTOM_SPECIAL_MULTIPLIERS=json.dumps(ICON_OFFSET_BOTTOM_SPECIAL_MULTIPLIERS)
	), http.client.OK


@generator_blueprint.route("/avatar/centralni")
@generator_blueprint.route("/avatar/ikona/<string:icon_localization>")
@generator_blueprint.route("/avatar/lokalizace/<string:base_localization>")
def view_avatar_generator_with_localization(
	icon_localization: typing.Union[None, str] = None,
	base_localization: typing.Union[None, str] = None
) -> typing.Tuple[flask.Response, int]:
	if (
		(
			icon_localization is not None
			and icon_localization not in [
				"benesov-1",
				"benesov-2",
				"benesov-3",
				"beroun",
				"brandys-nad-labem",
				"bilina",
				"bohumin",
				"caslav",
				"cesky-tesin",
				"chudova-plana",
				"ceska-trebova",
				"ceske-budejovice",
				"cheb",
				"chomutov",
				"chrast",
				"chrudim",
				"domazlice",
				"decin",
				"frydek-mistek",
				"havirov",
				"havlickuv-brod",
				"hermanuv-mestec",
				"holesov",
				"holysov",
				"hradec",
				"jesenice-jilove",
				"jihlava",
				"jindrichuv-hradec",
				"kadan",
				"karlovy-lazne",
				"karlovy-vary",
				"karvina",
				"kladno",
				"klatovy",
				"kolin",
				"kralupy-nad-vltavou",
				"kralovec",
				"kutna-hora",
				"lysa-nad-labem",
				"louny-bila-na-fialove",
				"louny-zlata-na-fialove",
				"louny-fialova-na-bile",
				"louny-fialova-na-zlate",
				"litomerice",
				"loket",
				"martin-kakora",
				"moravska-trebova",
				"most",
				"melnik",
				"milovice",
				"mlada-boleslav",
				"mnisecko-a-dolni-berounka",
				"neratovice",
				"nymburk",
				"napajedla",
				"olomouc",
				"olomouc-kandidat",
				"olomouc-volim",
				"opava",
				"orlova",
				"ostrava-poruba",
				"ostrava",
				"ostrava-jih",
				"otrokovice",
				"pardubice",
				"pisek",
				"plzen",
				"podebrady",
				"policka",
				"poruba",
				"prachatice",
				"praha-1",
				"praha-10",
				"praha-11",
				"praha-12",
				"praha-13",
				"praha-14",
				"praha-15",
				"praha-18",
				"praha-2",
				"praha-21",
				"praha-3",
				"praha-4",
				"praha-5",
				"praha-6",
				"praha-7",
				"praha-8",
				"praha-9",
				"prerov",
				"pribor",
				"pribram",
				"prostejov",
				"pro-usti",
				"rotava",
				"roudnice-nad-labem",
				"roznov",
				"rudoltice",
				"sobeslav",
				"sokolov",
				"spolecne-s-piraty-zelena",
				"spolecne-s-piraty-oranzova",
				"spolecne-s-piraty-fialova",
				"strakonice",
				"sumperk",
				"svitavy",
				"susice",
				"tabor",
				"tachov",
				"telc",
				"teplice",
				"terezin",
				"trebic",
				"trutnov",
				"tyn-nad-vltavou",
				"uherske-hradiste",
				"usti-nad-labem",
				"valec",
				"vejprnice",
				"valmez",
				"volary",
				"zatec",
				"zelezny-brod",
				"zlin",
				"ukrajina-cz",
				"ukrajina-eu",
			]
		) or (
			base_localization is not None
			and base_localization not in [
				"benesov-1",
				"benesov-2",
				"benesov-3",
				"beroun",
				"blansko",
				"bohumin",
				"boskovicko",
				"brandys-nad-labem",
				"breclavsko",
				"brno",
				"caslav",
				"ceska-lipa",
				"ceska-trebova",
				"ceske-budejovice",
				"cesky-tesin",
				"cheb",
				"chrast",
				"chrudim",
				"chudova-plana",
				"decin",
				"domazlice",
				"frydek-mistek",
				"havirov",
				"havlickuv-brod",
				"hermanuv-mestec",
				"hodonin",
				"holesov",
				"holysov",
				"hradec",
				"jablonec",
				"jesenice-jilove",
				"jihlava",
				"jilemnice",
				"jindrichuv-hradec",
				"kadan",
				"karlovy-vary",
				"karvina",
				"kladno",
				"klatovy",
				"kolin",
				"kralupy-nad-vltavou",
				"kralovec",
				"kromeriz",
				"kutna-hora",
				"liberec",
				"livtinov",
				"loket",
				"louny",
				"lysa-nad-labem",
				"marianske-lazne",
				"melnik",
				"mikulov",
				"milovice",
				"mlada-boleslav",
				"mnisecko-a-dolni-berounka",
				"moravska-ostrava-a-privoz",
				"moravska-trebova",
				"most",
				"napajedla",
				"neratovice",
				"nova-role",
				"nymburk",
				"olomouc",
				"opava",
				"orlova",
				"ostrava-jih",
				"ostrava-poruba",
				"ostrava",
				"otrokovice",
				"pardubice",
				"pelhrimov",
				"pisek",
				"plzen",
				"podebrady",
				"policka",
				"poruba",
				"prachatice",
				"praha-1",
				"praha-2",
				"praha-3",
				"praha-4",
				"praha-5",
				"praha-6",
				"praha-7",
				"praha-8",
				"praha-9",
				"praha-10",
				"praha-11",
				"praha-11-2",
				"praha-12",
				"praha-13",
				"praha-14",
				"praha-15",
				"praha-18",
				"praha-21",
				"prerov",
				"pribor",
				"pribram",
				"prostejov",
				"rotava",
				"roudnice-nad-labem",
				"roznov",
				"rudoltice",
				"rychnov",
				"slovacko",
				"sobeslav",
				"sokolov",
				"strakonice",
				"sumperk",
				"susice",
				"svitavy",
				"tabor",
				"tachov",
				"telc",
				"teplice",
				"terezin",
				"trebic",
				"trutnov",
				"turnov",
				"tyn-nad-vltavou",
				"uherske-hradiste",
				"usti-nad-labem",
				"valec",
				"valmez",
				"vejprnice",
				"volary",
				"zatec",
				"zelezny-brod",
				"zlin",
				"znojemsko"
			]
		)
	):
		raise werkzeug.exceptions.NotFound
	
	dark = (
		flask.request.args.get("tmava", "ne")
		== "ano"
	)
	
	icon_height_multiplier = 1
	icon_offset_bottom_multiplier = 1
	
	if icon_localization is not None:
		icon_height_multiplier = ICON_HEIGHT_SPECIAL_MULTIPLIERS.get(icon_localization, icon_height_multiplier)
		icon_offset_bottom_multiplier = ICON_OFFSET_BOTTOM_SPECIAL_MULTIPLIERS.get(icon_localization, icon_offset_bottom_multiplier)
	
	return flask.render_template(
		"avatar_public.html",
		icon_localization=icon_localization,
		base_localization=base_localization,
		icon_height_multiplier=icon_height_multiplier,
		icon_offset_bottom_multiplier=icon_offset_bottom_multiplier,
		dark=dark
	), http.client.OK


@generator_blueprint.route("/kralovec")
def view_kralovec() -> typing.Tuple[flask.Response, int]:
	return view_avatar_generator_with_localization(
		icon_localization="kralovec"
	)
