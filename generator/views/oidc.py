"""Views for OpenID Connect authentication.

.. seealso::
	`Official OpenID Connect specification <https://openid.net/specs/openid-co\
	nnect-core-1_0.html>`_
"""

import datetime
import os

import authlib.integrations.requests_client
import authlib.jose
import authlib.oidc.core
import flask
import requests
import werkzeug.exceptions


oidc_blueprint = flask.Blueprint(
	"oidc",
	__name__,
	url_prefix="/oidc"
)

__all__ = [
	"login",
	"openid_blueprint",
]


@oidc_blueprint.route("/login")
def login() -> flask.Response:
	"""Redirects the user to a URL to log in through.

	:returns: A response with the redirect URL.
	"""

	oidc_url = flask.current_app.config["OIDC_BASE_URL"]

	with authlib.integrations.requests_client.OAuth2Session(
		client_id=os.environ["OIDC_CLIENT_ID"],
		client_secret=os.environ["OIDC_CLIENT_SECRET"],
		authorization_endpoint=f"{oidc_url}protocol/openid-connect/auth",
		token_endpoint=f"{oidc_url}protocol/openid-connect/token",
		userinfo_endpoint=f"{oidc_url}protocol/openid-connect/userinfo",
		jwks_uri=f"{oidc_url}protocol/openid-connect/certs",
		scope="openid"
	) as session:
		url, state = session.create_authorization_url(
			session.metadata["authorization_endpoint"],
			redirect_uri=flask.url_for("oidc.authenticate", _external=True, _scheme='https')
		)

		return flask.redirect(url)


@oidc_blueprint.route("/authenticate")
def authenticate() -> flask.Response:
	"""Authenticates the user based on the current request's ``code`` and
	``state`` arguments.

	:returns: A redirect to the ``generator.view`` endpoint. A cookie with a
		JWT authentication token is attached. Expires in ``JWT_EXPIRES_AFTER``
		seconds, 1 week by default.
	"""

	code = flask.request.args.get("code")
	state = flask.request.args.get("state")

	if code is None or state is None:
		raise werkzeug.exceptions.BadRequest

	oidc_url = flask.current_app.config["OIDC_BASE_URL"]

	with authlib.integrations.requests_client.OAuth2Session(
		client_id=os.environ["OIDC_CLIENT_ID"],
		client_secret=os.environ["OIDC_CLIENT_SECRET"],
		authorization_endpoint=f"{oidc_url}protocol/openid-connect/auth",
		token_endpoint=f"{oidc_url}protocol/openid-connect/token",
		userinfo_endpoint=f"{oidc_url}protocol/openid-connect/userinfo",
		jwks_uri=f"{oidc_url}protocol/openid-connect/certs",
		scope="openid"
	) as session:
		try:
			token = session.fetch_token(
				session.metadata["token_endpoint"],
				code=code,
				state=state,
				redirect_uri=flask.url_for("oidc.authenticate", _external=True, _scheme='https')
			)
		except (
			authlib.integrations.requests_client.OAuthError,
			requests.HTTPError
		) as exception:
			flask.current_app.logger.debug(
				"Error during OIDC authentication: %s",
				exception
			)

			raise werkzeug.exceptions.Unauthorized from exception

		jwks = session.get(session.metadata["jwks_uri"]).json()

		claims = authlib.jose.jwt.decode(
			token["id_token"],
			jwks,
			claims_cls=authlib.oidc.core.CodeIDToken
		)
		claims.validate()

		userinfo = session.get(session.metadata["userinfo_endpoint"]).json()

	current_timestamp = round(datetime.datetime.now().timestamp())

	jwt = (
		authlib.jose.jwt.encode(
			{
				"alg": "HS256"
			},
			{
				"iss": "generator-grafiky",
				"iat": current_timestamp,
				"exp": (
					current_timestamp
					+ flask.current_app.config["JWT_EXPIRES_AFTER"]
				),
				"sub": userinfo["sub"]  # Currently unused, maybe of use later
			},
			flask.current_app.config["JWT_SECRET_KEY"]
		).
		decode("utf-8")
	)

	response = flask.make_response(
		flask.redirect(
			flask.url_for("generator.view")
		)
	)

	response.set_cookie(
		"token",
		jwt
	)

	return response
