import datetime

import sqlalchemy

from . import Base
from .utils import UUID, get_uuid

__all__ = ["Concept"]


class Concept(Base):
	__tablename__ = "concepts"

	id = sqlalchemy.Column(
		UUID,
		primary_key=True,
		default=get_uuid
	)

	user_id = sqlalchemy.Column(
		sqlalchemy.String(64),
		nullable=False
	)

	data = sqlalchemy.Column(
		sqlalchemy.Text,
		nullable=False
	)
