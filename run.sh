#!/bin/bash

##
# Runs the app in a Gunicorn server, optimized for a 4-core system. (4 cores * 2 + 1)
#
# Gunicorn documentation: https://docs.gunicorn.org/en/latest/index.html
# Flask documentation, more options and information: https://flask.palletsprojects.com/en/2.0.x/patterns/appfactories/
##

# https://stackoverflow.com/a/13864829
# Thanks to Lionel and BSMP!

# Check if this script has already been run. If not, reflect database models
# and save that information for next time by creating an ``already_run.lock``
# file.
if [ ! -f already_run.lock ];
then
	FLASK_APP=generator python -m flask reflect &&
	touch already_run.lock
fi

# If unset, do ``INFO`` by default
if [[ -z "${LOGGING_LEVEL+set}" ]]; then
	export LOGGING_LEVEL="INFO";
fi

# Run the Gunicorn server in another
python -m gunicorn -w 9 -b :8080 generator:"create_app()"
