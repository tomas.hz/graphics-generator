jQuery - Licensed under the [MIT License](https://mit-license.org/).

Select2 - Licensed under the [MIT License](https://mit-license.org/).

Bebas Neue font - Licensed under the [SIL Open Font License v1.1](https://opensource.org/licenses/OFL-1.1). Made by Ryoichi Tsunekawa.

Roboto Condensed font - Licensed under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license. Made by Google.
